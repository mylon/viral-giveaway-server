# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161020080829) do

  create_table "apps", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.string   "code",       limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "campaign_winners", force: :cascade do |t|
    t.integer  "campaign_result_id", limit: 4, null: false
    t.integer  "user_id",            limit: 4, null: false
    t.integer  "ticket_id",          limit: 4, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "name",              limit: 255,                 null: false
    t.string   "description",       limit: 255, default: "",    null: false
    t.string   "cover_image_url",   limit: 255,                 null: false
    t.datetime "start_at",                                      null: false
    t.datetime "end_at",                                        null: false
    t.integer  "point_cost",        limit: 4,                   null: false
    t.integer  "order_num",         limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.boolean  "regulary",          limit: 1,   default: false, null: false
    t.string   "period",            limit: 255, default: "",    null: false
    t.integer  "clone_campaign_id", limit: 4
    t.string   "tag",               limit: 255, default: "",    null: false
    t.string   "type",              limit: 255, default: "",    null: false
    t.integer  "processing_time",   limit: 4,   default: 0,     null: false
  end

  add_index "campaigns", ["order_num", "start_at", "end_at", "deleted_at"], name: "index_campaigns_4", using: :btree

  create_table "comment_likes", force: :cascade do |t|
    t.integer  "user_id",    limit: 4, null: false
    t.integer  "comment_id", limit: 4, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id",           limit: 4,                 null: false
    t.integer  "news_id",           limit: 4,                 null: false
    t.text     "comment_text",      limit: 65535,             null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "parent_comment_id", limit: 4,     default: 0
  end

  create_table "currency_exchange_rates", force: :cascade do |t|
    t.string   "from",         limit: 255,                           null: false
    t.string   "to",           limit: 255,                           null: false
    t.decimal  "rate",                     precision: 20, scale: 10, null: false
    t.date     "published_at",                                       null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  create_table "dream_photos", force: :cascade do |t|
    t.integer  "user_id",          limit: 4,                  null: false
    t.integer  "photo_type",       limit: 4,                  null: false
    t.text     "comment",          limit: 65535,              null: false
    t.string   "user_photo_url",   limit: 255,   default: "", null: false
    t.string   "base_photo_url",   limit: 255,   default: "", null: false
    t.string   "result_photo_url", limit: 255,   default: "", null: false
    t.datetime "completed_at"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "dream_photos", ["user_id"], name: "index_dream_photos_1", using: :btree

  create_table "facebook_feed_pages", force: :cascade do |t|
    t.string   "facebook_page_id",     limit: 255,             null: false
    t.string   "facebook_page_type",   limit: 255,             null: false
    t.string   "facebook_page_region", limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "city_id",              limit: 4,   default: 0
  end

  create_table "facebook_page_data", force: :cascade do |t|
    t.string   "facebook_page_id",       limit: 255
    t.string   "facebook_page_url",      limit: 255
    t.string   "facebook_page_image",    limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "facebook_page_category", limit: 255
    t.string   "facebook_page_name",     limit: 255
    t.text     "twitter_link",           limit: 65535
    t.string   "instagram_link",         limit: 255
    t.string   "google_link",            limit: 255
  end

  create_table "fortune_telling_results", force: :cascade do |t|
    t.date     "target_date",                      null: false
    t.integer  "constellation",      limit: 4,     null: false
    t.text     "love_text_jp",       limit: 65535, null: false
    t.text     "love_text_en",       limit: 65535, null: false
    t.integer  "love_point_100",     limit: 4,     null: false
    t.integer  "love_point_5a",      limit: 4,     null: false
    t.integer  "love_point_5b",      limit: 4,     null: false
    t.integer  "love_point_10",      limit: 4,     null: false
    t.text     "money_text_jp",      limit: 65535, null: false
    t.text     "money_text_en",      limit: 65535, null: false
    t.integer  "money_point_100",    limit: 4,     null: false
    t.integer  "money_point_5a",     limit: 4,     null: false
    t.integer  "money_point_5b",     limit: 4,     null: false
    t.integer  "money_point_10",     limit: 4,     null: false
    t.text     "work_text_jp",       limit: 65535, null: false
    t.text     "work_text_en",       limit: 65535, null: false
    t.integer  "work_point_100",     limit: 4,     null: false
    t.integer  "work_point_5a",      limit: 4,     null: false
    t.integer  "work_point_5b",      limit: 4,     null: false
    t.integer  "work_point_10",      limit: 4,     null: false
    t.text     "total_text_jp",      limit: 65535, null: false
    t.text     "total_text_en",      limit: 65535, null: false
    t.integer  "total_point_100",    limit: 4,     null: false
    t.string   "lucky_direction_jp", limit: 255,   null: false
    t.string   "lucky_direction_en", limit: 255,   null: false
    t.string   "lucky_color_jp",     limit: 255,   null: false
    t.string   "lucky_color_en",     limit: 255,   null: false
    t.string   "lucky_food_jp",      limit: 255,   null: false
    t.string   "lucky_food_en",      limit: 255,   null: false
    t.string   "lucky_fashion_jp",   limit: 255,   null: false
    t.string   "lucky_fashion_en",   limit: 255,   null: false
    t.string   "lucky_item_jp",      limit: 255,   null: false
    t.string   "lucky_item_en",      limit: 255,   null: false
    t.string   "lucky_person_jp",    limit: 255,   null: false
    t.string   "lucky_person_en",    limit: 255,   null: false
    t.string   "lucky_place_jp",     limit: 255,   null: false
    t.string   "lucky_place_en",     limit: 255,   null: false
    t.integer  "lucky_number",       limit: 4,     null: false
    t.datetime "sent_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "fortune_telling_results", ["target_date", "constellation"], name: "index_fortune_telling_results_1", unique: true, using: :btree

  create_table "fortune_telling_users", force: :cascade do |t|
    t.integer  "user_id",       limit: 4, null: false
    t.date     "birthdate",               null: false
    t.integer  "constellation", limit: 4, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "fortune_telling_users", ["user_id"], name: "index_fortune_telling_users_1", unique: true, using: :btree

  create_table "information", force: :cascade do |t|
    t.string   "title",        limit: 255,   null: false
    t.text     "content",      limit: 65535
    t.string   "video_url",    limit: 255
    t.string   "image_url",    limit: 255
    t.datetime "published_at",               null: false
    t.integer  "user_id",      limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "information_views", force: :cascade do |t|
    t.integer  "information_id", limit: 4, null: false
    t.integer  "user_id",        limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "invitations", force: :cascade do |t|
    t.integer  "from_user_id", limit: 4, null: false
    t.integer  "to_user_id",   limit: 4, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "line_sticker_requests", force: :cascade do |t|
    t.integer  "user_id",      limit: 4,               null: false
    t.string   "line_id",      limit: 255,             null: false
    t.string   "sticker_name", limit: 255,             null: false
    t.integer  "status",       limit: 4,               null: false
    t.integer  "coin",         limit: 4,   default: 0, null: false
    t.datetime "sent_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "line_sticker_requests", ["user_id", "status"], name: "index_line_sticker_requests_1", using: :btree

  create_table "local_picks_device_tokens", force: :cascade do |t|
    t.integer  "user_id",             limit: 4
    t.string   "region",              limit: 255
    t.integer  "city_id",             limit: 4
    t.string   "device_type",         limit: 255, default: ""
    t.string   "device_token",        limit: 255, default: ""
    t.string   "device_notification", limit: 255, default: "Y"
    t.string   "unique_device_id",    limit: 255, default: "0"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "local_push_notification_logs", force: :cascade do |t|
    t.integer  "device_id",   limit: 4
    t.string   "message",     limit: 255
    t.string   "title",       limit: 255
    t.string   "news_link",   limit: 255
    t.string   "image_link",  limit: 255
    t.integer  "category_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "log_summary_app_log_per_sources", id: false, force: :cascade do |t|
    t.integer  "time",               limit: 4,               null: false
    t.string   "source",             limit: 255,             null: false
    t.integer  "h0_access",          limit: 4,   default: 0, null: false
    t.integer  "h0_register",        limit: 4,   default: 0, null: false
    t.integer  "h0_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h0_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h0_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h0_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h0_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h1_access",          limit: 4,   default: 0, null: false
    t.integer  "h1_register",        limit: 4,   default: 0, null: false
    t.integer  "h1_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h1_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h1_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h1_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h1_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h2_access",          limit: 4,   default: 0, null: false
    t.integer  "h2_register",        limit: 4,   default: 0, null: false
    t.integer  "h2_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h2_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h2_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h2_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h2_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h3_access",          limit: 4,   default: 0, null: false
    t.integer  "h3_register",        limit: 4,   default: 0, null: false
    t.integer  "h3_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h3_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h3_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h3_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h3_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h4_access",          limit: 4,   default: 0, null: false
    t.integer  "h4_register",        limit: 4,   default: 0, null: false
    t.integer  "h4_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h4_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h4_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h4_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h4_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h5_access",          limit: 4,   default: 0, null: false
    t.integer  "h5_register",        limit: 4,   default: 0, null: false
    t.integer  "h5_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h5_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h5_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h5_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h5_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h6_access",          limit: 4,   default: 0, null: false
    t.integer  "h6_register",        limit: 4,   default: 0, null: false
    t.integer  "h6_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h6_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h6_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h6_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h6_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h7_access",          limit: 4,   default: 0, null: false
    t.integer  "h7_register",        limit: 4,   default: 0, null: false
    t.integer  "h7_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h7_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h7_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h7_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h7_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h8_access",          limit: 4,   default: 0, null: false
    t.integer  "h8_register",        limit: 4,   default: 0, null: false
    t.integer  "h8_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h8_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h8_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h8_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h8_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h9_access",          limit: 4,   default: 0, null: false
    t.integer  "h9_register",        limit: 4,   default: 0, null: false
    t.integer  "h9_facebook_like",   limit: 4,   default: 0, null: false
    t.integer  "h9_facebook_share",  limit: 4,   default: 0, null: false
    t.integer  "h9_twitter_share",   limit: 4,   default: 0, null: false
    t.integer  "h9_twitter_follow",  limit: 4,   default: 0, null: false
    t.integer  "h9_cpi",             limit: 4,   default: 0, null: false
    t.integer  "h10_access",         limit: 4,   default: 0, null: false
    t.integer  "h10_register",       limit: 4,   default: 0, null: false
    t.integer  "h10_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h10_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h10_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h10_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h10_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h11_access",         limit: 4,   default: 0, null: false
    t.integer  "h11_register",       limit: 4,   default: 0, null: false
    t.integer  "h11_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h11_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h11_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h11_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h11_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h12_access",         limit: 4,   default: 0, null: false
    t.integer  "h12_register",       limit: 4,   default: 0, null: false
    t.integer  "h12_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h12_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h12_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h12_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h12_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h13_access",         limit: 4,   default: 0, null: false
    t.integer  "h13_register",       limit: 4,   default: 0, null: false
    t.integer  "h13_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h13_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h13_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h13_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h13_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h14_access",         limit: 4,   default: 0, null: false
    t.integer  "h14_register",       limit: 4,   default: 0, null: false
    t.integer  "h14_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h14_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h14_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h14_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h14_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h15_access",         limit: 4,   default: 0, null: false
    t.integer  "h15_register",       limit: 4,   default: 0, null: false
    t.integer  "h15_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h15_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h15_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h15_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h15_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h16_access",         limit: 4,   default: 0, null: false
    t.integer  "h16_register",       limit: 4,   default: 0, null: false
    t.integer  "h16_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h16_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h16_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h16_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h16_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h17_access",         limit: 4,   default: 0, null: false
    t.integer  "h17_register",       limit: 4,   default: 0, null: false
    t.integer  "h17_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h17_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h17_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h17_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h17_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h18_access",         limit: 4,   default: 0, null: false
    t.integer  "h18_register",       limit: 4,   default: 0, null: false
    t.integer  "h18_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h18_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h18_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h18_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h18_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h19_access",         limit: 4,   default: 0, null: false
    t.integer  "h19_register",       limit: 4,   default: 0, null: false
    t.integer  "h19_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h19_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h19_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h19_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h19_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h20_access",         limit: 4,   default: 0, null: false
    t.integer  "h20_register",       limit: 4,   default: 0, null: false
    t.integer  "h20_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h20_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h20_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h20_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h20_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h21_access",         limit: 4,   default: 0, null: false
    t.integer  "h21_register",       limit: 4,   default: 0, null: false
    t.integer  "h21_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h21_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h21_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h21_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h21_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h22_access",         limit: 4,   default: 0, null: false
    t.integer  "h22_register",       limit: 4,   default: 0, null: false
    t.integer  "h22_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h22_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h22_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h22_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h22_cpi",            limit: 4,   default: 0, null: false
    t.integer  "h23_access",         limit: 4,   default: 0, null: false
    t.integer  "h23_register",       limit: 4,   default: 0, null: false
    t.integer  "h23_facebook_like",  limit: 4,   default: 0, null: false
    t.integer  "h23_facebook_share", limit: 4,   default: 0, null: false
    t.integer  "h23_twitter_share",  limit: 4,   default: 0, null: false
    t.integer  "h23_twitter_follow", limit: 4,   default: 0, null: false
    t.integer  "h23_cpi",            limit: 4,   default: 0, null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "log_summary_daus", id: false, force: :cascade do |t|
    t.integer  "time",       limit: 4,             null: false
    t.integer  "dau",        limit: 4, default: 0, null: false
    t.integer  "user_num",   limit: 4, default: 0, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "log_summary_daus", ["time"], name: "index_primary_key", unique: true, using: :btree

  create_table "log_summary_maus", id: false, force: :cascade do |t|
    t.integer  "time",       limit: 4,             null: false
    t.integer  "mau",        limit: 4, default: 0, null: false
    t.integer  "user_num",   limit: 4, default: 0, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "log_summary_maus", ["time"], name: "index_primary_key", unique: true, using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,     null: false
    t.integer  "type",       limit: 4,     null: false
    t.string   "title",      limit: 255,   null: false
    t.text     "content",    limit: 65535
    t.integer  "point_num",  limit: 4
    t.datetime "viewed_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "mobile_country_codes", force: :cascade do |t|
    t.string   "code",         limit: 255, null: false
    t.string   "country_iso",  limit: 255, null: false
    t.string   "country_name", limit: 255, null: false
    t.string   "country_code", limit: 255, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "news_bookmarks", force: :cascade do |t|
    t.integer  "user_id",    limit: 4, null: false
    t.integer  "news_id",    limit: 4, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "news_pick_device_tokens", force: :cascade do |t|
    t.integer  "user_id",             limit: 4
    t.string   "region",              limit: 255,               null: false
    t.integer  "city_id",             limit: 4
    t.string   "device_type",         limit: 255, default: "",  null: false
    t.string   "device_token",        limit: 255, default: ""
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "device_notification", limit: 255, default: "Y", null: false
    t.string   "unique_device_id",    limit: 255, default: "0"
  end

  create_table "news_picks", force: :cascade do |t|
    t.string   "title",                   limit: 255,                null: false
    t.integer  "category_id",             limit: 4,                  null: false
    t.string   "region",                  limit: 255,                null: false
    t.string   "news_url",                limit: 255,   default: "", null: false
    t.string   "image_url",               limit: 255,   default: "", null: false
    t.integer  "city_id",                 limit: 4
    t.integer  "publish_status",          limit: 4,                  null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.text     "tags",                    limit: 65535
    t.integer  "hot_news",                limit: 4,     default: 0
    t.string   "publishers",              limit: 255
    t.string   "web_url",                 limit: 255
    t.text     "news_description",        limit: 65535
    t.integer  "news_event",              limit: 4,     default: 0,  null: false
    t.integer  "news_promotion",          limit: 4,     default: 0,  null: false
    t.integer  "news_staff_checked",      limit: 4,     default: 1,  null: false
    t.string   "created_by",              limit: 255
    t.integer  "classified_sub_category", limit: 4,     default: 0
    t.string   "news_pick_shop",          limit: 255
    t.integer  "show_in_list",            limit: 4,     default: 1,  null: false
    t.string   "updated_by",              limit: 255
    t.integer  "is_magazine",             limit: 4,     default: 0
  end

  create_table "news_picks_sources", force: :cascade do |t|
    t.string   "source_url", limit: 255,             null: false
    t.string   "region",     limit: 255,             null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "city_id",    limit: 4,   default: 0
  end

  create_table "news_tags", force: :cascade do |t|
    t.string   "tag",         limit: 255,               null: false
    t.string   "region",      limit: 255,               null: false
    t.integer  "city_id",     limit: 4,     default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "category_id", limit: 4,     default: 0
    t.text     "tags_group",  limit: 65535
  end

  create_table "nightlife_chatrooms", force: :cascade do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "group_id",   limit: 4
  end

  create_table "nightlife_group_users", force: :cascade do |t|
    t.integer  "group_id",   limit: 4,             null: false
    t.integer  "user_id",    limit: 4,             null: false
    t.integer  "is_temp",    limit: 4, default: 0
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "nightlife_groups", force: :cascade do |t|
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "createdby_user_id", limit: 4
    t.integer  "createdby_match",   limit: 4,   default: 0
    t.string   "group_barcode",     limit: 255
    t.integer  "single_single",     limit: 4,   default: 0
    t.integer  "single_group",      limit: 4,   default: 0
    t.integer  "group_group",       limit: 4,   default: 0
    t.integer  "group_join",        limit: 4,   default: 0
  end

  create_table "nightlife_nearby_clubs", force: :cascade do |t|
    t.integer  "club_item_id", limit: 4,   null: false
    t.string   "club_title",   limit: 255, null: false
    t.string   "club_url",     limit: 255, null: false
    t.string   "club_image",   limit: 255, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "nightlife_user_images", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,               null: false
    t.string   "image_url",  limit: 255,             null: false
    t.integer  "is_profile", limit: 4,   default: 0, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "nightlife_user_requests", force: :cascade do |t|
    t.integer  "from_user_id", limit: 4,             null: false
    t.integer  "to_user_id",   limit: 4,             null: false
    t.integer  "is_match",     limit: 4, default: 0, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "nightlife_users", force: :cascade do |t|
    t.string   "account_id",             limit: 255,                null: false
    t.string   "token",                  limit: 255,                null: false
    t.string   "locale",                 limit: 255,                null: false
    t.string   "name",                   limit: 255
    t.string   "email",                  limit: 255,                null: false
    t.string   "encrypted_password",     limit: 255, default: "",   null: false
    t.date     "birthdate"
    t.integer  "gender",                 limit: 4
    t.boolean  "notification",           limit: 1,   default: true, null: false
    t.string   "country",                limit: 255, default: "",   null: false
    t.datetime "last_accessed_at",                                  null: false
    t.datetime "next_notification_at"
    t.string   "verification_code",      limit: 255
    t.datetime "verified_at"
    t.string   "uid",                    limit: 255, default: "",   null: false
    t.string   "provider",               limit: 255, default: "",   null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "canonical_email",        limit: 255
    t.string   "an_source",              limit: 255
    t.string   "an_referral_code",       limit: 255
    t.string   "tag_line",               limit: 255
    t.integer  "show_men",               limit: 4,   default: 1
    t.integer  "show_women",             limit: 4,   default: 1
    t.string   "device_udid",            limit: 255
    t.integer  "is_dummy",               limit: 4,   default: 0
    t.integer  "match_notification",     limit: 4,   default: 1
    t.integer  "message_notification",   limit: 4,   default: 1
    t.integer  "like_notification",      limit: 4,   default: 1
    t.integer  "is_online",              limit: 4
    t.string   "platform",               limit: 255
  end

  create_table "offers", force: :cascade do |t|
    t.integer  "type",                limit: 4,                     null: false
    t.integer  "provider",            limit: 4,                     null: false
    t.integer  "platform",            limit: 4,                     null: false
    t.string   "name",                limit: 255,                   null: false
    t.text     "description",         limit: 65535
    t.datetime "start_at",                                          null: false
    t.datetime "end_at",                                            null: false
    t.string   "app_icon_url",        limit: 255
    t.string   "cover_image_url",     limit: 255
    t.string   "video_url",           limit: 255
    t.string   "youtube_video_id",    limit: 255
    t.string   "click_url",           limit: 255
    t.string   "provider_offer_id",   limit: 255
    t.integer  "provider_payment",    limit: 4
    t.integer  "provider_point",      limit: 4
    t.integer  "publish_status",      limit: 4,                     null: false
    t.boolean  "promotional",         limit: 1,     default: false, null: false
    t.integer  "promotional_payment", limit: 4
    t.integer  "order_num",           limit: 4
    t.string   "country",             limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "preview_url",         limit: 255
  end

  create_table "operators", force: :cascade do |t|
    t.string   "account_id", limit: 255, null: false
    t.string   "name",       limit: 255, null: false
    t.string   "email",      limit: 255
    t.string   "password",   limit: 255, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "points", force: :cascade do |t|
    t.integer  "user_id",          limit: 4, null: false
    t.integer  "transfer_user_id", limit: 4
    t.integer  "amount",           limit: 4, null: false
    t.integer  "balance",          limit: 4, null: false
    t.integer  "item_type",        limit: 4, null: false
    t.integer  "item_id",          limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "purchase_logs", force: :cascade do |t|
    t.integer  "user_id",        limit: 4,     null: false
    t.integer  "platform",       limit: 4,     null: false
    t.string   "transaction_id", limit: 255,   null: false
    t.text     "receipt",        limit: 65535, null: false
    t.string   "item",           limit: 255,   null: false
    t.integer  "item_id",        limit: 4,     null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "push_notification_message_logs", force: :cascade do |t|
    t.integer  "device_id",   limit: 4
    t.string   "message",     limit: 255
    t.string   "title",       limit: 255
    t.string   "news_link",   limit: 255
    t.string   "image_link",  limit: 255
    t.integer  "category_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "rails_push_notifications_apns_apps", force: :cascade do |t|
    t.text     "apns_dev_cert",  limit: 65535
    t.text     "apns_prod_cert", limit: 65535
    t.boolean  "sandbox_mode",   limit: 1
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "rails_push_notifications_gcm_apps", force: :cascade do |t|
    t.string   "gcm_key",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "rails_push_notifications_mpns_apps", force: :cascade do |t|
    t.text     "cert",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "rails_push_notifications_notifications", force: :cascade do |t|
    t.text     "destinations", limit: 65535
    t.integer  "app_id",       limit: 4
    t.string   "app_type",     limit: 255
    t.text     "data",         limit: 65535
    t.text     "results",      limit: 65535
    t.integer  "success",      limit: 4
    t.integer  "failed",       limit: 4
    t.boolean  "sent",         limit: 1,     default: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "system_informations", force: :cascade do |t|
    t.boolean  "maintenance", limit: 1, default: false, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.integer  "campaign_id", limit: 4,   null: false
    t.integer  "user_id",     limit: 4,   null: false
    t.string   "number",      limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "trackings", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.string   "track_id",   limit: 255, null: false
    t.integer  "type",       limit: 4,   null: false
    t.integer  "status",     limit: 4,   null: false
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "account_id",             limit: 255,                              null: false
    t.string   "token",                  limit: 255,                              null: false
    t.string   "locale",                 limit: 255,                              null: false
    t.string   "name",                   limit: 255
    t.string   "email",                  limit: 255,                              null: false
    t.string   "canonical_email",        limit: 255,                              null: false
    t.string   "encrypted_password",     limit: 255, default: ""
    t.date     "birthdate"
    t.integer  "gender",                 limit: 4
    t.boolean  "notification",           limit: 1,   default: true,               null: false
    t.string   "country",                limit: 255, default: ""
    t.datetime "last_accessed_at",                                                null: false
    t.datetime "next_notification_at"
    t.string   "verification_code",      limit: 255
    t.datetime "verified_at"
    t.string   "uid",                    limit: 255, default: "",                 null: false
    t.string   "provider",               limit: 255, default: "",                 null: false
    t.integer  "platform",               limit: 4,                                null: false
    t.string   "an_source",              limit: 255, default: "",                 null: false
    t.string   "an_referral_code",       limit: 255, default: "",                 null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,                  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.string   "tag_line",               limit: 255, default: "Nice to meet you"
    t.boolean  "is_verified",            limit: 1,   default: false
  end

end
