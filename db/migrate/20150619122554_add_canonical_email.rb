class AddCanonicalEmail < ActiveRecord::Migration
  def change
    add_column :users, :canonical_email, :string, null: false, :after => :email
    User.all.each do |u|
      u.canonical_email = u.email.downcase
      u.save!
    end
  end
end
