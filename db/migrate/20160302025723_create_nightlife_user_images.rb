class CreateNightlifeUserImages < ActiveRecord::Migration
  def change
    create_table :nightlife_user_images do |t|
      t.integer :user_id, null: false
      t.string  :image_url, null: false
      t.integer :is_profile, null: false, default: 0
      t.timestamps null: false
    end
  end
end
