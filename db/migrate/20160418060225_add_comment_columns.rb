class AddCommentColumns < ActiveRecord::Migration
  def change
  	add_column :comments, :parent_comment_id, :integer, null: true, default: 0
  end
end
