class AddIsMagazineColumn < ActiveRecord::Migration
  def change
  	add_column :news_picks, :is_magazine, :integer, null: true, default: 0
  end
end
