class CreateNightlifeUserRequests < ActiveRecord::Migration
  def change
    create_table :nightlife_user_requests do |t|
      t.integer  :from_user_id,   null: false
      t.integer  :to_user_id,   null: false
      t.integer  :is_match,   null: false, default: 0
      t.timestamps null: false
    end
  end
end
