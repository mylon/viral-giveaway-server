class CreateNightlifeGroups < ActiveRecord::Migration
  def change
    create_table :nightlife_groups do |t|
      t.integer  :createdby_user_id,   null: false
      t.string   :group_barcode,   null: true
      t.integer  :is_temp,   null: true, default: 0
      t.string   :from_group_id,   null: true
      t.timestamps null: false
    end
  end
end
