class AddIndex < ActiveRecord::Migration
  def change

=begin
    # users
    add_index :users, [:account_id, :deleted_at], :name => 'index_users_1'
    add_index :users, [:account_id, :deleted_at, :verified_at], :name => 'index_users_2'
    add_index :users, [:account_id, :verification_code, :deleted_at], :name => 'index_users_3'
    add_index :users, [:canonical_email, :encrypted_password, :deleted_at, :verified_at], :name => 'index_users_4'
    add_index :users, [:canonical_email, :provider, :uid, :deleted_at], :name => 'index_users_5'

    # campaigns
    add_index :campaigns, [:deleted_at], :name => 'index_campaigns_1'
    add_index :campaigns, [:start_at, :end_at, :deleted_at], :name => 'index_campaigns_2'

    # offers 
    add_index :offers, [:type, :publish_status, :platform, :country, :start_at, :end_at, :deleted_at], :name => 'index_offers_1'

    # trackings
    add_index :trackings, [:type, :status, :created_at, :deleted_at], :name => 'index_trackings_1'
    add_index :trackings, [:type, :status, :user_id, :track_id, :deleted_at], :name => 'index_trackings_2'

    # information
    add_index :information, [:published_at, :deleted_at], :name => 'index_information_1'
    
    # information_views
    add_index :information_views, [:user_id, :information_id, :deleted_at], :name => 'index_information_views_1'
    
    # operators 
    add_index :operators, [:account_id, :password, :deleted_at], :name => 'index_operators_1'
    add_index :operators, [:email, :password, :deleted_at], :name => 'index_operators_2'
    
    # tickets
    add_index :tickets, [:campaign_id, :number], :name => 'index_tickets_1'
    add_index :tickets, [:campaign_id, :user_id], :name => 'index_tickets_2'
    add_index :tickets, [:user_id], :name => 'index_tickets_3'
    
    # invitations 
    add_index :invitations, [:from_user_id, :to_user_id], :name => 'index_invitations_1'
    add_index :invitations, [:from_user_id], :name => 'index_invitations_2'
    add_index :invitations, [:to_user_id], :name => 'index_invitations_3'

    # messages 
    add_index :messages, [:user_id, :viewed_at], :name => 'index_messages_1'
    
    # points 
    add_index :points, [:user_id], :name => 'index_points_1'
=end
  end
end
