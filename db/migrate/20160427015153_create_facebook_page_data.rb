class CreateFacebookPageData < ActiveRecord::Migration
  def change
    create_table :facebook_page_data do |t|
      t.integer :facebook_page_id,   null: true
      t.string  :facebook_page_url,   null: true
      t.string  :facebook_page_image,   null: true	
      t.timestamps null: false
    end
  end
end
