class CreateNightlifeChatrooms < ActiveRecord::Migration
  def change
    create_table :nightlife_chatrooms do |t|
      t.integer  :is_group,   null: false, default: 0
      t.integer  :group_one,   null: true
      t.integer  :group_two,   null: true
      t.integer  :user_one,   null: true
      t.integer  :user_two,   null: true
      t.timestamps null: false
    end
  end
end
