class AddLuckyColor < ActiveRecord::Migration
  def change
    add_column :fortune_telling_results, :lucky_color_jp, :string, null: false, after: :lucky_direction_en
    add_column :fortune_telling_results, :lucky_color_en, :string, null: false, after: :lucky_color_jp
  end
end
