class AddRemoveNpNotificationColumn < ActiveRecord::Migration
  def change
  	add_column :news_pick_device_tokens, :device_notification , :string, null: false, default: "N"
  	remove_column :users, :np_notification, :string
  end
end
