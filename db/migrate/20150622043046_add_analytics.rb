class AddAnalytics < ActiveRecord::Migration
  def change
    add_column :users, :an_referral_code, :string, null: false, default: '', :after => :provider
    add_column :users, :an_source, :string, null: false, default: '', :after => :provider
  end
end
