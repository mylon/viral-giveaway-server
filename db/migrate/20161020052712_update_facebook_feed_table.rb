class UpdateFacebookFeedTable < ActiveRecord::Migration
  def change
  	change_column :facebook_feed_pages, :facebook_page_region, :string, null: true
  end
end
