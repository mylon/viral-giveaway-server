class AddUsersField < ActiveRecord::Migration
  def change
  	add_column :users, :np_notification, :string, null: false, default: "N"
  	add_column :users, :np_ios_device_token ,:string, null: false, default: ""
  	add_column :users, :np_android_device_token ,:string, null: false, default: ""
  end
end
