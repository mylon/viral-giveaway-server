class CreateNewsPickDeviceTokens < ActiveRecord::Migration
  def change
    create_table :news_pick_device_tokens do |t|    
      t.integer   :user_id, null: false
      t.string    :region, null: false    
      t.integer   :city_id  
      t.string    :device_type, null: false, default: ''
      t.string    :device_token, default: ''  
      t.timestamps null: false
    end
  end
end
