class AddInstagramLink < ActiveRecord::Migration
  def change
  	add_column :facebook_page_data, :instagram_link, :string, null: true
  end
end
