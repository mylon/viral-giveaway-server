class AddLine < ActiveRecord::Migration
  def change
    create_table :line_sticker_requests do |t|
      t.integer :user_id, null: false
      t.string :line_id, null: false
      t.string :sticker_name, null: false
      t.integer :status, null: false
      t.integer :coin, null: false, default: 0
      t.datetime :sent_at
      t.timestamps null: false
    end
    add_index :line_sticker_requests, [:user_id, :status], :name => 'index_line_sticker_requests_1'
    
    Campaign.delete_all(tag: 'LineSticker')
    Campaign.create(
      :type => 'line_sticker',
      :name => 'LINE Sticker', 
      :description => '',
      :cover_image_url => '/assets/web/campaigns/vg_line_main_pic.png',
      :start_at => Date.new(1970,1,1),
      :end_at => Date.new(9999,12,31), 
      :point_cost => 120,
      :regulary => false,
      :tag => 'LineSticker',
      :processing_time => 20 * 60, # 20m
      :order_num => 40,
    )

  end
end
