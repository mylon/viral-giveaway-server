class CreateFacebookFeedPages < ActiveRecord::Migration
  def change
    create_table :facebook_feed_pages do |t|
      t.string  :facebook_page_id, null: false
      t.string  :facebook_page_type, null: false
      t.string  :facebook_page_region, null: false
      t.timestamps null: false
    end
  end
end
