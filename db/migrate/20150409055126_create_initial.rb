class CreateInitial < ActiveRecord::Migration
  def change

    create_table :users do |t|
      t.string      :account_id, null: false
      t.string      :token, null: false
      t.string      :locale, null: false
      t.string      :name, null: false
      t.string      :email, null: false
      t.string      :encrypted_password, null: false, default: ""
      t.date        :birthdate
      t.integer     :gender
      t.boolean     :notification, null: false, default: true
      t.string      :country, null: false, default: ''
      t.datetime    :last_accessed_at, null: false
      t.datetime    :next_notification_at
      t.string      :verification_code
      t.datetime    :verified_at
      #omniauth
      t.string      :uid,      null: false, default: ""
      t.string      :provider, null: false, default: ""
      ## Recoverable
      t.string      :reset_password_token
      t.datetime    :reset_password_sent_at
      ## Rememberable
      t.datetime    :remember_created_at
      ## Trackable
      t.integer     :sign_in_count, default: 0, null: false
      t.datetime    :current_sign_in_at
      t.datetime    :last_sign_in_at
      t.string      :current_sign_in_ip
      t.string      :last_sign_in_ip
      t.datetime    :deleted_at
      t.timestamps null: false
    end

    create_table :campaigns do |t|
      t.string      :name, null: false
      t.string      :description, null: false, default: ''
      t.string      :cover_image_url, null: false
      t.datetime    :start_at, null: false
      t.datetime    :end_at, null: false
      t.integer     :point_cost, null: false
      t.integer     :order_num
      t.datetime    :deleted_at
      t.timestamps null: false
    end
    
    create_table :campaign_winners do |t|
      t.integer     :campaign_result_id, null: false
      t.integer     :user_id, null: false
      t.integer     :ticket_id, null: false
      t.timestamps null: false
    end

    create_table :offers do |t|
      t.integer     :type, null: false
      t.integer     :provider, null: false
      t.integer     :platform, null: false
      t.string      :name, null: false
      t.text        :description
      t.datetime    :start_at, null: false
      t.datetime    :end_at, null: false
      t.string      :app_icon_url
      t.string      :cover_image_url
      t.string      :video_url
      t.string      :youtube_video_id
      t.string      :click_url
      t.string      :provider_offer_id
      t.integer     :provider_payment     # cent
      t.integer     :provider_point
      t.integer     :publish_status, null: false
      t.boolean     :promotional, null: false, default: false
      t.integer     :promotional_payment
      t.integer     :order_num
      t.string      :country
      t.datetime    :deleted_at
      t.timestamps null: false
    end

    create_table :trackings do |t|
      t.integer     :user_id, null: false
      t.string      :track_id, null: false
      t.integer     :type, null: false
      t.integer     :status, null: false
      t.datetime    :deleted_at
      t.timestamps null: false
    end

    create_table :information do |t|
      t.string      :title, null: false
      t.text        :content
      t.string      :video_url
      t.string      :image_url
      t.datetime    :published_at, null: false
      t.integer     :user_id
      t.datetime    :deleted_at
      t.timestamps null: false
    end

    create_table :information_views do |t|
      t.integer     :information_id, null: false
      t.integer     :user_id
      t.datetime    :deleted_at
      t.timestamps null: false
    end

    create_table :operators do |t|
      t.string      :account_id, null: false
      t.string      :name, null: false
      t.string      :email
      t.string      :password, null: false
      t.datetime    :deleted_at
      t.timestamps null: false
    end

    create_table :mobile_country_codes do |t|
      t.string      :code, null: false
      t.string      :country_iso, null: false
      t.string      :country_name, null: false
      t.string      :country_code, null: false
      t.timestamps null: false
    end

    create_table :system_informations do |t|
      t.boolean  :maintenance, null: false, default: false

      t.timestamps null: false
    end
    SystemInformation.first_or_create(:maintenance => false)

    create_table :tickets do |t|
      t.integer     :campaign_id, null: false
      t.integer     :user_id, null: false
      t.string      :number, null: false
      t.timestamps null: false
    end

    create_table :invitations do |t|
      t.integer     :from_user_id, null: false
      t.integer     :to_user_id, null: false
      t.timestamps null: false
    end

    create_table :messages do |t|
      t.integer     :user_id, null: false
      t.integer     :type, null: false
      t.string      :title, null: false
      t.text        :content
      t.integer     :point_num
      t.datetime    :viewed_at
      t.timestamps null: false
    end

    create_table :points do |t|
      t.integer     :user_id, null: false
      t.integer     :transfer_user_id
      t.integer     :amount, null: false
      t.integer     :balance, null: false
      t.integer     :item_type, null: false
      t.integer     :item_id
      t.timestamps null: false
    end
  end
end
