class AddNotShowNewsColumn < ActiveRecord::Migration
  def change
  	add_column :news_picks, :show_in_list, :boolean, null: true, default: 1
  end
end
