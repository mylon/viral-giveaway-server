class AddNewsPickSubClassified < ActiveRecord::Migration
  def change
  	add_column :news_picks, :classified_sub_category, :integer, null: true, default: 0
  end
end
