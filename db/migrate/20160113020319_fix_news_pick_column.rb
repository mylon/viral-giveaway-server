class FixNewsPickColumn < ActiveRecord::Migration
  def change
  	remove_column :news_picks, :news_event , :string
  	remove_column :news_picks, :news_promotion , :string
  	add_column :news_picks, :news_event , :integer, null: false, default: 0
  	add_column :news_picks, :news_promotion , :integer, null: false, default: 0
  end
end
