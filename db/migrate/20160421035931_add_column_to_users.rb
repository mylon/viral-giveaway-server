class AddColumnToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :is_verified, :boolean, null: true, default: false
  end
end
