class AddLogSummaryTables < ActiveRecord::Migration
=begin
  def change
    create_table :log_summary_daus, id: false do |t|
      t.integer :time, null: false
      t.integer :dau, null: false, default: 0
      t.integer :user_num, null: false, default: 0
      t.timestamps null: false
    end
    #add_index :log_summary_daus, :time, unique: true, name: 'index_primary_key'

    create_table :log_summary_maus, id: false do |t|
      t.integer :time, null: false
      t.integer :mau, null: false, default: 0
      t.integer :user_num, null: false, default: 0
      t.timestamps null: false
    end
    #add_index :log_summary_maus, :time, unique: true, name: 'index_primary_key'

    create_table :log_summary_app_log_per_sources, id: false do |t|
      t.integer :time, null: false
      t.string  :source, null: false
      24.times do |h|
        t.integer "h#{h}_access".to_sym, null: false, default: 0
        t.integer "h#{h}_register".to_sym, null: false, default: 0
        t.integer "h#{h}_facebook_like".to_sym, null: false, default: 0
        t.integer "h#{h}_facebook_share".to_sym, null: false, default: 0
        t.integer "h#{h}_twitter_share".to_sym, null: false, default: 0
        t.integer "h#{h}_twitter_follow".to_sym, null: false, default: 0
        t.integer "h#{h}_cpi".to_sym, null: false, default: 0
      end
      t.timestamps null: false
    end
    #add_index :log_summary_app_log_per_sources, [:time, :source], unique: true, name: 'index_primary_key'

  end
=end
end
