class AddColumnPublishByStaffNewsPicks < ActiveRecord::Migration
  def change
  	add_column :news_picks, :news_staff_checked , :integer, null: false, default: 1
  end
end
