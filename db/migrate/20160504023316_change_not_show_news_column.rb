class ChangeNotShowNewsColumn < ActiveRecord::Migration
  def change
  	change_column :news_picks, :show_in_list, :integer, null: false, default: 1
  end
end
