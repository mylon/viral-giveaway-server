class AddUniqueDeviceIdAndroid < ActiveRecord::Migration
  def change
  	add_column :news_pick_device_tokens, :unique_device_id, :string, null: true, default: 0
  end
end
