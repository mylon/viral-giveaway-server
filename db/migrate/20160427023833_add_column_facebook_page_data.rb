class AddColumnFacebookPageData < ActiveRecord::Migration
  def change
  	add_column :facebook_page_data, :facebook_page_name, :string, null: true
  end
end
