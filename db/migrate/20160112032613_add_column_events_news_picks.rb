class AddColumnEventsNewsPicks < ActiveRecord::Migration
  def change
  	add_column :news_picks, :news_description , :text
  	add_column :news_picks, :news_event , :string, null: false, default: "N"
  	add_column :news_picks, :news_promotion , :string, null: false, default: "N"
  end
end
