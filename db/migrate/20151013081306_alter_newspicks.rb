class AlterNewspicks < ActiveRecord::Migration
  def change
    create_table :news_picks do |t|
      t.string    :title, null: false  	 
      t.integer   :category_id, null: false
      t.string    :region, null: false     
      t.string    :news_url, null: false, default: ''
      t.string    :image_url, null: false, default: ''
      t.integer   :city_id    
      t.integer   :publish_status, null: false
      t.datetime  :deleted_at
      t.timestamps null: false
    end
  end
end
