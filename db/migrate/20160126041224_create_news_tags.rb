class CreateNewsTags < ActiveRecord::Migration
  def change
    create_table :news_tags do |t|
      t.string :tag, null: false
      t.string :region, null: false
      t.integer :city_id, null: true, default: 0
      t.timestamps null: false
    end
  end
end
