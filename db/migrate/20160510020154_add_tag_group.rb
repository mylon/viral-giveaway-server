class AddTagGroup < ActiveRecord::Migration
  def change
  	add_column :news_tags, :tags_group, :text, null: true, default: nil
  end
end
