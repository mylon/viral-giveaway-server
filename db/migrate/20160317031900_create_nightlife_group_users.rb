class CreateNightlifeGroupUsers < ActiveRecord::Migration
  def change
    create_table :nightlife_group_users do |t|
      t.integer  :group_id,   null: false
      t.integer  :user_id,   null: false
      t.integer  :is_temp,   null: true, default: 0
      t.timestamps null: false
    end
  end
end
