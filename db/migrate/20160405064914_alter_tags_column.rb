class AlterTagsColumn < ActiveRecord::Migration
  def change
  	add_column :news_tags, :category_id, :integer, null: true, default: 0
  end
end
