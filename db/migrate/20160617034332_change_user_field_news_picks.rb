class ChangeUserFieldNewsPicks < ActiveRecord::Migration
  def change
  	change_column :news_picks, :created_by, :string, null: true
  	change_column :news_picks, :updated_by, :string, null: true
  end
end
