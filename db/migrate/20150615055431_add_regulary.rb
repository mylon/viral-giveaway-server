class AddRegulary < ActiveRecord::Migration
  def change
    add_column :campaigns, :regulary, :boolean, :null => false, :default => false
    add_column :campaigns, :period, :string, :null => false, :default => ""
    add_column :campaigns, :clone_campaign_id, :integer
    Campaign.update_all(:regulary => true, :period => '1w', :clone_campaign_id => 1)
  end
end
