class ChangeFacebookPageData < ActiveRecord::Migration
  def change
  	change_column :facebook_page_data, :facebook_page_id, :string, null: true
  end
end
