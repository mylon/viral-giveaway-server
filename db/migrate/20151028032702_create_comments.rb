class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer   :user_id, null: false
      t.integer   :news_id, null: false
      t.text	  :comment_text, null: false	
      t.timestamps null: false
    end
  end
end
