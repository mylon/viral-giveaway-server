class AddTagLineUser < ActiveRecord::Migration
  def change
  	add_column :users, :tag_line, :string, null: true, default: 'Nice to meet you'
  end
end
