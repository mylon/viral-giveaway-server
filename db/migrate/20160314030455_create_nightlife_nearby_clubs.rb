class CreateNightlifeNearbyClubs < ActiveRecord::Migration
  def change
    create_table :nightlife_nearby_clubs do |t|
      t.integer :club_item_id, null: false
      t.string  :club_title, null: false
      t.string  :club_url, null: false
      t.string  :club_image, null: false
      t.timestamps null: false
    end
  end
end