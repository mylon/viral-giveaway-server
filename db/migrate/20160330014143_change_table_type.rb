class ChangeTableType < ActiveRecord::Migration
  def change
  	change_column :news_pick_device_tokens, :user_id, :integer, null: true
  end
end
