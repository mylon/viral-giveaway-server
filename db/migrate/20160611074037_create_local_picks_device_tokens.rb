class CreateLocalPicksDeviceTokens < ActiveRecord::Migration
  def change
    create_table :local_picks_device_tokens do |t|
 	  t.integer   :user_id, null: true
      t.string    :region, null: true    
      t.integer   :city_id, null: true      
      t.string    :device_type, null: true, default: ''
      t.string    :device_token, null: true, default: ''  
      t.string    :device_notification, null: true, default: 'Y'
      t.string    :unique_device_id, null: true, default: 0 
      t.timestamps null: false
    end
  end
end
