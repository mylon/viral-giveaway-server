class AddFacebookPageData < ActiveRecord::Migration
  def change
  	add_column :facebook_page_data, :facebook_page_category, :string, null: true
  end
end
