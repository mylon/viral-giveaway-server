class CreateNewsBookmarks < ActiveRecord::Migration
  def change
    create_table :news_bookmarks do |t|
      t.integer   :user_id, null: false
      t.integer   :news_id, null: false
      t.timestamps null: false
    end
  end
end
