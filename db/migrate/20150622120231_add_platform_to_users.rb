class AddPlatformToUsers < ActiveRecord::Migration
  def change
    add_column :users, :platform, :integer, null: false, :after => :provider
  end
end
