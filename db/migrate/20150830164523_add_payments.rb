class AddPayments < ActiveRecord::Migration
=begin
  
rescue Exception => e
  
end
  def change
    create_table :purchase_logs do |t|
      t.integer :user_id, null: false
      t.integer :platform, null: false
      t.string :transaction_id, null: false
      t.text :receipt, null: false
      t.string :item, null: false
      t.integer :item_id, null: false
      t.timestamps null: false
    end
    add_index :purchase_logs, [:platform, :transaction_id], :unique => true, :name => 'index_purchase_logs_1'

    add_column :dream_photos, :billing_status, :integer, null: false, default: 0 
  end
=end
end
