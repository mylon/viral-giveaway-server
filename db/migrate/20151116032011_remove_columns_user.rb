class RemoveColumnsUser < ActiveRecord::Migration
  def change
  	remove_column :users, :np_ios_device_token, :string
  	remove_column :users, :np_android_device_token, :string
  end
end
