class AddPreviewUrl < ActiveRecord::Migration
  def change
    add_column :offers, :preview_url, :string
  end
end
