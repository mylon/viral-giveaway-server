class CreateLocalPushNotificationLogs < ActiveRecord::Migration
  def change
    create_table :local_push_notification_logs do |t|
 	  t.integer  :device_id,   null: true
      t.string  :message, null: true
      t.string  :title,   null: true
      t.string  :news_link,   null: true
      t.string  :image_link,   null: true
      t.integer  :category_id,   null: true
      t.timestamps null: false
    end
  end
end
