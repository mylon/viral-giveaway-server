class AddRemoveNpNotificationColumnFix < ActiveRecord::Migration
  def change
  		remove_column :news_pick_device_tokens, :device_notification , :string
  		add_column :news_pick_device_tokens, :device_notification , :string, null: false, default: "Y"
  end
end
