class ChangeColumnUsers < ActiveRecord::Migration
  def change
  		change_column :users, :country, :string, null: true
  end
end
