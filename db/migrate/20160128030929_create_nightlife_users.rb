class CreateNightlifeUsers < ActiveRecord::Migration
  def change
    create_table :nightlife_users do |t|
      t.string      :account_id, null: false
      t.string      :token, null: false
      t.string      :locale, null: false
      t.string      :name, null: false
      t.string      :email, null: false
      t.string      :encrypted_password, null: false, default: ""
      t.date        :birthdate
      t.integer     :gender
      t.boolean     :notification, null: false, default: true
      t.string      :country, null: false, default: ''
      t.datetime    :last_accessed_at, null: false
      t.datetime    :next_notification_at
      t.string      :verification_code
      t.datetime    :verified_at
      #omniauth
      t.string      :uid,      null: false, default: ""
      t.string      :provider, null: false, default: ""
      ## Recoverable
      t.string      :reset_password_token
      t.datetime    :reset_password_sent_at
      ## Rememberable
      t.datetime    :remember_created_at
      ## Trackable
      t.integer     :sign_in_count, default: 0, null: false
      t.datetime    :current_sign_in_at
      t.datetime    :last_sign_in_at
      t.string      :current_sign_in_ip
      t.string      :last_sign_in_ip
      t.datetime    :deleted_at
      t.timestamps null: false
    end
  end
end
