class AddCurrencyExchangeRate < ActiveRecord::Migration
  def change
    create_table :currency_exchange_rates do |t|
      t.string      :from, null: false
      t.string      :to, null: false
      t.decimal     :rate, null: false, :precision => 20, :scale => 10
      t.date        :published_at, null: false
      t.timestamps null: false
    end
  end
end
