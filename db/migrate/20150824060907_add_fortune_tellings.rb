class AddFortuneTellings < ActiveRecord::Migration
  def change
    create_table :fortune_telling_users do |t|
      t.integer :user_id, null: false
      t.date :birthdate, null: false
      t.integer :constellation, null: false
      t.timestamps null: false
    end
    add_index :fortune_telling_users, [:user_id], :unique => true, :name => 'index_fortune_telling_users_1'

    create_table :fortune_telling_results do |t|
      t.date :target_date, null: false
      t.integer :constellation, null: false
      %w(love money work).each do |k|
        t.text "#{k}_text_jp".to_sym, null: false
        t.text "#{k}_text_en".to_sym, null: false
        t.integer "#{k}_point_100".to_sym, null: false
        t.integer "#{k}_point_5a".to_sym, null: false
        t.integer "#{k}_point_5b".to_sym, null: false
        t.integer "#{k}_point_10".to_sym, null: false
      end
      t.text :total_text_jp, null: false
      t.text :total_text_en, null: false
      t.integer :total_point_100, null: false
      %w(direction food fashion item person place).each do |k|
        t.string "lucky_#{k}_jp".to_sym, null: false
        t.string "lucky_#{k}_en".to_sym, null: false
      end
      t.integer :lucky_number, null: false
      t.datetime :sent_at
      t.timestamps null: false
    end
    add_index :fortune_telling_results, [:target_date, :constellation], :unique => true, :name => 'index_fortune_telling_results_1'
    
    Campaign.delete_all(tag: 'FortuneTelling')
    Campaign.create(
      :type => 'fortune_telling',
      :name => 'Fortune Telling', 
      :description => '',
      :cover_image_url => '/assets/web/campaigns/vg_fortune_main_pic.png',
      :start_at => Date.new(1970,1,1),
      :end_at => Date.new(9999,12,31), 
      :point_cost => 30,
      :regulary => false,
      :tag => 'FortuneTelling',
      :order_num => 30,
    )

  end
end
