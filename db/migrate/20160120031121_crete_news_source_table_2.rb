class CreteNewsSourceTable2 < ActiveRecord::Migration
  def change
  	create_table :news_picks_sources do |t|
      t.string    :source_url, null: false   
      t.string    :region, null: false     
      t.datetime  :deleted_at
      t.timestamps null: false
    end
  end
end
