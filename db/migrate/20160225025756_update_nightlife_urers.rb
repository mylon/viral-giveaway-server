class UpdateNightlifeUrers < ActiveRecord::Migration
  def change
  	add_column :nightlife_users, :tag_line, :string, null: true
  	add_column :nightlife_users, :show_men, :integer, null: true, default: 1
  	add_column :nightlife_users, :show_women, :integer, null: true, default: 1
  	add_column :nightlife_users, :device_udid, :string, null: true
  	add_column :nightlife_users, :is_dummy, :integer, null: true, default: 0
  	add_column :nightlife_users, :match_notification, :integer, null: true, default: 1
  	add_column :nightlife_users, :message_notification, :integer, null: true, default: 1
  	add_column :nightlife_users, :like_notification, :integer, null: true, default: 1
  	add_column :nightlife_users, :is_online, :integer, null: true
  	remove_column :nightlife_users, :platform, :integer
  	add_column :nightlife_users, :platform, :string, null: true
  end
end
