class ChangeNameUserColumn < ActiveRecord::Migration
  def change
  	change_column :users, :name, :string, null: true
  	change_column :nightlife_users, :name, :string, null: true
  end
end
