class CreateDreamPhotoTables < ActiveRecord::Migration
  def change
    create_table :dream_photos do |t|
      t.integer   :user_id, null: false
      t.integer   :photo_type, null: false, defualt: 0
      t.text      :comment, null: false
      t.string    :user_photo_url, null: false, default: ''
      t.string    :base_photo_url, null: false, default: ''
      t.string    :result_photo_url, null: false, default: ''
      t.datetime  :completed_at
      t.timestamps null: false
    end
    
    add_index :dream_photos, [:user_id], :name => 'index_dream_photos_1'
    
    add_column :campaigns, :type, :string, null: false, default: ''
    add_column :campaigns, :processing_time, :integer, null: false, default: 0
    
    add_index :campaigns, [:order_num, :start_at, :end_at, :deleted_at], :name => 'index_campaigns_4'

    Campaign.connection.schema_cache.clear!
    Campaign.reset_column_information

    Campaign.update_all({:type => 'prize', :order_num => 10})
    Campaign.delete_all(tag: 'DreamPhoto')
    Campaign.create(
      :type => 'dream_photo',
      :name => 'DreamPhoto', 
      :description => '',
      :cover_image_url => '/assets/web/campaigns/vg_dream_photo_main_pic.png',
      :start_at => Date.new(1970,1,1),
      :end_at => Date.new(9999,12,31), 
      :point_cost => 50,
      :regulary => false,
      :tag => 'DreamPhoto',
      :processing_time => 20 * 60, # 20m
      :order_num => 20,
    )
  end
end
