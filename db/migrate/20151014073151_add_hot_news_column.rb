class AddHotNewsColumn < ActiveRecord::Migration
  def change
  	add_column :news_picks, :hot_news, :integer, :default => 0
  end
end
