class CreateApps < ActiveRecord::Migration
=begin
  def change
    create_table :apps do |t|
      t.string :name, null: false
      t.string :code, null: false
      t.timestamps null: false
    end
    add_index :apps, [:code], :unique => true, :name => 'index_apps_1'

    app = App.create(name: "VIRAL GIVEAWAY", code: "vg")
    App.create(name: "DREAM PHOTO", code: "dp")
 
    create_table :user_apps do |t|
      t.integer :user_id, null: false
      t.integer :app_id, null: false
      t.timestamps null: false
    end
    add_index :user_apps, [:user_id, :app_id], :unique => true, :name => 'index_user_apps_1'
    User.all.each do |u|
      UserApp.create(user: u, app: app)
    end
   
    add_column :information, :app_id, :integer, null: false
    Information.update_all({:app_id => app.id})
    add_index :information, [:app_id, :published_at, :deleted_at], :name => 'index_information_2'
  end
=end
end
