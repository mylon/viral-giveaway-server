class ChangeTwitterColumnTwitter < ActiveRecord::Migration
  def change
  	change_column :facebook_page_data, :twitter_link, :text, null: true
  end
end
