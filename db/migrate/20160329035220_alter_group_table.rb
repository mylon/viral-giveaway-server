class AlterGroupTable < ActiveRecord::Migration
  def change
  	remove_column :nightlife_groups, :group_barcode, :string
  	remove_column :nightlife_groups, :createdby_user_id, :integer
  	add_column :nightlife_groups, :createdby_user_id, :integer, null: true
  	add_column :nightlife_groups, :createdby_match, :integer, null: true, default: 0
  	add_column :nightlife_groups, :group_barcode, :string, null: true
  	add_column :nightlife_groups, :single_single, :integer, null: true, default: 0
  	add_column :nightlife_groups, :single_group, :integer, null: true, default: 0
  	add_column :nightlife_groups, :group_group, :integer, null: true, default: 0
  	add_column :nightlife_groups, :group_join, :integer, null: true, default: 0
  	remove_column :nightlife_chatrooms, :is_group, :integer
  	remove_column :nightlife_chatrooms, :group_one, :integer
  	remove_column :nightlife_chatrooms, :group_two, :integer
  	remove_column :nightlife_chatrooms, :user_one, :integer
  	remove_column :nightlife_chatrooms, :user_two, :integer
  	add_column :nightlife_chatrooms, :group_id, :integer
  end
end
