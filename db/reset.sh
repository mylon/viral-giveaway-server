#!/bin/bash -x

BASEDIR=$(dirname $0); if [ $(expr match "$BASEDIR" ^\/) -eq 0 ]; then BASEDIR=$(pwd)/$BASEDIR; fi
PJROOT="$BASEDIR/../"
RAILS_ENV=$(echo $RAILS_ENV)
RAILS_ENV=${RAILS_ENV:-development}

$PJROOT/bin/rake db:reset RAILS_ENV=$RAILS_ENV
$PJROOT/bin/rake db:migrate
$PJROOT/bin/rails r Tasks::ImportMccTask.execute -f db/data/mcc-mnc-table.csv
$PJROOT/bin/rake db:seed
#$PJROOT/bin/rails r Tasks::Provider::AppDriver::AggregateTask.execute
#$PJROOT/bin/rails r Tasks::Provider::Adatha::AggregateTask.execute
