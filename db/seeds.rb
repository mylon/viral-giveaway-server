app = App.where({code:'vg'}).first

#####################################
# operators 
#####################################
Operator.register('fukata',  'Tatsuya Fukata', 'tatsuya.fukata@gmail.com', '85yym18nMYTVJQNxMb99VDkAnrnbP2fT')
Operator.register('yoji',    'Yoji Matsuyama', 'yoji@adfit.asia',          'd7Np9LmjPfjNI2vBV5mt6CYvLGZbQqU5')
Operator.register('satoshi', 'Satoshi Miyake', 'satoshi@adfit.asia',       'VFffQXDmQrBjj3vd72UkE5NSJmNRrQIf')

#####################################
# users 
#####################################
#u1 = User.register({
#  :name      => 'Sample',
#  :email     => 'tatsuya.fukata@gmail.com',
#  :birthdate => '07/11/1986',
#  :gender    => Rails.application.config.gender_map[:male],
#})
#d1 = Device.register(u1, {
#  :imsi => 'dummy',
#  :imei => 'dummy',
#  :android_id => 'dummy',
#  :device => 'dummy',
#  :model => 'dummy',
#  :product => 'dummy',
#  :manufacturer => 'dummy',
#  :display => 'dummy',
#  :brand => 'dummy',
#  :os_version => '4.4.2',
#})
#u1.device = d1
#u1.save!

#####################################
# information 
#####################################
Information.create(:app => app, :title => 'Title1', :content => 'Content1', :published_at => Time.now, :video_url => nil, :image_url => nil)
Information.create(:app => app, :title => 'Title2', :content => 'Content2', :published_at => Time.now, :video_url => nil, :image_url => 'http://dummyimage.com/600x400/000/fff.png')
Information.create(:app => app, :title => 'Title3', :content => 'Content3', :published_at => Time.now, :video_url => 'https://www.youtube.com/watch?v=nmFDRYkxEmo', :image_url => 'http://dummyimage.com/600x400/000/fff.png')

#####################################
# campaigns
#####################################
Campaign.create(
  :name => 'iPhone6', 
  :description => '',
  :cover_image_url => '/assets/web/campaigns/vg_iphone_main_pic.png',
  :start_at => Date.today(), 
  :end_at => Date.today().next_week(), 
  :point_cost => 5,
  :regulary => true,
  :period => '1w',
  :tag => 'iPhone',
  :clone_campaign_id => 1,
)
