class Tasks::SendmailTask < Tasks::Base
  def self.execute
    require 'optparse'

    @opt = {}
    OptionParser.new do |parser|
      parser.on('-u VALUE', '--uid VALUE', 'user_id') {|v| @opt[:user_id] = v}
      parser.on('-s VALUE', '--subject VALUE', 'subject') {|v| @opt[:subject] = v}
      parser.on('-b VALUE', '--body VALUE', 'body') {|v| @opt[:body] = v}
      parser.on('-d VALUE', '--delay VALUE', 'delay') {|v| @opt[:delay] = v}
      parser.parse!(ARGV)
    end
    pre_execute(@opt)

    Aws.config.update({
      region: Rails.application.config.sqs_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    })

    begin
      u = User.find(@opt[:user_id])
      u.send_email!(
        VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
        @opt[:subject],
        @opt[:body],
        {
          :delay => @opt[:delay],
        }
      )
    rescue => e
      log_error e
    end

    post_execute(@opt)
  end
end
