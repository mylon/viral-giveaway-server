class Tasks::UpdatedNewsPicksTagsTaskOne < Tasks::Base

  def self.execute

    where = {
      :deleted_at => nil,
      :publish_status => 2       
    }
    
    newspicks = NewsPick.where(where).order('id desc').limit(10000).offset(10000)
    if !newspicks.blank? then
      newspicks.each do |news|
        tags_array = []
        news_tag_array = []
        array_tag_no = ""
        result_search_tag = ""
        combine_final_array = []
        if !news.news_pick_shop.blank? then
            news_tag_array.push(news.news_pick_shop)
        end

        #if news.tags then
        #  old_tags = news.tags.split(/,/)
        #end

        region_no = ""
        if news.region == 'th' then
          region_no = 1
        elsif news.region == 'jp' then
          region_no = 2
        elsif news.region == 'en' then
          region_no = 3
        end

        city_condition = [ 0, '0']
        if news.city_id.to_i > 0 then
          city_condition.push(news.city_id.to_i)
        end

        category_condition = [ 0, '0']
        if news.category_id.to_i > 0 then
          category_condition.push(news.category_id.to_i)
        end
   
        begin
          news_update = nil
          tags_array = NewsTag.where({region: region_no}).where("city_id IS NULL OR city_id IN (?)", city_condition).where("category_id IS NULL OR category_id IN (?)", category_condition)
          tags_array.each do |each_tag|
          if news.news_description.downcase.include?(each_tag.tag.downcase) or news.title.downcase.include?(each_tag.tag.downcase) then
                  if !news_tag_array.include?(each_tag.tag.downcase) then
                    news_tag_array.push(each_tag.tag.downcase)
                  end
                  if !each_tag.tags_group.blank? then
                      group_tags = each_tag.tags_group.split(',')
                      group_tags.each do |each_group_tag|
                          if !news_tag_array.include?(each_group_tag.downcase) then
                            news_tag_array.push(each_group_tag.downcase)
                          end
                      end
                  end
            end
          end
          #merge two array test
          #combine_final_array = old_tags | news_tag
          if !news_tag_array.blank? then
            result_search_tag = news_tag_array.join(",")
          end
          if !result_search_tag.blank? then
            NewsPick.transaction do
                news_update = NewsPick.find_by(id: news.id) 
                news_update.tags = result_search_tag
                news_update.updated_at = Time.now
                news_update.news_staff_checked = 1
                news_update.save!
            end
          end
        rescue => e
          log_error(e)
        end
      end
    end
  end
end
