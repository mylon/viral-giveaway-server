class Tasks::EMR::Plugin::Base < Tasks::EMR::Base
  def initialize(params)
    @name = params['batch']
    @job_flow_id = params['job_flow_id']

    @emr = Aws::EMR::Client.new(
      region: Rails.application.config.emr_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )

    @s3 = Aws::S3::Client.new(
      region: Rails.application.config.s3_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )

    @cluster = @emr.describe_cluster({ cluster_id: @job_flow_id })
    logger.debug "cluster=#{@cluster.inspect}"
    @steps = @emr.list_steps({ cluster_id: @job_flow_id })
    logger.debug "steps=#{@steps.inspect}"
    @batch_step = @steps.steps.select{|step| step.name == @name}.first
    logger.debug "batch_step=#{@batch_step.inspect}"
  end

  def complete?
    @cluster.cluster.status.state_change_reason.code == "ALL_STEPS_COMPLETED"
  end

  def running?
    @cluster.cluster.status.state == "RUNNING"
  end

  def error?
    is_error = @batch_step.nil? or
    %w{TERMINATED_WITH_ERRORS}.include?(@cluster.cluster.status.state) or
    %w{INTERNAL_ERROR VALIDATION_ERROR INSTANCE_FAILURE BOOTSTRAP_FAILURE STEP_FAILURE}.include?(@cluster.cluster.status.state_change_reason.code) or 
    "Steps completed with errors" == @cluster.cluster.status.state_change_reason.message
    logger.warn "job_flow_id=#{@job_flow_id} is error" if is_error
    is_error
  end

  def fetch_result
    output = nil 
    @batch_step.config.args.each_with_index do |arg,index|
      if arg == '-output' then
        output = @batch_step.config.args[index+1]
        break
      end
    end
    logger.debug "output=#{output}"

    uri = URI.parse(output)
    objects = @s3.list_objects({
      bucket: uri.host,
      prefix: uri.path.gsub(/^\//, ''),
    })
    logger.debug "objects=#{objects.inspect}"

    @results = []  
    @tmpdir = Dir.mktmpdir  
    objects.contents.each do |content|
      if /.*(part-[0-9]+)$/ =~ content.key then
        logger.debug "fetch content=#{content.key}"
        path = "#{@tmpdir}/#{$1}"
        @s3.get_object(response_target:path, bucket:uri.host, key:content.key)
        @results.push(path)
      end
    end
  end

  def import_data
    fetch_result()

    @data = {}
    @results.each do |path|
      File.open(path, 'r:utf-8') do |f|
        while line = f.gets
          parts = parse(line)
          append_data(parts)
        end
      end
    end
    
    register
  end

  def logging
    if error? then
      Fluent::Logger.post("vg.emr_error_log", {
        :batch => @name,
        :job_flow_id => @job_flow_id,
      })
    else
      start_time = @cluster.cluster.status.timeline.creation_date_time.to_i
      end_time = @cluster.cluster.status.timeline.end_date_time.to_i
      Fluent::Logger.post("vg.emr_complete_log", {
        :batch => @name,
        :job_flow_id => @job_flow_id,
        :start_time => start_time,
        :end_time => end_time,
        :elapsed_time => end_time - start_time,
      })
    end
  end
end
