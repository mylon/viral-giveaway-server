class Tasks::EMR::Plugin::AggregateMau < Tasks::EMR::Plugin::Base
  def initialize(params)
    super(params)
  end

  def run
    if error? then
      logging()
      return
    end
    if running? then
      throw :skip_delete
    end
    
    begin
      import_data()
      logging()
    ensure
      FileUtils.remove_entry_secure @tmpdir if @tmpdir
    end   
  end

  def parse(line)
    parts = line.split("\t")
    date = Date.strptime(parts[0], "%Y-%m")
    time = date.to_time.to_i
    {
      :date => date,
      :mau  => parts[1],
      :time => time,
    }
  end

  def append_data(parts)
    @data[:mau] ||= [] 
    @data[:mau].push(parts)
  end

  def register
    LogSummaryMau.transaction do
      columns = [:time, :mau, :user_num]
      values = @data[:mau].map do |dat| 
        [dat[:time], dat[:mau], User.where("created_at < ?", dat[:date].since(1.month)).count] 
      end
      LogSummaryMau.import columns, values, :on_duplicate_key_update => [:mau, :user_num]
    end
  end
end
