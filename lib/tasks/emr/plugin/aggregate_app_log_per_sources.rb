class Tasks::EMR::Plugin::AggregateAppLogPerSources < Tasks::EMR::Plugin::Base
  def initialize(params)
    super(params)
  end

  def run
    if error? then
      logging()
      return
    end
    if running? then
      throw :skip_delete
    end

    begin
      import_data()
      logging()
    ensure
      FileUtils.remove_entry_secure @tmpdir if @tmpdir
    end   
  end

  def parse(line)
    parts = line.split("\t")
    keys = parts[0].split('::')
    time = Time.strptime(keys[0], "%Y-%m-%dT%H")
    date = time.to_date
    {
      :date => date,
      :source => keys[1],
      :time => time,
      :timestamp => date.to_time.to_i,
      :value => JSON.parse(parts[1], {:symbolize_names => true})
    }
  end

  def append_data(parts)
    @data[:app_log] ||= [] 
    @data[:app_log].push(parts)
  end

  def register
    LogSummaryAppLogPerSource.transaction do
      @data[:app_log].each do |dat|
        h = dat[:time].hour
        columns = [:time, :source, "h#{h}_access", "h#{h}_register", "h#{h}_facebook_like", "h#{h}_facebook_share", "h#{h}_twitter_share", "h#{h}_twitter_follow", "h#{h}_cpi"]
        from = dat[:time]
        to = dat[:time].since(1.hour)
        values = [
          dat[:timestamp], 
          dat[:source] || "none", 
          dat[:value][:accessed_count], 
          User.where({:an_source => dat[:source].to_s}).where("? <= created_at AND created_at < ?", from, to).count(),
          Tracking.joins(:user).where({'users.an_source' => dat[:source].to_s, :type => Rails.application.config.tracking_type_map[:facebook_like]}).where("? <= trackings.created_at AND trackings.created_at < ?", from, to).count(),
          Tracking.joins(:user).where({'users.an_source' => dat[:source].to_s, :type => Rails.application.config.tracking_type_map[:facebook_share]}).where("? <= trackings.created_at AND trackings.created_at < ?", from, to).count(),
          Tracking.joins(:user).where({'users.an_source' => dat[:source].to_s, :type => Rails.application.config.tracking_type_map[:twitter_share]}).where("? <= trackings.created_at AND trackings.created_at < ?", from, to).count(),
          Tracking.joins(:user).where({'users.an_source' => dat[:source].to_s, :type => Rails.application.config.tracking_type_map[:twitter_follow]}).where("? <= trackings.created_at AND trackings.created_at < ?", from, to).count(),
          Tracking.joins(:user).where({'users.an_source' => dat[:source].to_s, :type => %w{app_driver adatha avazu mobvista}.map{|t| Rails.application.config.tracking_type_map[t.to_sym]}}).where("? <= trackings.created_at AND trackings.created_at < ?", from, to).count(),
        ] 
        LogSummaryAppLogPerSource.import columns, [values], :on_duplicate_key_update => ["h#{h}_access", "h#{h}_register", "h#{h}_facebook_like", "h#{h}_facebook_share", "h#{h}_twitter_share", "h#{h}_twitter_follow", "h#{h}_cpi"]
      end
    end
  end
end
