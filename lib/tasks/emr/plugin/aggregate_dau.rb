class Tasks::EMR::Plugin::AggregateDau < Tasks::EMR::Plugin::Base
  def initialize(params)
    super(params)
  end

  def run
    if error? then
      logging()
      return
    end
    if running? then
      throw :skip_delete
    end

    begin
      import_data()
      logging()
    ensure
      FileUtils.remove_entry_secure @tmpdir if @tmpdir
    end   
  end

  def parse(line)
    parts = line.split("\t")
    date = Date.strptime(parts[0], "%Y-%m-%d")
    time = date.to_time.to_i
    {
      :date => date,
      :dau  => parts[1],
      :time => time,
    }
  end

  def append_data(parts)
    @data[:dau] ||= [] 
    @data[:dau].push(parts)
  end

  def register
    LogSummaryDau.transaction do
      columns = [:time, :dau, :user_num]
      values = @data[:dau].map do |dat| 
        [dat[:time], dat[:dau], User.where("created_at < ?", dat[:date].since(1.days)).count] 
      end
      LogSummaryDau.import columns, values, :on_duplicate_key_update => [:dau, :user_num]
    end
  end
end
