class Tasks::EMR::Base < Tasks::Base
   def method_missing(action, *args)
     self.class.__send__ action, *args
   end
end
