class Tasks::EMRJobWorkerTask < Tasks::Base
  def self.execute
    require 'serverengine'

    pre_execute(nil)

    se = ServerEngine.create(nil, MyWorker, {
      daemonize: false,
      log: Rails.application.config.emr_job_worker_log,
      log_level: Rails.application.config.emr_job_worker_log_level,
      log_rotate_age: Rails.application.config.emr_job_worker_log_rotate_age,
      log_rotate_size: Rails.application.config.emr_job_worker_log_rotate_size,
    })
    se.run

    post_execute(nil)
  end

  module MyWorker
    def run
      @plugins = {}
      register_plugin('aggregate_dau', Tasks::EMR::Plugin::AggregateDau)
      register_plugin('aggregate_mau', Tasks::EMR::Plugin::AggregateMau)
      register_plugin('aggregate_app_log_per_sources', Tasks::EMR::Plugin::AggregateAppLogPerSources)

      Aws.config.update({
        region: Rails.application.config.sqs_region,
        access_key_id: Rails.application.config.aws_access_key_id,
        secret_access_key: Rails.application.config.aws_secret_access_key
      })
      @poller = Aws::SQS::QueuePoller.new(Rails.application.config.emr_sqs_queue_url)
      @ses = Aws::SES::Client.new(
        region: Rails.application.config.ses_region,
      )

      until @stop
        @poller.before_request do |stats|
          throw :stop_polling if @stop
        end

        @poller.poll do |msg|
          begin
            parse = JSON.parse(msg.body)
            params = JSON.parse(parse["Message"])
            logger.debug "parse=#{parse}"
            logger.info "params=#{params}"
            run_plugin(params)
          rescue => e
            logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
            throw :skip_delete
          end
        end
      end
    end

    def stop
      @stop = true
    end

    def register_plugin(name, clazz)
      @plugins[name] = clazz
    end

    def run_plugin(params)
      batch = @plugins[params['batch']]
      logger.info "Start batch=#{params['batch']}, job_flow_id=#{params['job_flow_id']}"
      batch.new(params).run
      logger.info "Finish batch=#{params['batch']}, job_flow_id=#{params['job_flow_id']}"
    end
  end
end
