class Tasks::RunEMRJobFlowTask < Tasks::Base
  def self.execute
    require 'optparse'
    require 'aws-sdk'
    require 'time'

    @opt = {}
    OptionParser.new do |parser|
      parser.on('-b VALUE', '--batch VALUE', 'batch name') {|v| @opt[:batch] = v}
      parser.on('-f VALUE', '--from VALUE', 'from(GMT) YYYY-MM-DD HH:00:00') {|v| @opt[:from] = Time.strptime(v, "%Y-%m-%d %H:%M:%S")}
      parser.on('-t VALUE', '--to VALUE', 'to(GMT) YYYY-MM-DD HH:00:00') {|v| @opt[:to] = Time.strptime(v, "%Y-%m-%d %H:%M:%S")}
      parser.on('-m VALUE', '--master-isntance-type VALUE', 'master instance type m1.medium') {|v| @opt[:master_instance_type] = v}
      parser.on('-s VALUE', '--slave-isntance-type VALUE', 'slave instance type m1.medium') {|v| @opt[:slave_instance_type] = v}
      parser.on('-c VALUE', '--instance-count VALUE', 'instance count 1') {|v| @opt[:instance_count] = v}
      parser.on('-j VALUE', '--jof-flow-id VALUE', 'job flow id') {|v| @opt[:job_flow_id] = v}
      parser.on('-d', '--dry-run', 'dry run mode') {|v| @opt[:dry_run] = v}
      parser.parse!(ARGV)
    end
    pre_execute(@opt)

    # validate option
    raise ArgumentError, "require batch" unless @opt[:batch]
    raise ArgumentError, "require from < to" if @opt[:job_flow_id].nil? and @opt[:from] >= @opt[:to]

    if @opt[:job_flow_id] then
      notify_sns()
    else
      register_job_flow()
    end

    post_execute(@opt)
  end

  def self.notify_sns
    @sns = Aws::SNS::Client.new(
      region: Rails.application.config.emr_notify_sns_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )
    resp = @sns.publish(
      topic_arn: Rails.application.config.emr_notify_sns,
      subject: "re-entry #{@opt[:job_flow_id]}",
      message: {:job_flow_id => @opt[:job_flow_id], :batch => @opt[:batch]}.to_json,
    )
    logger.debug "notify_sns=#{resp.inspect}"
  end

  def self.register_job_flow
    @batch_option = Rails.application.config.emr_batch_option[@opt[:batch].to_sym]

    @emr = Aws::EMR::Client.new(
      region: Rails.application.config.emr_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )
    @now = Time.now

    run_job_flow_option = self.job_flow_option()
    logger.debug "run_job_flow_option=#{run_job_flow_option}"

    if @opt[:dry_run] then
      logger.info "DRY RUN"
    else
      job_flow_id = self.run_job_flow(job_flow_option)
      logger.info "job=#{job_flow_id}"
    end
  end

  def self.run_job_flow(job_flow_option)
    resp = @emr.run_job_flow(job_flow_option)
    job_flow_id = resp.job_flow_id

    @emr.add_job_flow_steps({
      job_flow_id: job_flow_id,
      steps: [
        {
          name: 'notify_sns',
          action_on_failure: 'CONTINUE',
          hadoop_jar_step: {
            jar: 's3://elasticmapreduce/libs/script-runner/script-runner.jar',
            args: [
              "s3://#{Rails.application.config.emr_script_bucket}/emr/notify_sns.sh",
              job_flow_id,
              @opt[:batch],
              Rails.application.config.emr_notify_sns,
            ],
          }
        },
      ]
    })

    job_flow_id
  end

  def self.job_flow_option
    input_path = self.input_path(%w(app_log))
    logger.debug "input_path=#{input_path}"

    job_name = "#{@opt[:batch]}_#{@now.strftime('%Y%m%d%H%M%S')}"
    logger.debug "job_name=#{job_name}"

    master_instance_type = @opt[:master_instance_type] || @batch_option[:master_instance_type]
    slave_instance_type = @opt[:slave_instance_type] || @batch_option[:slave_instance_type]
    instance_count = @opt[:instance_count] || @batch_option[:instance_count]

    run_job_flow_option = {
      name: job_name,
      log_uri: "s3://#{Rails.application.config.emr_log_bucket}/emr/logs/#{@now.strftime('%Y/%m/%d/%H/%M')}/#{job_name}",
      ami_version: '3.8.0',
      instances: {
        master_instance_type: master_instance_type,
        slave_instance_type: slave_instance_type,
        instance_count: instance_count,
        ec2_key_name: Rails.application.config.emr_ec2_key_name,
      },
      steps: [
        {
          name: @opt[:batch],
          action_on_failure: 'CONTINUE',
          hadoop_jar_step: {
            jar: '/home/hadoop/contrib/streaming/hadoop-streaming.jar',
            args: [
              "-files",
              "s3n://#{Rails.application.config.emr_script_bucket}/emr/streaming/#{@opt[:batch]}/mapper.rb,s3n://#{Rails.application.config.emr_script_bucket}/emr/streaming/#{@opt[:batch]}/reducer.rb",
              "-mapper",
              "mapper.rb",
              "-reducer",
              "reducer.rb",
              "-input",
              input_path,
              "-output",
              "s3n://#{Rails.application.config.emr_log_bucket}/emr/output/#{@now.strftime('%Y/%m/%d/%H/%M')}/#{job_name}",
            ],
          }
        },
      ],
      job_flow_role: "EMR_EC2_DefaultRole",
      service_role: "EMR_DefaultRole",
    }
  end

  def self.input_path(log_types)
    paths = []
    current = @opt[:from]
    while current < @opt[:to] do
      if current.day == 1 and current.hour == 0 and target = current.since(1.month).ago(1.hour) and target < @opt[:to] then
        paths.push(current.strftime("%Y/%m/*/*/*"))
        current = current.since(1.month)
      elsif current.hour == 0 and current.since(1.days).ago(1.hour) < @opt[:to] then
        paths.push(current.strftime("%Y/%m/%d/*/*"))
        current = current.since(1.days)
      elsif current < @opt[:to] then
        paths.push(current.strftime("%Y/%m/%d/%H/*"))
        current = current.since(1.hours)
      else
        break
      end
    end

    input_paths = []
    paths.each do |path|
      log_types.each do |log|
        input_paths.push "s3n://#{Rails.application.config.emr_log_bucket}/logs/#{log}/#{path}"
      end
    end
    input_paths.join(',')
  end
end
