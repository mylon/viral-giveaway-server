class Tasks::PublishedUsualNewsPicksTask < Tasks::Base

  def self.execute
    Rails.application.config.newspick_region_map.each do |region|
      begin
        where = {
          :region => region,
          :deleted_at => nil,
          :publish_status => 4,
          :news_staff_checked =>1,    
          :category_id => 16
        }
        getnews = NewsPick.where(where).order('id asc').limit(1).select('id')
        getnews.each do |each_news|
          newspick = nil
          NewsPick.transaction do 
            newspick = NewsPick.find_by(id: each_news.id)
            newspick.publish_status = 2  
            newspick.created_at = Time.now    
            newspick.updated_at = Time.now  
            newspick.save!
          end
        end                 
      rescue => e
        log_error(e)
      end
    end
  end

end
