class Tasks::Mail::FortuneTellingTask < Tasks::Mail::Base
  def self.execute
    require 'optparse'
    
    @opt = {}
    OptionParser.new do |parser|
      parser.on('-u VALUE', '--uid VALUE', 'user_id') {|v| @opt[:user_id] = v}
      parser.on('-d VALUE', '--date VALUE', 'yyyy-mm-dd') {|v| @opt[:date] = Date.strptime(v, "%Y-%m-%d")}
      parser.parse!(ARGV)
    end
    pre_execute(@opt)

    run()

    post_execute(@opt)
  end

  def self.run
    results = [] 
    if @opt[:date] then
      results = FortuneTellingResult.where({target_date: @opt[:date]})
    else
      results = FortuneTellingResult.where({target_date: Date.today})
    end
    if results.length == 0 then
      logger.warn "Not found result"
      return
    end
    unless FortuneTellingResult.ready?(results.first.target_date) then
      logger.warn "#{results.first.target_date} is not raedy status." 
      return
    end

    results.each do |result|
      where = {constellation: result.constellation}
      where[:user_id] = @opt[:user_id] if @opt[:user_id]
      FortuneTellingUser.where(where).each do |u|
        sendmail(u.user, result)
      end
      result.sent_at = Time.now
      result.save!
    end
  end

  def self.sendmail(user, result)
    logger.debug "target_date=#{result.target_date}, constellation=#{result.constellation}, user=#{user.id}"
    from = VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name)
    subject = I18n.t("mail.fortune_telling_subject") % result.target_date.strftime("%m/%d/%Y")
    body = render(
      template: 'fortune_telling',
      layout: 'mail',
      locals: {
        user: user,
        result: result,
      },
    ) 
    user.send_email!(from, subject, body, {html: true})
  end
end
