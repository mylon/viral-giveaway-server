class Tasks::Mail::DailyTask < Tasks::Mail::Base
  def self.execute
    require 'optparse'

    @opt = {}
    OptionParser.new do |parser|
      parser.on('-u VALUE', '--uid VALUE', 'user id') {|v| @opt[:uid] = v}
      parser.on('-t VALUE', '--template VALUE', 'template name') {|v| @opt[:template] = v}
      parser.parse!(ARGV)
    end
    pre_execute(@opt)

    users = []
    if @opt[:uid] then
      user = User.where({:deleted_at => nil, :id => @opt[:uid]}).first
      users.push(user) if user
    else
      users = User.where({
        :deleted_at => nil,
        :notification => true,
      })
    end

    users.each do |user|
      template = ""
      promotion_offers = [] 
      if user.include_segment?(:only_register) then
        promotion_offers = fetch_promotion_offers(user)
        template = 'daily_only_registration'
      elsif user.include_segment?(:only_action) then
        promotion_offers = fetch_promotion_offers(user)
        template = 'daily_only_action'
      else
        last_cpi_tracking = user.last_cpi_tracking()
        next unless last_cpi_tracking
        
        promotion_offers = fetch_promotion_offers(user, last_cpi_tracking.created_at)
        template = 'daily_new_arrival'
      end
      next if promotion_offers.length == 0
      template = @opt[:template] if @opt[:template]

      body = render(
        :template => template, 
        :layout => 'mail',
        :locals => {
          :user => user,
          :promotion_offers => promotion_offers,
        },
      )
      logger.info body
      user.send_email(VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name), I18n.t('mail.daily_subject'), body, {:html => true})
    end

    post_execute(@opt)
  end

  def self.fetch_promotion_offers(user, since_time=nil)
    params = {
      :category => 'promotion',
      :country => user.country,
      :platform => user.platform,
      :exclude_installed => true,
    }
    params[:since_time] = since_time if since_time 
    Offer.search(user, params)
  end
end
