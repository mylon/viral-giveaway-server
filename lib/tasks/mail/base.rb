class Tasks::Mail::Base < Tasks::Base
  def self.controller_path
    ''
  end

  def self.render(options)
    av = ActionView::Base.new(Rails.root.join('app', 'views'))
    av.assign(options[:locals])

    template = "mail/#{options[:template]}.html.erb"
    layout = options[:layout].nil? ? nil : "layouts/#{options[:layout]}.html.erb"
    av.render(
      :template => template,
      :layout => layout,
    )
  end
end
