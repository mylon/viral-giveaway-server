class Tasks::AddNewsPicksFeedTask < Tasks::Base

  def self.execute
    require 'rss'
    require 'open-uri'

    feeds = []
    tag_value = []

    NewsTag.all.each do |tags|
      tag_value.push(tags)
    end
    
    NewsPicksSource.find_each do |source|
      url = source.source_url
      region = source.region.to_i
      city_id = source.city_id.to_i
      publish_status = 3
      category_id = 0
      news_url = " "
      image_url = "nil"
      hot_news = 0
      news_promotion='N'
      news_event="N"
      news_staff_checked=0
      tags_array = []
      array_tag_no = ""

      region_txt = ""
      if region == 1 then
        region_txt = 'th'
      elsif region == 2 then
        region_txt = 'jp'
      elsif region == 3 then
        region_txt = 'en'
      end

     array_tag_no = tag_value.each_index.select{|i| (tag_value[i].region.to_i == region.to_i && (tag_value[i].city_id.to_i == city_id.to_i || tag_value[i].city_id.to_i == 0))}
     array_tag_no.each do |tag_index|
      tags_array.push(tag_value[tag_index].tag.strip);
     end

      open(url, :read_timeout => 99999) do |rss|
          feed = RSS::Parser.parse(rss)
          feed.items.each do |item|
            news_tag = []
            result_search_tag = ""
            check_duplicate_news = NewsPick.where(["news_url LIKE ?", "%#{item.link}%"]).first
            if !check_duplicate_news then
              begin
                newspick = nil
                tags_array.each do |each_tag|
                  if item.description.include?(each_tag) then
                    news_tag.push(each_tag)
                  end
                end

                if !news_tag.blank? then
                  result_search_tag = news_tag.join(",")
                end

                clean_description = ""
                if item.description != "" then
                  clean_description = ActionView::Base.full_sanitizer.sanitize(item.description.gsub(/<\/?[^>]*>/, "").tr("\n","").tr("\t",""))
                end

                clean_url = ""
                if item.title != "" then
                  clean_url = item.title.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
                end

                NewsPick.transaction do
                    newspick = NewsPick.register(
                      item.title, 
                      category_id, 
                      region_txt, 
                      item.link, 
                      image_url, 
                      city_id, 
                      publish_status, 
                      result_search_tag, 
                      hot_news, 
                      item.author, 
                      clean_url, 
                      news_promotion, 
                      news_event, 
                      clean_description, 
                      news_staff_checked
                    ) 
                end
              rescue => e
                log_error(e)
              end
            end
        end
      end
    end

  end

end
