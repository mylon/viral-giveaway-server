class Tasks::UpdateCurrencyExchangeRateTask < Tasks::Base
  def self.execute
    require 'date'

    conn = Faraday.new(:url => 'https://query.yahooapis.com') do |c|
      c.use FaradayMiddleware::ParseJson, content_type: 'application/json'
      c.use Faraday::Request::UrlEncoded
      c.use Faraday::Adapter::NetHttp
    end

    Rails.application.config.exchange_currencies.each do |currency|
      begin
        from = currency
        to = 'USD'
        uri = "/v1/public/yql?q=select * from yahoo.finance.xchange where pair in (\"#{from}#{to}\")&format=json&env=store://datatables.org/alltableswithkeys"
        response = conn.get do |req|
          req.url uri
          req.options.timeout = 10
          req.options.open_timeout = 5
        end

        results = response.body['query']['results']
        published_at = Date.strptime(results['rate']['Date'], '%m/%d/%Y')
        rate = results['rate']['Rate'].to_f
        next if rate == 0

        CurrencyExchangeRate.transaction do
          logger.debug("from=#{from}, to=#{to}, rate=#{rate}, published_at=#{published_at}")
          CurrencyExchangeRate.register(from, to, published_at, rate)
        end
      rescue => e
        log_error(e)
      end
    end
  end
end
