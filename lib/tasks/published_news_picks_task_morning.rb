class Tasks::PublishedNewsPicksTaskMorning < Tasks::Base

  def self.execute
    Rails.application.config.newspick_region_map.each do |region|
      city_list = Rails.application.config.newspick_city
      if region == 'jp' then
        city_list = Rails.application.config.newspick_jpcity
      end
      city_list.each do |city|
        begin
          where = {
            :region => region,
            :deleted_at => nil,
            :publish_status => 3,
            :news_staff_checked =>1,    
            :news_promotion =>0,   
            :hot_news=>0,
            :city_id => city
          }
          getnews = NewsPick.where(where).order('id asc').limit(2).select('id')
          getnews.each do |each_news|
            newspick = nil
            NewsPick.transaction do 
              newspick = NewsPick.find_by(id: each_news.id)
              newspick.publish_status = 2  
              newspick.created_at = Time.now    
              newspick.updated_at = Time.now  
              newspick.save!
            end
          end                 
        rescue => e
          log_error(e)
        end
      end
    end
  end

end
