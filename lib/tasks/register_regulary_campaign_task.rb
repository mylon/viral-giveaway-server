class Tasks::RegisterRegularyCampaignTask < Tasks::Base
  def self.execute
    pre_execute(nil)

    target_date = Date.today.since 2.days
    ids = Campaign.select("max(id) id").where({:deleted_at => nil, :regulary => true}).group(:clone_campaign_id).map{|c| c.id}
    Campaign.where({:id => ids}).each do |c|
      if target_date < c.end_at then
        next
      end

      cc = c.dup
      cc.start_at = c.end_at
      cc.end_at = cc.start_at.since(self.parse_period(cc.period))
      cc.save!
      logger.debug cc
    end

    post_execute(nil)
  end

  def self.parse_period(period)
    case period
    when /^([0-9])+m$/
      $1.to_i.month
    when /^([0-9])+w$/
      $1.to_i.week
    else
      raise ArgumentError, "Couldn't parse period=#{period.to_s}"
    end
  end
end
