class Tasks::SendmailWorkerTask < Tasks::Base
  def self.execute
    require 'serverengine'

    pre_execute(nil)

    se = ServerEngine.create(nil, MyWorker, {
      daemonize: false,
      log: Rails.application.config.send_mail_worker_log,
      log_level: Rails.application.config.send_mail_worker_log_level,
      log_rotate_age: Rails.application.config.send_mail_worker_log_rotate_age,
      log_rotate_size: Rails.application.config.send_mail_worker_log_rotate_size,
    })
    se.run

    post_execute(nil)
  end

  module MyWorker
    def run
      Aws.config.update({
        region: Rails.application.config.sqs_region,
        access_key_id: Rails.application.config.aws_access_key_id,
        secret_access_key: Rails.application.config.aws_secret_access_key
      })
      @poller = Aws::SQS::QueuePoller.new(Rails.application.config.sendmail_sqs_queue_url)
      @ses = Aws::SES::Client.new(
        region: Rails.application.config.ses_region,
      )

      until @stop
        @poller.before_request do |stats|
          throw :stop_polling if @stop
        end

        @poller.poll do |msg|
          begin
            parse = JSON.parse(msg.body)
            params = JSON.parse(parse["Message"])
            logger.debug "parse=#{parse}"
            logger.info "params=#{params}"
            if params['opt']['raw'] then
              resp = send_raw_email(params)
            else
              resp = send_email(params)
            end
            logger.info "message_id=#{resp.message_id}"
          rescue => e
            logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
            throw :skip_delete
          end
        end
      end
    end

    def stop
      @stop = true
    end

    def send_raw_email(params)
      options = {
        raw_messages: {
          data: params['body']
        }
      }
      @ses.send_raw_email(options)
    end

    def send_email(params)
      message = {
        subject: {
          data: params['subject'],
          charset: 'UTF-8',
        },
        body: {}
      } 
      if params['opt']['text'] then
        message[:body][:text] = {
          data: params['body'],
          charset: 'UTF-8',
        }
      else
        message[:body][:html] = {
          data: params['body'],
          charset: 'UTF-8',
        }
      end

      @ses.send_email(
        source: params['from'],
        destination: {
          to_addresses: params['to'],
        },
        message: message,
        reply_to_addresses: [ params['from'] ],
        return_path: params['from'],
      )
    end
  end

end
