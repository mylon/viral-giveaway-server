class Tasks::Base
  @@logger = Rails.application.config.task_logger

  Fluent::Logger::FluentLogger.open(nil, :host=>Rails.application.config.fluent_host, :port=>Rails.application.config.fluent_port)

  def self.logger
    @@logger
  end

  def self.pre_execute(opt)
    log_start(opt)
  end

  def self.post_execute(opt)
    log_finish(opt)
  end

  def self.log_start(opt)
    logger.info "#{self.to_s} START opt=#{opt}"
  end

  def self.log_finish(opt)
    logger.info "#{self.to_s} FINISH opt=#{opt}"
  end

  def self.log_error(e)
    logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
  end
end
