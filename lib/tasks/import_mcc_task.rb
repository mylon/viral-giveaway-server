class Tasks::ImportMccTask < Tasks::Base
  def self.execute
    require 'optparse'

    @opt = {}
    OptionParser.new do |parser|
      parser.on('-f VALUE', '--file VALUE', 'import reward code filepath') {|v| @opt[:file] = v}
      parser.parse!(ARGV)
    end
    pre_execute(@opt)
    raise ArgumentError, "#{@opt[:file]} is not found" unless @opt[:file] and File.file?(@opt[:file])


    errors = []
    MobileCountryCode.transaction do
      open(@opt[:file]) do |file|
        line = 0
        file.each do |csv_line|
          line += 1
          next if line == 1 # header

          csv_line.strip!
          next unless csv_line

          values = csv_line.split(",")

          MobileCountryCode.where({
            :code => values[0],
          }).first_or_initialize(
            :code => values[0],
            :country_iso => values[4],
            :country_name => values[5],
            :country_code => values[6],
          ).save!
        end
      end
    end

    errors.each do |e|
      logger.warn e
    end

    post_execute(@opt)
  end
end
