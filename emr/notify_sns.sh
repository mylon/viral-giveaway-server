#!/usr/bin/env bash

JOB_FLOW_ID=$1
BATCH=$2
SNS_ARN=$3

aws sns publish --topic-arn=$SNS_ARN --subject="finished EMR ${JOB_FLOW_ID}" --message="{\"job_flow_id\":\"${JOB_FLOW_ID}\",\"batch\":\"${BATCH}\"}"
