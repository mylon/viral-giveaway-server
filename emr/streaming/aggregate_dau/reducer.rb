#!/usr/bin/env ruby
require 'json'

data = {}
sessions = {}
while line = STDIN.gets do
  begin
    line.chomp!
    next unless line
    parts = line.split("\t")

    keys = parts[0].split('::')
    data_key = keys[0]
    session_id = keys[1]
    next if sessions[session_id]
    sessions[session_id] = true
    
    data[data_key] = data[data_key].to_i + parts[1].to_i
  rescue => e
    STDERR.puts "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
  end
end

data.each do |k,v|
  puts "#{k}\t#{v}"
end
