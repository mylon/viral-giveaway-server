#!/usr/bin/env ruby
require 'json'

while line = STDIN.gets do
  begin
    line.chomp!
    next unless line
    parts = line.split("\t")

    data_key = parts[0][0,10] #YYYY-MM-DD
    log = JSON.parse(parts[2], {:symbolize_names => true})
    next if log[:account_id].to_s.length == 0

    puts "#{data_key}::#{log[:account_id].to_s}\t1"
  rescue => e
    STDERR.puts "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
  end
end
