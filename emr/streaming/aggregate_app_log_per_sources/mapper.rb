#!/usr/bin/env ruby
require 'json'

while line = STDIN.gets do
  begin
    line.chomp!
    next unless line
    parts = line.split("\t")

    log = JSON.parse(parts[2], {:symbolize_names => true})
    data_key = "#{parts[0].split(':')[0]}::#{log[:session_id].to_s}"
    data = nil
    case log[:path]
    when '/'
      data = {:source => log[:source].to_s, :accessed_count => 1}
    when '/v1/users/register',
        /^\/users\/auth\/[^\/]+\/callback$/
      data = {:source => log[:source].to_s, :accessed_count => 1, :registered_count => 1}
    else
      next
    end

    puts "#{data_key}\t#{JSON.generate(data)}"
  rescue => e
    STDERR.puts "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
  end
end
