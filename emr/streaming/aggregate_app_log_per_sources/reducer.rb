#!/usr/bin/env ruby
require 'json'

data = {}
while line = STDIN.gets do
  begin
    line.chomp!
    next unless line
    parts = line.split("\t")

    keys = parts[0].split('::')
    date = keys[0]

    log = JSON.parse(parts[1], {:symbolize_names => true})

    data_key = "#{date}::#{log[:source]}"
    data[data_key] = {
      :accessed_count => 0,
      :registered_count => 0,
    } unless data[data_key]
    data[data_key][:accessed_count] += log[:accessed_count].to_i
    data[data_key][:registered_count] += log[:registered_count].to_i
  rescue => e
    STDERR.puts "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
  end
end

data.each do |k,v|
  puts "#{k}\t#{JSON.generate(v)}"
end

