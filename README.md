# viral-giveaway-server

## Servers

### Production

- WEB, API
	- 54.179.136.25
	- 54.179.140.196
- ADMIN
	- 54.179.141.24
- Image Proxy
	- 52.74.105.78

### Staging

- WEB, API, ADMIN, Image Proxy
	- 52.74.120.111

## Setup

- see [mylon / viral-giveaway-vagrant / source / README.md — Bitbucket](https://bitbucket.org/mylon/viral-giveaway-vagrant/src/HEAD/README.md)

## Database Migration

- Use migrate feature of Rails.	

## Manage Daemon

- Use [Supervisor: A Process Control System — Supervisor 3.1.3 documentation](http://supervisord.org/)

## Deploy

- Use [A remote server automation and deployment tool written in Ruby.](http://capistranorb.com/#)
- Deploy command is below.

	`$ BRANCH=[branch] cap [production|staging] deploy`

## Fileserver

- [mylon / fileserver — Bitbucket](https://bitbucket.org/mylon/fileserver)
- Put in important file (aws key,ssl,keystore...etc)

## API

- Use [Ruby on Rails](http://rubyonrails.org/)
- Controllers is `app/controllers/api/v1/`
- Authenticate code is `app/controllers/api/v1/base_controller.rb#authenticate_api`

### API for DreamPhoto

- Controllers is `app/controllers/api/v1/apps/`
- Authenticate app code is `app/controllers/api/v1/apps/base_controller.rb#authenticate_app_api`

### API Document for DreamPhoto

- Repository is [mylon / viral-giveaway-api-doc — Bitbucket](https://bitbucket.org/mylon/viral-giveaway-api-doc)


## WEB

- Use [AngularJS — Superheroic JavaScript MVW Framework](https://angularjs.org/)
- Path is `app/assets/javascripts/`

## ADMIN

- Use [AngularJS — Superheroic JavaScript MVW Framework](https://angularjs.org/)
- Path is `app/assets/javascripts/`
- How to create operator.
	- You can create operator on admin if you can login to admin.
	- Execute on rails console if can't login to admin. `Operator.register('account_id',  'name', 'email', 'password')` 

## Log Analytics

### Access log

- Use [Treasure Data - Analytics Infrastructure, Simplified in the Cloud - Treasure Data](http://www.treasuredata.com/)

### CPI, Conversion ...etc

- Use [AWS | Amazon Elastic MapReduce (EMR) | Hadoop MapReduce in the Cloud](https://aws.amazon.com/elasticmapreduce/)
- Import worker is `lib/tasks/emr_job_worker_task.rb`
- Register job flow task is `lib/tasks/run_emr_job_worker_task.rb`. pls check crontab in admin server.

## Image Proxy Server

- Our site is SSL but provided image is Not SSL so need proxy by nginx.

## DNS
- Use [AWS | Amazon Route 53 - Domain Name Server - DNS Service](https://aws.amazon.com/route53/)

## Server Monitoring
- Use [Mackerel: A Revolutionary New Kind of Application]
- Generally problems(hdd, connection, cpu, memory) notify to #alerts channel on slack.

## A you change server configuration?
- Use [Ansible is Simple IT Automation](http://www.ansible.com/home)
- Change Steps is below

	1. Edit ansible task in viral-giveaway-vagrant
	2. Reflect config to local vagrant
		
		$ ANSIBLE_TAGS=[tags] vagrant provision
		
	3. Reflect remote servers
	
 		$ ansible-playbook -s -i [staging, production] -t [tags] site.yml
 		
## Regulary Tasks

### Add past winners

- Edit `app/assets/javascripts/angular/web/controllers/web_controller.js#showWinnerDialog`

### Add fixed position dialog

- Edit `app/assets/javascripts/angular/web/controllers/web_controller.js#isDialogOpen`

## Tips

### App path on Staging, Production

- Path is `/home/ubuntu/app/viral-giveaway-server/current`

### OAuth redirect failed in local

- It's not problem actual successful. So why failed? Because not appended port when Rails redirect_to so please append port to browser url manually.