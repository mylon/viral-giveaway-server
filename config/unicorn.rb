case ENV['RAILS_ENV']
when 'production'
  worker_processes 4
when 'test'
  worker_processes 4
else
  worker_processes 4
end
  

# This loads the application in the master process before forking
# worker processes
# Read more about it here:
# http://unicorn.bogomips.org/Unicorn/Configurator.html
preload_app true

timeout 9999

# This is where we specify the socket.
# We will point the upstream Nginx module to this socket later on
listen ENV['RAILS_APP_SOCK'], :backlog => 64

pid ENV['RAILS_APP_PID']

# Set the path of the log files inside the log folder of the testapp
stderr_path "log/unicorn.err.log"
stdout_path "log/unicorn.out.log"

before_fork do |server, worker|
  defined?(ActiveRecord::Base) and ActiveRecord::Base.connection.disconnect!

  old_pid = "#{server.config[:pid]}.oldbin"
    if old_pid != server.pid
      begin
        sig = (worker.nr + 1) >= server.worker_processes ? :QUIT : :TTOU
        Process.kill(sig, File.read(old_pid).to_i)
      rescue Errno::ENOENT, Errno::ESRCH
      end
    end

    sleep 1
  end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and ActiveRecord::Base.establish_connection
end
