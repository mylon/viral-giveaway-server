Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  
  config.logger = Logger.new("log/#{Rails.env}.log", 10, 100 * 1024 * 1024)
  config.task_logger = Logger.new(STDOUT)

  config.api_endpoint = 'http://127.0.0.1:8081'

  config.app_driver_params = [
    { :adfit_platform => 1, :site_id => "2502", :media_id => "439", :key => "58cd53e897dbea12a600b1fbff5e7e80", :platform => 3 }, # android 
  ]

  config.fyber_security_tokens = [
    '', #android
  ] 

  config.super_rewards_secret_keys = [
    '', #android
  ] 

  config.supersonicads_private_keys = [
    '0823f3', #android
  ] 

  config.woobi_secret_keys = [
    '', #android
  ]

  config.personaly_apps = [
    {:app_hash => '', :app_secret => ''},
  ] 

  config.aarki_secret_keys = [
    '', #android
  ]

  config.trialpay_secret_keys = [
    '', #android
  ]

  config.taptica_params = [
    { :adfit_platform => 1, :token => "%3d%3d", :platforms => %w(Android), :countries => %w(TH), :min_payout => '', :categories => %w(), :version => '1' },
  ]

  config.adatha_params = [
    { :adfit_platform => 1, :aff_id => '1306', :api_key => "679aa25f0ad667bc6edea3d50f6eb731cea258850eb82ca21e1cdfa0b8330a77", :network_id => 'dotcominfoway' },
  ]

  config.avazu_params = [
    { :adfit_platform => 1, :username => 'ADFIT', :password => '1ftzZApm', :traffic_souce_id => '15551', :device_type => 'Mobile' },
  ]

  config.mobvista_params = [
    { :adfit_platform => 1, :apikey => 'cdb1aae498825176cebff0aeb0eb7a61' },
  ]

  config.fluent_host = 'localhost'
  config.fluent_port = 24224

  config.google_api_key = 'AIzaSyDirnT7cUXTva-A1QYqOFOFPno13NGbJlU';
  config.google_billing_public_key = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1Nicf+wu22Dy+MIvpIc19ee6eNr7cnml4QbSw+qOLawWhaHtY432g4d0WgPtx7SL6YBYzjalJLCQ+khzP8yVZiQx6SbE9DXF6+OMk38s/6eEfIosEyvgF+FaLPOTvF2EeFA2T/6CA98jlODSXNDyQq0IMFRDNyWuy5UiyIJTCDeVeGxaoPR6Vr2GxDrdi6/QDUgkQD/aSMCyWDoE7E1RW2TsHMeLIqgmoDohsQpUOvgtrkQxY+lGmuTGZjflsaS3CokfnfKUh8mA9LMlqCu9DEynLxgz4G4Sau22oUgaEFkoILzk4NVSCBTRxo4jAOwu/12+ypB3sLY4zk4fHsR1oQIDAQAB';

  config.static_host = 'https://s3-ap-southeast-1.amazonaws.com/dev-static.viral-giveaway.com'

  config.aws_access_key_id = 'AKIAJ7YGNWKVXXIWLBCA'
  config.aws_secret_access_key = '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'
  config.emr_region = 'ap-southeast-1'
  config.emr_script_bucket = "dev-misc.viral-giveaway.com"
  config.emr_log_bucket = "logs.viral-giveaway.com"
  config.emr_notify_sns = "arn:aws:sns:ap-southeast-1:302209448085:VG-EMR-DEV"
  config.emr_notify_sns_region = "ap-southeast-1"
  config.emr_ec2_key_name = "viral-giveaway"
  config.emr_sqs_queue_url = 'https://sqs.ap-southeast-1.amazonaws.com/302209448085/vg-emr-dev'
  config.ses_region = 'us-east-1'
  config.sqs_region = 'ap-southeast-1'
  config.sendmail_sqs_queue_url = 'https://sqs.ap-southeast-1.amazonaws.com/302209448085/vg-mail-dev'
  config.sns_topic_arn = 'arn:aws:sns:ap-southeast-1:302209448085:VG-MAIL-DEV'
  config.s3_region = 'ap-southeast-1'
  config.s3_static_bucket = 'dev-static.viral-giveaway.com'

  config.ses_noreply_source = "no-reply@viral-giveaway.com"
  config.ses_noreply_name = "Viral Giveaway"
  config.ses_info_source = "info@viral-giveaway.com"
  config.ses_info_name = "Viral Giveaway"

  config.np_noreply_source = "no-reply@local-picks.com"
  config.np_noreply_name = "Local Picks"
  config.np_info_source = "info@local-picks.com"
  config.np_info_name =  "Local Picks"

  config.geocoder_redis_host = 'localhost'
  config.geocoder_redis_port = 6379
  config.geocoder_redis_db   = 0
  config.geocoder_redis_ttl  = 60

  config.session_servers = 'redis://localhost:6379/1'
  config.session_expire_in = 1.day
  config.session_expire_after = 1.day

  config.facebook_app_id             = "941211849243700"
  config.facebook_app_secret         = "01f1b5ba8a9689fd551dbe162d85269b"
  config.twitter_consumer_key        = 'MA2hqlp6aoq2li83WKF4cmXBf'
  config.twitter_consumer_secret     = 'D4YKtytz0GtiF7wBmwvFVyGrbRNGgZyIg8Gh0pVdtHVLm9B3pK'
  config.google_oauth2_client_id     = '125285589119-s4stodqr0m8suuc4piej7575j2um4fiq.apps.googleusercontent.com'
  config.google_oauth2_client_secret = 'MSypNsuIyLKw6hWco7F1F1fO'

  config.newspick_bkk_jp_facebook_app_id = '1700082590222462'
  config.newspick_bkk_jp_facebook_app_secret = '974a668c49c9660b3cd96962de7565b7'
  config.newspick_bkk_jak_facebook_app_id = '1211014965590000'
  config.newspick_bkk_jak_facebook_app_secret = 'f58478b5a49e079195c16f2646f3ed62'  
  config.newspick_bkk_nyk_facebook_app_id = '1586489505013912'
  config.newspick_bkk_nyk_facebook_app_secret = '8c3110f3d7aac92318b04d2d35660fa1'  

  config.newspick_bkk_jp_twitter_consumer_key = 'DCiNrjN2XfuQUeePOigmIV8hv'
  config.newspick_bkk_jp_twitter_consumer_secret = 'Dzdr583jUhocI9sZYSLVugBl80uE5COImcoTi9j7ajJQnQ7xEH' 
  config.newspick_jak_jp_twitter_consumer_key = 'Xufj3xXGEsxgFjA60xiImwzev'
  config.newspick_jak_jp_twitter_consumer_secret = 'GysZrxvhKzpknTsavzLolXvhJ3t6p2qRdoYm0PtcQ0WmqCg6n5' 
  config.newspick_nyk_jp_twitter_consumer_key = 'bymMKLFrBu8AVyvSFzLYdxsrI'
  config.newspick_nyk_jp_twitter_consumer_secret = 'VNUvoVBzKjbm1W37jb82BnuqnZIWtfa67AQ87BQfDWgrzVdXJV' 

  config.nightlife_facebook_app_id = '739891342809062'
  config.nightlife_facebook_app_secret = 'fff51d97a9a283264f56c05f658cf8b1' 
  config.nightlife_facebook_access_token = '739891342809062|UNuZKBCGf_IAKt0Pe-MvP6nxBMI'

  config.verify_host = 'http://localhost:8082'
  config.rp_endpoint = 'http://localhost:8083'

  config.send_mail_worker_log = Rails.root.join('log', 'sendmail_worker.log').to_s
  config.send_mail_worker_log_level = 'debug'
  config.send_mail_worker_log_rotate_age = 5 
  config.send_mail_worker_log_rotate_size = 1 * 1024 * 1024 
  
  config.sidekiq_queue_url = 'redis://localhost:6379/2'

  config.emr_job_worker_log = Rails.root.join('log', 'emr_job_worker.log').to_s
  config.emr_job_worker_log_level = 'debug'
  config.emr_job_worker_log_rotate_age = 5 
  config.emr_job_worker_log_rotate_size = 1 * 1024 * 1024 

  config.emr_batch_option = {
    :aggregate_dau => {
      :master_instance_type => 'm1.medium',
      :slave_instance_type => 'm1.medium',
      :instance_count => 1,
    },
    :aggregate_mau => {
      :master_instance_type => 'm1.medium',
      :slave_instance_type => 'm1.medium',
      :instance_count => 1,
    },
    :aggregate_app_log_per_sources => {
      :master_instance_type => 'm1.medium',
      :slave_instance_type => 'm1.medium',
      :instance_count => 1,
    },
  }

  config.dream_photo_s3_bucket = "dev-dreamphoto.viral-giveaway.com"
  config.dream_photo_s3_prefix = ""

  config.nightlife_s3_bucket = "dev.nightlife8hrs.com"
  config.nightlife_s3_prefix = ""

  config.db_host_nightlife = "development"
end
