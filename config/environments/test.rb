Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = false

  # Configure static asset server for tests with Cache-Control for performance.
  config.serve_static_files = true 
  config.static_cache_control = 'public, max-age=3600'

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr
  config.active_support.test_order = :sorted

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  config.logger = Logger.new("log/#{Rails.env}.log", 10, 100 * 1024 * 1024)
  config.logger.level = Logger::DEBUG
  config.task_logger = Logger.new("log/task.#{Rails.env}.log", 10, 100 * 1024 * 1024)
  config.task_logger.level = Logger::DEBUG

  config.api_endpoint = 'https://stage-api.viral-giveaway.com'
  
  config.app_driver_params = [
    { :adfit_platform => 1, :site_id => "2502", :media_id => "439", :key => "58cd53e897dbea12a600b1fbff5e7e80", :platform => 3 }, # android 
  ]

  config.fyber_security_tokens = [
    '', #android
  ] 

  config.super_rewards_secret_keys = [
    '', #android
  ] 

  config.supersonicads_private_keys = [
    '0823f3', #android
  ] 

  config.woobi_secret_keys = [
    '', #android
  ]

  config.personaly_apps = [
    {:app_hash => '', :app_secret => ''},
  ] 

  config.aarki_secret_keys = [
    '', #android
  ]

  config.trialpay_secret_keys = [
    '', #android
  ]

  config.taptica_params = [
    { :adfit_platform => 1, :token => "%3d%3d", :platforms => %w(Android), :countries => %w(TH), :min_payout => '', :categories => %w(), :version => '1' },
  ]

  config.adatha_params = [
    { :adfit_platform => 1, :aff_id => '1306', :api_key => "679aa25f0ad667bc6edea3d50f6eb731cea258850eb82ca21e1cdfa0b8330a77", :network_id => 'dotcominfoway' },
  ]

  config.fluent_host = 'localhost'
  config.fluent_port = 24224

  config.google_api_key = 'AIzaSyDirnT7cUXTva-A1QYqOFOFPno13NGbJlU';
  config.google_billing_public_key = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1Nicf+wu22Dy+MIvpIc19ee6eNr7cnml4QbSw+qOLawWhaHtY432g4d0WgPtx7SL6YBYzjalJLCQ+khzP8yVZiQx6SbE9DXF6+OMk38s/6eEfIosEyvgF+FaLPOTvF2EeFA2T/6CA98jlODSXNDyQq0IMFRDNyWuy5UiyIJTCDeVeGxaoPR6Vr2GxDrdi6/QDUgkQD/aSMCyWDoE7E1RW2TsHMeLIqgmoDohsQpUOvgtrkQxY+lGmuTGZjflsaS3CokfnfKUh8mA9LMlqCu9DEynLxgz4G4Sau22oUgaEFkoILzk4NVSCBTRxo4jAOwu/12+ypB3sLY4zk4fHsR1oQIDAQAB';

  config.static_host = 'https://s3-ap-southeast-1.amazonaws.com/stage-static.viral-giveaway.com'

  config.aws_access_key_id = 'AKIAJ7YGNWKVXXIWLBCA'
  config.aws_secret_access_key = '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'
  config.ses_region = 'us-east-1'
  config.sqs_region = 'ap-southeast-1'
  config.sqs_queue_url = 'https://sqs.ap-southeast-1.amazonaws.com/302209448085/vg-mail-test'
  config.sns_topic_arn = 'arn:aws:sns:ap-southeast-1:302209448085:VG-MAIL-TEST'
  config.s3_region = 'ap-southeast-1'
  config.s3_static_bucket = 'stage-static.viral-giveaway.com'

  config.ses_noreply_source = "no-reply@viral-giveaway.com"
  config.ses_noreply_name = "Viral Giveaway"
  config.ses_info_source = "info@viral-giveaway.com"
  config.ses_info_name = "Viral Giveaway"

  config.geocoder_redis_host = 'localhost'
  config.geocoder_redis_port = 6379
  config.geocoder_redis_db   = 0
  config.geocoder_redis_ttl  = 60

  config.session_servers = 'redis://localhost:6379/1'
  config.session_expire_in = 1.day
  config.session_expire_after = 1.day

  config.facebook_app_id             = "650776348355589"
  config.facebook_app_secret         = "12c98689a31df9b21b73ad62c5ae9cc7"
  config.twitter_consumer_key        = 'MA2hqlp6aoq2li83WKF4cmXBf'
  config.twitter_consumer_secret     = 'D4YKtytz0GtiF7wBmwvFVyGrbRNGgZyIg8Gh0pVdtHVLm9B3pK'
  config.google_oauth2_client_id     = '125285589119-s4stodqr0m8suuc4piej7575j2um4fiq.apps.googleusercontent.com'
  config.google_oauth2_client_secret = 'MSypNsuIyLKw6hWco7F1F1fO'

  config.verify_host = 'https://stage.viral-giveaway.com'

  config.send_mail_worker_log = Rails.root.join('log', 'sendmail_worker.log').to_s
  config.send_mail_worker_log_level = 'debug'
  config.send_mail_worker_log_rotate_age = 5 
  config.send_mail_worker_log_rotate_size = 1 * 1024 * 1024 
  
  config.sidekiq_queue_url = 'redis://localhost:6379/2'

  config.dream_photo_s3_bucket = "stage-dreamphoto.viral-giveaway.com"
  config.dream_photo_s3_prefix = ""

end
