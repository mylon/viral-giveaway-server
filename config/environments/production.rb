Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like nginx, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = Uglifier.new(mangle: false)
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false 

  # Generate digests for assets URLs.
  config.assets.digest = true 

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Set to :debug to see everything in the log.
  config.log_level = :info

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = "http://assets.example.com"

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Disable automatic flushing of the log to improve performance.
  # config.autoflush_log = false

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  config.logger = Logger.new("log/#{Rails.env}.log", 10, 100 * 1024 * 1024)
  config.logger.level = Logger::WARN
  config.task_logger = Logger.new("log/task.#{Rails.env}.log", 10, 100 * 1024 * 1024)
  config.task_logger.level = Logger::WARN

  #config.api_endpoint = 'https://api.viral-giveaway.com'
  config.api_endpoint = 'http://vg-production-api-1349383635.ap-southeast-1.elb.amazonaws.com'

  config.app_driver_params = [
    { :adfit_platform => 1, :site_id => "2502", :media_id => "438", :key => "58cd53e897dbea12a600b1fbff5e7e80", :platform => 3 }, # android 
  ]

  config.fyber_security_tokens = [
    '', #android
  ] 

  config.super_rewards_secret_keys = [
    '', #android
  ] 

  config.supersonicads_private_keys = [
    '91cc80', #android
  ] 

  config.woobi_secret_keys = [
    '', #android
  ]

  config.personaly_apps = [
    {:app_hash => '', :app_secret => ''},
  ] 

  config.aarki_secret_keys = [
    '', #android
  ]

  config.trialpay_secret_keys = [
    '', #android
  ]

  config.taptica_params = [
    { :adfit_platform => 1, :token => "%3d%3d", :platforms => %w(Android), :countries => %w(TH), :min_payout => '', :categories => %w(), :version => '1' },
  ]

  config.adatha_params = [
    { :adfit_platform => 1, :aff_id => '1306', :api_key => "679aa25f0ad667bc6edea3d50f6eb731cea258850eb82ca21e1cdfa0b8330a77", :network_id => 'dotcominfoway' },
  ]

  config.avazu_params = [
    { :adfit_platform => 1, :username => 'ADFIT', :password => '1ftzZApm', :traffic_souce_id => '15551', :device_type => 'Mobile' },
  ]

  config.mobvista_params = [
    { :adfit_platform => 1, :apikey => 'cdb1aae498825176cebff0aeb0eb7a61' },
  ]

  config.fluent_host = 'localhost'
  config.fluent_port = 24224

  config.google_api_key = 'AIzaSyDJzWhlG2fWAKT69xaFe_9eVCjOahyhq6E';
  config.google_billing_public_key = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1Nicf+wu22Dy+MIvpIc19ee6eNr7cnml4QbSw+qOLawWhaHtY432g4d0WgPtx7SL6YBYzjalJLCQ+khzP8yVZiQx6SbE9DXF6+OMk38s/6eEfIosEyvgF+FaLPOTvF2EeFA2T/6CA98jlODSXNDyQq0IMFRDNyWuy5UiyIJTCDeVeGxaoPR6Vr2GxDrdi6/QDUgkQD/aSMCyWDoE7E1RW2TsHMeLIqgmoDohsQpUOvgtrkQxY+lGmuTGZjflsaS3CokfnfKUh8mA9LMlqCu9DEynLxgz4G4Sau22oUgaEFkoILzk4NVSCBTRxo4jAOwu/12+ypB3sLY4zk4fHsR1oQIDAQAB';

  config.static_host = 'https://s3-ap-southeast-1.amazonaws.com/static.viral-giveaway.com'

  config.aws_access_key_id = 'AKIAJ7YGNWKVXXIWLBCA'
  config.aws_secret_access_key = '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'
  config.emr_region = 'ap-southeast-1'
  config.emr_script_bucket = "misc.viral-giveaway.com"
  config.emr_log_bucket = "logs.viral-giveaway.com"
  config.emr_notify_sns = "arn:aws:sns:ap-southeast-1:302209448085:VG-EMR"
  config.emr_notify_sns_region = "ap-southeast-1"
  config.emr_ec2_key_name = "viral-giveaway"
  config.emr_sqs_queue_url = 'https://sqs.ap-southeast-1.amazonaws.com/302209448085/vg-emr'
  config.ses_region = 'us-east-1'
  config.sqs_region = 'ap-southeast-1'
  config.sendmail_sqs_queue_url = 'https://sqs.ap-southeast-1.amazonaws.com/302209448085/vg-mail'
  config.sns_topic_arn = 'arn:aws:sns:ap-southeast-1:302209448085:VG-MAIL'
  config.s3_region = 'ap-southeast-1'
  config.s3_static_bucket = 'static.viral-giveaway.com'

  config.ses_noreply_source = "no-reply@viral-giveaway.com"
  config.ses_noreply_name = "Viral Giveaway"
  config.ses_info_source = "info@viral-giveaway.com"
  config.ses_info_name = "Viral Giveaway"

  config.np_noreply_source = "no-reply@local-picks.com"
  config.np_noreply_name = "Local Picks"
  config.np_info_source = "info@local-picks.com"
  config.np_info_name =  "Local Picks"

  config.geocoder_redis_host = 'vg-production-redis.whpeop.0001.apse1.cache.amazonaws.com'
  config.geocoder_redis_port = 6379
  config.geocoder_redis_db   = 0
  config.geocoder_redis_ttl  = 86400
  
  config.session_servers = 'redis://vg-production-redis.whpeop.0001.apse1.cache.amazonaws.com:6379/1'
  config.session_expire_in = 30.day
  config.session_expire_after = 30.day

  config.facebook_app_id             = "941211849243700"
  config.facebook_app_secret         = "01f1b5ba8a9689fd551dbe162d85269b"
  config.twitter_consumer_key        = 'WoTXM1vXArkqfgBfHFJI1wgtt'
  config.twitter_consumer_secret     = 'uwooGJ3Nym7lXgaFenk9MZGrNUCP1T9TKsQe9gjEYrGJ7rbrRW'
  config.google_oauth2_client_id     = '664052411814-o3hve4qprv1dllc1n53cvfjg9d6jv61b.apps.googleusercontent.com'
  config.google_oauth2_client_secret = '7gi3aNDX1YtidYV6VmzkhHA_'

  config.newspick_bkk_jp_facebook_app_id = '1700082590222462'
  config.newspick_bkk_jp_facebook_app_secret = '974a668c49c9660b3cd96962de7565b7' 
  config.newspick_bkk_jak_facebook_app_id = '1211014965590000'
  config.newspick_bkk_jak_facebook_app_secret = 'f58478b5a49e079195c16f2646f3ed62' 
  config.newspick_bkk_jp_twitter_consumer_key = 'DCiNrjN2XfuQUeePOigmIV8hv'
  config.newspick_bkk_jp_twitter_consumer_secret = 'Dzdr583jUhocI9sZYSLVugBl80uE5COImcoTi9j7ajJQnQ7xEH' 
  config.newspick_jak_jp_twitter_consumer_key = 'Xufj3xXGEsxgFjA60xiImwzev'
  config.newspick_jak_jp_twitter_consumer_secret = 'GysZrxvhKzpknTsavzLolXvhJ3t6p2qRdoYm0PtcQ0WmqCg6n5' 

  config.nightlife_facebook_app_id = '739891342809062'
  config.nightlife_facebook_app_secret = 'fff51d97a9a283264f56c05f658cf8b1' 
 
  #config.verify_host = 'https://viral-giveaway.com'
  config.verify_host = 'http://vg-production-admin-775206068.ap-southeast-1.elb.amazonaws.com'
  config.rp_endpoint = 'https://rp.viral-giveaway.com'

  config.send_mail_worker_log = Rails.root.join('log', 'sendmail_worker.log').to_s
  config.send_mail_worker_log_level = 'info'
  config.send_mail_worker_log_rotate_age = 10 
  config.send_mail_worker_log_rotate_size = 100 * 1024 * 1024 
  
  config.sidekiq_queue_url = 'redis://vg-production-redis.whpeop.0001.apse1.cache.amazonaws.com:6379/2'

  config.emr_job_worker_log = Rails.root.join('log', 'emr_job_worker.log').to_s
  config.emr_job_worker_log_level = 'info'
  config.emr_job_worker_log_rotate_age = 10 
  config.emr_job_worker_log_rotate_size = 100 * 1024 * 1024 

  config.emr_batch_option = {
    :aggregate_dau => {
      :master_instance_type => 'm1.medium',
      :slave_instance_type => 'm1.medium',
      :instance_count => 1,
    },
    :aggregate_mau => {
      :master_instance_type => 'm1.medium',
      :slave_instance_type => 'm1.medium',
      :instance_count => 1,
    },
    :aggregate_app_log_per_sources => {
      :master_instance_type => 'm1.medium',
      :slave_instance_type => 'm1.medium',
      :instance_count => 1,
    },
  }
  
  config.dream_photo_s3_bucket = "dreamphoto.viral-giveaway.com"
  config.dream_photo_s3_prefix = ""

  config.nightlife_s3_bucket = "prod.nightlife8hrs.com"
  config.nightlife_s3_prefix = ""

  config.db_host_nightlife = "production"
end
