require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module VG
  class Application < Rails::Application
    config.autoload_paths += %W(#{config.root}/lib)
    config.autoload_paths += Dir["#{config.root}/lib/**/"]
    config.autoload_paths += Dir["#{config.root}/app/workers/*.rb"]
    config.assets.precompile += %w( web.js web.css admin.js admin.css )
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}').to_s]

#    config.active_job.queue_adapter = :sidekiq

    config.i18n.default_locale = :en

    config.version = '1.0.0'
    config.versions = config.version.split('.').map{|v| v.to_i}
    config.vg_app_id = 1

    config.exchange_currencies = %w{INR}

    config.initial_user_id = 131467
    config.token_length = 64
    config.verification_code_length = 64
    config.app_verification_code_length = 6
    config.password_reset_length = 12
    config.information_load_num = 100
    config.ticket_load_num = 100
    config.porshe_campaign_id = 1
    config.friend_cpi_point_num = 3
    config.facebook_share_point_num = 5 
    config.facebook_like_point_num = 5 
    config.twitter_share_point_num = 5 
    config.twitter_follow_point_num = 5 
    config.invite_point_num = 2 
    config.invite_limit = 20 
    config.gcm_per_limit = 1000
    config.user_age_threshold = 18
    config.geocoder_retry_num = 5
    config.ticket_digit_num = 10
    config.mail_promotion_offer_num = 5

    config.facebook_page_id = 823735791015041
    config.twitter_account_id = 'viral_giveaway'

    config.special_payout_week_days = {
#      '4' => true, # thursday
    }
    config.special_payout_rate = 3

    config.name_length_max = 64 
    config.user_id_length_min = 6
    config.username_length_max = 64
    config.email_length_max = 256
    config.email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    config.birthdate_format = '%m/%d/%Y'
    config.password_length_min = 8
    config.password_length_max = 32

    config.facebook_birthdate_format = '%m/%d/%Y'

    config.offer_search_limit = 1000 
   
    config.gender_map = {
      :secret => 1,
      :male   => 2,
      :female => 3,
    }
    config.gender_symbol_map = Hash[ config.gender_map.map{|k,v| [v,k] } ]
    
    config.billing_provider_map = {
      :google => 1,
      :apple => 2,
    }

    config.billing_status_map = {
      :none => 0,
      :pending => 1,
      :complete => 2,
    }

    config.offer_platform_map = {
      :android => 1,
      :ios     => 2,
      :all     => 999,
    }

    config.offer_publish_status_map = {
      :draft     => 1,
      :publish   => 2,
      :unpublish => 3,
      :category_schedule => 4,
    }

    config.offer_type_map = {
      :information   =>  1,
      :cpi           =>  2,
      :video         =>  4,
      #:nativex       =>  5,
      #:super_rewards =>  6,
      #:supersonicads =>  7,
      #:woobi         =>  8,
      #:personaly     => 10,
      #:adcolony      => 11,
      #:aarki         => 12,
      #:trialpay      => 13,
      #:taptica       => 14,
      :adatha           => 15,
      #:fyber         => 16,
      :avazu          => 17,
      :mobvista       => 18,
    }

    config.offer_provider_map = {
      :viral_giveaway =>  1,
      :app_driver    =>  2,
      #:fyber         =>  3,
      #:nativex       =>  4,
      #:super_rewards =>  5,
      #:tyroo         =>  6,
      #:supersonicads =>  7,
      #:woobi         =>  8,
      #:personaly     => 10,
      #:adcolony      => 11,
      #:aarki         => 12,
      #:trialpay      => 13,
      #:taptica       => 14,
      :adatha           => 15,
      :avazu          => 16,
      :mobvista       => 17,
    }
    config.offer_provider_symbol_map = Hash[ config.offer_provider_map.map{|k,v| [v,k] } ]

    config.offer_types_per_category = {
      :wall        => [
        :nativex,
        :super_rewards,
        :supersonicads,
        :woobi,
        :personaly,
        :adcolony,
        :aarki,
        :trialpay,
        :fyber,
      ],
      :application => [ :cpi ],
      :video       => [],
      :information => [ :information, :video ],
    } 

    config.tracking_type_map = {
      :app_driver    => 1,
      #:fyber         => 2,
      #:nativex       => 3,
      #:super_rewards => 4,
      #:tyroo         => 5,
      #:supersonicads => 6,
      #:woobi         => 7,
      #:personaly     => 9,
      #:adcolony      => 10,
      #:aarki         => 11,
      #:trialpay      => 12,
      #:taptica       => 13,
      :adatha           => 14,
      :friend_cpi    => 15,
      :daily_bonus    => 16,
      :facebook_share => 17,
      :twitter_share  => 18,
      :show_invitation_dialog => 19,
      :avazu          => 20,
      :mobvista       => 21,
      :facebook_like => 22,
      :twitter_follow => 23,
    }
    config.tracking_type_symbol_map = Hash[ config.tracking_type_map.map{|k,v| [v,k] } ]

    config.tracking_status_map = {
      :conversioned => 1,
      :skip         => 2,
    }

    config.notification_type_map = {
      :move_campaign_list   => 1,
      :move_offer_list      => 2,
      :move_ticket_list     => 3,
      :move_setting         => 4,
      :move_information     => 5,
    }

    config.point_item_type_map = {
      :cpi            => 1,
      :invitation     => 2,
      :friend_cpi     => 3,
      :facebook_share => 4,
      :daily_bonus    => 5,
      :twitter_share  => 6,
      :ticket         => 7,
      :grant_operator => 8,
      :facebook_like  => 9,
      :twitter_follow => 10,
      :dream_photo    => 11,
      :fortune_telling => 12,
      :line_sticker => 13,
    }
    config.point_item_type_symbol_map = Hash[ config.point_item_type_map.map{|k,v| [v,k] } ]

    config.message_type_map = {
      :default        => 1, 
      :cpi            => 2, 
      :friend_cpi     => 3, 
      :facebook_share => 4, 
      :twitter_share  => 5, 
      :daily_bonus    => 6, 
      :invitation     => 7, 
      :facebook_like  => 8,
      :twitter_follow => 9,
    }
    config.message_type_symbol_map = Hash[ config.message_type_map.map{|k,v| [v,k] } ]

    config.app_driver_country_map = {
      '1' => 'tw',
      '2' => 'jp',
      '3' => 'kp',
      '4' => 'cn',
      '5' => 'th',
      '6' => 'id',
      '7' => 'sg',
      '8' => 'my',
      '9' => 'vn',
      '10' => 'ph',
      '11' => 'hk',
      '999' => 'all',
    }

    config.question_map = {
      '1' => 'Viral giveaway is simply too addictive to me.',
      '2' => 'I am no longer interested in winning a giveaway.',
      '3' => 'Eaming points and tickets are too difficult.',
      '4' => 'I am sick of receiving promotional email.',
      '5' => 'Winning prize is not attractive enough.',
      '6' => 'Not enough apps to install to earn points.',
      '7' => 'How to join this giveaway is not clear enough.',
      '8' => "I don't like only other people winning the prize.",
      '9' => 'Design is so discouraging to continue.',
    }

    config.mail_segment_map = {
      :all                                 => 1,
      :only_register                       => 2,
      :only_action                         => 3,
      :done_cpi_have_promotional_offer     => 4,
      :done_cpi_not_have_promotional_offer => 5,
    }

    config.mail_segment_label_map = {
      :all                                 => "ALL",
      :only_register                       => "Only Register",
      :only_action                         => "Only Action",
      :done_cpi_have_promotional_offer     => "Done CPI and have Promotional Offer",
      :done_cpi_not_have_promotional_offer => "Don't have Promotional Offer",
    }

    config.log_type_label_map = {
      :dau => 'DAU',
      :mau => 'MAU',
      :app_log_per_sources => 'APP Log per Sources',
    }

    config.dream_photo_photo_type_map = {
      :face => 1,
      :full_body => 2,
    }

    config.line_sticker_status_map = {
      :pending => 1,
      :sent => 2,
      :cancel => 3,
    }
    config.line_sticker_status_symbol_map = Hash[ config.line_sticker_status_map.map{|k,v| [v,k] } ]

    config.fortune_telling_csv_column_num = 32 
    config.fortune_telling_csv_date_format = "%Y-%m-%d"
    config.fortune_telling_csv_columns = %w(
      date 
      constellation 
      love_Text 
      love_Point_100 
      love_Point_5A 
      love_Point_5B 
      love_Point_10 
      money_Text 
      money_Point_100 
      money_Point_5A 
      money_Point_5B 
      money_Point_10 
      work_Text 
      work_Point_100 
      work_Point_5A 
      work_Point_5B 
      work_Point_10 
      total_Text 
      total_Point_100 
      lucky_direction 
      lucky_number 
      lucky_color 
      lucky_food 
      lucky_fashion 
      lucky_item 
      lucky_person 
      lucky_place 
      today_year 
      today_mon 
      today_mday 
      today_wday 
      today_weekday
    )

    config.fortune_telling_constellation_map = {
      aries: 1,
      taurus: 2,
      gemini: 3,
      cancer: 4,
      leo: 5,
      virgo: 6,
      libra: 7,
      scorpio: 8,
      sagittarius: 9,
      capricorn: 10,
      aquarius: 11,
      pisces: 12,
    } 
    #see http://www.geocities.jp/koci_sax/holo.htm
    config.fortune_telling_constellation_date_map = {
      aries: [{m:3,d:21}, {m:4,d:20}],
      taurus: [{m:4,d:21}, {m:5,d:20}],
      gemini: [{m:5,d:21}, {m:6,d:21}],
      cancer: [{m:6,d:22}, {m:7,d:23}],
      leo: [{m:7,d:24}, {m:8,d:23}],
      virgo: [{m:8,d:24}, {m:9,d:23}],
      libra: [{m:9,d:24}, {m:10,d:23}],
      scorpio: [{m:10,d:24}, {m:11,d:22}],
      sagittarius: [{m:11,d:23}, {m:12,d:22}],
      capricorn: [[{m:12,d:23}, {m:12,d:31}],[{m:1,d:1}, {m:1,d:20}]],
      aquarius: [{m:1,d:21}, {m:2,d:19}],
      pisces: [{m:2,d:20}, {m:3,d:20}],
    }
    config.fortune_telling_constellations = %w(牡羊座 牡牛座 双子座 蟹座 獅子座 乙女星座 天秤座 蠍座 射手座 山羊座 水瓶座 魚座)

    config.newspick_region = {
      :th => 1,
      :jp => 2,
      :en => 3,
    }
    config.newspick_region_map = Hash[ config.newspick_region.map{|k,v| [v,k] } ]

    config.newspick_region_txt = {
      :th => "th",
      :jp => "jp",
      :en => "en",
    }
     config.newspick_region_txt_map = Hash[ config.newspick_region_txt.map{|k,v| [v,k] } ]

    config.localpicks_region_menu = {
      :en => "English",
      :jp => "Japanese",
      :th => "Thai",
    }
    config.localpicks_region_menu_map = Hash[ config.localpicks_region_menu.map{|k,v| [v,k] } ]

    config.newspick_city = {
      :bangkok => 1,
      :hongkong  => 2,     
      :taipei => 3,
      :tokyo => 4,
      :singapore  => 5,     
      :jakarta => 6,     
      :manila => 7,      
      :seoul => 8,      
      :kaulalumpur => 9,  
      :hochiminhcity => 10,       
      :sanfrancisco => 11,
      :hawaii => 12,
      :paris => 13,
      :brisbane => 14,
      :melbourne => 15,
      :saopaulo => 16,
      :vancouver => 17,
      :sydney => 18,
      :newyork => 19,
      :hanoi => 20,
      #:shagnhai => 21,
      :losangeles => 22,
      :london => 23,
      :mumbai => 24,
      :frankfurt => 25,
      #:washington => 26,
      #:canberra => 27,
      :barcelona => 28,
      :rome => 29, 
      :berlin => 30,
      :yangon => 31,
      :vientiane => 32,
      :phnom_penh => 33,
      :kyoto => 34, 
      :moscow => 35,
      :new_delhi => 36,
      :mexico_city => 37,
      :osaka => 38,
      :sapporo => 39,
      :sendai => 40,
      :saitama => 41,
      :kawasaki => 42,
      :yokohama => 43,
      :nagoya => 44,
      :kobe => 45,
      :hiroshima => 46,
      :fukuoka => 47,
      :bali => 48,
      :chiang_mai => 49,
      :phuket => 50,
      :bengaluru => 51,
      :chennai => 52,
      :agra => 53,
      :kolkata => 54,
      :hyderabad => 55,
      :dubai => 56,
      :johannesburg => 57,
      :cape_town => 58,
      :casablanca => 59,
      :montreal => 60,
      :toronto => 61,
      :guadalajara => 62,
      :athens => 63,
      :milan => 64,
      :vienna => 65,
      :madrid => 66,
      :amsterdam => 67,
      :prague => 68,
      :belgrade => 69,
      :munich => 70,
      :budapest => 71,
      :sofia => 72,
      :warsaw => 73,
      :zagreb => 74,
      :kiev => 75,
      :istanbul => 76,
      :sankt_petersburg => 77,
      :quezon => 78,
      :makati => 79,
      :da_nang => 80,
      :pattaya => 81,
      :jaipur => 82,
      :rio_de_janeiro => 83,
      :buenos_aires => 84,
      :chicago => 85,
      :las_vegas => 86,
      :miami => 87,
      :cancun => 88,
      :marrakesh => 89,
      :cairo => 90,
      :lagos => 91,
      :nairobi => 92,
      :abu_dhabi => 93,
      :manchester => 94,
      :stockholm => 95,
      :brussel => 96,
      :dublin => 97,
    }
    config.newspick_city_map = Hash[ config.newspick_city.map{|k,v| [v,k] } ]

    config.newspick_jpcity = {
      :bangkok => 1,
      :hongkong  => 2,     
      :taipei => 3,
      :tokyo => 4,
      :singapore  => 5,     
      :jakarta => 6,     
      :manila => 7,      
      :seoul => 8,      
      :kaulalumpur => 9,  
      :hochiminhcity => 10,       
      :sanfrancisco => 11,
      :hawaii => 12,
      :paris => 13,
      :brisbane => 14,
      :melbourne => 15,
      :saopaulo => 16,
      :vancouver => 17,
      :sydney => 18,
      :newyork => 19,
      :hanoi => 20,
      #:shagnhai => 21,
      :losangeles => 22,
      :london => 23,
      :mumbai => 24,
      :frankfurt => 25,
      #:washington => 26,
      #:canberra => 27,
      :barcelona => 28,
      :rome => 29, 
      :berlin => 30,
      :yangon => 31,
      :vientiane => 32,
      :phnom_penh => 33,
      :kyoto => 34,
      :moscow => 35,
      :new_delhi => 36,
      :mexico_city => 37,
      :osaka => 38,
      :sapporo => 39,
      :sendai => 40,
      :saitama => 41,
      :kawasaki => 42,
      :yokohama => 43,
      :nagoya => 44,
      :kobe => 45,
      :hiroshima => 46,
      :fukuoka => 47,
      :bali => 48,
      :chiang_mai => 49,
      :phuket => 50,
      :bengaluru => 51,
      :chennai => 52,
      :agra => 53,
      :kolkata => 54,
      :hyderabad => 55,
      :dubai => 56,
      :johannesburg => 57,
      :cape_town => 58,
      :casablanca => 59,
      :montreal => 60,
      :toronto => 61,
      :guadalajara => 62,
      :athens => 63,
      :milan => 64,
      :vienna => 65,
      :madrid => 66,
      :amsterdam => 67,
      :prague => 68,
      :belgrade => 69,
      :munich => 70,
      :budapest => 71,
      :sofia => 72,
      :warsaw => 73,
      :zagreb => 74,
      :kiev => 75,
      :istanbul => 76,
      :sankt_petersburg => 77,
      :quezon => 78,
      :makati => 79,
      :da_nang => 80,
      :pattaya => 81,
      :jaipur => 82,
      :rio_de_janeiro => 83,
      :buenos_aires => 84,
      :chicago => 85,
      :las_vegas => 86,
      :miami => 87,
      :cancun => 88,
      :marrakesh => 89,
      :cairo => 90,
      :lagos => 91,
      :nairobi => 92,
      :abu_dhabi => 93,
      :manchester => 94,
      :stockholm => 95,
      :brussel => 96,
      :dublin => 97,
    }

    config.newspick_jpcity_map = Hash[ config.newspick_jpcity.map{|k,v| [v,k] } ]

    config.localpick_city = {
      "bangkok" => 1,
      "hongkong"  => 2,     
      "taipei" => 3,
      "tokyo" => 4,
      "singapore"  => 5,     
      "jakarta" => 6,     
      "manila" => 7,      
      "seoul" => 8,      
      "kualalumpur" => 9,  
      "hochiminh city" => 10,       
      "san francisco" => 11,
      "hawaii" => 12,
      "paris" => 13,
      "brisbane" => 14,
      "melbourne" => 15,
      "sao paulo" => 16,
      "vancouver" => 17,
      "sydney" => 18,
      "newyork" => 19,
      "hanoi" => 20,
      #"shagnhai" => 21,
      "los angeles" => 22,
      "london" => 23,
      "mumbai" => 24,
      "frankfurt" => 25,
      #"washington" => 26,
      #"canberra" => 27,
      "barcelona" => 28,
      "rome" => 29, 
      "berlin" => 30,
      "yangon" => 31,
      "vientiane" => 32,
      "phnompenh" => 33,
      "kyoto" => 34, 
      "moscow" => 35,
      "newd elhi" => 36,
      "mexico city" => 37,
      "osaka" => 38,
      "sapporo" => 39,
      "sendai" => 40,
      "saitama" => 41,
      "kawasaki" => 42,
      "yokohama" => 43,
      "nagoya" => 44,
      "kobe" => 45,
      "hiroshima" => 46,
      "fukuoka" => 47,
      "bali" => 48,
      "chiang mai" => 49,
      "phuket" => 50,
      "bengaluru" => 51,
      "chennai" => 52,
      "agra" => 53,
      "kolkata" => 54,
      "hyderabad" => 55,
      "dubai" => 56,
      "johannesburg" => 57,
      "cape town" => 58,
      "casablanca" => 59,
      "montreal" => 60,
      "toronto" => 61,
      "guadalajara" => 62,
      "athens" => 63,
      "milan" => 64,
      "vienna" => 65,
      "madrid" => 66,
      "amsterdam" => 67,
      "prague" => 68,
      "belgrade" => 69,
      "munich" => 70,
      "budapest" => 71,
      "sofia" => 72,
      "warsaw" => 73,
      "zagreb" => 74,
      "kiev" => 75,
      "istanbul" => 76,
      "sankt peterburg" => 77,
      "quezon" => 78,
      "makati" => 79,
      "danang" => 80,
      "pattaya" => 81,
      "jaipur" => 82,
      "rio de janeiro" => 83,
      "buenos aires" => 84,
      "chicago" => 85,
      "lasvegas" => 86,
      "miami" => 87,
      "cancun" => 88,
      "marrakesh" => 89,
      "cairo" => 90,
      "lagos" => 91,
      "nairobi" => 92,
      "abudhabi" => 93,
      "manchester" => 94,
      "stockholm" => 95,
      "brussel" => 96,
      "dublin" => 97,
    }
    config.localpick_city_map = Hash[ config.localpick_city.map{|k,v| [v,k] } ]

    config.localpick_home_city_select = {
      "bangkok" => 1,
      "barcelona" => 28,
      "berlin" => 30,
      "hawaii" => 12,
      "hochiminh city" => 10,
      "hong kong" => 2,
      "jakarta" => 6,
      "kuala lumpur" => 9,
      "kyoto" => 34,
      "london" => 23,
      "los angeles" => 22,
      "manila" => 7,
      "mexico city" => 37,
      "mumbai" => 24,
      "new york" => 19,
      "paris" => 13,
      "rome" => 29,
      "san francisco" => 11,
      "sao paulo" => 16,
      "singapore" => 5,
      "sydney" => 18,
      "taipei" => 3,
      "tokyo" => 4,
      "vancouver" => 17,
      "bali" => 48,
      "dubai" => 56,
      "warsaw" => 73,
      "istanbul" => 76,
      "buenos aires" => 84,
      "marrakesh" => 89,
      "cairo" => 90,
      "amsterdam" => 67,
      "prague" => 68,
      "stockholm" => 95,
      "phuket" => 50
    }
    config.localpick_home_city_select_map = Hash[ config.localpick_home_city_select.map{|k,v| [v,k] } ]

    config.localpick_home_city_list = [
      {
        "city_id" => 1,
        "city_name" => "bangkok",
        "lat" => 13.7563,
        "lng" => 100.5018,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/bangkok_400x200px.jpg"
      },
      {
        "city_id" => 28,
        "city_name" => "barcelona",
        "lat" => 41.3851,
        "lng" => 2.1734,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/barcelonaa_400x200px.jpg"
      },
      {
        "city_id" => 30,
        "city_name" => "berlin",
        "lat" => 52.5200,
        "lng" => 13.4050,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/berlin_400x200px.jpg"
      },
      {
        "city_id" => 12,
        "city_name" => "hawaii",
        "lat" => 19.8968,
        "lng" => 155.5828,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/hawaii_400x200px.jpg"
      },
      {
        "city_id" => 10,
        "city_name" => "hochiminh city",
        "lat" => 10.8231,
        "lng" => 106.6297,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/hochiminh_400x200px.jpg"
      },
      {
        "city_id" => 2,
        "city_name" => "hong kong",
        "lat" => 22.3964,
        "lng" => 114.1095,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/hongkong_400x200px.jpg"
      },
      {
        "city_id" => 6,
        "city_name" => "jakarta",
        "lat" => 6.1745,
        "lng" => 106.8227,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/jakarta_400x200px.jpg"
      },
      {

        "city_id" => 9,
        "city_name" => "kuala lumpur",
        "lat" => 3.1390,
        "lng" => 101.6869,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/kualalumpur_400x200px.jpg"
      },
      {

        "city_id" => 34,
        "city_name" => "kyoto",
        "lat" => 35.0116,
        "lng" => 135.7680,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/kyoto_400x200px.jpg"
      },
      {
        "city_id" => 23,
        "city_name" => "london",
        "lat" => 51.5074,
        "lng" => 0.1278,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/london_400x200px.jpg"
      },
      {
        "city_id" => 22,
        "city_name" => "los angeles",
        "lat" => 34.0522,
        "lng" => 118.2437,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/la_400x200px.jpg"
      },
      {
        "city_id" => 7,
        "city_name" => "manila",
        "lat" => 14.5995,
        "lng" => 120.9842,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/manila_400x200px.jpg"
      },
      {
        "city_id" => 37,
        "city_name" => "mexico city",
        "lat" => 23.6345,
        "lng" => 102.5528,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/maxicocity_400x200px.jpg"
      },
      {
        "city_id" => 24,
        "city_name" => "mumbai",
        "lat" => 19.0760,
        "lng" => 72.8777,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/mumbai_400x200px.jpg"
      },
      {
        "city_id" => 19,
        "city_name" => "new york",
        "lat" => 40.7128,
        "lng" => 74.0059,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/newyork_400x200px.jpg"
      },
      {
        "city_id" => 13,
        "city_name" => "paris",
        "lat" => 48.8566,
        "lng" => 2.3522,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/newyork_400x200px.jpg"
      },
      {
        "city_id" => 29,
        "city_name" => "rome",
        "lat" => 41.9028,
        "lng" => 12.4964,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/rome_400x200px.jpg"
      },
      {
        "city_id" => 11,
        "city_name" => "san francisco",
        "lat" => 37.7749,
        "lng" => 122.4194,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/sanfransisco_400x200px.jpg"
      },
      {
        "city_id" => 16,
        "city_name" => "sao paulo",
        "lat" => 23.5505,
        "lng" => 46.6333,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/saopaolo_400x200px.jpg"
      },
      {
        "city_id" => 5,
        "city_name" => "singapore",
        "lat" => 1.3521,
        "lng" => 103.8198,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/singapore_400x200px.jpg"
      },
      {
        "city_id" => 18,
        "city_name" => "sydney",
        "lat" => 33.8688,
        "lng" => 151.2093,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/sydney_400x200px.jpg"
      },
      {
        "city_id" => 3,
        "city_name" => "taipei",
        "lat" => 25.030671,
        "lng" => 121.529529,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/taipei_400x200px.jpg"
      },
      {
        "city_id" => 4,
        "city_name" => "tokyo",
        "lat" => 35.662952,
        "lng" => 139.731652,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/tokyo_400x200px.jpg"
      },
      {
        "city_id" => 17,
        "city_name" => "vancouver",
        "lat" => 49.286330,
        "lng" => -123.127399,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/vancouver_400x200px.jpg"
      },
      {
        "city_id" => 48,
        "city_name" => "bali",
        "lat" => -8.709135,
        "lng" => 115.172138,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/bali_400x200px.jpg"
      },
      {
        "city_id" => 56,
        "city_name" => "dubai",
        "lat" => 25.2048,
        "lng" => 55.2708,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/dubai_400x200px.jpg"
      },
      {
        "city_id" => 73,
        "city_name" => "warsaw",
        "lat" => 52.234564, 
        "lng" => 21.018795,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/warsaw_400x200px.jpg"
      },
      {
        "city_id" => 76,
        "city_name" => "istanbul",
        "lat" => 41.030974, 
        "lng" => 28.975806,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/istanbul_400x200px.jpg"
      },
      {
        "city_id" => 84,
        "city_name" => "buenos aires",
        "lat" => -34.596463, 
        "lng" => -58.375912,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/buenosaires_400x200px.jpg"
      },
      {
        "city_id" => 89,
        "city_name" => "marrakesh",
        "lat" => 31.625343, 
        "lng" => -7.989489,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/marrakesh_400x200px.jpg"
      },
      {
        "city_id" => 90,
        "city_name" => "cairo",
        "lat" => 30.048255, 
        "lng" => 31.234529,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/cairo_400x200px.jpg"
      },
      {
        "city_id" => 67,
        "city_name" => "amsterdam",
        "lat" => 52.363462, 
        "lng" => 4.901699,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/amsterdam_400x200px.jpg"
      },
      {
        "city_id" => 68,
        "city_name" => "prague",
        "lat" => 50.099731, 
        "lng" => 14.421648,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/prague_400x200px.jpg"
      },
      {
        "city_id" => 95,
        "city_name" => "stockholm",
        "lat" => 59.3293,
        "lng" => 18.0686,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/stockholm_400x200px.jpg"
      },
=begin
      {
        "city_id" => 66,
        "city_name" => "madrid",
        "lat" => 40.452842, 
        "lng" => -3.688463,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/amsterdam_400x200px.jpg"
      },
=end
      {
        "city_id" => 50,
        "city_name" => "phuket",
        "lat" => 7.9519,
        "lng" => 98.3381,
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/phuket_400x200px.jpg"
      },
=begin
      {
        "city_id" => 63,
        "city_name" => "athens",
        "lat" => "",
        "lng" => "",
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/amsterdam_400x200px.jpg"
      },

      {
        "city_id" => 35,
        "city_name" => "moscow",
        "lat" => "",
        "lng" => "",
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/amsterdam_400x200px.jpg"
      },
     
      {
        "city_id" => 64,
        "city_name" => "milan",
        "lat" => "",
        "lng" => "",
        "image" => "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/city_flag/amsterdam_400x200px.jpg"
      }
=end 
    ]

    #Remove art, traffic,hotel and nightlife
    #and move art and hotel news to spot
    config.newspick_category = {
      :matome => 14, 
      :news => 1, 
      :gourmet => 2,
      :fashion => 3,
      :beauty => 4, 
      #:hotel => 5,
      :travel => 6,   
      #:art => 7,
      #:nightlife => 8,
      :sport => 9,    
      :entertainment => 10,
      #:traffic => 11,
      :life_style => 12,
      :learning => 13,  
      :spot => 15,
      :usualday => 16,
      #:classified => 97,
      :tech => 99
    }
    config.newspick_category_map = Hash[ config.newspick_category.map{|k,v| [v,k] } ]

    config.newspick_jp_category = {
      "まとめ" => 14, 
      "ニュース" => 1,  
      "グルメ" => 2,
      "ファッション" => 3,
      "美容" => 4,
      #"ホテル" => 5,
      "トラベル" => 6,  
      #"アート" => 7,    
      #"ナイトライフ" => 8,     
      "スポーツ" => 9,  
      "エンタメ" => 10,     
      #"交通" => 11,      
      "生活" => 12,  
      "学ぶ" => 13, 
      "スポット" => 15,
      "日々つらつら" => 16,
      "お役立ち" => 97,  
      "tech" => 99    
    }
    config.newspick_jp_category_map = Hash[ config.newspick_jp_category.map{|k,v| [v,k] } ]

    config.newspick_jp_category_select = {
      "sy まとめ" => 14,
      "n ニュース" => 1,  
      "g グルメ" => 2,
      "f ファッション" => 3,
      "b 美容" => 4,
      #"ホテル" => 5,
      "t トラベル" => 6,  
      #"アート" => 7,    
      #"ナイトライフ" => 8,     
      "ss スポーツ" => 9,  
      "e エンタメ" => 10,     
      #"交通" => 11,      
      "se 生活" => 12,  
      "m 学ぶ" => 13,  
      "sp スポット" => 15,
      "h 日々つらつら" => 16,
      #"o お役立ち" => 97,  
      "tech" => 99    
    }
    config.newspick_jp_category_select_map = Hash[ config.newspick_jp_category_select.map{|k,v| [v,k] } ]

    config.localpicks_category = {
      :matome => 14, 
      :news => 1, 
      :gourmet => 2,
      :fashion => 3,
      :beauty => 4, 
      #:hotel => 5,
      :travel => 6,   
      #:art => 7,
      #:nightlife => 8,
      :sport => 9,    
      :entertainment => 10,
      #:traffic => 11,
      :life_style => 12,
      :learning => 13,  
      :spot => 15,
      :usualday => 16,
      #:classified => 97,
      :tech => 99
    }
    config.localpicks_category_map = Hash[ config.localpicks_category.map{|k,v| [v,k] } ]

    config.newspick_category_color = [    
      {:id => 999, :jptitle => 'トップ',:title => 'Top', :link => 'top', :color => '#ed1c24'},  
      {:id => 14, :jptitle => 'まとめ',:title => 'Matome', :link => 'matome', :color => '#f16521'},   
      {:id => 1, :jptitle => 'ニュース',:title => 'News', :link => 'news', :color => '#244f75'},  
      {:id => 2, :jptitle => 'グルメ', :title => 'Gourmet', :link => 'gourmet', :color => '#716558'}, 
      {:id => 15, :jptitle => 'スポット',:title => 'Spot', :link => 'spot', :color => '#91268f'},
      {:id => 3, :jptitle => 'ファッション',:title => 'Fashion', :link => 'fashion', :color => '#006738'},
      {:id => 4, :jptitle => '美容',:title => 'Beauty', :link => 'beauty', :color => '#d91b5b'},
      {:id => 99, :jptitle => 'テクノロジー',:title => 'Tech', :link => 'tech', :color => '#91268f'},
      #{:id => 5, :jptitle => 'ホテル',:title => 'Hotel', :link => 'hotel', :color => '#91268f'},
      {:id => 6, :jptitle => 'トラベル',:title => 'Travel', :link => 'travel', :color => '#ec008b'},
      #{:id => 7, :jptitle => 'アート',:title => 'Art', :link => 'art', :color => '#8cc63e'},    
      #{:id => 8, :jptitle => 'ナイトライフ',:title => 'Nigthlife', :link => 'nightlife', :color => '#f7931d'},  
      {:id => 9, :jptitle => 'スポーツ',:title => 'Sports', :link => 'sport', :color => '#fbaf3f'},  
      {:id => 10, :jptitle => 'エンタメ',:title => 'Entertainment', :link => 'entertainment', :color => '#f499c1'},        
      #{:id => 11, :jptitle => '交通',:title => 'Traffic', :link => 'traffic', :color => '#ef4036'},     
      {:id => 12, :jptitle => '生活',:title => 'Lifestyle', :link => 'life_style', :color => '#bbbdc0'},  
      {:id => 13, :jptitle => '学ぶ',:title => 'Learning', :link => 'learning', :color => '#9c762b'}, 
      {:id => 16, :jptitle => '日々つらつら',:title => 'Usual Day', :link => 'usualday', :color => '#8cc63e'}
    ]

    config.localpicks_category_color = [    
      {:id => 999, :thtitle => 'ท็อป', :jptitle => 'トップ',:title => 'Top', :link => 'top', :color => '#ed1c24'}, 
      {:id => 14, :thtitle => 'รวม', :jptitle => 'まとめ',:title => 'Matome', :link => 'matome', :color => '#f16521'},   
      {:id => 1, :thtitle => 'ข่าว', :jptitle => 'ニュース',:title => 'News', :link => 'news', :color => '#244f75'},  
      {:id => 2, :thtitle => 'อาหาร', :jptitle => 'グルメ', :title => 'Gourmet', :link => 'gourmet', :color => '#716558'}, 
      {:id => 15, :thtitle => 'สถานที่', :jptitle => 'スポット',:title => 'Things to Do', :link => 'things_to_do', :color => '#91268f'}, 
      {:id => 3, :thtitle => 'แฟชั่น', :jptitle => 'ファッション',:title => 'Fashion', :link => 'fashion', :color => '#006738'},
      {:id => 4, :thtitle => 'บิวตี้', :jptitle => '美容',:title => 'Beauty', :link => 'beauty', :color => '#d91b5b'},
      {:id => 99, :thtitle => 'เทคโนโลยี', :jptitle => 'テクノロジー',:title => 'Tech', :link => 'tech', :color => '#91268f'},
      #{:id => 5, :jptitle => 'ホテル',:title => 'Hotel', :link => 'hotel', :color => '#91268f'},
      {:id => 6, :thtitle => 'ท่องเที่ยว', :jptitle => 'トラベル',:title => 'Travel', :link => 'travel', :color => '#ec008b'},
      #{:id => 7, :jptitle => 'アート',:title => 'Art', :link => 'art', :color => '#8cc63e'},    
      #{:id => 8, :jptitle => 'ナイトライフ',:title => 'Nigthlife', :link => 'nightlife', :color => '#f7931d'},  
      {:id => 9, :thtitle => 'กีฬา', :jptitle => 'スポーツ',:title => 'Sports', :link => 'sport', :color => '#fbaf3f'},  
      {:id => 10, :thtitle => 'เอนเตอร์เทนเมนต์', :jptitle => 'エンタメ',:title => 'Entertainment', :link => 'entertainment', :color => '#f499c1'},        
      #{:id => 11, :jptitle => '交通',:title => 'Traffic', :link => 'traffic', :color => '#ef4036'},     
      {:id => 12, :thtitle => 'ไลฟ์สไตล์', :jptitle => '生活',:title => 'Lifestyle', :link => 'life_style', :color => '#bbbdc0'},  
      {:id => 13, :thtitle => 'การศึกษา', :jptitle => '学ぶ',:title => 'Learning', :link => 'learning', :color => '#9c762b'}, 
      #{:id => 16, :thtitle => 'ตลก', :jptitle => '日々つらつら',:title => 'Diary', :link => 'diary', :color => '#8cc63e'},
    ]

    config.sub_classified_category_menu = [
       {:id => 0, :parent_cateogry => 97, :jptitle => 'すべて', :title => 'All', :link => 'all'},  
       {:id => 1, :parent_cateogry => 97, :jptitle => '売ります買います',:title => 'For Sale', :link => 'for_sale'},  
       {:id => 2, :parent_cateogry => 97, :jptitle => 'お家関係',:title => 'Housing', :link => 'housing'},  
       {:id => 3, :parent_cateogry => 97, :jptitle => '仲間募集',:title => 'Friends', :link => 'friends'},  
       {:id => 4, :parent_cateogry => 97, :jptitle => 'サービス',:title => 'Services', :link => 'services'},  
       {:id => 5, :parent_cateogry => 97, :jptitle => 'コミュニティ',:title => 'Community', :link => 'community'},  
    ]

    config.sub_classified_category = {
      "For Sale" => 1,
      "Housing" => 2,
      "Friends" => 3,
      "Services" => 4,
      "Community" => 5,
    }

    config.sub_classified_category_jp = {
      "売ります買います" => 1,
      "お家関係" => 2,
      "仲間募集" => 3,
      "サービス" => 4,
      "コミュニティ" => 5,
    }

    config.nearby_menu = [
       {:id => 1,  :jptitle => 'レストラン', :title => 'Restaurants', :search_word => 'restaurant', :search_word_jp => 'restaurant'}, 
       {:id => 2,  :jptitle => 'カフェ', :title => 'Cafes', :search_word => 'cafe', :search_word_jp => 'cafe'},   
       {:id => 3,  :jptitle => 'バー', :title => 'Bars', :search_word => 'bar', :search_word_jp => 'bar'},  
       {:id => 4,  :jptitle => 'ホテル', :title => 'Hotels', :search_word => 'hotel', :search_word_jp => 'hotel'},  
       {:id => 5,  :jptitle => '美容', :title => 'Beauty', :search_word => 'beauty', :search_word_jp => 'beauty'},  
       {:id => 6,  :jptitle => 'ショッピング', :title => 'Shopping', :search_word => 'shopping', :search_word_jp => 'shopping'}, 
       {:id => 7,  :jptitle => 'クラブ', :title => 'Clubs', :search_word => 'club', :search_word_jp => 'club'},  
       {:id => 8, :jptitle => 'ミュージアム', :title => 'Museum', :search_word => 'museum', :search_word_jp => 'museum'},
       {:id => 9,  :jptitle => 'マーケット', :title => 'Markets', :search_word => 'market', :search_word_jp => 'market'},
       {:id => 10,  :jptitle => 'ライブ', :title => 'Lives', :search_word => 'live', :search_word_jp => 'live'},
       {:id => 11,  :jptitle => 'ナイトライフ', :title => 'Nightlife', :search_word => 'nightlife', :search_word_jp => 'nightlife'},
        #{:id => 9,  :jptitle => 'イベント', :title => 'Events', :search_word => 'event', :search_word_jp => 'evetnt'},  
      
    ]

    config.localpicks_nearby_menu = [
       #{:id => 1,  :thtitle => 'ร้านอาหาร',  :jptitle => 'レストラン', :title => 'Restaurants', :search_word => 'restaurant', :search_word_jp => 'restaurant'}, 
       {:id => 1,  :thtitle => 'อาหารตะวันตก', :jptitle => '西洋料理', :title => 'Western', :search_word => 'western', :search_word_jp => 'western'}, 
       {:id => 12,  :thtitle => 'อาหารอาเซียน', :jptitle => 'アジア料理', :title => 'Asian', :search_word => 'asian', :search_word_jp => 'asian'}, 
       {:id => 13,  :thtitle => 'อาหารญี่ปุ่น', :jptitle => '日本料理', :title => 'Japanese', :search_word => 'japanese', :search_word_jp => 'japanese'}, 
       {:id => 14,  :thtitle => 'อาหารนานาชาติ', :jptitle => 'その他レストラン', :title => 'More Restaurants', :search_word => 'more', :search_word_jp => 'more'}, 
       {:id => 2,  :thtitle => 'คาเฟ่',  :jptitle => 'カフェ', :title => 'Cafes', :search_word => 'cafe', :search_word_jp => 'cafe'},   
       {:id => 3,  :thtitle => 'บาร์',  :jptitle => 'バー', :title => 'Bars', :search_word => 'bar', :search_word_jp => 'bar'},  
       {:id => 4,  :thtitle => 'โรงแรม',  :jptitle => 'ホテル', :title => 'Hotels', :search_word => 'hotel', :search_word_jp => 'hotel'},  
       {:id => 5,  :thtitle => 'ความงาม',  :jptitle => '美容', :title => 'Beauty', :search_word => 'beauty', :search_word_jp => 'beauty'},  
       {:id => 6,  :thtitle => 'ชอปปิ้ง',  :jptitle => 'ショッピング', :title => 'Shopping', :search_word => 'shopping', :search_word_jp => 'shopping'}, 
       {:id => 7,  :thtitle => 'คลับ',  :jptitle => 'クラブ', :title => 'Clubs', :search_word => 'club', :search_word_jp => 'club'},  
       {:id => 8,  :thtitle => 'พิพิธภัณฑ์', :jptitle => 'ミュージアム', :title => 'Museum', :search_word => 'museum', :search_word_jp => 'museum'},
       {:id => 9,  :thtitle => 'ตลาด',  :jptitle => 'マーケット', :title => 'Markets', :search_word => 'market', :search_word_jp => 'market'},
       {:id => 10,  :thtitle => 'ไลฟ์สไตล์',  :jptitle => 'ライブ', :title => 'Lives', :search_word => 'live', :search_word_jp => 'live'},
       {:id => 11,  :thtitle => 'ไนท์ไลฟ์',  :jptitle => 'ナイトライフ', :title => 'Nightlife', :search_word => 'nightlife', :search_word_jp => 'nightlife'},
        #{:id => 9,  :jptitle => 'イベント', :title => 'Events', :search_word => 'event', :search_word_jp => 'evetnt'},  
      
    ]

    config.city_facebook_url = {
      '0' => 'https://www.facebook.com/ローカルピックス-1295743873786243/',
      '1' => 'https://www.facebook.com/バンコクピックス-1001368049935912/', 
      '2' => 'https://www.facebook.com/香港ピックス-971244642960410/',
      '3' => 'https://www.facebook.com/台北ピックス-1952422111649001/',
      '4' => '',
      '5' => 'https://www.facebook.com/シンガポールピックス-191401817891087/',
      '6' => 'https://www.facebook.com/ジャカルタピックス-1559666147678370/',
      '7' => 'https://www.facebook.com/マニラピックス-1577386555918541/',
      '8' => 'https://www.facebook.com/ソウルピックス-246539039014986/',
      '9' => 'https://www.facebook.com/クアラルンプールピックス-770410889756475/',
      '10' => 'https://www.facebook.com/ホーチミンピックス-958402320925638/',
      '11' => 'https://www.facebook.com/サンフランシスコピックス-1213318718696075/',
      '12' => 'https://www.facebook.com/ハワイピックス-235151860159456/',
      '13' => 'https://www.facebook.com/パリピックス-1148495381836076/',
      '14' => 'https://www.facebook.com/ブリスベンピックス-980428308708343/',
      '15' => 'https://www.facebook.com/メルボルンピックス-573709946122213/',
      '16' => '',
      '17' => 'https://www.facebook.com/バンクーバーピックス-563462557168199/',
      '18' => 'https://www.facebook.com/シドニーピックス-252833708381692/',
      '19' => 'https://www.facebook.com/ニューヨークピックス-122332008158721/',
      '20' => 'https://www.facebook.com/ハノイピックス-1578473132475852/',
      '21' => '',
      '22' => 'https://www.facebook.com/ロサンゼルスピックス-951323011611389/',
      '23' => 'https://www.facebook.com/ロンドンピックス-1683616831885854/',
      '24' => 'https://www.facebook.com/ムンバイピックス-515857378610822/',
      '28' => 'https://www.facebook.com/バルセロナピックス-1164204763610425/',
      '29' => 'https://www.facebook.com/ローマピックス-253344941706235/',
      '30' => 'https://www.facebook.com/ベルリンピックス-253703738324420/',
      '31' => 'https://www.facebook.com/ヤンゴンピックス-260390650993977/',
      '32' => 'https://www.facebook.com/ビエンチャンピックス-646053905546615/',
      '33' => 'https://www.facebook.com/プノンペンピックス-1047576681944779/',
      '38' => '',
    }

    config.city_facebook_en_url = {
      '0' => 'https://www.facebook.com/Local-Picks-1699190647000819/',
      '1' => 'https://www.facebook.com/Bangkok-Picks-206510593075876/', 
      '2' => 'https://www.facebook.com/Hongkong-Picks-1550936738549284/',
      '3' => 'https://www.facebook.com/Taipei-Picks-1164157620281600/',
      '4' => 'https://www.facebook.com/Tokyo-Picks-574047509441227/',
      '5' => 'https://www.facebook.com/Singapore-Picks-1605256233120781/',
      '6' => 'https://www.facebook.com/Jakarta-Picks-1147787905283958/',
      '7' => 'https://www.facebook.com/Manila-Picks-889430971182281/',
      '8' => 'https://www.facebook.com/Seoul-Picks-1716999968579844/',
      '9' => 'https://www.facebook.com/Kuala-Lumpur-Picks-1064878893568717/',
      '10' => 'https://www.facebook.com/HCMC-Picks-1790582304561707/',
      '11' => 'https://www.facebook.com/サンフランシスコピックス-1213318718696075/',
      '12' => 'https://www.facebook.com/ハワイピックス-235151860159456/',
      '13' => 'https://www.facebook.com/Paris-Picks-580028538833837/',
      '14' => 'https://www.facebook.com/ブリスベンピックス-980428308708343/',
      '15' => 'https://www.facebook.com/メルボルンピックス-573709946122213/',
      '16' => '',
      '17' => 'https://www.facebook.com/バンクーバーピックス-563462557168199/',
      '18' => 'https://www.facebook.com/シドニーピックス-252833708381692/',
      '19' => 'https://www.facebook.com/ニューヨークピックス-122332008158721/',
      '20' => 'https://www.facebook.com/Hanoi-Picks-1732827000326030/',
      '21' => '',
      '22' => 'https://www.facebook.com/ロサンゼルスピックス-951323011611389/',
      '23' => 'https://www.facebook.com/ロンドンピックス-1683616831885854/',
      '24' => 'https://www.facebook.com/Mumbai-Picks-1025435307503911/',
      '28' => 'https://www.facebook.com/Barcelona-Picks-1719734538266400/',
      '29' => 'https://www.facebook.com/Rome-Picks-1725048774431350/',
      '30' => 'https://www.facebook.com/Berlin-Picks-799646713468483/',
      '31' => 'https://www.facebook.com/Yangon-Picks-283149902025037/',
      '32' => 'https://www.facebook.com/Vientiane-Picks-851371858300827/',
      '33' => 'https://www.facebook.com/Phnom-Penh-Picks-1740855449490005/',
      '34' => 'https://www.facebook.com/Kyoto-Picks-1008198449248658/',
      '38' => 'https://www.facebook.com/Osaka-Picks-569620779886915/',
    }

    config.city_facebook_th_url = {
      '0' => 'https://www.facebook.com/Local-Picks-TH-604398826404559/',
      '1' => 'https://www.facebook.com/バンコクピックス-1001368049935912/', 
      '2' => 'https://www.facebook.com/Hongkong-Picks-TH-637616243058621/',
      '3' => 'https://www.facebook.com/Taipei-Picks-TH-1763080493934947/',
      '4' => 'https://www.facebook.com/Tokyo-Picks-TH-243048019400539/',
      '5' => 'https://www.facebook.com/Singapore-Picks-TH-1201084909925704',
      '6' => 'https://www.facebook.com/ジャカルタピックス-1559666147678370/',
      '7' => 'https://www.facebook.com/マニラピックス-1577386555918541/',
      '8' => 'https://www.facebook.com/Seoul-Picks-TH-1691164987812720/',
      '9' => 'https://www.facebook.com/Kuala-Lumpur-Picks-TH-1759984444225275/',
      '10' => 'https://www.facebook.com/ホーチミンピックス-958402320925638/',
      '11' => 'https://www.facebook.com/サンフランシスコピックス-1213318718696075/',
      '12' => 'https://www.facebook.com/ハワイピックス-235151860159456/',
      '13' => 'https://www.facebook.com/Paris-Picks-TH-385726331602504/',
      '14' => 'https://www.facebook.com/ブリスベンピックス-980428308708343/',
      '15' => 'https://www.facebook.com/メルボルンピックス-573709946122213/',
      '16' => '',
      '17' => 'https://www.facebook.com/バンクーバーピックス-563462557168199/',
      '18' => 'https://www.facebook.com/シドニーピックス-252833708381692/',
      '19' => 'https://www.facebook.com/New-York-Picks-TH-1170529116325108/',
      '20' => 'https://www.facebook.com/ハノイピックス-1578473132475852/',
      '21' => '',
      '22' => 'https://www.facebook.com/Los-Angeles-Picks-TH-1580742258890222/',
      '23' => 'https://www.facebook.com/London-Picks-TH-1188799511183695/',
      '34' => 'https://www.facebook.com/Kyoto-Picks-TH-593688720804138/',
      '38' => 'https://www.facebook.com/Osaka-Pick-Th-568680056673853/',
    }

    config.city_twitter_url = {
      '0' => 'https://twitter.com/localpicksja',
      '1' => 'https://twitter.com/bangkokpicksja',
      '2' => 'https://twitter.com/hkpicksja',
      '3' => 'https://twitter.com/TPEpicks',
      '4' => '',
      '5' => 'https://twitter.com/sinpicksja',
      '6' => 'https://twitter.com/JKTpicks',
      '7' => 'https://twitter.com/mnlapicks',
      '8' => 'https://twitter.com/SELpicks',
      '9' => 'https://twitter.com/KLpicks',
      '10' => 'https://twitter.com/hcmcpicks',
      '11' => 'https://twitter.com/sfopicks',
      '12' => 'https://twitter.com/hawaiipicksja',
      '13' => 'https://twitter.com/parispicksja',
      '14' => 'https://twitter.com/bnepicks',
      '15' => 'https://twitter.com/melpicksja',
      '16' => '',
      '17' => 'https://twitter.com/YVRpicksja',
      '18' => 'https://twitter.com/sydpicks',
      '19' => 'https://twitter.com/NYpicks_ja',
      '20' => 'https://twitter.com/hanoipicks',
      '21' => '',
      '22' => 'https://twitter.com/LAXpicks',
      '23' => 'https://twitter.com/londonpicksja',
      '24' => 'https://twitter.com/mumbaipicksja',
      '28' => 'https://twitter.com/barcelonapicksj',
      '29' => 'https://twitter.com/romepicksja',
      '30' => 'https://twitter.com/berlinpicksja',
      '31' => 'https://twitter.com/yangonpicksja',
      '32' => 'https://twitter.com/vientianepicksj',
      '33' => 'https://twitter.com/phnompenhpicksj',
      '38' => '',
    }

    config.city_twitter_en_url = {
      '0' => 'https://twitter.com/localpicksen',
      '1' => 'https://twitter.com/bangkokpicksen',
      '2' => 'https://twitter.com/hongkongpicksen',
      '3' => 'https://twitter.com/taipeipicksen',
      '4' => 'https://twitter.com/tokyopicksen',
      '5' => 'https://twitter.com/sinpicksen',
      '6' => 'https://twitter.com/jakartapicksen',
      '7' => 'https://twitter.com/manilapicksen',
      '8' => 'https://twitter.com/seoulpicksen',
      '9' => 'https://twitter.com/klpicksen',
      '10' => 'https://twitter.com/hcmcpicksen',
      '11' => 'https://twitter.com/sfopicks',
      '12' => 'https://twitter.com/hawaiipicksja',
      '13' => 'https://twitter.com/parispicksen',
      '14' => 'https://twitter.com/bnepicks',
      '15' => 'https://twitter.com/melpicksja',
      '16' => '',
      '17' => 'https://twitter.com/YVRpicksja',
      '18' => 'https://twitter.com/sydpicks',
      '19' => 'https://twitter.com/NYpicks_ja',
      '20' => 'https://twitter.com/hanoipicksen',
      '21' => '',
      '22' => 'https://twitter.com/LAXpicks',
      '23' => 'https://twitter.com/londonpicksja',
      '24' => 'https://twitter.com/mumbaipicksen',
      '28' => 'https://twitter.com/barcelonapickse',
      '29' => 'https://twitter.com/romepicksen',
      '30' => 'https://twitter.com/berlinpicksen',
      '31' => 'https://twitter.com/yangonpicksen',
      '32' => 'https://twitter.com/vientianepicks',
      '33' => 'https://twitter.com/phnompenhpicks',
      '34' => 'https://twitter.com/kyotopicksen',
      '38' => 'https://twitter.com/osakapicksen',
    }

    config.city_twitter_th_url = {
      '0' => 'https://twitter.com/localpicksth',
      '1' => 'https://twitter.com/bangkokpicksja',
      '2' => 'https://twitter.com/hongkongpicksth',
      '3' => 'https://twitter.com/taipeipicksth',
      '4' => 'https://twitter.com/tokyopicksth',
      '5' => 'https://twitter.com/sinpicksth',
      '6' => 'https://twitter.com/JKTpicks',
      '7' => 'https://twitter.com/mnlapicks',
      '8' => 'https://twitter.com/seoulpicksth',
      '9' => 'https://twitter.com/klpicksth',
      '10' => 'https://twitter.com/hcmcpicks',
      '11' => 'https://twitter.com/sfopicks',
      '12' => 'https://twitter.com/hawaiipicksja',
      '13' => 'https://twitter.com/parispicksth',
      '14' => 'https://twitter.com/bnepicks',
      '15' => 'https://twitter.com/melpicksja',
      '16' => '',
      '17' => 'https://twitter.com/YVRpicksja',
      '18' => 'https://twitter.com/sydpicks',
      '19' => 'https://twitter.com/newyorkpicksth',
      '20' => 'https://twitter.com/hanoipicks',
      '21' => '',
      '22' => 'https://twitter.com/laxpicksth',
      '23' => 'https://twitter.com/londonpicksth',
      '34' => 'https://twitter.com/kyotopicksth',
      '38' => 'https://twitter.com/osakapicksth',
    }

    config.news_city_logo = {
      '1' => 'bkk_picks_profile_fb.png',
      '2' => 'hkg_picks_profile_fb.png',
      '3' => 'tpe_picks_profile_fb.png',
      '4' => 'tyo_picks_profile_fb.png',
      '5' => 'sin_picks_profile_fb.png',
      '6' => 'jkt_picks_profile_fb.png',
      '7' => 'mnl_picks_profile_fb.png',
      '8' => 'sel_picks_profile_fb.png',
      '9' => 'kul_picks_profile_fb.png',
      '10' => 'hcmc_picks_profile_fb.png',
      '11' => 'sfo_picks_profile_fb.png',
      '12' => 'hawaii_picks_profile_fb.png',
      '13' => 'par_picks_profile_fb.png',
      '14' => 'bne_picks_profile_fb.png',
      '15' => 'mel_picks_profile_fb.png',
      '16' => 'sao_picks_profile_fb.png',
      '17' => 'yvr_picks_profile_fb.png',
      '18' => 'syd_picks_profile_fb.png',
      '19' => 'nyc_picks_profile_fb.png',
      '20' => 'han_picks_profile_fb.png',
      '21' => 'shanghai_picks_profile_fb.png',
      '22' => 'lax_picks_profile_fb.png',
      '23' => 'lon_picks_profile_fb.png',
      '24' => 'mumbai_picks_profile_fb.png',
      '25' => '',
      '26' => '',
      '27' => '',
      '28' => 'barcelona_picks_profile_fb.png',
      '29' => 'rome_picks_profile_fb.png',
      '30' => 'berlin_picks_profile_fb.png',
      '31' => 'yangon_picks_profile_fb.png',
      '32' => 'vientiane_picks_profile_fb.png',
      '33' => 'phnompenh_picks_profile_fb.png',
      '34' => 'kyoto_picks_profile_fb.png',
      '35' => 'moscow_picks_profile_fb.png',
      '36' => 'delhi_picks_profile_fb.png',
      '37' => 'mexico_picks_profile_fb.png',
      '38' => 'osaka_picks_profile_fb.png',
    }

    config.newspick_hotnews = {
      :no => 0,
      :yes => 1,      
    }

    config.newspick_events = {
      :no => 0,
      :yes => 1,      
    }

    config.newspick_promotions = {
      :no => 0,
      :yes => 1,      
    }

    config.newspick_app = {      
      :japanese_bangkok => 1,
      :japanese_hongkong  => 2,     
      :japanese_taipei => 3,
      :japanese_tokyo => 4,
      :japanese_singapore  => 5,     
      :japanese_jakarta => 6,     
      :japanese_manila => 7,      
      :japanese_seoul => 8,      
      :japanese_kaulalumpur => 9,  
      :japanese_hochiminhcity => 10,       
      :japanese_sanfrancisco => 11,
      :japanese_hawaii => 12,
      :japanese_paris => 13,
      :japanese_brisbane => 14,
      :japanese_melbourne => 15,
      :japanese_saopaulo => 16,
      :japanese_vancouver => 17,
      :japanese_sydney => 18,
      :japanese_newyork => 19,
      :japanese_hanoi => 20,
      :japanese_shagnhai => 21,
      :japanese_losangeles => 22,
      :japanese_london => 23,
      :japanese_mumbai => 24,
      :japanese_frankfurt => 25,
      :japanese_washington => 26,
      :japanese_canberra => 27,
      :japanese_barcelona => 28,
      :japanese_rome => 29, 
      :japanese_berlin => 30,
      :japanese_yangon => 31,
      :japanese_vientiane => 32,
      :japanese_phnom_penh => 33,
      :japanese_kyoto => 34,
      :japanese_moscow => 35,
      :japanese_delhi => 36,
      :japanese_mexico_city => 37,
      :japanese_osaka => 38,
      :english_bangkok => 39,
      :english_hongkong  => 40,     
      :english_taipei => 41,
      :english_tokyo => 42,
      :english_singapore  => 43,     
      :english_jakarta => 44,     
      :english_manila => 45,      
      :english_seoul => 46,      
      :english_kaulalumpur => 47,  
      :english_hochiminhcity => 48,       
      :english_sanfrancisco => 49,
      :english_hawaii => 50,
      :english_paris => 51,
      :english_brisbane => 52,
      :english_melbourne => 53,
      :english_saopaulo => 54,
      :english_vancouver => 55,
      :english_sydney => 56,
      :english_newyork => 57,
      :english_hanoi => 58,
      :english_shagnhai => 59,
      :english_losangeles => 60,
      :english_london => 61,   
      :english_mumbai => 62, 
      :english_frankfurt => 63,
      :english_washington => 64,
      :english_canberra => 65,
      :english_barcelona => 66,
      :english_rome => 67, 
      :english_berlin => 68,
      :english_yangon => 69,
      :english_vientiane => 70,
      :english_phnom_penh => 71,
      :english_kyoto => 72,
      :english_moscow => 73,
      :english_delhi => 74,
      :english_mexico_city => 75,
      :english_osaka => 76,
      :thai_bangkok => 77,
      :thai_hongkong  => 78,     
      :thai_taipei => 79,
      :thai_tokyo => 80,
      :thai_singapore  => 81,     
      :thai_jakarta => 82,     
      :thai_manila => 83,      
      :thai_seoul => 84,      
      :thai_kaulalumpur => 85,  
      :thai_hochiminhcity => 86,       
      :thai_sanfrancisco => 87,
      :thai_hawaii => 88,
      :thai_paris => 89,
      :thai_brisbane => 90,
      :thai_melbourne => 91,
      :thai_saopaulo => 92,
      :thai_vancouver => 93,
      :thai_sydney => 94,
      :thai_newyork => 95,
      :thai_hanoi => 96,
      :thai_shagnhai => 97,
      :thai_losangeles => 98,
      :thai_london => 99,   
      :thai_mumbai => 100, 
      :thai_frankfurt => 101,
      :thai_washington => 102,
      :thai_canberra => 103,   
      :thai_barcelona => 104,
      :thai_rome => 105, 
      :thai_berlin => 106,
      :thai_yangon => 107,
      :thai_vientiane => 108,
      :thai_phnom_penh => 109,
      :thai_kyoto => 110,
      :thai_moscow => 111,
      :thai_delhi => 112,
      :thai_mexico_city => 113,
      :thai_osaka => 114,  
    }

    config.tag_csv_column_num = 5
    config.tag_csv_date_format = "%Y-%m-%d"
    config.tag_csv_columns = %w(
      tag 
      region 
      city_id 
      category_id
      tags_group
    )

    config.news_app_s3_bucket_url = "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/"

    config.localpicks_sidebar = [
      {:id => 1, :name => 'Tweets'},
      {:id => 2, :name => 'Now'},
      {:id => 3, :name => 'News'},
      {:id => 4, :name => 'Events'},
      {:id => 'line'},
      {:id => 6,  :name => 'Video', :search_word => 'filter:videos', :search_word_fb => 'filter:videos'},
      {:id => 97, :name => 'Best', :search_word => 'best', :search_word_fb => 'best'},
      {:id => 96, :name => 'Must', :search_word => 'must', :search_word_fb => 'must'},
      {:id => 95, :name => 'Ranking', :search_word => 'ranking', :search_word_fb => 'ranking'},
      {:id => 94, :name => 'Local', :search_word => 'news', :search_word_fb => 'news'},
      {:id => 93, :name => 'Incidents', :search_word => 'incident', :search_word_fb => 'incident'},
      {:id => 92, :name => 'Accidents', :search_word => 'accident', :search_word_fb => 'accident'},
      {:id => 91, :name => 'Blogs', :search_word => 'blog', :search_word_fb => 'blog'},
      {:id => 90, :name => 'Delicious', :search_word => 'delicious', :search_word_fb => 'delicious'},
      {:id => 'line'},
      {:id => 8, :name => 'Gourmet', :search_word => 'gourmet', :search_word_fb => 'gourmet'},
      {:id => 9, :name => 'Restaurant', :search_word => 'restaurant', :search_word_fb => 'restaurant'},
      {:id => 10, :name => 'Cafe', :search_word => 'cafe', :search_word_fb => 'cafe'},
      {:id => 11, :name => 'Bar', :search_word => 'bar', :search_word_fb => 'bar'},
      {:id => 13, :name => 'Fashion', :search_word => 'fashion', :search_word_fb => 'fashion'},
      {:id => 15, :name => 'Market', :search_word => 'market', :search_word_fb => 'market'},
      {:id => 16, :name => 'Museum', :search_word => 'museum', :search_word_fb => 'museum'},
      {:id => 17, :name => 'Gallery', :search_word => 'gallery', :search_word_fb => 'gallery'},
      {:id => 'line'},
      {:id => 18, :name => 'Sight', :search_word => 'sight', :search_word_fb => 'sight'},
      {:id => 19, :name => 'Street', :search_word => 'street', :search_word_fb => 'street'},
      {:id => 20, :name => 'Travel', :search_word => 'travel', :search_word_fb => 'travel'},
      {:id => 21, :name => 'Lifestyle', :search_word => 'lifestyle', :search_word_fb => 'lifestyle'},
      {:id => 89, :name => 'Property', :search_word => 'property', :search_word_fb => 'property'},
      {:id => 23, :name => 'Interior', :search_word => 'interior', :search_word_fb => 'interior'},
      {:id => 87, :name => 'Expat', :search_word => 'expat', :search_word_fb => 'expat'},
      {:id => 'line'},
      {:id => 25, :name => 'Art', :search_word => 'art', :search_word_fb => 'art'},
      {:id => 26, :name => 'Exhibition', :search_word => 'exhibition', :search_word_fb => 'exhibition'},
      {:id => 27, :name => 'Festival', :search_word => 'festival', :search_word_fb => 'festival'},
      {:id => 29, :name => 'Concert', :search_word => 'concert', :search_word_fb => 'concert'},
      {:id => 'line'},
      {:id => 98, :name => 'Feedback', :search_word => 'feedback', :search_word_fb => 'feedback'},
      {:id => 99, :name => 'Settings', :search_word => 'settings', :search_word_fb => 'settings'},
    ]

    config.localpicks_jp_sidebar = [
      {:id => 1, :name => 'Tweets'},
      {:id => 2, :name => 'Now'},
      {:id => 3, :name => 'News'},
      {:id => 4, :name => 'Events'},
      {:id => 'line'},
      {:id => 6, :name => '動画', :search_word => 'filter:videos', :search_word_fb => 'filter:videos'},
      {:id => 97, :name => 'ベスト', :search_word => 'ベスト', :search_word_fb => 'best'},
      {:id => 89, :name => 'おすすめ', :search_word => 'おすすめ', :search_word_fb => 'recommend'},
      {:id => 95, :name => 'ランキング', :search_word => 'ランキング', :search_word_fb => 'ranking'},
      {:id => 94, :name => 'ローカル', :search_word => 'ニュース', :search_word_fb => 'news'},
      {:id => 93, :name => '事件', :search_word => '事件', :search_word_fb => 'incident'},
      {:id => 92, :name => '事故', :search_word => '事故', :search_word_fb => 'accident'},
      {:id => 91, :name => 'ブログ', :search_word => 'ブログ', :search_word_fb => 'blog'},
      {:id => 88, :name => '日本人', :search_word => '日本人', :search_word_fb => 'japanese'},
      {:id => 90, :name => '美味しい', :search_word => '美味しい', :search_word_fb => 'delicious'},
      {:id => 'line'},
      {:id => 8, :name => 'グルメ', :search_word => 'グルメ', :search_word_fb => 'gourmet'},
      {:id => 9, :name => 'レストラン', :search_word => 'レストラン', :search_word_fb => 'restaurant'},
      {:id => 10, :name => 'カフェ', :search_word => 'カフェ', :search_word_fb => 'cafe'},
      {:id => 11, :name => 'バー', :search_word => 'バー', :search_word_fb => 'bar'},
      {:id => 13, :name => 'ファッション', :search_word => 'ファッション', :search_word_fb => 'fashion'},
      {:id => 15, :name => 'マーケット', :search_word => 'マーケット', :search_word_fb => 'market'},
      {:id => 16, :name => 'ミュージアム', :search_word => 'ミュージアム', :search_word_fb => 'museum'},
      {:id => 17, :name => '美術館', :search_word => '美術館', :search_word_fb => 'art museum'},
      {:id => 18, :name => '博物館', :search_word => '博物館', :search_word_fb => 'other museum'},
      {:id => 19, :name => 'ギャラリー', :search_word => 'ギャラリー', :search_word_fb => 'gallery'},
      {:id => 'line'},
      {:id => 20, :name => '観光', :search_word => '観光', :search_word_fb => 'sight'},
      {:id => 21, :name => 'ストリート', :search_word => 'ストリート', :search_word_fb => 'street'},
      {:id => 22, :name => 'トラベル', :search_word => 'トラベル', :search_word_fb => 'travel'},
      {:id => 23, :name => '生活', :search_word => '生活', :search_word_fb => 'lifestyle'},
      {:id => 88, :name => '不動産', :search_word => '不動産', :search_word_fb => 'property'},
      {:id => 25, :name => 'インテリア', :search_word => 'インテリア', :search_word_fb => 'interior'},
      {:id => 87, :name => '駐在', :search_word => '駐在', :search_word_fb => 'expat'},
      {:id => 'line'},
      {:id => 27, :name => 'アート', :search_word => 'アート', :search_word_fb => 'art'},
      {:id => 28, :name => '展覧会', :search_word => '展覧会', :search_word_fb => 'exhibition'},
      {:id => 29, :name => 'フェスティバル', :search_word => 'フェスティバル', :search_word_fb => 'festival'},
      {:id => 31, :name => 'コンサート', :search_word => 'コンサート', :search_word_fb => 'concert'},
      {:id => 'line'},
      {:id => 98, :name => 'フィードバック', :search_word => 'フィードバック', :search_word_fb => 'feedback'},
      {:id => 99, :name => '設定', :search_word => '設定', :search_word_fb => 'settings'},
    ]

    config.localpicks_th_sidebar = [
      {:id => 1, :name => 'Tweets'},
      {:id => 2, :name => 'Now'},
      {:id => 3, :name => 'News'},
      {:id => 4, :name => 'Events'},
      {:id => 'line'},
      {:id => 6, :name => 'Video', :search_word => 'filter:videos', :search_word_fb => 'filter:videos'},
      {:id => 7, :name => 'Pantip', :search_word => 'pantip', :search_word_fb => 'pantip'},
      {:id => 7, :name => 'ที่สุด', :search_word => 'ที่สุด', :search_word_fb => 'best'},
      {:id => 8, :name => 'แนะนำ', :search_word => 'สวย', :search_word_fb => 'recommend'},
      {:id => 94, :name => 'ท้องถิ่น', :search_word => 'ข่าว', :search_word_fb => 'news'},
      {:id => 93, :name => 'เหตุการณ์', :search_word => 'เหตุการณ์', :search_word_fb => 'incident'},
      {:id => 92, :name => 'อุบัติเหตุ', :search_word => 'อุบัติเหตุ', :search_word_fb => 'accident'},
      {:id => 91, :name => 'ไทย', :search_word => 'ไทย', :search_word_fb => 'thai'},
      {:id => 90, :name => 'อร่อย', :search_word => 'อร่อย', :search_word_fb => 'tasty'},
      {:id => 'line'},
      {:id => 9, :name => 'อาหาร', :search_word => 'อาหาร', :search_word_fb => 'restaurant'},
      {:id => 10, :name => 'คาเฟ่', :search_word => 'คาเฟ่', :search_word_fb => 'cafe'},
      {:id => 15, :name => 'พิพิธภัณฑ์', :search_word => 'พิพิธภัณฑ์', :search_word_fb => 'museum'},
      {:id => 16, :name => 'ตลาด', :search_word => 'ตลาด', :search_word_fb => 'market'},
      {:id => 'line'},
      {:id => 30, :name => 'ถนน', :search_word => 'ถนน', :search_word_fb => 'street'},
      {:id => 31, :name => 'ท่องเที่ยว', :search_word => 'ท่องเที่ยว', :search_word_fb => 'travel'},
      {:id => 'line'},
      {:id => 98, :name => 'Feedback', :search_word => 'feedback', :search_word_fb => 'settings'},
      {:id => 99, :name => 'Settings', :search_word => 'settings', :search_word_fb => 'settings'},
    ]

    config.localpicks_jp_sidebar_two = [
      {:id => 1, :name => 'Now'},
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Nearby", 
        :sub_menu => [
            {:id => 1, :name => 'レストラン', :type => 'nearby', :search_word_social => 'レストラン' , :search_word => 'restaurant', :search_word_fb => 'restaurant'},
            {:id => 2, :name => 'フードコート', :type => 'nearby',:search_word_social => 'フードコート' , :search_word => 'food court', :search_word_fb => 'food court'},
            {:id => 'line'},
            {:id => 3, :name => '中華', :type => 'nearby',:search_word_social => '中華' , :search_word => 'chinese restaurant', :search_word_fb => 'chinese restaurant'},
            {:id => 4, :name => 'フレンチ', :type => 'nearby',:search_word_social => 'フレンチ' , :search_word => 'french restaurant', :search_word_fb => 'french restaurant'},
            {:id => 5, :name => 'ドイツ料理', :type => 'nearby',:search_word_social => 'ドイツ料理' , :search_word => 'german restaurant', :search_word_fb => 'german restaurant'},
            {:id => 6, :name => 'ギリシャ料理', :type => 'nearby',:search_word_social => 'ギリシャ料理' , :search_word => 'greek restaurant', :search_word_fb => 'greek restaurant'},
            {:id => 7, :name => 'インド料理', :type => 'nearby',:search_word_social => 'インド料理' , :search_word => 'indian restaurant', :search_word_fb => 'indian restaurant'},
            {:id => 8, :name => 'イタリアン', :type => 'nearby',:search_word_social => 'イタリアン' , :search_word => 'italian restaurant', :search_word_fb => 'italian restaurant'},
            {:id => 9, :name => '日本料理', :type => 'nearby',:search_word_social => '日本料理' , :search_word => 'japanese restaurant', :search_word_fb => 'japanese restaurant'},
            {:id => 10, :name => '韓国料理', :type => 'nearby',:search_word_social => '韓国料理' , :search_word => 'korean restaurant', :search_word_fb => 'korean restaurant'},
            {:id => 11, :name => 'メキシコ料理', :type => 'nearby',:search_word_social => 'メキシコ料理' , :search_word => 'mexican restaurant', :search_word_fb => 'mexican restaurant'},
            {:id => 12, :name => '中東料理', :type => 'nearby', :search_word_social => '中東料理' ,:search_word => 'middle eastern restaurant', :search_word_fb => 'middle eastern restaurant'},
            {:id => 13, :name => 'スペイン料理', :type => 'nearby', :search_word_social => 'スペイン料理', :search_word => 'spanish restaurant', :search_word_fb => 'spanish restaurant'},
            {:id => 14, :name => 'タイ料理', :type => 'nearby',:search_word_social => 'タイ料理' , :search_word => 'thai restaurant', :search_word_fb => 'thai restaurant'},
            {:id => 15, :name => 'ベトナム料理', :type => 'nearby',:search_word_social => 'ベトナム料理' , :search_word => 'vietnamese restaurant', :search_word_fb => 'vietnamese restaurant'},
            {:id => 32, :name => 'インドネシア料理', :type => 'nearby',:search_word_social => 'インドネシア料理' , :search_word => 'indonesian restaurant', :search_word_fb => 'indonesian restaurant'},
            {:id => 34, :name => 'ロシア料理', :type => 'nearby',:search_word_social => 'ロシア料理' , :search_word => 'russian restaurant', :search_word_fb => 'russian restaurant'},
            {:id => 33, :name => 'ハラル', :type => 'nearby',:search_word_social => 'ハラル' ,:search_word => 'halal', :search_word_fb => 'halal'},
            {:id => 73, :name => 'オーガニック', :type => 'nearby',:search_word_social => 'オーガニック' ,:search_word => 'organic', :search_word_fb => 'organic'},
            {:id => 'line'},
            {:id => 16, :name => '寿司', :type => 'nearby',:search_word_social => '寿司' , :search_word => 'sushi', :search_word_fb => 'sushi'},
            {:id => 17, :name => 'ラーメン', :type => 'nearby',:search_word_social => 'ラーメン' , :search_word => 'ramen', :search_word_fb => 'ramen'},
            {:id => 18, :name => 'ステーキ', :type => 'nearby',:search_word_social => 'ステーキ' , :search_word => 'steak', :search_word_fb => 'steak'},
            {:id => 19, :name => 'バーガー', :type => 'nearby',:search_word_social => 'バーガー' , :search_word => 'burger', :search_word_fb => 'burger'},
            {:id => 71, :name => 'パスタ', :type => 'nearby', :search_word_social => 'パスタ',:search_word => 'pasta', :search_word_fb => 'pasta'},
            {:id => 72, :name => 'ピザ', :type => 'nearby', :search_word_social => 'ピザ',:search_word => 'pizza', :search_word_fb => 'pizza'},
            {:id => 20, :name => 'ビストロ', :type => 'nearby',:search_word_social => 'ビストロ' , :search_word => 'bistro', :search_word_fb => 'bistro'},
            {:id => 21, :name => 'タパス', :type => 'nearby',:search_word_social => 'タパス' , :search_word => 'tapas', :search_word_fb => 'tapas'},
            {:id => 'line'},
            {:id => 22, :name => 'カフェ', :type => 'nearby',:search_word_social => 'カフェ' , :search_word => 'cafe', :search_word_fb => 'cafe'},
            {:id => 23, :name => 'バー', :type => 'nearby',:search_word_social => 'バー' , :search_word => 'bar', :search_word_fb => 'bar'},
            {:id => 31, :name => 'ワイン', :type => 'nearby',:search_word_social => 'ワイン' , :search_word => 'wine', :search_word_fb => 'wine'},
            {:id => 24, :name => 'ホテル', :type => 'nearby',:search_word_social => 'ホテル' , :search_word => 'hotel', :search_word_fb => 'hotel'},
            {:id => 25, :name => 'スパ', :type => 'nearby',:search_word_social => 'スパ', :search_word => 'spa', :search_word_fb => 'spa'},
            {:id => 26, :name => 'ショッピング', :type => 'nearby',:search_word_social => 'ショッピング' , :search_word => 'shopping', :search_word_fb => 'shopping'},
            {:id => 26, :name => 'お土産', :type => 'nearby',:search_word_social => 'お土産' , :search_word => 'gift', :search_word_fb => 'gift'},
            {:id => 26, :name => 'ジュエリー', :type => 'nearby',:search_word_social => 'ジュエリー' , :search_word => 'jewelry', :search_word_fb => 'jewelry'},
            {:id => 26, :name => '雑貨', :type => 'nearby',:search_word_social => '雑貨' , :search_word => 'decor', :search_word_fb => 'decor'},
            {:id => 27, :name => 'スーパーマーケット', :type => 'nearby',:search_word_social => 'スーパーマーケット' , :search_word => 'supermarket', :search_word_fb => 'supermarket'},
            {:id => 28, :name => 'マーケット', :type => 'nearby',:search_word_social => 'マーケット' , :search_word => 'market', :search_word_fb => 'market'},
            {:id => 71, :name => '病院', :type => 'nearby',:search_word_social => '病院' , :search_word => 'hospital', :search_word_fb => 'hospital'},
            {:id => 71, :name => '薬局', :type => 'nearby',:search_word_social => '薬局' , :search_word => 'pharmacy', :search_word_fb => 'pharmacy'},
            {:id => 29, :name => 'ミュージアム', :type => 'nearby',:search_word_social => 'ミュージアム' , :search_word => 'museum', :search_word_fb => 'museum'},
            {:id => 31, :name => '映画', :type => 'nearby',:search_word_social => '映画' , :search_word => 'cinema', :search_word_fb => 'cinema'},
            {:id => 30, :name => 'ナイトクラブ', :type => 'nearby',:search_word_social => 'ナイトクラブ' , :search_word => 'nightclub', :search_word_fb => 'nightclub'},
        ]
      },
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "People", 
        :sub_menu => [
            {:id => 31, :cat_id => 999, :hot_news => 1, :name => 'トップ', :type => 'news',:search_word_social => 'トップ' , :search_word => 'top', :search_word_fb => 'top'},
            #{:id => 32, :cat_id => 14,  :hot_news => 0, :name => 'まとめ', :type => 'news',:search_word_social => 'まとめ' , :search_word => 'matome', :search_word_fb => 'matome'},
            {:id => 33, :cat_id => 1, :hot_news => 0, :name => 'ニュース', :type => 'news',:search_word_social => 'ニュース' , :search_word => 'news', :search_word_fb => 'news'},
            {:id => 34, :cat_id => 2, :hot_news => 0, :name => 'グルメ', :type => 'news',:search_word_social => 'グルメ' , :search_word => 'gourmet', :search_word_fb => 'gourmet'},
            #{:id => 35, :cat_id => 16, :hot_news => 0, :name => 'スポット', :type => 'news',:search_word_social => 'スポット' , :search_word => 'things to do', :search_word_fb => 'things to do'},
            {:id => 36, :cat_id => 3, :hot_news => 0, :name => 'ファッション', :type => 'news',:search_word_social => 'ファッション' , :search_word => 'fashion', :search_word_fb => 'fashion'},
            {:id => 37, :cat_id => 4, :hot_news => 0, :name => '美容', :type => 'news',:search_word_social => '美容' , :search_word => 'beauty', :search_word_fb => 'beauty'},
            #{:id => 38, :cat_id => 99, :hot_news => 0, :name => 'テクノロジー', :type => 'news',:search_word_social => 'テクノロジー' , :search_word => 'tech', :search_word_fb => 'tech'},
            {:id => 39, :cat_id => 6, :hot_news => 0, :name => 'トラベル', :type => 'news',:search_word_social => 'トラベル' , :search_word => 'travel', :search_word_fb => 'travel'},
            {:id => 40, :cat_id => 9, :hot_news => 0, :name => 'スポーツ', :type => 'news',:search_word_social => 'スポーツ' , :search_word => 'sports', :search_word_fb => 'sports'},
            {:id => 41, :cat_id => 10, :hot_news => 0, :name => 'エンタメ', :type => 'news',:search_word_social => 'エンタメ' , :search_word => 'entertainment', :search_word_fb => 'entertainment'},
            {:id => 42, :cat_id => 12, :hot_news => 0, :name => '生活', :type => 'news',:search_word_social => '生活' , :search_word => 'lifestyle', :search_word_fb => 'lifestyle'},
            {:id => 43, :cat_id => 13, :hot_news => 0, :name => '学ぶ', :type => 'news',:search_word_social => '学ぶ' , :search_word => 'learning', :search_word_fb => 'learning'},
        ]
      },
      {:id => 'line'},
      {:id => 'head', :name => "Events"},
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Tweets",
        :sub_menu => [
            {:id => 44, :name => 'ローカル', :type => 'tweets',:search_word_social => 'local' , :search_word => 'local', :search_word_fb => 'local'},
            {:id => 45, :name => 'ストリート', :type => 'tweets',:search_word_social => 'ストリート' , :search_word => 'street', :search_word_fb => 'street'},
            {:id => 46, :name => '観光', :type => 'tweets',:search_word_social => '観光' , :search_word => 'travel', :search_word_fb => 'travel'},
            {:id => 47, :name => '動画', :type => 'tweets',:search_word_social => 'filter:videos' , :search_word => 'filter:videos', :search_word_fb => 'filter:videos'},
            {:id => 48, :name => 'ベスト', :type => 'tweets',:search_word_social => 'ベスト' , :search_word => 'best', :search_word_fb => 'best'},
            {:id => 49, :name => 'おすすめ', :type => 'tweets',:search_word_social => 'おすすめ' , :search_word => 'must', :search_word_fb => 'must'},
            {:id => 50, :name => 'ランキング', :type => 'tweets',:search_word_social => 'ランキング' , :search_word => 'ranking', :search_word_fb => 'ranking'},
            {:id => 51, :name => '事件', :type => 'tweets',:search_word_social => '事件' , :search_word => 'incidents', :search_word_fb => 'incidents'},
            {:id => 52, :name => '事故', :type => 'tweets',:search_word_social => '事故' , :search_word => 'accidents', :search_word_fb => 'accidents'},
            {:id => 53, :name => 'ブログ', :type => 'tweets',:search_word_social => 'ブログ' , :search_word => 'blogs', :search_word_fb => 'blogs'},
            {:id => 54, :name => '日本人', :type => 'tweets',:search_word_social => '日本人' , :search_word => 'japanese', :search_word_fb => 'japanese'},
            {:id => 55, :name => '美味しい', :type => 'tweets', :search_word_social => 'おいしい' ,:search_word => 'delicious', :search_word_fb => 'delicious'},
        ]
      },
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Lifestyle",
        :sub_menu => [
          {:id => 56, :name => 'グルメ', :type => 'lifestyle', :search_word_social => 'グルメ' ,:search_word => 'gourmet', :search_word_fb => 'gourmet'},
          {:id => 57, :name => 'ファッション', :type => 'lifestyle', :search_word_social => 'ファッション' ,:search_word => 'gourmet', :search_word_fb => 'gourmet'},
          {:id => 58, :name => '生活', :type => 'lifestyle', :search_word_social => '生活' ,:search_word => 'lifestyle', :search_word_fb => 'lifestyle'},
          {:id => 59, :name => '不動産', :type => 'lifestyle', :search_word_social => '不動産' ,:search_word => 'property', :search_word_fb => 'property'},
          {:id => 60, :name => 'インテリア', :type => 'lifestyle',:search_word_social => 'インテリア' , :search_word => 'interior', :search_word_fb => 'interior'},
          {:id => 61, :name => 'ギャラリー', :type => 'lifestyle', :search_word_social => 'ギャラリー' ,:search_word => 'gallery', :search_word_fb => 'gallery'},
          {:id => 71, :name => 'アクティビティ', :type => 'lifestyle', :search_word_social => 'アクティビティ' ,:search_word => 'activities', :search_word_fb => 'activities'},
          {:id => 62, :name => '美術館', :type => 'lifestyle', :search_word_social => '美術館' ,:search_word => 'art museum', :search_word_fb => 'art museum'},
          {:id => 63, :name => '博物館', :type => 'lifestyle', :search_word_social => '博物館' ,:search_word => 'other museum', :search_word_fb => 'other museum'},
          {:id => 64, :name => '駐在', :type => 'lifestyle', :search_word_social => '駐在' ,:search_word => 'expat', :search_word_fb => 'expat'},
        ]
      },
      {:id => 'line'},
      {:id => 65, :name => 'アート', :type => 'gnews', :search_word_social => 'アート' ,:search_word => 'art', :search_word_fb => 'art'},
      {:id => 66, :name => '展覧会', :type => 'gnews', :search_word_social => '展覧会' ,:search_word => 'exhibition', :search_word_fb => 'exhibition'},
      {:id => 67, :name => 'フェスティバル', :type => 'gnews', :search_word_social => 'フェスティバル' ,:search_word => 'festival', :search_word_fb => 'festival'},
      {:id => 68, :name => 'コンサート', :type => 'gnews', :search_word_social => 'コンサート' ,:search_word => 'concert', :search_word_fb => 'concert'},
      {:id => 'line'},
      {:id => 69, :name => 'フィードバック', :type => 'feedback', :search_word_social => 'フィードバック' ,:search_word => 'feedback', :search_word_fb => 'feedback'},
      {:id => 70, :name => '設定', :type => 'setting', :search_word_social => '設定' ,:search_word => 'setting', :search_word_fb => 'setting'},
    ]

    config.localpicks_en_sidebar_two = [
      {:id => 1, :name => 'Now'},
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Nearby",
        :sub_menu => [
            {:id => 1, :name => 'Restaurants', :type => 'nearby', :search_word_social => 'restaurant' , :search_word => 'restaurant', :search_word_fb => 'restaurant'},
            {:id => 2, :name => 'Food Courts', :type => 'nearby', :search_word_social => 'food court' , :search_word => 'food court', :search_word_fb => 'food court'},
            {:id => 'line'},
            {:id => 3, :name => 'Chinese', :type => 'nearby',:search_word_social => 'chinese restaurant' , :search_word => 'chinese restaurant', :search_word_fb => 'chinese restaurant'},
            {:id => 4, :name => 'French', :type => 'nearby',:search_word_social => 'french restaurant' , :search_word => 'french restaurant', :search_word_fb => 'french restaurant'},
            {:id => 5, :name => 'German', :type => 'nearby',:search_word_social => 'german restaurant' , :search_word => 'german restaurant', :search_word_fb => 'german restaurant'},
            {:id => 6, :name => 'Greek', :type => 'nearby', :search_word_social => 'greek restaurant' ,:search_word => 'greek restaurant', :search_word_fb => 'greek restaurant'},
            {:id => 7, :name => 'Indian', :type => 'nearby',:search_word_social => 'indian restaurant' , :search_word => 'indian restaurant', :search_word_fb => 'indian restaurant'},
            {:id => 8, :name => 'Italian', :type => 'nearby',:search_word_social => 'italian restaurant' , :search_word => 'italian restaurant', :search_word_fb => 'italian restaurant'},
            {:id => 9, :name => 'Japanese', :type => 'nearby', :search_word_social => 'japanese restaurant' ,:search_word => 'japanese restaurant', :search_word_fb => 'japanese restaurant'},
            {:id => 10, :name => 'Korean', :type => 'nearby', :search_word_social => 'korean restaurant'  ,:search_word => 'korean restaurant', :search_word_fb => 'korean restaurant'},
            {:id => 11, :name => 'Mexican', :type => 'nearby',:search_word_social => 'mexican restaurant' , :search_word => 'mexican restaurant', :search_word_fb => 'mexican restaurant'},
            {:id => 12, :name => 'Middle Eastern', :type => 'nearby',:search_word_social => 'middle eastern restaurant' , :search_word => 'middle eastern restaurant', :search_word_fb => 'middle eastern restaurant'},
            {:id => 13, :name => 'Spanish', :type => 'nearby', :search_word_social => 'spanish restaurant' ,:search_word => 'spanish restaurant', :search_word_fb => 'spanish restaurant'},
            {:id => 14, :name => 'Thai', :type => 'nearby',:search_word_social => 'thai restaurant' , :search_word => 'thai restaurant', :search_word_fb => 'thai restaurant'},
            {:id => 15, :name => 'Vietnamese', :type => 'nearby',:search_word_social => 'vietnamese restaurant' , :search_word => 'vietnamese restaurant', :search_word_fb => 'vietnamese restaurant'},
            {:id => 32, :name => 'Indonesian', :type => 'nearby',:search_word_social => 'indonesian restaurant' , :search_word => 'indonesian restaurant', :search_word_fb => 'indonesian restaurant'},
            {:id => 34, :name => 'Russian', :type => 'nearby',:search_word_social => 'russian restaurant' , :search_word => 'russian restaurant', :search_word_fb => 'russian restaurant'},
            {:id => 33, :name => 'Halal', :type => 'nearby',:search_word_social => 'halal' , :search_word => 'halal', :search_word_fb => 'halal'},
            {:id => 73, :name => 'Organic', :type => 'nearby',:search_word_social => 'organic' ,:search_word => 'organic', :search_word_fb => 'organic'},
            {:id => 'line'},
            {:id => 16, :name => 'Sushi', :type => 'nearby',:search_word_social => 'sushi' , :search_word => 'sushi', :search_word_fb => 'sushi'},
            {:id => 17, :name => 'Ramen', :type => 'nearby',:search_word_social => 'ramen' , :search_word => 'ramen', :search_word_fb => 'ramen'},
            {:id => 18, :name => 'Steak', :type => 'nearby', :search_word_social => 'steak' , :search_word => 'steak', :search_word_fb => 'steak'},
            {:id => 19, :name => 'Burger', :type => 'nearby',:search_word_social => 'burger' , :search_word => 'burger', :search_word_fb => 'burger'},
            {:id => 71, :name => 'Pasta', :type => 'nearby', :search_word_social => 'pasta',:search_word => 'pasta', :search_word_fb => 'pasta'},
            {:id => 72, :name => 'Pizza', :type => 'nearby', :search_word_social => 'pizza',:search_word => 'pizza', :search_word_fb => 'pizza'},
            {:id => 20, :name => 'Bistro', :type => 'nearby',:search_word_social => 'bistro' , :search_word => 'bistro', :search_word_fb => 'bistro'},
            {:id => 21, :name => 'Tapas', :type => 'nearby', :search_word_social => 'tapas' ,:search_word => 'tapas', :search_word_fb => 'tapas'},
            {:id => 'line'},
            {:id => 22, :name => 'Cafe', :type => 'nearby',:search_word_social => 'cafe' , :search_word => 'cafe', :search_word_fb => 'cafe'},
            {:id => 23, :name => 'Bar', :type => 'nearby',:search_word_social => 'bar' , :search_word => 'bar', :search_word_fb => 'bar'},
            {:id => 31, :name => 'Wine', :type => 'nearby',:search_word_social => 'wine' , :search_word => 'wine', :search_word_fb => 'wine'},
            {:id => 24, :name => 'Hotel', :type => 'nearby',:search_word_social => 'hotel' , :search_word => 'hotel', :search_word_fb => 'hotel'},
            {:id => 25, :name => 'Spa', :type => 'nearby',:search_word_social => 'spa' , :search_word => 'spa', :search_word_fb => 'spa'},
            {:id => 26, :name => 'Shopping', :type => 'nearby',:search_word_social => 'shopping' , :search_word => 'shopping', :search_word_fb => 'shopping'},
            {:id => 26, :name => 'Gift', :type => 'nearby',:search_word_social => 'gift' , :search_word => 'gift', :search_word_fb => 'gift'},
            {:id => 26, :name => 'Jewelry', :type => 'nearby',:search_word_social => 'jewelry' , :search_word => 'jewelry', :search_word_fb => 'jewelry'},
            {:id => 26, :name => 'Decor', :type => 'nearby',:search_word_social => 'decor' , :search_word => 'decor', :search_word_fb => 'decor'},
            {:id => 27, :name => 'Supermarket', :type => 'nearby',:search_word_social => 'supermarket' , :search_word => 'supermarket', :search_word_fb => 'supermarket'},
            {:id => 28, :name => 'Market', :type => 'nearby',:search_word_social => 'market' , :search_word => 'market', :search_word_fb => 'market'},
            {:id => 71, :name => 'Hospital', :type => 'nearby',:search_word_social => 'hospital' , :search_word => 'hospital', :search_word_fb => 'hospital'},
            {:id => 71, :name => 'Pharmacy', :type => 'nearby',:search_word_social => 'pharmacy' , :search_word => 'pharmacy', :search_word_fb => 'pharmacy'},
            {:id => 29, :name => 'Museum', :type => 'nearby',:search_word_social => 'museum' , :search_word => 'museum', :search_word_fb => 'museum'},
            {:id => 31, :name => 'Cinema', :type => 'nearby',:search_word_social => 'cinema' , :search_word => 'cinema', :search_word_fb => 'cinema'},
            {:id => 30, :name => 'Nightclub', :type => 'nearby',:search_word_social => 'nightclub' , :search_word => 'nightclub', :search_word_fb => 'nightclub'},
        ]
      },
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "People",
        :sub_menu => [
            {:id => 31, :cat_id => 999, :hot_news => 1,:name => 'Top', :type => 'news', :search_word_social => 'top' ,:search_word => 'top', :search_word_fb => 'top'},
            #{:id => 32, :cat_id => 14, :hot_news => 0,:name => 'Matome', :type => 'news', :search_word_social => 'matome' ,:search_word => 'matome', :search_word_fb => 'matome'},
            {:id => 33, :cat_id => 1, :hot_news => 0,:name => 'News', :type => 'news', :search_word_social => 'news' ,:search_word => 'news', :search_word_fb => 'news'},
            {:id => 34, :cat_id => 2, :hot_news => 0,:name => 'Gourmet', :type => 'news', :search_word_social => 'gourmet' ,:search_word => 'gourmet', :search_word_fb => 'gourmet'},
            #{:id => 35, :cat_id => 16, :hot_news => 0,:name => 'Things to Do', :type => 'news', :search_word_social => 'things to do' ,:search_word => 'things to do', :search_word_fb => 'things to do'},
            {:id => 36, :cat_id => 3, :hot_news => 0,:name => 'Fashion', :type => 'news', :search_word_social => 'fashion' ,:search_word => 'fashion', :search_word_fb => 'fashion'},
            {:id => 37, :cat_id => 4, :hot_news => 0,:name => 'Beauty', :type => 'news', :search_word_social => 'beauty' ,:search_word => 'beauty', :search_word_fb => 'beauty'},
            #{:id => 38, :cat_id => 99, :hot_news => 0,:name => 'Tech', :type => 'news', :search_word_social => 'tech' ,:search_word => 'tech', :search_word_fb => 'tech'},
            {:id => 39, :cat_id => 6, :hot_news => 0,:name => 'Travel', :type => 'news', :search_word_social => 'travel' ,:search_word => 'travel', :search_word_fb => 'travel'},
            {:id => 40, :cat_id => 9, :hot_news => 0,:name => 'Sports', :type => 'news', :search_word_social => 'sports' ,:search_word => 'sports', :search_word_fb => 'sports'},
            {:id => 41, :cat_id => 10, :hot_news => 0,:name => 'Entertainment', :type => 'news', :search_word_social => 'entertainment' ,:search_word => 'entertainment', :search_word_fb => 'entertainment'},
            {:id => 42, :cat_id => 12, :hot_news => 0,:name => 'Lifestyle', :type => 'news', :search_word_social => 'lifestyle' ,:search_word => 'lifestyle', :search_word_fb => 'lifestyle'},
            {:id => 43, :cat_id => 13, :hot_news => 0,:name => 'Learning', :type => 'news', :search_word_social => 'learning' ,:search_word => 'learning', :search_word_fb => 'learning'},
        ]
      },
      {:id => 'line'},
      {:id => 'head', :name => "Events"},
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Tweets",
        :sub_menu => [
            {:id => 44, :name => 'Local', :type => 'tweets', :search_word_social => 'local', :search_word => 'local', :search_word_fb => 'local'},
            {:id => 45, :name => 'Street', :type => 'tweets', :search_word_social => 'street', :search_word => 'street', :search_word_fb => 'street'},
            {:id => 46, :name => 'Travel', :type => 'tweets', :search_word_social => 'travel', :search_word => 'travel', :search_word_fb => 'travel'},
            {:id => 47, :name => 'Video', :type => 'tweets', :search_word_social => 'filter:videos', :search_word => 'filter:videos', :search_word_fb => 'filter:videos'},
            {:id => 48, :name => 'Best', :type => 'tweets', :search_word_social => 'best', :search_word => 'best', :search_word_fb => 'best'},
            {:id => 49, :name => 'Must', :type => 'tweets', :search_word_social => 'must', :search_word => 'must', :search_word_fb => 'must'},
            {:id => 50, :name => 'Ranking', :type => 'tweets', :search_word_social => 'ranking', :search_word => 'ranking', :search_word_fb => 'ranking'},
            {:id => 51, :name => 'Incidents', :type => 'tweets', :search_word_social => 'incidents', :search_word => 'incidents', :search_word_fb => 'incidents'},
            {:id => 52, :name => 'Accidents', :type => 'tweets', :search_word_social => 'accidents', :search_word => 'accidents', :search_word_fb => 'accidents'},
            {:id => 53, :name => 'Blogs', :type => 'tweets', :search_word_social => 'blogs', :search_word => 'blogs', :search_word_fb => 'blogs'},
            {:id => 54, :name => 'Delicious', :type => 'tweets', :search_word_social => 'delicious', :search_word => 'delicious', :search_word_fb => 'delicious'},
        ]
      },
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Lifestyle",
        :sub_menu => [
            {:id => 56, :name => 'Gourmet', :type => 'lifestyle', :search_word_social => 'gourmet',:search_word => 'gourmet', :search_word_fb => 'gourmet'},
            {:id => 57, :name => 'Fashion', :type => 'lifestyle', :search_word_social => 'fashion',:search_word => 'fashion', :search_word_fb => 'fashion'},
            {:id => 58, :name => 'Lifestyle', :type => 'lifestyle', :search_word_social => 'lifestyle',:search_word => 'lifestyle', :search_word_fb => 'lifestyle'},
            {:id => 59, :name => 'Property', :type => 'lifestyle', :search_word_social => 'property',:search_word => 'property', :search_word_fb => 'property'},
            {:id => 60, :name => 'Interior', :type => 'lifestyle', :search_word_social => 'interior',:search_word => 'interior', :search_word_fb => 'interior'},
            {:id => 61, :name => 'Gallery', :type => 'lifestyle', :search_word_social => 'gallery',:search_word => 'gallery', :search_word_fb => 'gallery'},
            {:id => 71, :name => 'Activities', :type => 'gnews', :search_word_social => 'activities',:search_word => 'activities', :search_word_fb => 'activities'},
            {:id => 62, :name => 'Expat', :type => 'lifestyle', :search_word_social => 'expat',:search_word => 'expat', :search_word_fb => 'expat'},
        ]
      },
      {:id => 'line'},
      {:id => 65, :name => 'Art', :type => 'gnews', :search_word_social => 'art',:search_word => 'art', :search_word_fb => 'art'},
      {:id => 66, :name => 'Exhibition', :type => 'gnews', :search_word_social => 'exhibition',:search_word => 'exhibition', :search_word_fb => 'exhibition'},
      {:id => 67, :name => 'Festival', :type => 'gnews', :search_word_social => 'festival',:search_word => 'festival', :search_word_fb => 'festival'},
      {:id => 68, :name => 'Concert', :type => 'gnews', :search_word_social => 'concert',:search_word => 'concert', :search_word_fb => 'concert'},
      {:id => 'line'},
      {:id => 69, :name => 'Feedback', :type => 'feedback', :search_word_social => 'feedback',:search_word => 'feedback', :search_word_fb => 'feedback'},
      {:id => 70, :name => 'Settings', :type => 'setting', :search_word_social => 'settings',:search_word => 'settings', :search_word_fb => 'setting'},
    ]

    config.localpicks_th_sidebar_two = [
      {:id => 1, :name => 'Now'},
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Nearby",
        :sub_menu => [
          {:id => 1, :name => 'ภัตตาคาร', :type => 'nearby', :search_word_social => 'ภัตตาคาร',:search_word => 'restaurant', :search_word_fb => 'restaurant'},
          {:id => 2, :name => 'ศูนย์อาหาร', :type => 'nearby', :search_word_social => 'ศูนย์อาหาร',:search_word => 'food court', :search_word_fb => 'food court'},
          {:id => 'line'},
          {:id => 3, :name => 'อาหารจีน', :type => 'nearby', :search_word_social => 'อาหารจีน',:search_word => 'chinese restaurant', :search_word_fb => 'chinese restaurant'},
          {:id => 4, :name => 'อาหารฝรั่งเศส', :type => 'nearby', :search_word_social => 'อาหารฝรั่งเศส',:search_word => 'french restaurant', :search_word_fb => 'french restaurant'},
          {:id => 5, :name => 'อาหารเยอรมัน', :type => 'nearby', :search_word_social => 'อาหารเยอรมัน',:search_word => 'german restaurant', :search_word_fb => 'german restaurant'},
          {:id => 6, :name => 'อาหารกรีก', :type => 'nearby', :search_word_social => 'อาหารกรีก',:search_word => 'greek restaurant', :search_word_fb => 'greek restaurant'},
          {:id => 7, :name => 'อาหารอินเดีย', :type => 'nearby', :search_word_social => 'อาหารอินเดีย',:search_word => 'indian restaurant', :search_word_fb => 'indian restaurant'},
          {:id => 8, :name => 'อาหารอิตาเลียน', :type => 'nearby', :search_word_social => 'อาหารอิตาเลียน',:search_word => 'italian restaurant', :search_word_fb => 'italian restaurant'},
          {:id => 9, :name => 'อาหารญี่ปุ่่น', :type => 'nearby', :search_word_social => 'อาหารญี่ปุ่่น',:search_word => 'japanese restaurant', :search_word_fb => 'japanese restaurant'},
          {:id => 10, :name => 'อาหารเกาหลี', :type => 'nearby', :search_word_social => 'อาหารเกาหลี',:search_word => 'korean restaurant', :search_word_fb => 'korean restaurant'},
          {:id => 11, :name => 'อาหารเม็กซิกัน', :type => 'nearby', :search_word_social => 'อาหารเม็กซิกัน',:search_word => 'mexican restaurant', :search_word_fb => 'mexican restaurant'},
          {:id => 12, :name => 'อาหารอาหรับ', :type => 'nearby', :search_word_social => 'อาหารอาหรับ',:search_word => 'middle eastern restaurant', :search_word_fb => 'middle eastern restaurant'},
          {:id => 13, :name => 'อาหารสเปน', :type => 'nearby', :search_word_social => 'อาหารสเปน',:search_word => 'spanish restaurant', :search_word_fb => 'spanish restaurant'},
          {:id => 14, :name => 'อาหารไทย', :type => 'nearby', :search_word_social => 'อาหารไทย',:search_word => 'thai restaurant', :search_word_fb => 'thai restaurant'},
          {:id => 15, :name => 'อาหารเวียดนาม', :type => 'nearby', :search_word_social => 'อาหารเวียดนาม',:search_word => 'vietnamese restaurant', :search_word_fb => 'vietnamese restaurant'},
          {:id => 32, :name => 'อาหารอินโดนีเซีย', :type => 'nearby', :search_word_social => 'อาหารอินโดนีเซีย',:search_word => 'indonesian restaurant', :search_word_fb => 'indonesian restaurant'},
          {:id => 34, :name => 'อาหารรัสเซีย', :type => 'nearby', :search_word_social => 'อาหารรัสเซีย',:search_word => 'russian restaurant', :search_word_fb => 'russian restaurant'},
          {:id => 33, :name => 'อาหารฮาลาล', :type => 'nearby', :search_word_social => 'อาหารฮาลาล',:search_word => 'halal', :search_word_fb => 'halal'},
          {:id => 73, :name => 'อาหารสุขภาพ', :type => 'nearby',:search_word_social => 'อาหารสุขภาพ' ,:search_word => 'organic', :search_word_fb => 'organic'},
          {:id => 'line'},
          {:id => 16, :name => 'ซูชิ', :type => 'nearby', :search_word_social => 'ซูชิ',:search_word => 'sushi', :search_word_fb => 'sushi'},
          {:id => 17, :name => 'ราเมน', :type => 'nearby', :search_word_social => 'สเต็ก',:search_word => 'ramen', :search_word_fb => 'ramen'},
          {:id => 18, :name => 'สเต็ก', :type => 'nearby', :search_word_social => 'สเต็ก',:search_word => 'steak', :search_word_fb => 'steak'},
          {:id => 19, :name => 'เบอร์เกอร์', :type => 'nearby', :search_word_social => 'เบอร์เกอร์',:search_word => 'burger', :search_word_fb => 'burger'},
          {:id => 71, :name => 'พาสต้า', :type => 'nearby', :search_word_social => 'พาสต้า',:search_word => 'pasta', :search_word_fb => 'pasta'},
          {:id => 72, :name => 'พิซซ่า', :type => 'nearby', :search_word_social => 'พิซซ่า',:search_word => 'pizza', :search_word_fb => 'pizza'},
          {:id => 20, :name => 'บิสโตร', :type => 'nearby', :search_word_social => 'บิสโตร',:search_word => 'bistro', :search_word_fb => 'bistro'},
          {:id => 21, :name => 'ทาปาส', :type => 'nearby', :search_word_social => 'ทาปาส',:search_word => 'tapas', :search_word_fb => 'tapas'},
          {:id => 'line'},
          {:id => 22, :name => 'คาเฟ่', :type => 'nearby', :search_word_social => 'คาเฟ่',:search_word => 'cafe', :search_word_fb => 'cafe'},
          {:id => 23, :name => 'บาร์', :type => 'nearby', :search_word_social => 'บาร์',:search_word => 'bar', :search_word_fb => 'bar'},
          {:id => 31, :name => 'ไวน์', :type => 'nearby', :search_word_social => 'ไวน์',:search_word => 'wine', :search_word_fb => 'wine'},
          {:id => 24, :name => 'โรงแรม', :type => 'nearby', :search_word_social => 'โรงแรม',:search_word => 'hotel', :search_word_fb => 'hotel'},
          {:id => 25, :name => 'สปา', :type => 'nearby', :search_word_social => 'สปา',:search_word => 'spa', :search_word_fb => 'spa'},
          {:id => 26, :name => 'ชอปปิ้ง', :type => 'nearby', :search_word_social => 'ชอปปิ้ง',:search_word => 'shopping', :search_word_fb => 'shopping'},
          {:id => 26, :name => 'ของขวัญ', :type => 'nearby',:search_word_social => 'ของขวัญ' , :search_word => 'gift', :search_word_fb => 'gift'},
          {:id => 26, :name => 'เพชรพลอย', :type => 'nearby',:search_word_social => 'เพชรพลอย' , :search_word => 'jewelry', :search_word_fb => 'jewelry'},
          {:id => 26, :name => 'ตกแต่ง', :type => 'nearby',:search_word_social => 'ตกแต่ง' , :search_word => 'decor', :search_word_fb => 'decor'},
          {:id => 27, :name => 'ซูเปอร์มาร์เก็ต', :type => 'nearby', :search_word_social => 'ซูเปอร์มาร์เก็ต',:search_word => 'supermarket', :search_word_fb => 'supermarket'},
          {:id => 28, :name => 'ตลาด', :type => 'nearby', :search_word_social => 'ตลาด',:search_word => 'market', :search_word_fb => 'market'},
          {:id => 71, :name => 'โรงพยาบาล', :type => 'nearby', :search_word_social => 'โรงพยาบาล',:search_word => 'hospital', :search_word_fb => 'hospital'},
          {:id => 71, :name => 'ร้านขายยา', :type => 'nearby', :search_word_social => 'ร้านขายยา',:search_word => 'pharmacy', :search_word_fb => 'pharmacy'},
          {:id => 29, :name => 'พิพิธภัณฑ์', :type => 'nearby', :search_word_social => 'พิพิธภัณฑ์',:search_word => 'museum', :search_word_fb => 'museum'},
          {:id => 31, :name => 'โรงหนัง', :type => 'nearby', :search_word_social => 'โรงหนัง',:search_word => 'cinema', :search_word_fb => 'cinema'},
          {:id => 30, :name => 'ไนท์คลับ', :type => 'nearby', :search_word_social => 'ไนท์คลับ',:search_word => 'nightclub', :search_word_fb => 'nightclub'},
        ]
      },
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "People",
        :sub_menu => [
          {:id => 31, :cat_id => 999, :hot_news => 1,:name => 'ข่าวเด่น', :type => 'news', :search_word_social => 'ข่าวเด่น',:search_word => 'top', :search_word_fb => 'top'},
          #{:id => 32, :cat_id => 14, :hot_news => 0,:name => 'ทั่วไป', :type => 'news', :search_word_social => 'ทั่วไป',:search_word => 'matome', :search_word_fb => 'matome'},
          {:id => 33, :cat_id => 1, :hot_news => 0,:name => 'ข่าว', :type => 'news', :search_word_social => 'ข่าว',:search_word => 'news', :search_word_fb => 'news'},
          {:id => 34, :cat_id => 2, :hot_news => 0,:name => 'อาหาร', :type => 'news', :search_word_social => 'อาหาร',:search_word => 'gourmet', :search_word_fb => 'gourmet'},
          #{:id => 35, :cat_id => 16, :hot_news => 0,:name => 'ทำอะไรดี', :type => 'news', :search_word_social => 'ทำอะไรดี',:search_word => 'things to do', :search_word_fb => 'things to do'},
          {:id => 36, :cat_id => 3, :hot_news => 0,:name => 'แฟชั่น', :type => 'news', :search_word_social => 'แฟชั่น',:search_word => 'fashion', :search_word_fb => 'fashion'},
          {:id => 37, :cat_id => 4, :hot_news => 0,:name => 'สวยงาม', :type => 'news', :search_word_social => 'สวยงาม',:search_word => 'beauty', :search_word_fb => 'beauty'},
          #{:id => 38, :cat_id => 99, :hot_news => 0,:name => 'เทคโนโลยี', :type => 'news', :search_word_social => 'เทคโนโลยี',:search_word => 'tech', :search_word_fb => 'tech'},
          {:id => 39, :cat_id => 6, :hot_news => 0,:name => 'ท่องเที่ยว', :type => 'news', :search_word_social => 'ท่องเที่ยว',:search_word => 'travel', :search_word_fb => 'travel'},
          {:id => 40, :cat_id => 9, :hot_news => 0,:name => 'กีฬา', :type => 'news', :search_word_social => 'กีฬา',:search_word => 'sports', :search_word_fb => 'sports'},
          {:id => 41, :cat_id => 10, :hot_news => 0,:name => 'เอนเตอร์เทนเมนต์', :type => 'news', :search_word_social => 'เอนเตอร์เทนเมนต์',:search_word => 'entertainment', :search_word_fb => 'entertainment'},
          {:id => 42, :cat_id => 12, :hot_news => 0,:name => 'ไลฟ์สไตล์', :type => 'news', :search_word_social => 'ไลฟ์สไตล์',:search_word => 'lifestyle', :search_word_fb => 'lifestyle'},
          {:id => 43, :cat_id => 13, :hot_news => 0,:name => 'เรียนรู้', :type => 'news', :search_word_social => 'เรียนรู้',:search_word => 'learning', :search_word_fb => 'learning'},
        ]
      },
      {:id => 'line'},
      {:id => 'head', :name => "Events"},
      {:id => 'line'},
      {
        :id => 'dropdown', 
        :name => "Tweets",
        :sub_menu => [
          {:id => 44, :name => 'ท้องถิ่น', :type => 'tweets', :search_word_social => 'local',:search_word => 'local', :search_word_fb => 'local'},
          {:id => 45, :name => 'ถนน', :type => 'tweets', :search_word_social => 'ถนน',:search_word => 'street', :search_word_fb => 'street'},
          {:id => 46, :name => 'ท่องเที่ยว', :type => 'tweets', :search_word_social => 'ท่องเที่ยว',:search_word => 'travel', :search_word_fb => 'travel'},
          {:id => 47, :name => 'วิดีโอ', :type => 'tweets', :search_word_social => 'filter:videos',:search_word => 'filter:videos', :search_word_fb => 'filter:videos'},
          {:id => 48, :name => 'Pantip', :type => 'tweets', :search_word_social => 'pantip',:search_word => 'pantip', :search_word_fb => 'pantip'},
          {:id => 49, :name => 'ที่สุด', :type => 'tweets', :search_word_social => 'ที่สุด',:search_word => 'best', :search_word_fb => 'best'},
          {:id => 51, :name => 'เหตุการณ์', :type => 'tweets', :search_word_social => 'เหตุการณ์',:search_word => 'incidents', :search_word_fb => 'incidents'},
          {:id => 52, :name => 'อุบัติเหตุ', :type => 'tweets', :search_word_social => 'อุบัติเหตุ',:search_word => 'accidents', :search_word_fb => 'accidents'},
          {:id => 53, :name => 'ไทย', :type => 'tweets', :search_word_social => 'ไทย',:search_word => 'thai', :search_word_fb => 'thai'},
          {:id => 54, :name => 'อร่อย', :type => 'tweets', :search_word_social => 'อร่อย',:search_word => 'delicious', :search_word_fb => 'delicious'},
        ]
      },
      {:id => 'line'},
      {:id => 69, :name => 'ความคิดเห็น', :type => 'feedback', :search_word_social => 'feedback',:search_word => 'feedback', :search_word_fb => 'feedback'},
      {:id => 70, :name => 'ตั้งค่า', :type => 'setting', :search_word_social => 'settings',:search_word => 'settings', :search_word_fb => 'setting'},
    ]

    config.region_menu_ios = [
       {:id => 0, :name => 'English', :code => 'en', :lang => 'en'},
       {:id => 1, :name => 'Japanese', :code => 'jp', :lang => 'ja'},
       {:id => 2, :name => 'Thai', :code => 'th', :lang => 'th'},
    ]

    ###################################################
    # for admin
    ###################################################
    config.admin_data_num = 50
    config.admin_request_date_format = '%Y/%m/%d %H:%M'
    config.admin_request_date_format_readable = 'yyyy/mm/dd HH:MM'
    config.admin_download_summary_types = [ 
      :app_driver, 
      #:fyber, 
      #:nativex, 
      #:super_rewards, 
      #:tyroo, 
      #:woobi, 
      #:personaly, 
      #:adcolony,
      #:aarki, 
      #:trialpay, 
      #:taptica, 
      :adatha, 
      :avazu,
      :mobvista,
    ]
    config.push_notification_title_length_max = 50
    config.push_notification_message_length_max = 200
    config.admin_offer_bulk_actions = %w(draft publish unpublish);
  end
end
