# config valid only for Capistrano 3.4
lock '3.4.0'

set :application, 'viral-giveaway-server'
set :repo_url, 'git@bitbucket.org:mylon/viral-giveaway-server.git'
set :branch, ENV['BRANCH']

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/ubuntu/app/viral-giveaway-server'

set :ssh_options, {
  forward_agent: true,
  keys: [
    File.join(ENV["HOME"], ".ssh", "viral-giveaway.pem"),
    File.join(ENV["HOME"], "ssh", "viral-giveaway.pem"),
  ],
#  verbose: :debug,
  user: 'ubuntu'
}

set :default_env, {
    path: "/home/ubuntu/local/ruby-2.2/bin:$PATH",
}

set :bundle_path, -> { shared_path.join('vendor/bundle') }

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/assets}

SSHKit.config.command_map[:rake] = 'bundle exec rake'

set :assets_roles, [:web, :app, :admin]    

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
      execute :sudo, "supervisorctl restart viral-giveaway-server-api"
    end

    on roles(:web), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
      execute :sudo, "supervisorctl restart viral-giveaway-server-web"
    end

    on roles(:admin), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
      execute :sudo, "supervisorctl restart viral-giveaway-server-admin"
      execute :sudo, "supervisorctl restart vg-sendmail-worker"
      execute :sudo, "supervisorctl restart vg-emr-job-worker"
      execute :sudo, "supervisorctl restart sidekiq"
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web, :admin), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      within release_path do
        execute :rake, 'tmp:cache:clear'
      end
    end
  end
end
