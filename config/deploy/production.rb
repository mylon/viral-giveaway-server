# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

#role :app,   %w{54.179.136.25 54.179.140.196}
#role :web,   %w{54.179.136.25 54.179.140.196}
role :app,   %w{54.179.136.25 54.179.140.196}
role :web,   %w{54.179.136.25}
#role :admin, %w{54.179.141.24}
#role :db,    %w{54.179.141.24}


# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

#server 'example.com', user: 'deploy', roles: %w{web app}, my_property: :my_value

namespace :deploy do
  task :published do
    on roles(:web, :admin), in: :sequence, wait: 5 do
      execute :cp, "-r #{release_path}/app/assets/templates/* #{release_path}/public/assets/"
      execute :cp, "-r #{release_path}/app/assets/javascripts/* #{release_path}/public/assets/"
      execute :cp, "-r #{release_path}/app/assets/stylesheets/* #{release_path}/public/assets/"
      execute :cp, "-r #{release_path}/app/assets/images/* #{release_path}/public/assets/"
    end
  end
end
