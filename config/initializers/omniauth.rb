if Rails.env.development? then
  OmniAuth.config.full_host = lambda do |env|
    scheme         = env['rack.url_scheme']
    local_host     = env['HTTP_HOST']
    forwarded_host = env['HTTP_X_FORWARDED_HOST']
    forwarded_port = env['HTTP_X_FORWARDED_PORT'] 
    forwarded_host.blank? ? "#{scheme}://#{local_host}:#{forwarded_port}" : "#{scheme}://#{forwarded_host}:#{forwarded_port}"
  end
else
  OmniAuth.config.full_host = lambda do |env|
    scheme         = env['rack.url_scheme']
    local_host     = env['HTTP_HOST']
    forwarded_host = env['HTTP_X_FORWARDED_HOST']
    forwarded_host.blank? ? "#{scheme}://#{local_host}" : "#{scheme}://#{forwarded_host}"
  end
end
