Sidekiq.configure_server do |config|
  config.redis = {
    url: Rails.application.config.sidekiq_queue_url,
    namespace: "sidekiq_#{Rails.env}", 
  }
end

Sidekiq.configure_client do |config|
  config.redis = {
    url: Rails.application.config.sidekiq_queue_url,
    namespace: "sidekiq_#{Rails.env}", 
  }
end
