class AutoexpireCacheRedis
  def initialize(store, ttl = 86400)
    @store = store
    @ttl = ttl
  end

  def [](url)
    @store.[](url)
  end

  def []=(url, value)
    @store.[]=(url, value)
    @store.expire(url, @ttl)
  end

  def keys
    @store.keys
  end

  def del(url)
    @store.del(url)
  end
end

Geocoder.configure(
  # geocoding options
  # :timeout      => 3,           # geocoding service timeout (secs)
  # :lookup       => :google,     # name of geocoding service (symbol)
  # :language     => :en,         # ISO-639 language code
  # :use_https    => false,       # use HTTPS for lookup requests? (if supported)
  # :http_proxy   => nil,         # HTTP proxy server (user:pass@host:port)
  # :https_proxy  => nil,         # HTTPS proxy server (user:pass@host:port)
  # :api_key      => 'AIzaSyCL260LpfI7iQUvpCmfHj0dfCjIlaimUr0',         # API key for geocoding service
  # :cache        => nil,         # cache object (must respond to #[], #[]=, and #keys)
  # :cache_prefix => "geocoder:", # prefix (string) to use for all cache keys

  # exceptions that should not be rescued by default
  # (if you want to implement custom error handling);
  # supports SocketError and TimeoutError
  # :always_raise => [],

  # calculation options
  # :units     => :mi,       # :km for kilometers or :mi for miles
  # :distances => :linear    # :spherical or :linear

  # geocoding service (see below for supported options):
  :lookup => :google,

  # to use an API key:
  :api_key => 'AIzaSyCL260LpfI7iQUvpCmfHj0dfCjIlaimUr0',

  # geocoding service request timeout, in seconds (default 3):
  :timeout => 5,

  # set default units to kilometers:
  :units => :km,

  :use_https => true,

  # caching (see below for details):
  :cache => AutoexpireCacheRedis.new(
    Redis.new(
      :host => Rails.application.config.geocoder_redis_host, 
      :port => Rails.application.config.geocoder_redis_port, 
      :db => Rails.application.config.geocoder_redis_db
   ), Rails.application.config.geocoder_redis_ttl),
  :cache_prefix => "..."
)
