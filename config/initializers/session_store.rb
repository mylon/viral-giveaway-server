# Be sure to restart your server when you modify this file.

#Rails.application.config.session_store :cookie_store, key: '_VG_session'
Rails.application.config.session_store :redis_store, 
  servers: Rails.application.config.session_servers, 
  expire_in: Rails.application.config.session_expire_in, 
  expire_after: Rails.application.config.session_expire_after
