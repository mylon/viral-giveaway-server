if Rails.env.development? or Rails.env.test? or Rails.env.staging? then
  require 'httplog'
  HttpLog.options[:logger] = Rails.logger
end
