class AutoexpireCacheRedis
  def initialize(store, ttl = 86400)
    @store = store
    @ttl = ttl
  end

  def [](url)
    @store.[](url)
  end

  def []=(url, value)
    @store.[]=(url, value)
    @store.expire(url, @ttl)
  end

  def keys
    @store.keys
  end

  def del(url)
    @store.del(url)
  end
end

Geocoder.configure(
  :cache => AutoexpireCacheRedis.new(
    Redis.new(
      :host => Rails.application.config.geocoder_redis_host, 
      :port => Rails.application.config.geocoder_redis_port, 
      :db => Rails.application.config.geocoder_redis_db
   ), Rails.application.config.geocoder_redis_ttl),
  :cache_prefix => "..."

)
