require 'rails_helper'

RSpec.describe Offer, :type => :model do
  describe "#humanize_payment" do
    subject { Offer.humanize_payment(point) }

    context "when point is just 1000" do
      let( :point ) { 1000 }
      it { is_expected.to eq "1k" }
    end
    context "when point is greater than 1000" do
      let( :point ) { 1100 }
      it { is_expected.to eq "1.1k" }
    end
    context "when point is less than 1000" do
      let( :point ) { 900 }
      it { is_expected.to eq "900" }
    end
  end

  describe "#special_payout?" do
    let!(:today) { Date.today }
    before do 
      Date.stub(:today).and_return(today)
    end
    subject { Offer.special_payout? }

    context "when sunday" do
      before { today.should_receive(:wday).and_return(0) } 
      it { is_expected.to eq false }
    end
    context "when monday" do
      before { today.should_receive(:wday).and_return(1) } 
      it { is_expected.to eq false }
    end
    context "when tuesday" do
      before { today.should_receive(:wday).and_return(2) } 
      it { is_expected.to eq false }
    end
    context "when wednesday" do
      before { today.should_receive(:wday).and_return(3) } 
      it { is_expected.to eq false }
    end
    context "when thursday" do
      before { today.should_receive(:wday).and_return(4) } 
      it { is_expected.to eq false }
    end
    context "when friday" do
      before { today.should_receive(:wday).and_return(5) } 
      it { is_expected.to eq false }
    end
    context "when saturday" do
      before { today.should_receive(:wday).and_return(6) } 
      it { is_expected.to eq false }
    end
  end
end
