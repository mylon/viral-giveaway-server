var app = window.App = angular.module('Vg', ['ngRoute','smart-table']);
app.run(function($rootScope, $location, Session, Api){
})
.constant('AUTH_EVENTS', {
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
})
.config(["$httpProvider", function($httpProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
}])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/',                              {templateUrl: '../assets/admin/login/index.html',                   controller: 'LoginController'})
        .when('/login',                         {templateUrl: '../assets/admin/login/index.html',                   controller: 'LoginController'})
        .when('/dashboard',                     {templateUrl: '../assets/admin/dashboard/index.html',               controller: 'DashboardController'})
        .when('/campaigns',                     {templateUrl: '../assets/admin/campaigns/list.html',                controller: 'CampaignsController'})
        .when('/campaigns/new',                 {templateUrl: '../assets/admin/campaigns/new.html',                 controller: 'CampaignsNewController'})
        .when('/campaigns/edit',                {templateUrl: '../assets/admin/campaigns/new.html',                 controller: 'CampaignsNewController'})
        .when('/offers',                        {templateUrl: '../assets/admin/offers/index.html',                  controller: 'OffersController'})
        .when('/offers/new',                    {templateUrl: '../assets/admin/offers/new.html',                    controller: 'OffersNewController'})
        .when('/offers/edit',                   {templateUrl: '../assets/admin/offers/new.html',                    controller: 'OffersNewController'})
        .when('/information',                   {templateUrl: '../assets/admin/information/index.html',             controller: 'InformationController'})
        .when('/information/new',               {templateUrl: '../assets/admin/information/new.html',               controller: 'InformationNewController'})
        .when('/information/edit',              {templateUrl: '../assets/admin/information/new.html',               controller: 'InformationNewController'})
        .when('/users',                         {templateUrl: '../assets/admin/users/index.html',                   controller: 'UsersController'})
        .when('/users/new',                     {templateUrl: '../assets/admin/users/new.html',                     controller: 'UsersNewController'})
        .when('/users/edit',                    {templateUrl: '../assets/admin/users/new.html',                     controller: 'UsersNewController'})
        .when('/operators',                     {templateUrl: '../assets/admin/operators/index.html',               controller: 'OperatorsController'})
        .when('/operators/new',                 {templateUrl: '../assets/admin/operators/new.html',                 controller: 'OperatorsNewController'})
        .when('/operators/edit',                {templateUrl: '../assets/admin/operators/new.html',                 controller: 'OperatorsNewController'})
        .when('/tickets',                       {templateUrl: '../assets/admin/tickets/index.html',                 controller: 'TicketsController'})
        .when('/messages',                      {templateUrl: '../assets/admin/messages/index.html',                controller: 'MessagesController'})
        .when('/messages/new',                  {templateUrl: '../assets/admin/messages/new.html',                  controller: 'MessagesNewController'})
        .when('/messages/edit',                 {templateUrl: '../assets/admin/messages/new.html',                  controller: 'MessagesNewController'})
        .when('/push_notifications/new',        {templateUrl: '../assets/admin/push_notifications/new.html',        controller: 'PushNotificationsNewController'})
        .when('/system_information',            {templateUrl: '../assets/admin/system_information/index.html',      controller: 'SystemInformationController'})
        .when('/logs',                          {templateUrl: '../assets/admin/logs/list.html',                     controller: 'LogsController'})
        .when('/dream_photos',                  {templateUrl: '../assets/admin/dream_photos/list.html',             controller: 'DreamPhotosController'})
        .when('/dream_photos/edit',             {templateUrl: '../assets/admin/dream_photos/new.html',              controller: 'DreamPhotosNewController'})
        .when('/fortune_telling_users',         {templateUrl: '../assets/admin/fortune_telling_users/list.html',    controller: 'FortuneTellingUsersController'})
        .when('/fortune_telling_results',       {templateUrl: '../assets/admin/fortune_telling_results/list.html',  controller: 'FortuneTellingResultsController'})
        .when('/fortune_telling_results/edit',  {templateUrl: '../assets/admin/fortune_telling_results/new.html',   controller: 'FortuneTellingResultsNewController'})
        .when('/line_sticker_requests',         {templateUrl: '../assets/admin/line_sticker_requests/list.html',    controller: 'LineStickerRequestsController'})
        .when('/line_sticker_requests/edit',    {templateUrl: '../assets/admin/line_sticker_requests/new.html',     controller: 'LineStickerRequestsNewController'})
        .when('/news_pick/en',                  {templateUrl: '../assets/admin/news_pick/list.html',                controller: 'NewsPickEnglishController'})
        .when('/news_pick/th',                  {templateUrl: '../assets/admin/news_pick/list.html',                controller: 'NewsPickThaiController'})
        .when('/news_pick/jp',                  {templateUrl: '../assets/admin/news_pick/list.html',                controller: 'NewsPickJapanController'})
        .when('/news_pick/en/new',              {templateUrl: '../assets/admin/news_pick/new.html',                 controller: 'NewsPickNewEnglishController'})
        .when('/news_pick/en/edit',             {templateUrl: '../assets/admin/news_pick/new.html',                 controller: 'NewsPickNewEnglishController'})
        .when('/news_pick/th/new',              {templateUrl: '../assets/admin/news_pick/new.html',                 controller: 'NewsPickNewThaiController'})
        .when('/news_pick/th/edit',             {templateUrl: '../assets/admin/news_pick/new.html',                 controller: 'NewsPickNewThaiController'})
        .when('/news_pick/jp/new',              {templateUrl: '../assets/admin/news_pick/new.html',                 controller: 'NewsPickNewJapanController'})
        .when('/news_pick/jp/edit',             {templateUrl: '../assets/admin/news_pick/new.html',                 controller: 'NewsPickNewJapanController'})
        .when('/news_pick_comment',             {templateUrl: '../assets/admin/news_pick_comment/list.html',        controller: 'NewsPickCommentController'})
        .when('/news_pick_comment/edit',        {templateUrl: '../assets/admin/news_pick_comment/new.html',         controller: 'NewsPickNewCommentController'})
        .when('/news_picks_source',             {templateUrl: '../assets/admin/news_picks_source/list.html',        controller: 'NewsPicksSourceController'})
        .when('/news_picks_source/new',         {templateUrl: '../assets/admin/news_picks_source/new.html',         controller: 'NewsPicksSourceNewController'})
        .when('/news_picks_source/edit',        {templateUrl: '../assets/admin/news_picks_source/new.html',         controller: 'NewsPicksSourceNewController'})
        .when('/news_pick_push_notification',   {templateUrl: '../assets/admin/news_pick_push_notification/new.html',      controller: 'NewsPickNewPushNotificationController'})
        .when('/local_picks_push_notification',   {templateUrl: '../assets/admin/local_picks_push_notification/new.html',      controller: 'LocalPicksNewPushNotificationController'})
        .when('/news_tag',                      {templateUrl: '../assets/admin/news_tag/list.html',                 controller: 'NewsTagController'})
        .when('/news_tag/new',                  {templateUrl: '../assets/admin/news_tag/new.html',                  controller: 'NewsTagNewController'})
        .when('/news_tag/edit',                 {templateUrl: '../assets/admin/news_tag/new.html',                  controller: 'NewsTagNewController'})
        .when('/news_multiple',                 {templateUrl: '../assets/admin/news_multiple/list.html',                 controller: 'NewsMultipleController'})
        .when('/news_multiple/new',             {templateUrl: '../assets/admin/news_multiple/new.html',                  controller: 'NewsMultipleNewController'})
        .when('/news_multiple/edit',            {templateUrl: '../assets/admin/news_multiple/new.html',                  controller: 'NewsMultipleNewController'})
        .when('/facebook_feed_page',                 {templateUrl: '../assets/admin/facebook_feed_page/list.html',                 controller: 'FacebookFeedPageController'})
        .when('/facebook_feed_page/new',             {templateUrl: '../assets/admin/facebook_feed_page/new.html',                  controller: 'FacebookFeedPageNewController'})
        .when('/facebook_feed_page/edit',            {templateUrl: '../assets/admin/facebook_feed_page/new.html',                  controller: 'FacebookFeedPageNewController'})
        .otherwise({ redirectTo: '/' })
        ;
}])
.config(function ($httpProvider) {
    $httpProvider.interceptors.push([
        '$injector',
        function ($injector) {
            return $injector.get('AuthInterceptor');
        }
    ]);
})
.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function (response) {
            var broadcastStatuses = {
                401: AUTH_EVENTS.notAuthenticated,
                403: AUTH_EVENTS.notAuthorized,
                419: AUTH_EVENTS.sessionTimeout,
                440: AUTH_EVENTS.sessionTimeout
            };
            console.log(response.data);
            if (broadcastStatuses[response.status]) {
                $rootScope.$broadcast(broadcastStatuses[response.status], response);
                return $q.reject(response);
            } else if (response.status == 400) {
                $rootScope.showErrorDialog(response.data);
                return $q.reject(response);
            }
        }
    };
})
.directive('clickAndDisable', function() {
  return {
    scope: {
      clickAndDisable: '&'
    },
    link: function(scope, iElement, iAttrs) {
      iElement.bind('click', function() {
        iElement.prop('disabled',true);
        scope.clickAndDisable().finally(function() {
          iElement.prop('disabled',false);
        })
      });
    }
  };
})
;
