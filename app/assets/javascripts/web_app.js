var app = window.App = angular.module('Vg', ['ngRoute', 'updateMeta']);
app.run(['$rootScope', '$location', 'VGLogger', function($rootScope, $location, VGLogger){
    $rootScope.$on("$routeChangeSuccess", function (event, current, previous, rejection) {
        VGLogger.post({
            path: current['$$route']['originalPath'],
        });
        if ( typeof window['ga'] === 'function' ) {
            ga('send', 'pageview', $location.path());
        }
        window.scrollTo(0, 0);
    });
}])
.constant('AUTH_EVENTS', {
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
})
.config(["$httpProvider","$compileProvider", function($httpProvider, $compileProvider) {
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|whatsapp):/);
}])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/',                      {templateUrl: '../assets/web/static/index.html',                controller: 'StaticController'                         })
        //.when('/signup',                {templateUrl: '../assets/web/users/new.html',                   controller: 'UsersNewController'                       })
        //.when('/users/verify',          {templateUrl: '../assets/web/users/verify.html',                controller: 'UsersVerifyController'                    })
        //.when('/users/password_reset',  {templateUrl: '../assets/web/users/password_reset.html',        controller: 'UsersPasswordResetController'             })
        //.when('/login',                 {templateUrl: '../assets/web/sessions/new.html',                controller: 'SessionsNewController'                    })
        //.when('/home',                  {templateUrl: '../assets/web/campaigns/list.html',              controller: 'CampaignsListController'                  })
        //.when('/tickets',               {templateUrl: '../assets/web/tickets/list.html',                controller: 'TicketsListController'                    })
        //.when('/offers',                {templateUrl: '../assets/web/offers/list.html',                 controller: 'OffersListController'                     })
        //.when('/information',           {templateUrl: '../assets/web/information/list.html',            controller: 'InformationListController'                })
        //.when('/settings',              {templateUrl: '../assets/web/settings/new.html',                controller: 'SettingsNewController'                    })
        //.when('/register_complete',     {templateUrl: '../assets/web/static/register_complete.html',    controller: 'StaticRegisterCompleteController'         })
        //.when('/unregister_complete',   {templateUrl: '../assets/web/static/unregister_complete.html',  controller: 'StaticUnregisterCompleteController'       })
        .otherwise({ redirectTo: '/' })
        ;
}])
.config(function ($httpProvider) {
    $httpProvider.interceptors.push([
        '$injector',
        function ($injector) {
            return $injector.get('AuthInterceptor');
        }
    ]);
})
.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function (response) {
            var broadcastStatuses = {
                401: AUTH_EVENTS.notAuthenticated,
                403: AUTH_EVENTS.notAuthorized,
                419: AUTH_EVENTS.sessionTimeout,
                440: AUTH_EVENTS.sessionTimeout
            };
            console.log(response);
            if (broadcastStatuses[response.status]) {
                $rootScope.$broadcast(broadcastStatuses[response.status], response);
                return $q.reject(response);
            } else if (response.status == 400) {
                $rootScope.showErrorDialog(response.data);
                return $q.reject(response);
            }
        }
    };
})
;
