var app = angular.module('Vg');
app.service('VGLogger', function($http, $rootScope, Api){
    this.post = function(log) {
        if (!log['path']) {
            return;
        }
        if ($rootScope.isLoadedConfig()) {
            console.log('VGLogger.post', log);
            Api.post(Api.url_for('/v1/logging'), log);
        } else {
            console.log('VGLogger.post retry', log);
            var that = this;
            setTimeout(function(){
                that.post(log);
            }, 500);
        }
    };
});
