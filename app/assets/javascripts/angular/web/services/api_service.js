var app = angular.module('Vg');
app.service('Api', function($http, $rootScope){
    this.keys = function(h) {
        var keys = [];
        if (h) {
            for (k in h) {
                keys.push(k);
            }
        }
        return keys.sort();
    };

    this.getQueryString = function(params) {
        var qs = '';
        if (params) {
            var keys = this.keys(params);
            console.log(keys);
            for (var i=0; i<keys.length; i++) {
                var k = keys[i];
                var v = params[k];
                if (qs.length > 0) {
                    qs += '&';
                }
                qs += k + '=' + v;
            }
        }
        return qs;
    };

    this.generateHeaders = function(headers) {
        headers = headers || {};
        if ($rootScope.config.user) {
            headers['X-UID']       = $rootScope.config.user.id;
            headers['X-API-TOKEN'] = $rootScope.config.user.token;
        }
        headers['X-SESSION-ID'] = $rootScope.config.session_id;
        headers['X-SOURCE'] = $rootScope.config.source;
        console.log('headers:', headers);
        return headers;
    }

    this.get = function(url, params) {
        if ( params ) {
            url += '?' + this.getQueryString(params);
        }
        console.log("url:", url);

        return $http.get(url, {headers: this.generateHeaders()});
    };

    this.post = function(url, params, headers) {
        return $http.post(url, params, {headers: this.generateHeaders(headers)}); 
    };

    this.glue = function(requests) {
        var url = this.url_for('/v1/glue?');
        for (var i=0; i<requests.length; i++) {
            var request = requests[i];
            var uri = 'uri['+i+']=' + request.uri;
            if ( i > 0 ) {
                url += '&';
            }
            url += uri;
            if ( request.params ) {
                url += encodeURIComponent( '?' + this.getQueryString(request.params) );
            }
        }
        console.log("url:", url);
        return $http.get(url, {headers: this.generateHeaders()});
    }

    this.url_for = function(uri) {
        return $rootScope.config.api.endpoint + uri;
    }
});
