var app = angular.module('Vg');
app.service('Cache', function(){
    this.storage = window.localStorage;

    this.get = function(key) {
        var dataStr = this.storage.getItem(key);
        var value = null;
        if ( dataStr ) {
            data = JSON.parse(dataStr);
            console.log('Cache.get: data=%o', data);
            if ( data['ttl'] > -1) {
                if ( data['savedAt'] + data['ttl'] > (new Date()).getTime() ) {
                    value = data['value'];
                } else {
                    this.delete(key);
                }
            } else {
                value = data['value'];
            }
        }
        console.log('Cache.get: key=%s, value=%o', key, value);
        return value;
    };

    this.set = function(key, value, extend_opt) {
        extend_opt = extend_opt || {}
        var opt = {
            ttl: 60 * 1000, // seconds
        };
        angular.extend( opt, angular.copy( extend_opt ) );
        var data = {
            ttl: opt['ttl'],
            savedAt: (new Date()).getTime(),
            value: value,
        }
        console.log('Cache.set: key=%s, data=%o', key, data);
        this.storage.setItem(key, JSON.stringify(data));
    };

    this.delete = function(key) {
        console.log('Cache.delete: key=%s', key);
        this.storage.removeItem(key);
    };

    this.flush = function() {
        console.log('Cache.flush');
        this.storage.clear();
    };
});
