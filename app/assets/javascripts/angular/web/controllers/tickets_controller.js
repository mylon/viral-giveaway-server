app.controller('TicketsListController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "TICKETS";

    $scope.tickets = null;
    $rootScope.initPageWithAuth(function() {
        $rootScope.showLoadingDialog();
        var requests = [
            { uri: '/v1/tickets', params: null },
            { uri: '/v1/points/count', params: null },
        ];
        Api.glue(requests)
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                var glueData = resp.data;
                $scope.tickets = glueData[0].response.data.tickets;
                $scope.point = glueData[1].response.data.count;
                angular.forEach($scope.tickets, function(t) {
                    var numbers = [];
                    for (var i=0; i<t.number.length; i++) {
                        numbers.push(t.number[i]);
                    }
                    t.number_str = numbers.join(' ');
                });
            });
    });

}]); 
