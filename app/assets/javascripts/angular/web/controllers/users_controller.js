var app = angular.module('Vg');
app.controller('UsersNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "SIGN UP";

    $scope.user = {};
    $scope.doRegister = function() {
        $rootScope.showLoadingDialog();
        $scope.user.referral_code = $rootScope.config.referral_code;
        Api.post(Api.url_for('/v1/users/register'), $scope.user)
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                $location.path('/register_complete')
            });
    };
}])
.controller('UsersVerifyController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = 'Verify';

    $rootScope.initPage(function(){
        var params = {
            id: $location.search()['id'],
            code: $location.search()['code']
        };
        $rootScope.showLoadingDialog();
        Api.post(Api.url_for('/v1/users/verify'), params)
            .success(function(resp){
                $rootScope.hideLoadingDialog();
                $scope.verified = resp.data.verified;
            });
    });
}])
.controller('UsersPasswordResetController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = 'Password Reset';

    $scope.sent = false;
    $scope.user = {};
    $scope.doRecover = function() {
        $rootScope.showLoadingDialog();
        Api.post(Api.url_for('/v1/users/password_reset'), $scope.user)
            .success(function(resp){
                $rootScope.hideLoadingDialog();
                $scope.sent = resp.data.sent;
            });
    };
}])
; 
