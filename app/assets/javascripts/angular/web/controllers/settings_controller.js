app.controller('SettingsNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "SETTINGS";

    $scope.edit_password = false;
    $scope.birthdate_year_range = 120;
    $scope.birthdate_year_start = (new Date()).getFullYear();
    $scope.user = [];
    $rootScope.initPageWithAuth(function() {
        $rootScope.showLoadingDialog();
        var requests = [
            { uri: '/v1/users/profile', params: null },
            { uri: '/v1/offers/recommends', params: null },
            { uri: '/v1/points/count', params: null },
        ];

        Api.glue(requests)
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                var glueData = resp.data;
                $scope.user = glueData[0].response.data;
                $scope.promotionOffers = glueData[1].response.data.offers;
                if ( $scope.promotionOffers.length > 0 ) {
                    $scope.promotionOffer = $scope.promotionOffers[0];
                }
                $scope.point = glueData[2].response.data.count;
            });
    });

    $scope.doSave = function() {
        $rootScope.showLoadingDialog();
        $scope.user.birthdate = $scope.user.birthdate_month + '/' + $scope.user.birthdate_day + '/' + $scope.user.birthdate_year;
        console.log($scope.user);
        Api.post(Api.url_for('/v1/users/profile'), $scope.user)
            .success(function(resp) {
                alertify.success('Update Successful');
                $rootScope.hideLoadingDialog();
                $scope.edit_password = false;
                $scope.user = resp.data;
            });
    };

    $scope.showDeleteAccountDialog = function() {
        $scope.deleteAccountStep = 'step1';
        $rootScope.deleteAccount = {};
    };

    $scope.submitDeleteAccount = function() {
        if ( $scope.deleteAccountStep == 'step1' ) {
            if ( parseInt($scope.deleteAccount.question_answer) > 0 ) {
                $scope.deleteAccountStep = 'step2';
            } else {
                $rootScope.showErrorDialog({
                    data: {
                        messages: ['Please select answer.'],
                    }
                })
            }

        } else if ( $scope.deleteAccountStep == 'step2' ) {
            $scope.deleteAccountStep = 'step3';
        } else if ( $scope.deleteAccountStep == 'step3' ) {
            if ( $rootScope.deleteAccount.password ) {
                $scope.doDeleteAccount();
            } else {
                $rootScope.showErrorDialog({
                    data: {
                        messages: ['Please input password.'],
                    }
                })
            }
        }
    };

    $scope.doDeleteAccount = function() {
        $rootScope.showLoadingDialog();
        Api.post(Api.url_for('/v1/users/unregister'), $rootScope.deleteAccount)
            .success(function(resp){
                $rootScope.hideLoadingDialog();
                $rootScope.config.user = null;
                $rootScope.reloadConfig();
                $scope.cancelDeleteAccount();
                $location.path('/unregister_complete');
            });
    };

    $scope.cancelDeleteAccount = function() {
        $scope.deleteAccountStep = null;
        $rootScope.deleteAccount = null;
    };

    $scope.movePromotionOffer = function() {
        var url = $rootScope.replaceOfferPlaceholder($scope.promotionOffer.click_url);
        window.open(url);
        $scope.cancelDeleteAccount();
    };

}]); 
