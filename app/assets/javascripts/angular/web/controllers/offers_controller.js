app.controller('OffersListController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "OFFERS";

    $scope.promotions   = [];
    $scope.walls        = [];
    $scope.applications = [];
    $scope.videos       = [];
    $scope.information  = [];
    $rootScope.initPageWithAuth(function() {
        $rootScope.showLoadingDialog();
        var requests = [
            { uri: '/v1/offers', params: { cat: 'promotion' } },
            { uri: '/v1/offers', params: { cat: 'wall' } },
            { uri: '/v1/offers', params: { cat: 'application' } },
            { uri: '/v1/offers', params: { cat: 'video' } },
            { uri: '/v1/offers', params: { cat: 'information' } },
            { uri: '/v1/points/count', params: null },
        ];
        Api.glue(requests)
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                var glueData = resp.data;
                console.log('glueData:', glueData);
                $scope.promotions   = glueData[0].response.data.offers;
                $scope.walls        = glueData[1].response.data.offers;
                $scope.applications = glueData[2].response.data.offers;
                $scope.videos       = glueData[3].response.data.offers;
                $scope.information  = glueData[4].response.data.offers;
                $scope.point        = glueData[5].response.data.count;
            });
    });

    $scope.showApp = function(offer) {
        var url = $rootScope.replaceOfferPlaceholder(offer.click_url);
        console.log("showApp. url:", url);
        window.open(url);
    };

    $scope.showVideo = function(offer) {
        var url = 'https://youtu.be/' + offer.youtube_video_id;
        console.log("showVideo. url:", url);
        window.open(url);
    };
 
}]); 
