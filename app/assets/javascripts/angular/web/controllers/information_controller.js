app.controller('InformationListController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    '$sce',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        $sce,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "INFORMATION";

    $rootScope.dialog = null;
    $scope.information = [];
    $rootScope.initPageWithAuth(function() {
        $rootScope.showLoadingDialog();
        var requests = [
            { uri: '/v1/information', params: null },
            { uri: '/v1/points/count', params: null },
        ];
        Api.glue(requests)
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                var glueData = resp.data;
                $scope.information = glueData[0].response.data.information;
                $scope.point = glueData[1].response.data.count;
            });
    });

    $scope.showDialog = function(information) {
        console.log('information:', information);
        $rootScope.dialog = {
            information: information,
            content_tag: $sce.trustAsHtml(information.content.replace(/\r?\n/g, '<br>')),
            video_tag: $sce.trustAsHtml('<iframe width="240" height="160" src="https://www.youtube.com/embed/' + information.video_url + '" frameborder="0" allowfullscreen></iframe>'),
        }; 
    };

    $scope.hideDialog = function() {
        var information = $rootScope.dialog.information;
        $rootScope.dialog = null;
        if ( information.viewed ) {
            return;
        }

        Api.post(Api.url_for('/v1/information/view'), {information_id: information.id})
            .success(function(resp){
                for (var i=0; i<$scope.information.length; i++) {
                    var info = $scope.information[i];
                    if ( information.id == info.id ) {
                        info.viewed = true;
                        break;
                    }
                }
            });
    }
}]); 
