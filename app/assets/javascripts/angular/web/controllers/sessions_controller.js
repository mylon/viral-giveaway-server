app.controller('SessionsNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "LOGIN";

    if ( $rootScope.config.user ) {
        $location.path('/home').search({});
    } else if ( window.vg.errors && window.vg.errors.length > 0 ) {
        alertify.error(window.vg.errors);
    }

    $scope.user = {};
    $scope.doLogin = function() {
        Api.post('/users/sessions', $scope.user)
            .success(function(resp){
                $rootScope.reloadConfig();
                var redirect = $location.search()['r'];
                var path = redirect ? window.decodeURIComponent(redirect) : '/home';
                $location.url(path);
            });
    };
}]); 
