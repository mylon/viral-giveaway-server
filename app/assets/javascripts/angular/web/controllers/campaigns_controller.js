app.controller('CampaignsListController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    '$filter',
    '$interval',
    '$sce',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    'Cache', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        $filter,
        $interval,
        $sce,
        AUTH_EVENTS, 
        Session, 
        Api,
        Cache
    ) {
    $scope.title = "VIRAL GIVEAWAY";

    var recommendOfferId = $location.search()['rid'];
    $scope.campaignCountdownTimer = null; 
    $scope.campaigns = [];
    $scope.currentCampaignIdx = -1;
    $scope.promotionOffer = null;
    $scope.invitation = {};
    $scope.user = null;
    $rootScope.ticketApplyDialog = null;
    $rootScope.sponsorDialog = null;
    $rootScope.invitationDialog = null;
    $rootScope.specialPromotionDialog = null;
    $scope.dreamPhotoSlideNum = 4; // start 0
    $scope.dreamPhotoSlideCurrentIdx = 0;
    $scope.birthdate_year_range = 120;
    $scope.birthdate_year_start = (new Date()).getFullYear();

    $rootScope.initPageWithAuth(function() {
        var showPromotitonOfferTimeDiff = 1000 * 60 * 60 * 6;
        var lastAccessedAt = Cache.get('home-last-accessed-at');
        console.log('lastAccessedAt:', lastAccessedAt);
        Cache.set('home-last-accessed-at', (new Date()).getTime(), { ttl: -1 });

        $scope.invitation.referral_code = $rootScope.config.referral_code;
        if ( !$rootScope.config.shown_invitation_dialog ) {
            $rootScope.invitationDialog = {};
        }

        $rootScope.showLoadingDialog();
        var requests = [
            { uri: '/v1/campaigns', params: null },
            { uri: '/v1/offers/recommends', params: null },
            { uri: '/v1/users/profile', params: null },
            { uri: '/v1/points/count', params: null },
            { uri: '/v1/messages', params: null },
        ];

        var recommendDataIdx = -1;
        if ( $rootScope.config.shown_invitation_dialog ) {
            if ( recommendOfferId ) {
                requests.push( { uri: '/v1/offers/recommends/' + recommendOfferId, params: null } );
                recommendDataIdx = 5;
            } else if ( lastAccessedAt == null || lastAccessedAt < (new Date).getTime() - showPromotitonOfferTimeDiff ) {
                requests.push( { uri: '/v1/offers/recommends', params: null } );
                recommendDataIdx = 5;
            }
        }
        Api.glue(requests)
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                var glueData = resp.data;
                $scope.campaigns = glueData[0].response.data.campaigns;
                $scope.startProcessCampaigns();

                $scope.promotionOffers = glueData[1].response.data.offers;
                if ( $scope.promotionOffers.length > 0 ) {
                    $scope.promotionOffer = $scope.promotionOffers[0];
                }

                $scope.user = glueData[2].response.data;
                $scope.point = glueData[3].response.data.count;
                $rootScope.handleMessageData(glueData[4].response.data.messages);
        
                if ( recommendDataIdx > -1 ) {
                    if ( glueData[recommendDataIdx].response.data.offers.length > 0 ) {
                        $scope.promotionOffer = glueData[recommendDataIdx].response.data.offers[0];
                        $rootScope.recommendedOfferDialog = {};
                    }
                }
            });
    });

    $scope.showLeftNavi = function() {
        if ( !$scope.campaigns || $scope.campaigns.length == 0 ) {
            return false;
        }
        if ( $scope.campaigns.length > 1 && $scope.currentCampaignIdx > 0 ) {
            return true;
        }
        return false;
    };

    $scope.showRightNavi = function() {
        if ( !$scope.campaigns || $scope.campaigns.length == 0 ) {
            return false;
        }
        if ( $scope.campaigns.length > 1 && $scope.currentCampaignIdx < 1 ) {
            return true;
        }
        return false;
    };

    $scope.startProcessCampaigns = function() {
        angular.forEach($scope.campaigns, function(c) {
            if ( c.type == 'prize' ) {
                $scope.startCountdown();
            } else if ( c.type == 'dream_photo' || c.type == 'line_sticker' ) {
                c.processing_time_label = c.processing_time / 60;
            }
        })
    };

    $scope.shareFacebook = function() {
        $rootScope.facebookDialog = {
            title: 'Share Facebook',
            note: 'When share on your wall',
            action: 'share_facebook',
        };
    };

    $scope.likeFacebook = function() {
        $rootScope.facebookDialog = {
            title: 'Like Facebook Page',
            note: 'When like to our page',
            action: 'like_facebook',
        };
    };

    $scope.cancelFacebook = function() {
        $rootScope.facebookDialog = null;
    };

    $scope.submitFacebook = function() {
        var action = $rootScope.facebookDialog.action;
        if ( 'share_facebook' == action ) {
            var msg = window.encodeURIComponent("https://viral-giveaway.com/?source=facebook");
            window.open('https://www.facebook.com/sharer/sharer.php?u=' + msg);
        } else if ( 'like_facebook' == action ) {
            window.open('https://www.facebook.com/pages/Viral-Giveaway/823735791015041');
        } else {
            return $scope.cancelFacebook();
        }

        Api.post(Api.url_for('/v1/bonus/' + action))
            .success(function(resp){
                $scope.cancelFacebook();
                $rootScope.reloadConfig();
                $route.reload();
            });
    };

    $scope.shareTwitter = function() {
        $scope.share = { provider: 'twitter' };
        $rootScope.shareDialog = {
            title: 'Share Twitter',
            note: 'When share with your followers',
        };
        $scope.share.message = 'Massive giveaway is on the way! (Weekly iPhone6) #ViralGiveaway #iPhone6 https://viral-giveaway.com/?source=twitter';
    };
    
    $scope.cancelShare = function() {
        $rootScope.shareDialog = null;
    };
 
    $scope.submitShare = function() {
        console.log('shareSubmit');
        window.location.href = '/users/auth/' + $scope.share.provider + '?action=post&message=' + window.encodeURIComponent($scope.share.message);
        $scope.share = null;
        $rootScope.shareDialog = null; 
    };

    $scope.followTwitter = function() {
        $rootScope.followDialog = {};
    };

    $scope.cancelFollow = function() {
        $rootScope.followDialog = null;
    }

    $scope.submitFollow = function() {
        window.location.href = '/users/auth/twitter?action=follow';
        $rootScope.followDialog = null; 
    }

    $scope.showLoginBonusDialog = function() {
        $rootScope.dialog = {
            title: 'Daily Login',
            content_tag: $sce.trustAsHtml('<p class="attention">You Get</p>'+
                '<p class="attention">1 Point/Daily Login</p>'+
                '<p class="subtext">Earn Extra Point!</p>'+
                '<p class="subtext">Simply start the app daily.</p>'),
        };
    };

    $scope.showActionBonusDialog = function() {
        $rootScope.dialog = {
            title: 'Action Bonus',
            content_tag: $sce.trustAsHtml('<p class="attention">You Get</p>'+
                '<p class="attention">3 Points/App installed by refered user</p>'+
                '<p class="subtext">Inscrease your chance to win by</p>'+
                '<p class="subtext">asking the refered user to install more apps!</p>'),
        };
    };
    
    $scope.showReferralBonusDialog = function() {
        $rootScope.dialog = {
            show_social_button: true,
            title: 'Referral Bonus',
            content_tag: $sce.trustAsHtml('<p class="attention">You Get</p>'+
                '<p class="attention">2 Points/Referral Code Entered</p>'+
                '<p class="subtext">Referred user will be prompt to enter</p>'+
                '<p class="subtext">your code</p>'+
                '<p class="referral-code">' + $rootScope.config.user.id + '</p>'+
                '<p class="subtext">once only when starting the app</p>'),
        };
    };

    $scope.hideDialog = function() {
        $rootScope.dialog = null;
    };

    $scope.moveOffers = function() {
        $location.path('/offers');
        $rootScope.specialPromotionDialog = null;
    };

    $scope.doSkip = function() {
        if ( $rootScope.invitationDialog ) {
            $rootScope.showLoadingDialog();
            Api.post(Api.url_for('/v1/users/track'), { type: 'invitation', value: 'ok' })
                .success(function(resp) {
                    $rootScope.hideLoadingDialog();
                    $rootScope.config.shown_invitation_dialog = true;
                });
        }

        $rootScope.invitationDialog = null;
        $rootScope.specialPromotionDialog = null;
    };

    $scope.movePromotionOffer = function() {
        var url = $rootScope.replaceOfferPlaceholder($scope.promotionOffer.click_url);
        window.open(url);
        $rootScope.specialPromotionDialog = null;
        $scope.hideRecommendedOfferDialog();
    };

    $scope.doInvited = function() {
        if ( !$scope.invitation.referral_code ) {
            $rootScope.showLoadingDialog();
            Api.post(Api.url_for('/v1/users/track'), { type: 'invitation', value: 'ok' })
                .success(function(resp) {
                    $rootScope.hideLoadingDialog();
                    $rootScope.config.shown_invitation_dialog = true;
                    $scope.showPromotionDialog();
                });
            return;
        }

        $rootScope.showLoadingDialog();
        Api.post(Api.url_for('/v1/users/invited'), { referral_code: $scope.invitation.referral_code })
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                $rootScope.config.shown_invitation_dialog = true;
                $scope.showPromotionDialog();
            });
    };

    $scope.showPromotionDialog = function() {
        $scope.promotionTimeCount = 10;
        var timer = null;
        timer = $interval(function() {
            console.log('promotionTimeCount: ', $scope.promotionTimeCount);
            if ( $scope.promotionTimeCount > 0 ) {
                $scope.promotionTimeCount--;
            } else {
                $interval.cancel(timer);
            }
        }, 1000);

        $rootScope.hideLoadingDialog();
        $rootScope.invitationDialog = null;
        $rootScope.specialPromotionDialog = {};
    };

    $scope.showWinner = function() {
        console.log('showWinner');
    };

    $scope.showSponsorDialog = function() {
        $rootScope.sponsorDialog = {
            title: 'SPONSOR',
        };
    };

    $scope.submitApply = function(campaign) {
        $scope.campaign = campaign;
        if ( campaign.type == 'prize' ) {
            $scope.showTicketApplyDialog(campaign);
        } else if ( campaign.type == 'dream_photo' ) {
            $scope.showDreamPhotoApplyDialog(campaign);
        } else if ( campaign.type == 'fortune_telling' ) {
            $scope.showFortuneTellingApplyDialog(campaign);
        } else if ( campaign.type == 'line_sticker' ) {
            $scope.showLineStickerApplyDialog(campaign);
        } else {
            alertify.error("Invalid Campaign Type.");
        }
    };

    $scope.showFortuneTellingApplyDialog = function(campaign) {
        $rootScope.fortuneTellingApplyDialog = {
            birthdate_year: $scope.user.birthdate_year,
            birthdate_month: $scope.user.birthdate_month,
            birthdate_day: $scope.user.birthdate_day,
        };
    };

    $scope.hideFortuneTellingApplyDialog = function() {
        $rootScope.fortuneTellingApplyDialog = null;
    };

    $scope.submitFortuneTellingApply = function() {
        var params = {
            id: $scope.campaign.id,
            birthdate: $rootScope.fortuneTellingApplyDialog.birthdate_month + '/' + $rootScope.fortuneTellingApplyDialog.birthdate_day + '/' + $rootScope.fortuneTellingApplyDialog.birthdate_year,
        }
        Api.post(Api.url_for("/v1/campaigns/apply"), params)
            .success(function(resp) {
                var data = resp.data;
                $scope.point = data.point;
                $scope.hideFortuneTellingApplyDialog();
                $rootScope.dialog = {
                    title: 'Congratulation!!',
                    content_tag: $sce.trustAsHtml('<div class="icon mail-icon"></div>'+
                                                  '<p class="subtext">Your Foretell will send to your email daily</p>'),
                };
            });
    };

    $scope.showLineStickerApplyDialog = function(campaign) {
        $rootScope.lineStickerApplyDialog = {
            lineId: "",
            stickerName: "",
            coin: "50",
        };
    };

    $scope.hideLineStickerApplyDialog = function() {
        $rootScope.lineStickerApplyDialog = null;
    };

    $scope.submitLineStickerApply = function() {
        var params = {
            id: $scope.campaign.id,
            line_id: $rootScope.lineStickerApplyDialog.lineId,
            sticker_name: $rootScope.lineStickerApplyDialog.stickerName,
            coin: $rootScope.lineStickerApplyDialog.coin,
        };
        Api.post(Api.url_for("/v1/campaigns/apply"), params)
            .success(function(resp) {
                var data = resp.data;
                $scope.point = data.point;
                $scope.hideLineStickerApplyDialog();
                $rootScope.dialog = {
                    title: 'Congratulation!!',
                    content_tag: $sce.trustAsHtml('<div class="icon sticker-icon"></div>'+
                                                  '<p class="subtext">Line Sticker will send to your Line account ASSP</p>'),
                };
            });
    };

    $scope.showTicketApplyDialog = function(campaign) {
        console.log('showTicketApplyDialog:', campaign);
        var errors = $rootScope.validProfile($scope.user);
        if ( errors.length > 0 ) {
            $rootScope.ticketApplyDialog = {
                title: 'Missing Information',
                content_tag: $sce.trustAsHtml('<p class="attention">Ooops!</p>' +
                                              '<div class="icon missing-information-icon"></div>' +
                                              '<p class="subtext">It takes less than a minute to complete!</p>' +
                                              '<p class="subtext">Please fill the required information!!</p>'),
            };
        } else {
            $rootScope.ticketApplyDialog = {
                title: 'Confirm the Entry',
                content_tag: $sce.trustAsHtml('<p class="attention">Woo hoo!</p>' +
                                              '<div class="icon apply-confirm-icon"></div>' +
                                              '<p class="subtext">Please hit the OK button</p>' +
                                              '<p class="subtext">to receive your ticket!!</p>'),
            };
        }
    };

    $scope.cancelTicketApply = function() {
        $rootScope.ticketApplyDialog = null;
    };

    $scope.submitTicketApply = function(campaign) {
        var errors = $rootScope.validProfile($scope.user);
        if ( errors.length > 0 ) {
            $location.path('/settings');
            $rootScope.ticketApplyDialog = null;
            return;
        } else {
            $rootScope.showLoadingDialog();
            Api.post(Api.url_for('/v1/campaigns/apply'), { id: campaign.id })
                .success(function(resp){
                    $rootScope.hideLoadingDialog();
                    var data = resp.data;
                    var ticket = data.ticket;
                    $scope.point = data.point;

                    $scope.campaign.ticket_num++;
                    $rootScope.ticketApplyDialog = null;
                    $rootScope.dialog = {
                        title: 'Good Luck!',
                        content_tag: $sce.trustAsHtml('<div class="icon got-ticket-icon"></div>'+
                                                     '<p class="ticket-number">' + ticket.number + '</p>' +
                                                     '<p class="subtext">Ticket is issued Successfully.</p>'),
                    };
                });
        }
    };

    $scope.stopCountdown = function() {
        $interval.cancel( $scope.campaignCountdownTimer );
    }
    $scope.startCountdown = function() {
        var countdownFunc = function(){
            for (var i=0; i<$scope.campaigns.length; i++) {
                var campaign = $scope.campaigns[i];
                if (campaign.type != 'prize') continue;
                
                var target_at = campaign.end_at_time;
                var time      = (new Date()).getTime() / 1000;
                var diff      = target_at - time;
                if ( diff <= 0 ) {
                    campaign.next_day  = 0;
                    campaign.next_time = '00:00:00';
                    continue;
                }
                campaign.next_time = '';
                campaign.next_day  = parseInt(diff / 86400);
                diff -= campaign.next_day * 86400;
                var hour = parseInt(diff / 3600);
                diff -= hour * 3600;
                var minute = parseInt(diff / 60);
                diff -= minute * 60;
                var second = parseInt(diff);
                var parts = [hour,minute,second];
                for (var j=0; j<parts.length; j++) {
                    var n = parts[j];
                    n = n >= 10 ? n : '0' + n;
                    parts[j] = n;
                }
                campaign.next_time = parts.join(':');
            }
        };
        countdownFunc();
        $scope.campaignCountdownTimer = $interval(countdownFunc, 1000);
    }

    $scope.stopCountdown = function() {
        $interval.cancel($scope.campaignCountdownTimer);
    };

    $scope.hideSponsorDialog = function() {
        $rootScope.sponsorDialog = null;
    };

    $scope.hideRecommendedOfferDialog = function() {
        $rootScope.recommendedOfferDialog = null;
    }

    $scope.showDreamPhotoMoreDialog = function() {
        $scope.dreamPhotoSlideCurrentIdx = 0;
        $rootScope.dreamPhotoMoreDialog = {};
    };
    $scope.hideDreamPhotoMoreDialog = function() {
        $rootScope.dreamPhotoMoreDialog = null;
    }

    $scope.showLeftPhotoNavi = function() {
        return $scope.dreamPhotoSlideCurrentIdx > 0;
    };
    $scope.showRightPhotoNavi = function() {
        return $scope.dreamPhotoSlideCurrentIdx < $scope.dreamPhotoSlideNum;
    };
    $scope.showPrevPhoto = function() {
        $scope.dreamPhotoSlideCurrentIdx--;
    };
    $scope.showNextPhoto = function() {
        $scope.dreamPhotoSlideCurrentIdx++;
    };
    $scope.showPhotoSlidePage = function(page) {
        return $scope.dreamPhotoSlideCurrentIdx == page;
    };

    $scope.showDreamPhotoApplyDialog = function() {
        $rootScope.dreamPhotoApplyDialog = {};
        $scope.dreamPhotoModel = {
            photo_type: 1, // face,
            photo1: null, 
            photo2: null, 
            comment: '', 
        };
    };
    $scope.hideDreamPhotoApplyDialog = function() {
        $rootScope.dreamPhotoApplyDialog = null;
        $rootScope.dreamPhotoModel = null;
    };
    $scope.submitDreamPhotoApply = function() {
        $rootScope.showLoadingDialog();
        console.log($scope.dreamPhotoModel);
        var formData = new FormData();
        formData.append('photo1', $scope.dreamPhotoModel.photo1);
        formData.append('photo2', $scope.dreamPhotoModel.photo2);
        formData.append('photo_type', $scope.dreamPhotoModel.photo_type);
        formData.append('comment', $scope.dreamPhotoModel.comment);
        formData.append('id', $scope.campaign.id);
        
        Api.post(Api.url_for('/v1/campaigns/apply'), formData, {'Content-Type': undefined})
            .success(function(resp){
                $rootScope.hideLoadingDialog();
                var data = resp.data;
                $scope.point = data.point;
                $scope.hideDreamPhotoApplyDialog();
                $rootScope.dialog = {
                    title: 'Congratulation!!',
                    content_tag: $sce.trustAsHtml('' +
                        '<p><img src="assets/web/congrad_icon.png" style="width:107px;height:40px;"/></p>' +
                        '<p class="attention small-text">Your Dream Photo will sent to your email in 20 min</p>' +
                        '<p class="subtext align-left xsmall-text">* Please fill your email in setting page</p>' +
                        '<p class="subtext align-left xsmall-text">** Which Photo can\'t match up we will notification and</p>' +
                        '<p class="subtext align-left xsmall-text">&nbsp;&nbsp;&nbsp;let\'s yout upload again</p>'),
                };
            });
    };

    $scope.toggleDreamPhotoType = function(type) {
        $scope.dreamPhotoModel.photo_type = type;
    };

    $scope.showPhotoPreview = function(photoId) {
        var file = event.target.files[0];
        if (/^image\//i.test(file.type)) {
            loadImage(photoId, file);
        } else {
            console.log("It is not image.");
        }
    };

    function loadImage(photoId, file) {
        var reader = new FileReader();

        reader.onloadend = function () {
            $scope.dreamPhotoModel[photoId] = reader.result;
            drawImage(photoId, reader.result, file.type);
        }

        reader.onerror = function () {
            alert('There was an error reading the file!');
        }

        reader.readAsDataURL(file);
    }

    function drawImage(photoId, dataUrl, fileType) {
        var maxWidth = 94;
        var maxHeight = 63;

        var image = new Image();
        image.src = dataUrl;

        image.onload = function () {
            var width = image.width;
            var height = image.height;

            var newWidth;
            var newHeight;

            if (width > height) {
                newHeight = height * (maxWidth / width);
                newWidth = maxWidth;
            } else {
                newWidth = width * (maxHeight / height);
                newHeight = maxHeight;
            }

            var canvas = $('#'+photoId+'-preview');

            canvas.get(0).width = newWidth;
            canvas.get(0).height = newHeight;

            var context = canvas.get(0).getContext('2d');

            context.drawImage(this, 0, 0, newWidth, newHeight);

            $scope.$apply();
        };

        image.onerror = function () {
            alert('There was an error processing your file!');
        };
    }
}])
.directive('indicator', function() {
    return {
        restrict: 'E',
        template: '' +
        '<div class="indicator-container">' +
            '<div class="indicator clearfix">' +
                '<ul>' +
                    '<li class="item" ng-class="{active:current == $index}" ng-repeat="n in [].constructor(total) track by $index"></li>' +
                '<ul>' +
            '</div>' +
        '</div>',
        scope: {
            total: '=myTotal',
            current: '=myCurrent',
        },
    };
})
.directive('flexslider', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var timer = null;
            timer = setInterval(function() {
                if ($('.flexslider .slides > li').size() > 0) {
                    console.log("flexslider ", $('.flexslider .slides > li').size());
                    $(element).flexslider({
                        animation: "slide",
                        controlsContainer: $(".custom-controls-container"),
                        customDirectionNav: $(".custom-navigation a"),
                        slideshowSpeed: 3000,
                    });
                    clearInterval(timer);
                }
            }, 300);
        }
    }
})
; 
