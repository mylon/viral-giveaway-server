var app = angular.module('Vg');
app.filter('encodeURI', function() {
    return window.encodeURIComponent;
});
app.filter('ssl_image', function($rootScope) {
    return function(src) {
        if ( src && src.match(/^http:\/\//) ) {
            return $rootScope.config.rp_endpoint + '/images/' + src;
        } else {
            return src;
        }
    };
});
app.controller('WebController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    '$interval',
    '$sce',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    'VGLogger', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        $interval,
        $sce,
        AUTH_EVENTS, 
        Session, 
        Api,
        VGLogger 
    ) {

    $rootScope.config = {
    };

    $rootScope.h = (new Date()).getTime();
    var loadedConfig = false;
    var canShowLoadingDialog = true;

    $rootScope.loadConfig = function() {
        Api.get('/config')
            .success(function(data){
                angular.extend( $rootScope.config, angular.copy( data ) );
                console.log('loaded config:', $rootScope.config);
                loadedConfig = true;
                $rootScope.hideLoadingDialog();
            });
    };

    $rootScope.reloadConfig = function() {
        loadedConfig = false;
        $rootScope.showLoadingDialog();
        $rootScope.loadConfig();
    };

    $rootScope.hideLoadingDialog = function() {
        canShowLoadingDialog = false;
    }
    $rootScope.showLoadingDialog = function() {
        canShowLoadingDialog = true;
    }
    $rootScope.canShowLoadingDialog = function() {
        return canShowLoadingDialog;
    };

    $rootScope.isLoadedConfig = function() {
        return loadedConfig;
    };

    $rootScope.authenticated = function() {
        return $rootScope.config.user;
    };
    $rootScope.initPage = function(initFunc, auth) {
        auth = auth || false;
        if ($rootScope.isLoadedConfig()) {
            if (auth && !$rootScope.authenticated()) {
                $rootScope.moveLogin();
            } else {
                if ($rootScope.authenticated()) {
                    Api.post(Api.url_for('/v1/bonus/daily'))
                        .success(function(resp){
                            initFunc();
                        });
                } else {
                    initFunc();
                }
            }
        } else {
            var timer = -1;
            timer = $interval(function(){
                if ($rootScope.isLoadedConfig()) {
                    console.log('loaded config!!!');
                    if (auth && !$rootScope.authenticated()) {
                        $rootScope.moveLogin();
                    } else {
                        if ($rootScope.authenticated()) {
                            Api.post(Api.url_for('/v1/bonus/daily'))
                                .success(function(resp){
                                    initFunc();
                                });
                        } else {
                            initFunc();
                        }
                    }
                    $interval.cancel(timer);
                } else {
                    console.log('loading config');
                }
            }, 200); 
        }
    }
    $rootScope.moveLogin = function() {
        var path = $location.url();
        $location.path('/login').search({}).search('r', path);
    };
    $rootScope.initPageWithAuth = function(initFunc) {
        $rootScope.initPage(initFunc, true)
    }

    // helpers
    $rootScope.showErrorDialog = function(data) {
        $rootScope.hideLoadingDialog();
        var msg = data.data.messages.join('<br/>');
        alertify.error(msg);
    };
    $rootScope.getGMTTime = function(timeStr) {
        var d = new Date(timeStr);
        var offset = d.getTimezoneOffset() * 60 * 1000;
        return d.getTime() + offset;
    };
    $rootScope.formatDate = function(time, format) {
        return $filter('date')(time, format);
    }
    $rootScope.formatDatePickerString = function(timeStr) {
        if (timeStr) {
            return $rootScope.formatDate($rootScope.getGMTTime(timeStr), 'yyyy/MM/dd HH:mm');
        } else {
            return '';
        }
    }
    $rootScope.getSessionUser = function() {
        return Session.user;
    };
    $rootScope.isAuthenticated = function() {
        return !!Session.user;
    };
    $rootScope.requireAuthenticate = function() {
//        Api.get('/operators/profile')
//            .success(function(data, status, headers, config){
//                console.log(data);
//                Session.create(data);
//            })
//            .error(function(data, status, headers, config){
//                console.log(data);
//            });
    };
    $rootScope.doLogout = function() {
        Api.get('/users/sessions/logout')
            .then(function(res){
                Session.destroy();
                $rootScope.config.user = null;
                $rootScope.reloadConfig();
                $location.path('/login');
            });
    };
    $rootScope.isActiveMenu = function(menu) {
        var path = $location.path();
        if ( menu === 'home' ) {
           return path.indexOf('/home') >= 0;
        } else if ( menu === 'offers' ) {
           return path.indexOf('/offers') >= 0;
        } else if ( menu === 'tickets' ) {
           return path.indexOf('/tickets') >= 0;
        } else if ( menu === 'information' ) {
           return path.indexOf('/information') >= 0;
        } else if ( menu === 'settings' ) {
           return path.indexOf('/settings') >= 0;
        }
    };

    $rootScope.replaceOfferPlaceholder = function(url) {
        url = url.replace("{USER_ID}", $rootScope.config.user.id);
        return url;
    };
    
    $rootScope.validProfile = function(user) {
        console.log('validProfile:', user);
        var errors = [];

        if ( !user.name ) {
            errors.push('NAME');
        }
        if ( !user.birthdate ) {
            errors.push('DOB');
        }
        if ( !user.gender ) {
            errors.push('GENDER');
        }

        return errors; 
    };

    $rootScope.handleMessageData = function(messages) {
        $rootScope.messages = messages;
        $rootScope.showMessageDialog(messages.shift());
    };

    $rootScope.showNextMessage = function() {
        if ( $rootScope.messages && $rootScope.messages.length > 0 ) {
            $rootScope.showMessageDialog( $rootScope.messages.shift() );
        } else {
            $rootScope.hideMessageDialog();
        }
    }

    $rootScope.showMessageDialog = function(data) {
        if ( data ) {
            var title = '';
            var description = '';
            if ( data.type == 'cpi' ) {
                title = 'App Install';
                description = '<p>Installed our recommend app.</p>' +
                              '<p>More Points More change to WIN!!</p>';
            } else if ( data.type == 'facebook_share' ) {
                title = 'Share on Facebook';
                description = '<p>Shared out app on facebook.</p>' +
                              '<p>More Points More change to WIN!!</p>';
            } else if ( data.type == 'twitter_share' ) {
                title = 'Share on Twitter';
                description = '<p>Tweeted our app on twitter.</p>' +
                              '<p>More Points More change to WIN!!</p>';
            } else if ( data.type == 'invitation' ) {
                title = 'Referral Bonus';
                description = '<p>Your referral code!</p>' +
                              '<p>More Points More change to WIN!!</p>';
            } else if ( data.type == 'daily_bonus' ) {
                title = 'Daily Bonus';
            } else if ( data.type == 'friend_cpi' ) {
                title = 'Action Bonus';
                description = '<p>Referred user installed our recommend app!</p>' +
                              '<p>More Points More change to WIN!!</p>';
            } else if ( data.type == 'facebook_like' ) {
                title = 'Like Facebook Page';
                description = '<p>Liked facebook page.</p>' +
                              '<p>More Points More change to WIN!!</p>';
            } else if ( data.type == 'twitter_follow' ) {
                title = 'Follow Twitter Accont';
                description = '<p>Followed twitter account.</p>' +
                              '<p>More Points More change to WIN!!</p>';
            }
            data.title = title;
            data.description = $sce.trustAsHtml(description);

            $rootScope.messageDialog = data;
        } else {
            $rootScope.hideMessageDialog();
        }
    };

    $rootScope.hideMessageDialog = function() {
        $rootScope.messages = null;
        $rootScope.messageDialog = null;
    };

    $rootScope.showTermsDialog = function() {
        VGLogger.post({
            path: '/terms',
        })
        $rootScope.termsDialog = {};
    };

    $rootScope.hideTermsDialog = function() {
        $rootScope.termsDialog = null;
    }

    $rootScope.showWinnerDialog = function() {
        $rootScope.winnerDialog = {
            content_tag: $sce.trustAsHtml(''+
                                 '<p>Date: 2015-09-30</p>'+
                                 '<iframe width="240" height="160" src="https://www.youtube.com/embed/rpWVLYrqqrE" frameborder="0" allowfullscreen></iframe>'+
                                 '<p>WINNER: Reetesh Chandra Srivastava</p>'+
                                 '<p><img src="/assets/web/winners/002_Reetesh_Chandra.jpg"/></p>'+
                                 '<p>Date: 2015-08-31</p>'+
                                 '<iframe width="240" height="160" src="https://www.youtube.com/embed/miHxXTP1hhI" frameborder="0" allowfullscreen></iframe>'+                                 
                                 '<p>WINNER: Anurag Roy</p>'+
                                 '<p><img src="/assets/web/winners/002_Anurag_Roy.jpg"/></p>'+
                                 '<p>WINNER: Manisha Pal</p>'+
                                 '<p><img src="/assets/web/winners/002_Manisha_Pal.jpg"/></p>'+
                                 '<p>Date: 2015-07-31</p>'+
                                 '<iframe width="240" height="160" src="https://www.youtube.com/embed/qnwJZr6HHP8" frameborder="0" allowfullscreen></iframe>'+
                                 '<p>Date: 2015-07-17</p>'+
                                 '<iframe width="240" height="160" src="https://www.youtube.com/embed/XB2C2BwZOEM" frameborder="0" allowfullscreen></iframe>'+
                                 "<p>Winner didn't declare the prize so it's carried over to next draw.</p>"),
        };
    };

    $rootScope.hideWinnerDialog = function() {
        $rootScope.winnerDialog = null;
    };

    //TODO optimize
    $rootScope.isDialogOpen = function() {
        var isOpen = $rootScope.messageDialog != null ||
               $rootScope.termsDialog != null ||
               $rootScope.ticketApplyDialog != null ||
               $rootScope.shareDialog != null ||
               $rootScope.dialog != null ||
               $rootScope.resendDialog != null ||
               $rootScope.deleteAccount != null ||
               $rootScope.likeDialog != null ||
               $rootScope.followDialog != null ||
               $rootScope.facebookDialog != null ||
               $rootScope.recommendedOfferDialog != null ||
               $rootScope.invitationDialog != null ||
               $rootScope.specialPromotionDialog != null ||
               $rootScope.winnerDialog != null ||
               $rootScope.dreamPhotoMoreDialog != null ||
               $rootScope.dreamPhotoApplyDialog != null ||
               $rootScope.fortuneTellingApplyDialog != null ||
               $rootScope.lineStickerApplyDialog != null ||
               $rootScope.sponsorDialog != null;
        if (isOpen) {
            var dv = window;
            var offetY = 0;
            if( document.defaultView ) {
                dv = document.defaultView;
                offetY = document.defaultView.pageYOffset;
            } else {
                offetY = document.documentElement.scrollTop;
            }

            document.documentElement.style.overflow = "hidden";
            dv.scrollTo(0, offetY);
        } else {
            document.documentElement.style.overflow = "auto";
        } 

        return isOpen;
    };

    // broadcast receivers
    $rootScope.$on(AUTH_EVENTS.notAuthenticated, $rootScope.doLogout);
    $rootScope.$on(AUTH_EVENTS.notAuthorized, $rootScope.doLogout);

    // load 
    $rootScope.loadConfig();
}]); 
