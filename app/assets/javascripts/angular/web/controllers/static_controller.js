app.controller('StaticController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    '$interval',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        $interval,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "VIRAL GIVEAWAY";
    
    $scope.next_time = '00:00:00';
    $scope.next_day = '0';
   
    $rootScope.initPage(function() {
        Api.get(Api.url_for('/v1/campaigns/tags/iPhone'))
            .success(function(resp) {
                var data = resp.data;
                var campaign = data.campaigns.length > 0 ? data.campaigns[0] : null;
                $scope.diff = campaign ? campaign.end_at_time - (new Date()).getTime() / 1000 : 0; 
                var countdownFunc = function(){
                    var diff = $scope.diff;
                    if ( diff <= 0 ) {
                        $scope.next_day  = 0;
                        $scope.next_time = '00:00:00';
                        return;
                    }
                    $scope.next_time = '';
                    $scope.next_day  = parseInt(diff / 86400);
                    diff -= $scope.next_day * 86400;
                    var hour = parseInt(diff / 3600);
                    diff -= hour * 3600;
                    var minute = parseInt(diff / 60);
                    diff -= minute * 60;
                    var second = parseInt(diff);
                    var parts = [hour,minute,second];
                    for (var j=0; j<parts.length; j++) {
                        var n = parts[j];
                        n = n >= 10 ? n : '0' + n;
                        parts[j] = n;
                    }
                    $scope.next_time = parts.join(':');
                    $scope.diff--;
                };
                countdownFunc();
                $interval(countdownFunc, 1000);
            });
    });
}])
.controller('StaticRegisterCompleteController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "Register Complete";

    $scope.resend = {};
    $scope.doResend = function() {
        $rootScope.showLoadingDialog();
        Api.post(Api.url_for('/v1/users/resend_verify'), $scope.resend)
            .success(function(resp) {
                $rootScope.hideLoadingDialog();
                $scope.hideResendDialog();
                alertify.success("Sent verify Email");
            });
    };
    $scope.showResendDialog = function() {
        $rootScope.resendDialog = {}; 
    };
    $scope.hideResendDialog = function() {
        $rootScope.resendDialog = null; 
    };
}])
.controller('StaticUnregisterCompleteController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {
    $scope.title = "Account Deleted";

}])
;
