var app = angular.module('Vg');
app.service('OperatorService', function(Api){
    this.authenticate = function(account_id_or_email, password, successCallback, errorCallback) {
        Api.post(
                '/operator/login', 
                {
                    'account_id_or_email': account_id_or_email,
                    'password': password,
                }
            ).success(successCallback)
            .error(errorCallback);
    };
});
