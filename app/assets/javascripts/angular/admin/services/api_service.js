var app = angular.module('Vg');
app.service('Api', function($http){
    this.keys = function(h) {
        var keys = [];
        if (h) {
            for (k in h) {
                keys.push(k);
            }
        }
        return keys.sort();
    };

    this.getQueryString = function(params) {
        var qs = '';
        if (params) {
            var keys = this.keys(params);
            console.log(keys);
            for (var i=0; i<keys.length; i++) {
                var k = keys[i];
                var v = params[k];
                if (qs.length > 0) {
                    qs += '&';
                }
                qs += k + '=' + v;
            }
        }
        return qs;
    };

    this.generateHeaders = function(headers) {
        headers = headers || {};
        console.log('headers:', headers);
        return headers;
    }

    this.get = function(url, params) {
        if ( params ) {
            url += '?' + this.getQueryString(params);
        }
        console.log("url=", url);

        return $http.get(url);
    };

    this.post = function(url, params, headers) {
        return $http.post(url, params, {headers: this.generateHeaders(headers)}); 
    };
});
