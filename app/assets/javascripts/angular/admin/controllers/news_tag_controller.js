var app = angular.module('Vg');
app.controller('NewsTagController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api   
    ) {
        $scope.requireAuthenticate();
        $scope.uploadParams = {};


        $scope.query = $location.search();
        delete $scope.query['since_id'];
        console.log($scope.query);

        $scope.tagsdata = [];
        $scope.tagsdataIds = [];
     

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/news_tag');
                return;
            }
           
            Api.get('/news_tag', $scope.query)
                .success(function(resp){
                    var data = resp.data;     
                     console.log($scope.query);
                    console.log(data);

                    if (append) {
                        $scope.tagsdata = $scope.tagsdata.concat(data);
                        $scope.tagsdataIds = $scope.tagsdataIds.concat(data.map(function(r){return r.id}))
                    } else {
                        $scope.tagsdata = data;
                        $scope.tagsdataIds = data.map(function(r){return r.id});
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.uploadResults = function() {
            if ($scope.uploadParams['result_data']) {
                var formData = new FormData();
                formData.append('result', $scope.uploadParams.result_data);
        
                Api.post('/news_tag/upload', formData, {'Content-Type': undefined})
                    .success(function(resp){
                    alertify.success('Save successful!');
                    $route.reload();
                });

            } else {
                alertify.error("Please specify result csv.");
            } 
        };

        $scope.preUploadResults = function(id) {
            var file = event.target.files[0];
            $scope.uploadParams['result_data'] = null;
            if (/^text\//i.test(file.type)) {
                loadResults(id, file, $scope);
            } else {
                alertify.error("It is not text.")
            }
        }

        function loadResults(id, file, scope) {
            var reader = new FileReader();

            reader.onprogress = function(data) {
                if (data.lengthComputable) {                                            
                    var progress = parseInt( ((data.loaded / data.total) * 100), 10 );
                    scope.preloadStatus = progress + "%";
                    scope.$apply(); 
                }
            }
            
            reader.onloadend = function () {
                $scope.uploadParams['result_data'] = reader.result;
                scope.preloadStatus += " Ready!!"
                scope.$apply(); 
            }

            reader.onerror = function () {
                alertify.error('There was an error reading the file!');
            }

            reader.readAsDataURL(file);
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this tag?', function(e) {
                if (e) {
                    Api.post('/news_tag/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }
        $scope.search();       
    }])
.controller('NewsTagNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api',     
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.tagsdata = {};
      
        var id = $location.search()['id'];
        if (id) {
            Api.get('/news_tag/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.tagsdata = data;                 
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.tagsdata);
            var uri = '/news_tag' + ($scope.tagsdata.id ? '/update' : '/create');          
            Api.post(uri, $scope.tagsdata)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.tagsdata = data;
                     window.location.href = '/#/news_tag?city_id=all&category_id=all';       
                })
                .error(function(data) {
                    console.log(data);
                });

        }
    }])
;
