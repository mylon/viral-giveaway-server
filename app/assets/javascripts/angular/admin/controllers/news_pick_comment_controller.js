var app = angular.module('Vg');
app.controller('NewsPickCommentController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api   
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.comments = [];
        $scope.commentsIds = [];
     

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/news_pick_comment');
                return;
            }

            console.log($scope.query);
            Api.get('/news_pick_comment', $scope.query)
                .success(function(resp){
                    var data = resp.data;     

                    if (append) {
                        $scope.comments = $scope.comments.concat(data);
                        $scope.commentsIds = $scope.commentsIds.concat(data.map(function(r){return r.id}))
                    } else {
                        $scope.comments = data;
                        $scope.commentsIds = data.map(function(r){return r.id});
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this comments?', function(e) {
                if (e) {
                    Api.post('/news_pick_comment/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }
        $scope.search();       
    }])
.controller('NewsPickNewCommentController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api',     
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.comments = {};
      
        var id = $location.search()['id'];
        if (id) {
            Api.get('/news_pick_comment/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.comments = data;                 
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.comments);
            var uri = '/news_pick_comment' + ($scope.comments.id ? '/update' : '/create');          
            Api.post(uri, $scope.comments)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.comments = data;
                })
                .error(function(data) {
                    console.log(data);
                });

        }
    }])
;
