var app = angular.module('Vg');
app.controller('FortuneTellingResultsController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.uploadParams = {};
        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/fortune_telling_results');
                return;
            }

            console.log($scope.query);
            Api.get('/fortune_telling_results', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.fortune_telling_results = $scope.fortune_telling_results.concat(data);
                    } else {
                        $scope.fortune_telling_results = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        };

        $scope.delete = function(date) {
            alertify.confirm('Are you delete result?', function(e) {
                if (e) {
                    Api.post("/fortune_telling_results/delete", {target_date: date})
                        .success(function(resp) {
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        };

        $scope.uploadResults = function() {
            if ($scope.uploadParams['result_data']) {
                var formData = new FormData();
                formData.append('result', $scope.uploadParams.result_data);
        
                Api.post('/fortune_telling_results/upload', formData, {'Content-Type': undefined})
                    .success(function(resp){
                    alertify.success('Save successful!');
                    $route.reload();
                });

            } else {
                alertify.error("Please specify result csv.");
            } 
        };

        $scope.preUploadResults = function(id) {
            var file = event.target.files[0];
            $scope.uploadParams['result_data'] = null;
            if (/^text\//i.test(file.type)) {
                loadResults(id, file, $scope);
            } else {
                alertify.error("It is not text.")
            }
        }

        function loadResults(id, file, scope) {
            var reader = new FileReader();

            reader.onprogress = function(data) {
                if (data.lengthComputable) {                                            
                    var progress = parseInt( ((data.loaded / data.total) * 100), 10 );
                    scope.preloadStatus = progress + "%";
                    scope.$apply(); 
                }
            }
            
            reader.onloadend = function () {
                $scope.uploadParams['result_data'] = reader.result;
                scope.preloadStatus += " Ready!!"
                scope.$apply(); 
            }

            reader.onerror = function () {
                alertify.error('There was an error reading the file!');
            }

            reader.readAsDataURL(file);
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }
        
        $scope.search();
    }])
.controller('FortuneTellingResultsNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.target_date = null;
        $scope.fortune_telling_results = [];
        var target_date = $location.search()['target_date'];
        if (target_date) {
            Api.get('/fortune_telling_results/'+target_date)
                .success(function(data) {
                    console.log(data);
                    $scope.target_date = data.target_date;
                    $scope.fortune_telling_results = data.fortune_telling_results;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            var params = {
                results: []
            }
            params.target_date = $scope.target_date;
            for ( var i=0; i<$scope.fortune_telling_results.length; i++) {
                r = $scope.fortune_telling_results[i];
                params.results.push({ 
                    target_date: r.target_date, 
                    constellation: r.constellation,
                    love_text_en: r.love_text_en, 
                    money_text_en: r.money_text_en, 
                    work_text_en: r.work_text_en, 
                    total_text_en: r.total_text_en, 
                }); 
            }
            Api.post('/fortune_telling_results/update', params)
                .success(function(resp){
                alertify.success('Save successful!');
                $scope.fortune_telling_result = resp.data;
            });
        }
    }])
;
