var app = angular.module('Vg');
app.controller('SystemInformationController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        Api.get('/system_information')
            .success(function(resp){
                $scope.system_information = resp;
                console.log($scope.system_information);
            });

        $scope.save = function() {
            Api.post('/system_information', $scope.system_information)
                .success(function(data){
                    $scope.config.system_information = data;
                    alertify.success('Save successful!');
                });

        }
    }])
;
