var app = angular.module('Vg');
app.controller('LineStickerRequestsController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/line_sticker_requests');
                return;
            }

            console.log($scope.query);
            Api.get('/line_sticker_requests', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.line_sticker_requests = $scope.line_sticker_requests.concat(data);
                    } else {
                        $scope.line_sticker_requests = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }
        
        $scope.search();
    }])
.controller('LineStickerRequestsNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.line_sticker_request = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/line_sticker_requests/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.line_sticker_request = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.line_sticker_request);
            var uri = '/line_sticker_requests/update';

            Api.post(uri, $scope.line_sticker_request)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.line_sticker_request = data;
                });
        }
    }])
;
