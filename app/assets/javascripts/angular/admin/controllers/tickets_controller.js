var app = angular.module('Vg');
app.controller('TicketsController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/tickets');
                return;
            }

            console.log($scope.query);
            Api.get('/tickets', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.tickets = $scope.tickets.concat(data);
                    } else {
                        $scope.tickets = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.search();
    }])
;
