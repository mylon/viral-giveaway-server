var app = angular.module('Vg');
app.controller('NewsPickThaiController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',   
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api   
    ) {
        $scope.requireAuthenticate();      

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.newsPick = [];
        $scope.fullnewsPick = [];
        $scope.newsPickIds = [];
        $scope.newsPickTitle = 'Thai News Pick';   
        $scope.region = "th";   
        $scope.createText = "Create Thai News"; 
        $scope.all_count = 0;

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/news_pick/th');
                return;
            }           
            console.log($scope.query);
            Api.get('/news_pick/th', $scope.query)
                .success(function(resp){
                    var data = resp.data; 
                    var full_data = resp.full_data;
                    var news_count = resp.news_count;   
                    $scope.operators = resp.operators;

                    if (append) {
                        $scope.newsPick = $scope.newsPick.concat(data);
                        $scope.all_count = news_count;
                    } else {
                        $scope.newsPick = data;
                        $scope.all_count = news_count;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this news?', function(e) {
                if (e) {
                    Api.post('/news_pick/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.search();
       
    }])
.controller('NewsPickNewThaiController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    '$http', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api,
        $http
    ) {
        $scope.requireAuthenticate();

        $scope.newspick = {};
        $scope.newText = "Thai News"; 
        $scope.regionValue = "th"; 

        var id = $location.search()['id'];
        if (id) {
            Api.get('/news_pick/th/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.newspick = data;
                    $scope.newspick.region = 'th'; 
                    $scope.newspick.news_staff_checked = 1;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.verifyDuplicateNews = function() {
            if($scope.newspick.news_url != "") {
                Api.post('news_pick/checkduplication', $scope.newspick)
                .success(function(data){ 
                    if(data) {
                        alertify.error("!!!!This news already Added!!!!");
                    }
                });
            }            
        }

        $scope.getURLcontent = function(){
            if($scope.newspick.news_url != "") {
                $http.get($scope.newspick).then(function(response) {
                    var raw_html = response.data;
                    console.log('in');
                     console.log(raw_html);
                });
            }
        }

        $scope.save = function() {
            console.log('save', $scope.newspick);
            var uri = '/news_pick' + ($scope.newspick.id ? '/update' : '/create');
            $scope.newspick.region = 'th';
            $scope.newspick.news_staff_checked = 1;
            Api.post(uri, $scope.newspick)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.newspick = data;
                    $scope.newspick.region = 'th';  
                    $scope.newspick.news_staff_checked = 1;
                    //window.location.href = '/#/news_pick/th';  
                    if (!$scope.newspick.id){ 
                        window.location.href = '/#/news_pick/th/new/#console';     
                    }       
                })
                .error(function(data) {
                    console.log(data);
                });

        }
    }])
.controller('NewsPickEnglishController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.newsPick = [];
        $scope.fullnewsPick = [];
        $scope.newsPickIds = [];
        $scope.newsPickTitle = 'English News Pick';
        $scope.region = "en"; 
        $scope.createText = "Create English News"; 
        $scope.all_count = 0;

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/news_pick/en');
                return;
            }

            console.log($scope.query);
            Api.get('/news_pick/en', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    var full_data = resp.full_data;  
                    var news_count = resp.news_count; 
                    $scope.operators = resp.operators;

                    if (append) {
                        $scope.newsPick = $scope.newsPick.concat(data);
                        $scope.all_count = news_count; 
                    } else {
                        $scope.newsPick = data;
                        $scope.all_count = news_count;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this news?', function(e) {
                if (e) {
                    Api.post('/news_pick/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.search();
       
    }])
.controller('NewsPickNewEnglishController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.newspick = {};
        $scope.newText = "English News"; 
        $scope.regionValue = "en"; 

        var id = $location.search()['id'];
        if (id) {
            Api.get('/news_pick/en/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.newspick = data;
                    $scope.newspick.region = 'en'; 
                    $scope.newspick.news_staff_checked = 1; 
                })
                .error(function(data) {
                    console.log(data);
                });
        } 

        $scope.verifyDuplicateNews = function() {
            if($scope.newspick.news_url != "") {
                Api.post('news_pick/checkduplication', $scope.newspick)
                .success(function(data){ 
                    if(data) {
                        alertify.error("!!!!This news already Added!!!!");
                    }
                });
            }            
        }       

        $scope.save = function() {
            console.log('save', $scope.newspick);
            var uri = '/news_pick' + ($scope.newspick.id ? '/update' : '/create');
            $scope.newspick.region = 'en';
            $scope.newspick.news_staff_checked = 1;
            Api.post(uri, $scope.newspick)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.newspick = data;
                    $scope.newspick.region = 'en';   
                    $scope.newspick.news_staff_checked = 1; 
                    //window.location.href = '/#/news_pick/en';  
                    if (!$scope.newspick.id){
                         window.location.href = '/#/news_pick/en/new/#console';  
                    }
                          
                })
                .error(function(data) {
                    console.log(data);
                });

        }        
    }])
.controller('NewsPickJapanController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.newsPick = [];
        $scope.newsPickIds = [];
        $scope.fullnewsPick = [];
        $scope.newsPickTitle = 'Japan News Pick';
        $scope.region = "jp";               
        $scope.createText = "Create Japan News";   
        $scope.all_count = 0;       


        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/news_pick/jp');
                return;
            }

            
            Api.get('/news_pick/jp', $scope.query)
                .success(function(resp){
                    var data = resp.data;                         
                    var full_data = resp.full_data;   
                    var news_count = resp.news_count;
                    $scope.operators = resp.operators;


                    if (append) {
                        $scope.newsPick = $scope.newsPick.concat(data);
                        $scope.all_count = news_count;
                    } else {
                        $scope.newsPick = data;
                        $scope.all_count = news_count; 
                    }

                    
                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this news?', function(e) {
                if (e) {
                    Api.post('/news_pick/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.search();
       
    }])
.controller('NewsPickNewJapanController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api',
    '$http', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api,
        $http
    ) {
        $scope.requireAuthenticate();

        $scope.newspick = {};
        $scope.newText = "Japan News"; 
        $scope.regionValue = "jp"; 

        $http.defaults.useXDomain = true;

        var id = $location.search()['id'];
        if (id) {
            Api.get('/news_pick/jp/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.newspick = data;
                    $scope.newspick.region = 'jp'; 
                    $scope.newspick.news_staff_checked = 1; 
                })
                .error(function(data) {
                    console.log(data);
                });
        }   

        $scope.verifyDuplicateNews = function() {
            if($scope.newspick.news_url != "") {
                Api.post('news_pick/checkduplication', $scope.newspick)
                .success(function(data){ 
                    if(data) {
                        alertify.error("!!!!This news already Added!!!!");
                    }
                });
            }            
        }    

        $scope.getURLcontent = function(){
            if($scope.newspick.news_url != "") {
                $http.get($scope.newspick).then(function(response) {
                    var raw_html = response.data;
                    console.log('in');
                     console.log(raw_html);
                });
            }
        }


        $scope.save = function() {
            console.log('save', $scope.newspick);
            var uri = '/news_pick' + ($scope.newspick.id ? '/update' : '/create');
            $scope.newspick.region = 'jp';
            $scope.newspick.news_staff_checked = 1;
            Api.post(uri, $scope.newspick)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.newspick = data;
                    $scope.newspick.region = 'jp';   
                    $scope.newspick.news_staff_checked = 1; 
                    console.log($scope.newspick[0].id);
                    if (!$scope.newspick.id){
                        window.location.href = '/#/news_pick/jp/new/#console'; 
                    }    
                })
                .error(function(data) {
                    console.log(data);
                });

        }
    }])
;
