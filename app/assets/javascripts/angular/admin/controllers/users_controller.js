var app = angular.module('Vg');
app.controller('UsersController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/users');
                return;
            }

            console.log($scope.query);
            Api.get('/users', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.users = $scope.users.concat(data);
                    } else {
                        $scope.users = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.grantPoint = function(user) {
            var point = window.prompt("How many grant points?", "100");
            Api.post('/users/grant_point', { user_id: user.id, point: point})
                .success(function(resp) {
                    alertify.success('Grant successful!');
                });
        };

        $scope.search();
    }])
.controller('UsersNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.user = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/users/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.user = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.user);
            var uri = '/users' + ($scope.user.id ? '/update' : '/create');

            Api.post(uri, $scope.user)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.user = data;
                });
        }

    }])
;
