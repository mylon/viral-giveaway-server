var app = angular.module('Vg');
app.controller('NewsPicksSourceController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api   
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.sources = [];
        $scope.sourcesIds = [];
     

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/news_picks_source');
                return;
            }

            console.log($scope.query);
            Api.get('/news_picks_source', $scope.query)
                .success(function(resp){
                    var data = resp.data;     

                    if (append) {
                        $scope.sources = $scope.sources.concat(data);
                        $scope.sourcesIds = $scope.sourcesIds.concat(data.map(function(r){return r.id}))
                    } else {
                        $scope.sources = data;
                        $scope.sourcesIds = data.map(function(r){return r.id});
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this sources?', function(e) {
                if (e) {
                    Api.post('/news_picks_source/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }
        $scope.search();       
    }])
.controller('NewsPicksSourceNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api',     
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.sources = {};
      
        var id = $location.search()['id'];
        if (id) {
            Api.get('/news_picks_source/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.sources = data;                 
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.sources);
            var uri = '/news_picks_source' + ($scope.sources.id ? '/update' : '/create');          
            Api.post(uri, $scope.sources)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.sources = data;
                     window.location.href = '/#/news_picks_source';       
                })
                .error(function(data) {
                    console.log(data);
                });

        }
    }])
;
