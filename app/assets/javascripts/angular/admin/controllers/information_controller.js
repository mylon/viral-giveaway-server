var app = angular.module('Vg');
app.controller('InformationController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        Api.get('/information')
            .success(function(data){
                $scope.informations = data;
            });


        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete information?', function(e) {
                if (e) {
                    Api.post('/information/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

    }])
.controller('InformationNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.information = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/information/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.information = data;
                    $scope.information.published_at = $rootScope.formatDatePickerString($scope.information.published_at);
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.information);
            var uri = '/information' + ($scope.information.id ? '/update' : '/create');

            Api.post(uri, $scope.information)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.information = data;
                    $scope.information.published_at = $rootScope.formatDatePickerString($scope.information.published_at);
                });
        }
    }])
;
