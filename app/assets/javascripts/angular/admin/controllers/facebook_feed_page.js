var app = angular.module('Vg');
app.controller('FacebookFeedPageController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',   
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api   
    ) {
        $scope.requireAuthenticate();      

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.facebookFeedPage = [];
        $scope.newsMultipleIds = [];
        $scope.newsPickTitle = 'Faecebook Feed'    
        $scope.createText = "Create Faecebook Feed"; 
        $scope.all_count = 0;

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/facebook_feed_page');
                return;
            }    
            console.log("search");       
            console.log($scope.query);
            Api.get('/facebook_feed_page/', $scope.query)
                .success(function(resp){
                    var data = resp.data; 
                    var feed_count = resp.feed_count;   
                    $scope.operators = resp.operators;

                    console.log(data);

                    if (append) {
                        $scope.facebookFeedPage = $scope.facebookFeedPage.concat(data);
                        $scope.feed_count = feed_count;
                    } else {
                        $scope.facebookFeedPage = data;
                        $scope.feed_count = feed_count;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this news?', function(e) {
                if (e) {
                    Api.post('/facebook_feed_page/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.search();
       
    }])
.controller('FacebookFeedPageNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    '$http', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api,
        $http
    ) {
        $scope.requireAuthenticate();

        $scope.facebookfeed = {};
        $scope.feedText = "Facebook Feed"; 

        var id = $location.search()['id'];
        if (id) {
            Api.get('/facebook_feed_page/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.facebookfeed = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.facebookfeed);
            var uri = '/facebook_feed_page' + ($scope.facebookfeed.id ? '/update' : '/create');
            Api.post(uri, $scope.facebookfeed)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.facebookfeed = data;
                    //window.location.href = '/#/news_pick/th';  
                    if (!$scope.facebookfeed.id){ 
                        window.location.href = '/#/facebook_feed_page/new/#console';     
                    }       
                })
                .error(function(data) {
                    console.log(data);
                });
            
        }
    }])
;
