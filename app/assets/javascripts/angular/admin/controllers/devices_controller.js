var app = angular.module('Vg');
app.controller('DevicesController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/devices');
                return;
            }

            console.log($scope.query);
            Api.get('/devices', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.devices = $scope.devices.concat(data);
                    } else {
                        $scope.devices = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.search();

    }])
.controller('DevicesNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.device = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/devices/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.device = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.device);
            var uri = '/devices' + ($scope.device.id ? '/update' : '/create');

            Api.post(uri, $scope.device)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.device = data;
                });
        }
    }])
;
