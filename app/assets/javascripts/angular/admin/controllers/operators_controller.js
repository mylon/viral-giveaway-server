var app = angular.module('Vg');
app.controller('OperatorsController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/operators');
                return;
            }

            console.log($scope.query);
            Api.get('/operators', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.operators = $scope.operators.concat(data);
                    } else {
                        $scope.operators = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete operator?', function(e) {
                if (e) {
                    Api.post('/operators/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.search();
    }])
.controller('OperatorsNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.operator = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/operators/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.operator = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            $scope.operator.password = $scope.operator.password_tmp;
            delete $scope.operator.password_tmp;
            console.log('save', $scope.operator);
            var uri = '/operators' + ($scope.operator.id ? '/update' : '/create');

            Api.post(uri, $scope.operator)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.operator = data;
                });
        }
    }])
;
