var app = angular.module('Vg');
app.controller('LocalPicksNewPushNotificationController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    '$timeout',
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        $timeout,
        Api   
    ) {
        $scope.requireAuthenticate();

        $scope.localpickpush = {};

        $scope.save = function() {
            console.log('save', $scope.localpickpush);
            var uri = '/local_picks_push_notification/create';           
            Api.post(uri, $scope.localpickpush)
                .success(function(data){
                    alertify.success('Push successful!');
                    console.log(data);
                    $scope.localpickpush = data;                   
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.functionThatReturnsPromise = function() {
            return $timeout(angular.noop, 6000);
        }
         
    }])
;
