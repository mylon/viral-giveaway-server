var app = angular.module('Vg');
app.controller('OffersController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.offers = [];
        $scope.offersIds = [];
        $scope.bulk = {
            all: false,
            checkIds: [],
            action: '',
            lastMasterIdx: -1,
        };

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/offers');
                return;
            }

            console.log($scope.query);
            Api.get('/offers', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.offers = $scope.offers.concat(data);
                        $scope.offersIds = $scope.offersIds.concat(data.map(function(r){return r.id}))
                    } else {
                        $scope.offers = data;
                        $scope.offersIds = data.map(function(r){return r.id});
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete offer?', function(e) {
                if (e) {
                    Api.post('/offers/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.doBulkAction = function() {
            if ( !$scope.bulk.action ) {
                return window.alert("Please specify action.");
            }
            if ( $scope.bulk.checkIds.length == 0 ) {
                return window.alert("Please check records.");
            }

            alertify.confirm('Are you run ' + $scope.bulk.action + '?', function(e) {
                if (e) {
                    Api.post('/offers/bulk/' + $scope.bulk.action, { ids: $scope.bulk.checkIds})
                        .success(function(resp) {
                            alertify.success('Bulk action successful!');
                            $route.reload();
                        });
                }
            });
        };

        $scope.doCheckAll = function() {
            if ($scope.bulk.all) {
                $scope.bulk.all = false;
                $scope.bulk.checkIds = [];
            } else {
                $scope.bulk.all = true;
                angular.forEach($scope.offers, function(r) {
                    $scope.bulk.checkIds.push(r.id);
                });
            }
            console.log($scope.bulk);
        };

        $scope.doCheck = function(event, id) {
            var masterIdx = $scope.offersIds.indexOf(id);
            var lastMasterIdx = $scope.bulk.lastMasterIdx;
            $scope.bulk.lastMasterIdx = masterIdx;

            var idx = $scope.bulk.checkIds.indexOf(id);
            if ( idx > -1 ) {
                $scope.bulk.all = false;
                delete $scope.bulk.checkIds[idx];
            } else {
                $scope.bulk.checkIds.push(id);
            }

            console.log("shfit=%s, masterIdx=%s, lastMasterIdx=%s", $rootScope.isShift(event), masterIdx, lastMasterIdx);
            if ($rootScope.isShift(event) && lastMasterIdx > -1) {
                var start = lastMasterIdx < masterIdx ? lastMasterIdx : masterIdx;
                var end = masterIdx > lastMasterIdx ? masterIdx : lastMasterIdx;
                for (var i=start; i<=end; i++) {
                    var _id = $scope.offers[i].id;
                    var _idx = $scope.bulk.checkIds.indexOf(_id);
                    if ( idx > -1 ) {
                        if ( _idx > -1 ) {
                            delete $scope.bulk.checkIds[_idx];
                        }
                    } else {
                        if ( _idx == -1 ) {
                            $scope.bulk.checkIds.push(_id);
                        }
                    }
                }
            }

            console.log($scope.bulk);
        };

        $scope.isChecked = function(id) {
            console.log($scope.bulk);
            return $scope.bulk.checkIds.indexOf(id) > -1;
        };

        $scope.search();
    }])
.controller('OffersNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.offer = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/offers/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.offer = data;
                    $scope.offer.start_at = $rootScope.formatDatePickerString($scope.offer.start_at);
                    $scope.offer.end_at   = $rootScope.formatDatePickerString($scope.offer.end_at);
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.offer);
            var uri = '/offers' + ($scope.offer.id ? '/update' : '/create');

            Api.post(uri, $scope.offer)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.offer = data;
                    $scope.offer.start_at = $rootScope.formatDatePickerString($scope.offer.start_at);
                    $scope.offer.end_at   = $rootScope.formatDatePickerString($scope.offer.end_at);
                });
        }

        $scope.changeProvider = function() {
            var c = $rootScope.config;
            if ( c.offer_provider_map['app_driver'] == $scope.offer.provider ||
                 c.offer_provider_map['taptica'] == $scope.offer.provider ) {
                $scope.offer.promotional = true;
            } else {
                $scope.offer.promotional = false;
            }
        }
    }])
;
