var app = angular.module('Vg');
app.controller('NewsPickNewPushNotificationController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    '$timeout',
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        $timeout,
        Api   
    ) {
        $scope.requireAuthenticate();

        $scope.newspickpush = {};

        $scope.save = function() {
            console.log('save', $scope.newspickpush);
            var uri = '/news_pick_push_notification/create';           
            Api.post(uri, $scope.newspickpush)
                .success(function(data){
                    alertify.success('Push successful!');
                    console.log(data);
                    $scope.newspickpush = data;                   
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.functionThatReturnsPromise = function() {
            return $timeout(angular.noop, 6000);
        }
         
    }])
;
