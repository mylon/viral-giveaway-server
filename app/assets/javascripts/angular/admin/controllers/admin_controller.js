var app = angular.module('Vg');
app.controller('AdminController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$filter',
    'AUTH_EVENTS', 
    'Session', 
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $filter,
        AUTH_EVENTS, 
        Session, 
        Api
    ) {

    $rootScope.config = {
        title: 'News App',
    };

    $rootScope.h = (new Date()).getTime();

    Api.get('/config')
        .success(function(data){
            angular.extend( $rootScope.config, angular.copy( data ) );
            console.log($rootScope.config);
        });

    // helpers
    $rootScope.showErrorDialog = function(data) {
        var msg = data.join('<br/>');
        alertify.error(msg);
    };
    $rootScope.getGMTTime = function(timeStr) {
        var d = new Date(timeStr);
        var offset = d.getTimezoneOffset() * 60 * 1000;
        return d.getTime() + offset;
    };
    $rootScope.formatDate = function(time, format) {
        return $filter('date')(time, format);
    }
    $rootScope.formatDatePickerString = function(timeStr) {
        if (timeStr) {
            return $rootScope.formatDate($rootScope.getGMTTime(timeStr), 'yyyy/MM/dd HH:mm');
        } else {
            return '';
        }
    }
    $rootScope.getSessionUser = function() {
        return Session.user;
    };
    $rootScope.isAuthenticated = function() {
        return !!Session.user;
    };
    $rootScope.requireAuthenticate = function() {
        Api.get('/operators/profile')
            .success(function(data, status, headers, config){
                console.log(data);                
                Session.create(data);
            })
            .error(function(data, status, headers, config){
                console.log(data);
            });
    };
    $rootScope.logout = function() {
        console.log('logout');
        Session.destroy();
        Api.post('/operators/logout')
            .then(function(res){
                $location.path('/');
            });
    }; 

    $rootScope.isShift = function(event) {
        var key = event.keyCode;
        var isShift = !!event.shiftKey;
        if ( isShift ) {
           if ( key == 16 ) {
               return false;
           } else {
               return true;
           }
        } else {
            return false;
        }
    };

    // broadcast receivers
    $rootScope.$on(AUTH_EVENTS.notAuthenticated, $rootScope.logout);
    $rootScope.$on(AUTH_EVENTS.notAuthorized, $rootScope.logout);
}]); 
