var app = angular.module('Vg');
app.controller('NewsMultipleController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',   
    'Api',    
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api   
    ) {
        $scope.requireAuthenticate();      

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.newsMultiple = [];
        $scope.newsMultipleIds = [];
        $scope.newsPickTitle = 'News Pick'    
        $scope.createText = "Create News"; 
        $scope.all_count = 0;

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/news_multiple');
                return;
            }    
            console.log("search");       
            console.log($scope.query);
            Api.get('/news_multiple/', $scope.query)
                .success(function(resp){
                    var data = resp.data; 
                    var news_count = resp.news_count;   
                    $scope.operators = resp.operators;

                    console.log(data);

                    if (append) {
                        $scope.newsMultiple = $scope.newsMultiple.concat(data);
                        $scope.all_count = news_count;
                    } else {
                        $scope.newsMultiple = data;
                        $scope.all_count = news_count;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete this news?', function(e) {
                if (e) {
                    Api.post('/news_multiple/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.search();
       
    }])
.controller('NewsMultipleNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    '$http', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api,
        $http
    ) {
        $scope.requireAuthenticate();

        $scope.newspick = {};
        $scope.newText = "News"; 

        var id = $location.search()['id'];
        if (id) {
            Api.get('/news_multiple/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.newspick = data;
                    $scope.newspick.news_staff_checked = 1;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.verifyDuplicateNews = function() {
            if($scope.newspick.news_url != "") {
                Api.post('news_multiple/checkduplication', $scope.newspick)
                .success(function(data){ 
                    if(data) {
                        alertify.error("!!!!This news already Added!!!!");
                    }
                });
            }            
        }

         $scope.checkDuplicateHTTP = function() {
            if($scope.newspick.image_url != "") {
                Api.post('news_multiple/countword', $scope.newspick)
                .success(function(data){ 
                    if(data) {
                        if (data >=2 ) {
                            alertify.error("!!!!You add image twice!!!!");
                        }
                    }
                });
            }
        }

        $scope.getURLcontent = function(){
            if($scope.newspick.news_url != "") {
                $http.get($scope.newspick).then(function(response) {
                    var raw_html = response.data;
                    console.log('in');
                     console.log(raw_html);
                });
            }
        }

        $scope.save = function() {
            console.log('save', $scope.newspick);
            var uri = '/news_multiple' + ($scope.newspick.id ? '/update' : '/create');
            $scope.newspick.news_staff_checked = 1;
            Api.post(uri, $scope.newspick)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.newspick = data;
                    $scope.newspick.news_staff_checked = 1;
                    //window.location.href = '/#/news_pick/th';  
                    if (!$scope.newspick.id){ 
                        window.location.href = '/#/news_multiple/new/#console';     
                    }       
                })
                .error(function(data) {
                    console.log(data);
                });
            
        }
    }])
;
