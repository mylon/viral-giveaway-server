var app = angular.module('Vg');
app.controller('DreamPhotosController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/dream_photos');
                return;
            }

            console.log($scope.query);
            Api.get('/dream_photos', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.dream_photos = $scope.dream_photos.concat(data);
                    } else {
                        $scope.dream_photos = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }
        
        $scope.search();
    }])
.controller('DreamPhotosNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.dream_photo = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/dream_photos/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.dream_photo = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            if ( !$scope.dream_photo.result_data ) {
                alertify.error("Please specify result photo.");
                return;
            }

            alertify.confirm('Will send result photo to user. Are you ok?', function(e) {
                if ( !e ) {
                    return;
                }

                var formData = new FormData();
                if ($scope.dream_photo.result_data) {
                    formData.append('result_photo', $scope.dream_photo.result_data);
                }
                formData.append('id', $scope.dream_photo.id);
        
                Api.post('/dream_photos/update', formData, {'Content-Type': undefined})
                    .success(function(resp){
                    alertify.success('Save successful!');
                    $scope.dream_photo = resp.data;
                });
            });
        }

        $scope.showPhotoPreview = function(photoId) {
            var file = event.target.files[0];
            if (/^image\//i.test(file.type)) {
                loadImage(photoId, file);
            } else {
                console.log("It is not image.");
            }
        }

        function loadImage(photoId, file) {
            var reader = new FileReader();

            reader.onloadend = function () {
                $scope.dream_photo[photoId + '_data'] = reader.result;
                drawImage(photoId, reader.result, file.type);
            }

            reader.onerror = function () {
                alert('There was an error reading the file!');
            }

            reader.readAsDataURL(file);
        }

        function drawImage(photoId, dataUrl, fileType) {
            var maxWidth = 250;
            var maxHeight = 250;

            var image = new Image();
            image.src = dataUrl;

            image.onload = function () {
                var width = image.width;
                var height = image.height;

                var newWidth;
                var newHeight;

                if (width > height) {
                    newHeight = height * (maxWidth / width);
                    newWidth = maxWidth;
                } else {
                    newWidth = width * (maxHeight / height);
                    newHeight = maxHeight;
                }

                var canvas = $('#preview-' + photoId); 
                
                canvas.get(0).width = newWidth;
                canvas.get(0).height = newHeight;

                var context = canvas.get(0).getContext('2d');

                context.drawImage(this, 0, 0, newWidth, newHeight);
                $scope.$apply();
            };

            image.onerror = function () {
                alert('There was an error processing your file!');
            };
        }

    }])
;
