var app = angular.module('Vg');
app.controller('DashboardController', ['$scope', '$location', 'Api', function($scope, $location, Api){
    $scope.requireAuthenticate();

    $scope.summary = {};

    $scope.loadSummary = function(resource) {
        Api.get('/' + resource + '/summary')
            .success(function(data){
                $scope.summary[resource] = data;
            })
            .error(function(data){
            });

    }

    $scope.loadData = function() {
        $scope.loadSummary('users');
        $scope.loadSummary('trackings');
    };

    $scope.loadData();
}]); 
