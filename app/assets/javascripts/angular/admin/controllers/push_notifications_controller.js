var app = angular.module('Vg');
app.controller('PushNotificationsNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();
        $scope.notification = {
            message: '<p style="padding:0 10px;">EXAMPLE</p>',
        };

        $scope.send = function() {
            if ( window.confirm("Are you sure?") ) {
                console.log('send', $scope.notification);
                var uri = '/push_notifications/create';
                Api.post(uri, $scope.notification)
                    .success(function(data){
                        alertify.success('Send successful!');
                        $location.path('/push_notifications/new');
                    });
            }
        };
    }]);
