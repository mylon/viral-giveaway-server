var app = angular.module('Vg');
app.controller('LogsController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.view = "assets/admin/logs/inc_" + $scope.query.log + ".html";
        $scope.search = function(refresh) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/logs');
                return;
            }
            console.log($scope.query);

            if ( !$scope.query.log ) return;
            if ( !$scope.query.from_at || !$scope.query.to_at ) return;

            Api.get('/logs', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    $scope.logs = data;
                    $scope.displayRows = [].concat($scope.logs);
                });
        };

        $scope.setRecentDays = function(n) {
            var now = new Date();
            var ago = new Date(now.getTime() - 1000 * 60 * 60 * 24 * n);
            $scope.query.from_at = ago.getFullYear() + "-" + (ago.getMonth() < 10 ? "0" : "") + (ago.getMonth() + 1) + "-" + (ago.getDate() < 10 ? "0" : "") + ago.getDate();
            $scope.query.to_at = now.getFullYear() + "-" + (now.getMonth() < 10 ? "0" : "") +  (now.getMonth() + 1) + "-" + (now.getDate() < 10 ? "0" : "") + now.getDate();
        };

        $scope.search();
    }])
    .directive('stRatio',function(){
        return {
            link:function(scope, element, attr){
                var ratio=+(attr.stRatio);
                element.css('width',ratio+'%');
            }
        };
    })
    .directive('stSum',function(){
        return {
            restrict: 'E',
            require: '^stTable',
            template: '{{value |number}}',
            scope: {},
            link:function(scope, element, attr, ctrl){
                scope.$watch(ctrl.getFilteredCollection, function(val){
                    var total = 0;
                    angular.forEach(val, function(r) {
                        total += r[attr.column] || 0;
                    });
                    scope.value = total;
                });
            }
        };
    })
    .directive('stAvg',function(){
        return {
            restrict: 'E',
            require: '^stTable',
            template: '{{value |number}}',
            scope: {},
            link:function(scope, element, attr, ctrl){
                scope.$watch(ctrl.getFilteredCollection, function(val){
                    var total = 0;
                    var length = (val || []).length
                    angular.forEach(val, function(r) {
                        total += r[attr.column] || 0;
                    });
                    scope.value = total / length;
                });
            }
        };
    })

;
