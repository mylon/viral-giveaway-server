var app = angular.module('Vg');
app.controller('LoginController', ['$scope', '$location', 'Session', 'Api', function($scope, $location, Session, Api){
    Api.get('/operators/logined')
        .success(function(data, status, headers, config){
            if ( data.logined ) {
                Api.get('/operators/profile')
                    .success(function(data2, status2, headers2, config2){
                        Session.create(data2);
                        $location.path('/dashboard');
                    });
            }
        });

    $scope.operator = {};
    $scope.login = function() {
        console.log($scope.operator);
        Api.post(
                '/operators/login',
                {
                    account_id_or_email: $scope.operator.account_id_or_email,
                    password: $scope.operator.password, 
                }
            )
            .success(function(data, status, headers, config) {
                console.log(data);
                Session.create(data);
                $location.path('/dashboard');
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    };
}]);
