var app = angular.module('Vg');
app.controller('CampaignsController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/campaigns');
                return;
            }

            console.log($scope.query);
            Api.get('/campaigns', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.campaigns = $scope.campaigns.concat(data);
                    } else {
                        $scope.campaigns = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.delete = function(id) {
            console.log('delete', id);
            alertify.confirm('Are you delete campaign?', function(e) {
                if (e) {
                    Api.post('/campaigns/delete', {id: id})
                        .success(function(data){
                            alertify.success('Delete successful!');
                            $route.reload();
                        });
                }
            });
        }

        $scope.search();
    }])
.controller('CampaignsNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.campaign = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/campaigns/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.campaign = data;
                    $scope.campaign.start_at = $rootScope.formatDatePickerString($scope.campaign.start_at);
                    $scope.campaign.end_at   = $rootScope.formatDatePickerString($scope.campaign.end_at);
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.campaign);
            var uri = '/campaigns' + ($scope.campaign.id ? '/update' : '/create');

            Api.post(uri, $scope.campaign)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.campaign = data;
                    $scope.campaign.start_at = $rootScope.formatDatePickerString($scope.campaign.start_at);
                    $scope.campaign.end_at   = $rootScope.formatDatePickerString($scope.campaign.end_at);
                });
        }
    }])
;
