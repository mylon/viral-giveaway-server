var app = angular.module('Vg');
app.controller('MessagesController', [
    '$scope', 
    '$rootScope', 
    '$location',
    '$route',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        $route,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.query = $location.search();
        delete $scope.query['since_id'];

        $scope.search = function(refresh, append) {
            if (refresh) {
                delete $scope.query['since_id'];
                $location.path('/messages');
                return;
            }

            console.log($scope.query);
            Api.get('/messages', $scope.query)
                .success(function(resp){
                    var data = resp.data;
                    if (append) {
                        $scope.messages = $scope.messages.concat(data);
                    } else {
                        $scope.messages = data;
                    }

                    if (data.length == resp.limit) {
                        $scope.query.since_id = data[data.length-1].id;
                    } else {
                        delete $scope.query.since_id;
                    }
                });
        }

        $scope.more = function() {
            console.log($scope.query);
            $scope.search(false, true);
        }

        $scope.search();
    }])
.controller('MessagesNewController', [
    '$scope', 
    '$rootScope', 
    '$location',
    'Api', 
    function(
        $scope, 
        $rootScope, 
        $location,
        Api
    ) {
        $scope.requireAuthenticate();

        $scope.message = {};
        var id = $location.search()['id'];
        if (id) {
            Api.get('/messages/'+id)
                .success(function(data) {
                    console.log(data);
                    $scope.message = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        $scope.save = function() {
            console.log('save', $scope.message);
            var uri = '/messages' + ($scope.message.id ? '/update' : '/create');

            Api.post(uri, $scope.message)
                .success(function(data){
                    alertify.success('Save successful!');
                    $scope.message = data;
                });
        }
    }])
;
