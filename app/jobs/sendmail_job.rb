class SendmailJob < ActiveJob::Base
  queue_as :sendmail

  def perform(from, to, subject, body, opt={})
    message = {
      :from    => from,
      :to      => to,
      :subject => subject,
      :body    => body,
      :opt     => opt,
    }
    
    sns = Aws::SNS::Client.new(
      region: Rails.application.config.sqs_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )
    resp = sns.publish({
      topic_arn: Rails.application.config.sns_topic_arn,
      message: message.to_json,
    })
    Rails.logger.info "message_id=#{resp.message_id}"
  end
end
