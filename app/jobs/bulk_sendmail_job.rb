class BulkSendmailJob < ActiveJob::Base
  queue_as :bulk_sendmail

  def render_mail(options)
    av = ActionView::Base.new(Rails.root.join('app', 'views'))
    av.assign(options[:locals])

    template = options[:template].nil? ? nil : "mail/#{options[:template]}.html.erb"
    layout = options[:layout].nil? ? nil : "layouts/#{options[:layout]}.html.erb"
    av.render(
      :template => template,
      :layout => layout,
    )
  end

  def perform(segment, from, subject, message, force, opt={})
    users = User.find_by_mail_segment(segment.to_sym)
    users.each do |u|
      begin
        job = nil
        body = render_mail({ 
          :template => 'bulk', 
          :layout => 'mail', 
          :locals => {
            :message => message,
          }, 
        })
        if force then
          job = u.send_email!(from, subject, body, opt)
        else
          job = u.send_email(from, subject, body, opt)
        end
        Rails.logger.info "sent bulk sendmail user=#{u.id}, job_id=#{job.job_id}"
      rescue => e
        Rails.logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
        Rails.logger.error "failed bulk sendmail user=#{u.id}"
      end
    end
  end
end
