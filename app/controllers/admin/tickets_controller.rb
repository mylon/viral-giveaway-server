class Admin::TicketsController < Admin::BaseController
  def list
    where = {}
    where[:number] = params[:number].to_s if params[:number].to_s.length > 0
    where[:campaign_id] = params[:campaign_id].to_i if params[:campaign_id].to_i > 0
    where['users.account_id'] = params[:account_id].to_i if params[:account_id].to_s.length > 0

    custom_where = ''
    custom_params = []
    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if (custom_where.length > 0 or where.length > 0) and where['users.account_id'].nil? and where[:campaign_id].nil? and where[:number].nil?
      custom_where += 'tickets.id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = Ticket.joins(:user, :campaign).where(where).where([custom_where, *custom_params]).order('tickets.id desc').limit(Rails.application.config.admin_data_num).map do |r|
      h = r.attributes
      h[:user] = r.user
      h[:campaign] = r.campaign
      h
    end
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }

  end
end
