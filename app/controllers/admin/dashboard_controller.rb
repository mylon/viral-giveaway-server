class Admin::DashboardController < Admin::BaseController
  def profile
    render :json => {
      :account_id => session[:account_id],
    }
  end
end
