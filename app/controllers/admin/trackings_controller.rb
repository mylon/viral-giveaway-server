class Admin::TrackingsController < Admin::BaseController
  def summary
    @today = Date.today
    @firstday = @today - (@today.day - 1)
    types = Rails.application.config.admin_download_summary_types
    
    summary = { 
      :all => self.summary_type( types.map{|t| Rails.application.config.tracking_type_map[t]} ), 
    }
    types.each do |t|
      summary[t] = self.summary_type(Rails.application.config.tracking_type_map[t])
    end

    render :json => summary
  end
  
  def summary_type(type)
    conversioned = Rails.application.config.tracking_status_map[:conversioned]
    {
      :today      => Tracking.where({:type => type, :status => conversioned, :deleted_at => nil}).where("created_at >= ? and created_at < ?", @today, @today + 1).count,
      :yesterday  => Tracking.where({:type => type, :status => conversioned, :deleted_at => nil}).where("created_at >= ? and created_at < ?", @today - 1, @today).count,
      :this_week  => Tracking.where({:type => type, :status => conversioned, :deleted_at => nil}).where("created_at >= ? and created_at < ?", @today - @today.wday, @today + (7 - @today.wday)).count,
      :last_week  => Tracking.where({:type => type, :status => conversioned, :deleted_at => nil}).where("created_at >= ? and created_at < ?", @today - @today.wday - 7, @today - @today.wday).count,
      :this_month => Tracking.where({:type => type, :status => conversioned, :deleted_at => nil}).where("created_at >= ? and created_at < ?", @firstday, @firstday >> 1).count,
      :last_month => Tracking.where({:type => type, :status => conversioned, :deleted_at => nil}).where("created_at >= ? and created_at < ?", @firstday << 1, @firstday).count,
    }
  end
end
