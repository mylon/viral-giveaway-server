class Admin::DevicesController < Admin::BaseController
  def list
    where = {}
    where['users.account_id'] = params[:account_id].to_s if params[:account_id].to_s.length > 0
    where[:android_id] = params[:android_id].to_s if params[:android_id].to_s.length > 0 
    where[:imei] = params[:imei].to_s if params[:imei].to_s.length > 0
    where[:imsi] = params[:imsi].to_s if params[:imsi].to_s.length > 0

    custom_where = ''
    custom_params = []
    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    begin
      data = Device.includes(:user).where(where).where([custom_where, *custom_params]).order('devices.id desc').limit(Rails.application.config.admin_data_num).map{|d| _device(d)}
    rescue => e
      log_error(e)
    end

    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end

  def show
    begin
      device = Device.find(params[:id].to_i)
      render :json => _device(device)
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      device = nil
      Device.transaction do
        device              = params[:device_obj]
        device.user         = params[:user]
        device.imsi         = params[:imsi]
        device.imei         = params[:imei]
        device.android_id   = params[:android_id]
        device.device       = params[:device]
        device.model        = params[:model]
        device.product      = params[:product]
        device.manufacturer = params[:manufacturer]
        device.display      = params[:display]
        device.brand        = params[:brand]
        device.os_version   = params[:os_version]
        device.hash_key     = device.generate_hash_key()
        device.save!
      end
      render :json => self._device(device) and return
    rescue => e
      log_error(e)
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def _device(src)
    dest = src.attributes
    dest[:user] = src.user
    dest[:account_id] = src.user.account_id
    dest
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:device_obj] = Device.find(params[:id].to_i)
        rescue
          errors.push('device not found')
        end
      else
        errors.push('id is required')
      end
    end

    if params.key?(:account_id) then
      user = User.where({:account_id => params[:account_id].to_s}).first
      if user then
        params[:user] = user
      else
        errors.push('user not found')
      end
    end

    errors
  end

end
