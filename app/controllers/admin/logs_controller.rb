class Admin::LogsController < Admin::BaseController
  def list
    if errors = self.validate(params) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    from_time = Time.strptime(params[:from_at], "%Y-%m-%d").to_i
    to_time = Time.strptime(params[:to_at], "%Y-%m-%d").to_i

    case params[:log]
    when "dau"
      data = LogSummaryDau.where("? <= time AND time < ?", from_time, to_time).order("time DESC").map do |r|
        date = Date.strptime(r.time.to_s, "%s")
        {
          date: date.strftime("%Y-%m-%d (#{%w(Sun Mon Tue Wed Thu Fri Sat)[date.wday]})"),
          dau: r.dau,
          user_num: r.user_num,
          dau_percent: BigDecimal((r.dau.to_f / r.user_num * 100).to_s).floor(4),
        }
      end
    when "mau"
      data = LogSummaryMau.where("? <= time AND time < ?", from_time, to_time).order("time DESC").map do |r|
        date = Date.strptime(r.time.to_s, "%s")
        {
          date: date.strftime("%Y-%m"),
          mau: r.mau,
          user_num: r.user_num,
          mau_percent: BigDecimal((r.mau.to_f / r.user_num * 100).to_s).floor(4),
        }
      end
    when "app_log_per_sources"
      data = LogSummaryAppLogPerSource.where("? <= time AND time < ?", from_time, to_time).order("time DESC").map do |r|
        date = Date.strptime(r.time.to_s, "%s")
        {
          date: date.strftime("%Y-%m-%d (#{%w(Sun Mon Tue Wed Thu Fri Sat)[date.wday]})"),
          source: r.source,
          access: 24.times.map{|h| r.send("h#{h}_access".to_sym)}.reduce(:+),
          register: 24.times.map{|h| r.send("h#{h}_register".to_sym)}.reduce(:+),
          facebook_like: 24.times.map{|h| r.send("h#{h}_facebook_like".to_sym)}.reduce(:+),
          facebook_share: 24.times.map{|h| r.send("h#{h}_facebook_share".to_sym)}.reduce(:+),
          twitter_share: 24.times.map{|h| r.send("h#{h}_twitter_share".to_sym)}.reduce(:+),
          twitter_follow: 24.times.map{|h| r.send("h#{h}_twitter_follow".to_sym)}.reduce(:+),
          cpi: 24.times.map{|h| r.send("h#{h}_cpi".to_sym)}.reduce(:+),
        }
      end
    end

    render :json => {
      :data => data 
    }
  end

  def validate(params)
    errors = []

    errors.push "require Log Type" unless params[:log]

    begin
      Time.strptime(params[:from_at], "%Y-%m-%d")
    rescue
      errors.push "Date From is require format yyyy-mm-dd"
    end

    begin
      Time.strptime(params[:to_at], "%Y-%m-%d")
    rescue
      errors.push "Date To is require format yyyy-mm-dd"
    end

    errors
  end
end
