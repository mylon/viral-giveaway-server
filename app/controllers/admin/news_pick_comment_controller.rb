class Admin::NewsPickCommentController < Admin::BaseController
	def list
		custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

		data = Comment.where([custom_where, *custom_params]).order('id desc').limit(100)		
		render :json => {
	      :data => data,
	      :limit => 100
	    }
	end

	def show
	    begin
	      comments = Comment.find(params[:id].to_i)
	      render :json => comments
	    rescue
	      render :nothing => true, :status => :not_found
	    end
	end

	def update
	    begin
	      	comments = nil
		    Comment.transaction do
		        comments = Comment.find_by(id: params[:id])   
		        comments.comment_text = params[:comment_text]
		        comments.updated_at = Time.now
		        comments.save!
		    end
	      render :json => comments and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end
	end

	def delete
	    begin
	      comments = Comment.where({id: params[:id].to_i}).first   
	      if comments then
	      	 comments.destroy!
	      	 render :nothing => true and return
	      else
	      	render :json => ['Cannot find comment ID'], :status => :bad_request  and return
	      end
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end   
	end
end
