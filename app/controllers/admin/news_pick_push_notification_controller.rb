class Admin::NewsPickPushNotificationController < Admin::BaseController
	def create

		if !params.key?(:ios) and !params.key?(:android) then
			render :json => ["error please select app type"], :status => :bad_request and return
		end

		message = nil
		title = nil
		image_link = nil
		news_link = nil
		category_id = nil

		if params.key?(:message) and params[:message] != "" then
			message = params[:message]
		end
		if params.key?(:title) and params[:title] != "" then
			title = params[:title]
		end
		if params.key?(:image_link) and params[:image_link] != "" then
			image_link = params[:image_link]
		end
		if params.key?(:news_link) and params[:news_link] != "" then
			news_link = params[:news_link]
		end
		if params.key?(:category_id) and params[:category_id] != "" then
			category_id = params[:category_id]
		end

		#ios push
		if params[:ios] then

			case params[:app].to_i
			when 1
				ios_pem = 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/push_pem/bkk_jp.pem'
				city_id = 1
				region = 'jp'
			when 2
				ios_pem = 'cert2'
			when 3
				ios_pem = 'cert3'
			when 4
				ios_pem = 'cert4'
			when 5
				ios_pem = 'cert5'
			when 6
				ios_pem = 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/push_pem/bkk_jp.pem'
				city_id = 6
				region = 'jp'
			else
				render :json => ["error please select app"], :status => :bad_request and return
			end

			apple_token = NewsPickDeviceToken.where({device_notification: 'Y', device_type: "ios", city_id: city_id, region: region}).where.not(device_token: nil).where.not(device_token: 0)
			apple_token_array = []
			if apple_token then
				apple_token.each do |apple|
					apple_token_array.push(apple)
				end
			end

			apns_prod_cert  = open(ios_pem) {|f| f.read }

			if !apple_token_array.blank? then
				apple_token_array.each do |device|
					pusher = Grocer.pusher(
					  certificate: StringIO.new(apns_prod_cert),      # required
					  passphrase:  "1234",                       # optional
					  gateway:     "gateway.push.apple.com", # optional; See note below.
					  port:        2195,                     # optional
					  retries:     3                         # optional
					)
					notification = Grocer::Notification.new(
						  device_token:      device.device_token,
						  alert:             message,
						  badge:             1,
						  category:          "",         # optional; used for custom notification actions
						  sound:             "default",         # optional
						  expiry:            Time.now + 60*60,     # optional; 0 is default, meaning the message is not stored
						  identifier:        1234,                 # optional; must be an integer
						  content_available: true,                   # optional; any truthy value will set 'content-available' to 1
						  custom: {
						    title:  title,
						    body: message,
						    link: image_link,
						    news_link: news_link,
				        	categoryId: category_id	
						  }                
					)
					result = pusher.push(notification)
					if result then
						push_message = PushNotificationMessageLog.register(device.id, message, title, news_link, image_link, category_id)
					end
				end
			end
		end
		#end ios push

		#android push
		if params[:android] then

			android_gcm_key = ''			

			case params[:app].to_i
			when 1
				android_gcm_key = 'AIzaSyDYti_PPtVW78ZAqyEmLhJfdLcpyO2SA9Q'	
				city_id = 1
				region = 'jp'	
				title = "BangkokPicks"		
			when 2
				android_gcm_key = 'certg2'		
			when 3
				android_gcm_key = 'certg3'	
			when 6
				city_id = 6
				region = 'jp'
				android_gcm_key = 'AIzaSyCzsFv0L3IC69dx1R56m7UVcLddb_h9kZc'		
			when 19
				city_id = 19
				region = 'jp'
				android_gcm_key = 'AIzaSyCaGmAkG-kv3o-3iMTaQ0FxJ67Oqcn4B1g'		
			else
				render :json => ["error please select app"], :status => :bad_request and return
			end

			android_token =  NewsPickDeviceToken.where({device_notification: 'Y', device_type: "android", city_id: city_id, region: region}).where.not(device_token: nil)
			android_token_array = []
			android_device_id = []
			if android_token then
				android_token.each do |android|
					android_token_array.push(android.device_token)
					android_device_id.push(android.id)
				end
			end

			if !android_token_array.blank? then
				gcm = GCM.new(android_gcm_key)
				registration_id = android_token_array
				options = {
		          'data' => {
		            'message' => message,
		            'title' => title
		          },
		            'collapse_key' => 'updated_state'
		        }
	        	response = gcm.send_notification(registration_id, options)
	        	if response then
	        		android_device_id.each do |device|
						push_message = PushNotificationMessageLog.register(device, message, title, news_link, image_link, category_id)
					end
				end
			end
		end
		#end android push

		render :nothing => true and return
	end

end
