class Admin::FortuneTellingUsersController < Admin::BaseController
  def list
    where = {}
    if params.key?(:account_id) and params[:account_id].length > 0 then
      where['users.account_id'] = params[:account_id]
    end

    custom_where = ''
    custom_params = []
    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = FortuneTellingUser.joins(:user).where(where).where([custom_where, *custom_params]).order('id desc').limit(Rails.application.config.admin_data_num).map do |src|
      dest = src.attributes
      dest[:user] = src.user
      dest
    end
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end
end
