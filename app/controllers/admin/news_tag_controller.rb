class Admin::NewsTagController < Admin::BaseController
	def list
		where = {}
		custom_city = ''
		custom_city_params = ''
	    where[:region] = params[:region].to_s if params[:region].to_s != ""
	    if params[:city_id] != "all" and params[:city_id] !=  "" and params[:city_id].to_i > 0 then
			where[:city_id] = params[:city_id].to_i 
		elsif params[:city_id] != "all" and params[:city_id].to_i == 0
			custom_city = 'city_id IS NULL OR city_id = 0'
		end

		$custom_category_params = ''
		if params[:category_id] != "all" and params[:category_id] !=  "" and params[:category_id].to_i > 0 then
			where[:category_id] = params[:category_id].to_i 
		elsif params[:category_id] != "all" and params[:category_id].to_i == 0
			custom_category_params = 'category_id IS NULL OR category_id = 0'
		end

		custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

	   	data = NewsTag.where(where).where([custom_where, *custom_params]).where(custom_city).where(custom_category_params).order('id desc').limit(100)	
		render :json => {
	      :data => data,
	      :limit => 100
	    }
	end

	def show
	   begin
	      newstag = NewsTag.find(params[:id].to_i)
	      render :json => newstag
	    rescue
	      render :nothing => true, :status => :not_found
	    end
	end

	def create
	    begin
	      if errors = self.validate(params) and errors.length > 0 then
	      	render :json => errors, :status => :bad_request and return
	      end
	      newstag = nil
	      if params[:category_id] == "" then
	      	params[:category_id] = 0
	      end
	      if params[:city_id] == "" then
	      	params[:city_id] = 0
	      end

	      if params[:tags_group] != "" then 
	      	 params[:tags_group] = params[:tags_group].gsub(/\s+/, "")
	      end

	      NewsTag.transaction do
	        newstag = NewsTag.register(params[:tag], params[:region], params[:city_id], params[:category_id], params[:tags_group])	       
	      end
	      render :json => newstag and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request and return
	    end
	end

	def validate(params)
		errors = []

		if params.key?(:tag) then
	      tag = params[:tag]
	      if tag.length == 0 then
	        errors.push('tag is required')
	      end
	    else
	      errors.push('tag is required')
	    end

		where = {
			:tag => params[:tag]
		}
		where[:region] = params[:region].to_s if params[:region].to_s != ""
		where[:city_id] = params[:city_id].to_i if params[:city_id] != "all" and params[:city_id] != ""
		where[:category_id] = params[:category_id].to_i if params[:category_id] != "all" and params[:category_id] != ""

		content = NewsTag.where(where).first	     
	    if content then
	      errors.push('This tag already in used please change')
	    end

	    errors
	end

	def update
	    begin
	      newstag = nil
	      if params[:category_id] == "" then
	      	params[:category_id] = 0
	      end
	      if params[:city_id] == "" then
	      	params[:city_id] = 0
	      end
	      if params[:tags_group] != "" then 
	      	 params[:tags_group] = params[:tags_group].gsub(/\s+/, "")
	      end
	      NewsTag.transaction do
	        newstag = NewsTag.find_by(id: params[:id])   
	        newstag.tag = params[:tag]
	        newstag.region = params[:region]
	        newstag.city_id = params[:city_id]
	        newstag.category_id = params[:category_id]
	        newstag.tags_group = params[:tags_group]
	        newstag.updated_at = Time.now
	        newstag.save!
	      end
	      render :json => newstag and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end
	end

	def upload
	    if errors = self.validate_upload(params) and errors.length > 0 then
	      render :json => errors, :status => :bad_request and return
	    end

	    begin
	      NewsTag.transaction do
	        NewsTag.bulk_load(params[:result_data].data.force_encoding("utf-8"))
	      end
	      render :json => {} and return
	    rescue => e
	      log_error e
	      render :json => [e.message], :status => :bad_request  and return
	    end
	end

	def validate_upload(params)
	    errors = []

	    params[:result_data] = URI::Data.new(params[:result])
	    params[:result_data].data.split(/\r?\n/).each_with_index do |line,i|
	      parts = line.split(",")
	      unless parts.length == Rails.application.config.tag_csv_column_num then
	        errors.push("line:#{i+1} column num is incorrect")
	      end
	    end

	    errors
	end

	def delete
	    begin
	      newstag = NewsTag.where({id: params[:id].to_i}).first   
	      if newstag then
	      	 newstag.destroy!
	      	 render :nothing => true and return
	      else
	      	render :json => ['Cannot find source ID'], :status => :bad_request  and return
	      end
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end   
	end
end


