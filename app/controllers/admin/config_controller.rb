class Admin::ConfigController < Admin::BaseController

  before_action :authenticate_api, except: [:index]
  before_action :public_api, only: [:index]

  def index
    data = {
      :system_information => SystemInformation.first,
      :offer_platform_map => Rails.application.config.offer_platform_map,
      :offer_type_map     => Rails.application.config.offer_type_map,
      :offer_provider_map => Rails.application.config.offer_provider_map,
      :offer_publish_status_map => Rails.application.config.offer_publish_status_map,
      :gender_map => Rails.application.config.gender_map,
      :message_type_map => Rails.application.config.message_type_map,
      :admin_download_summary_types => Rails.application.config.admin_download_summary_types,
      :admin_download_summary_types_with_all => [:all] + Rails.application.config.admin_download_summary_types,
      :is_env_development => Rails.env.development?,
      :is_env_test => Rails.env.test?,
      :is_env_production => Rails.env.production?,
      :admin_offer_bulk_actions => Rails.application.config.admin_offer_bulk_actions,
      :mail_segment_map => Rails.application.config.mail_segment_map,
      :mail_segment_label_map => Rails.application.config.mail_segment_label_map,
      :log_type_label_map => Rails.application.config.log_type_label_map,
      :app_map => Hash[App.all().map{|r| [r.id, r.name] }],
      :fortune_telling_constellations => Rails.application.config.fortune_telling_constellations,
      :line_sticker_status_map => Rails.application.config.line_sticker_status_map,
      :billing_status_map => Rails.application.config.billing_status_map,
      :newspick_region => Rails.application.config.newspick_region,
      :newspick_region_map => Rails.application.config.newspick_region_map,
      :newspick_region_txt => Rails.application.config.newspick_region_txt,
      :newspick_region_txt_map => Rails.application.config.newspick_region_txt_map,
      :newspick_city => Rails.application.config.newspick_city,      
      :newspick_category => Rails.application.config.newspick_category,
      :newspick_jp_category => Rails.application.config.newspick_jp_category,
      :newspick_jp_category_select => Rails.application.config.newspick_jp_category_select,
      :newspick_jp_category_select_map => Rails.application.config.newspick_jp_category_select_map,
      :newspick_jpcity => Rails.application.config.newspick_jpcity,      
      :newspick_hotnews => Rails.application.config.newspick_hotnews,
      :newspick_events => Rails.application.config.newspick_events,
      :newspick_promotions => Rails.application.config.newspick_promotions,   
      :newspick_app => Rails.application.config.newspick_app,  
      :sub_classified_category => Rails.application.config.sub_classified_category,   
      :sub_classified_category_jp => Rails.application.config.sub_classified_category_jp,
      :localpick_home_city_select => Rails.application.config.localpick_home_city_select, 
    }
    render :json => data 
  end
end
