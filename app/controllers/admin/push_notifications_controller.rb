class Admin::PushNotificationsController < Admin::BaseController
  def create
    if errors = self.validate(params) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      segment = params[:segment].to_s
      from = VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name)
      title = params[:title].to_s
      message = params[:message].to_s
      force = params[:force].to_s == "1"
      BulkSendmailJob.perform_later( segment, from, title, message, force )

      Fluent::Logger.post("vg.push_notification", {
        :operator => @operator.id,
        :segment  => Rails.application.config.mail_segment_map[segment.to_sym],
        :from     => from,
        :title    => title,
        :message  => message,
        :force    => force,
      })

      render :json => {} and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end
  end

  def validate(params)
    errors = []

    if params[:title].to_s.length == 0 then
      errors.push('title is required')
    end

    if params.key?(:segment) then
      unless Rails.application.config.mail_segment_map.keys.include?(params[:segment].to_s.to_sym) then
        errors.push('segment is invalid')
      end
    else
      errors.push('segment is required')
    end

    if params[:message].to_s.length == 0 then
      errors.push('message is required')
    end

  end
end
