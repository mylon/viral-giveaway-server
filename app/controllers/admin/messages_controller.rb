class Admin::MessagesController < Admin::BaseController
  def list
    where = {}
    where[:type] = params[:type].to_i if params[:type].to_i > 0
    where['users.account_id'] = params[:account_id].to_s if params[:account_id].to_s.length > 0

    custom_where = ''
    custom_params = []
    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if (custom_where.length > 0 or where.length > 0) and where['users.account_id'].nil?
      custom_where += 'messages.id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = Message.joins(:user).where(where).where([custom_where, *custom_params]).order('messages.id desc').limit(Rails.application.config.admin_data_num).map do |r|
      h = r.attributes
      h[:user] = r.user
      h
    end
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end

  def show
    begin
      message = Message.joins(:user).find(params[:id].to_i)
      render :json => self._message(message) 
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def create
    if errors = self.validate(params, :create) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      message = nil
      Message.transaction do
        message = self.register(params, :create)
      end
      render :json => self._message(message) and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      message = nil
      Message.transaction do
        message = self.register(params, :update)
      end
      render :json => self._message(message) and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end

  end

  def _message(message)
    h = message.attributes
    h[:user] = message.user
    h[:user_account_id] = message.user.account_id
    h
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:message] = Message.find(params[:id].to_i)
        rescue
          errors.push('message not found')
        end
      else
        errors.push('id is required')
      end
    end

    if params.key?(:type) then
      if Rails.application.config.message_type_map.select{|k,v| v == params[:type].to_i} == 0 then
        errors.push('type is invalid')
      end
    else
      errors.push('type is required')
    end

    if params.key?(:user_account_id) then
      params[:user] = User.where({:account_id => params[:user_account_id].to_s}).first
      errors.push('user not found') if params[:user].nil?
    end

    errors
  end

  def register(params, mode)
    message = mode == :create ? Message.new() : params[:message]
    message.type = params[:type].to_i
    message.user = params[:user]
    message.title = params[:title].to_s
    message.content = params[:content].to_s
    message.point_num = params[:point_num].to_i
    message.save!
    message
  end
end
