class Admin::SystemInformationController < Admin::BaseController
  def index
    render :json => SystemInformation.first
  end

  def update
    begin
      SystemInformation.transaction do
        s = SystemInformation.first
        s.maintenance = params[:maintenance] if params.key?(:maintenance)
        s.save!
      end
      render :json => SystemInformation.first
    rescue => e
      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
      render :nothing => true, :status => :internal_server_error
    end
  end
end
