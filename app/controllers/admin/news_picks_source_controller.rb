class Admin::NewsPicksSourceController < Admin::BaseController
def list
	    
		where = {
	      :deleted_at => nil	     
	    }
	    where[:region] = params[:region].to_s if params[:region].to_s != ""
		where[:city_id] = params[:city_id].to_i if params[:city_id].to_i > 0

		custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

	   	data = NewsPicksSource.where(where).where([custom_where, *custom_params]).order('id desc').limit(100)	
		render :json => {
	      :data => data,
	      :limit => 100
	    }
	end

	def show
	   begin
	      newspicksource = NewsPicksSource.find(params[:id].to_i)
	      render :json => newspicksource
	    rescue
	      render :nothing => true, :status => :not_found
	    end
	end

	def create
	    begin
	      newspicksource = nil
	      NewsPicksSource.transaction do
	        newspicksource = NewsPicksSource.register(params[:source_url], params[:region], params[:city_id])	       
	      end
	      render :json => newspicksource and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request and return
	    end
	end

	def update
	    begin
	      newspicksource = nil
	      NewsPicksSource.transaction do
	        newspicksource = NewsPicksSource.find_by(id: params[:id])   
	        newspicksource.source_url = params[:source_url]
	        newspicksource.region = params[:region]
	        newspicksource.city_id = params[:city_id]
	        newspicksource.updated_at = Time.now
	        newspicksource.save!
	      end
	      render :json => newspicksource and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end
	end

	def delete
	    begin
	      newspicksource = NewsPicksSource.where({id: params[:id].to_i}).first   
	      if newspicksource then
	      	 newspicksource.destroy!
	      	 render :nothing => true and return
	      else
	      	render :json => ['Cannot find source ID'], :status => :bad_request  and return
	      end
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end   
	end
end

