class Admin::FortuneTellingResultsController < Admin::BaseController
  def list
    where = {}
    if params.key?(:date) and params[:date].length > 0 then
      where[:target_date] = Date.strptime(params[:target_date], '%Y/%m/%d')
    end

    custom_where = ''
    custom_params = []

    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = FortuneTellingResult.where(where).where([custom_where, *custom_params]).group(:target_date).order("target_date desc").limit(Rails.application.config.admin_data_num).map do |r|
      {target_date:r.target_date, ready:FortuneTellingResult.ready?(r.target_date), sent_at:r.sent_at, created_at:r.created_at}
    end
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end

  def show
    begin
      results = FortuneTellingResult.where({target_date:params[:target_date]}).order("constellation asc")
      render json: {
        target_date: params[:target_date],
        fortune_telling_results: results,
      } and return
    rescue => e
      log_error e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def update
    begin
      FortuneTellingResult.transaction do
        params[:results].each do |result|
          r = FortuneTellingResult.where({target_date: result[:target_date], constellation: result[:constellation]}).first
          next unless r
          r.love_text_en = result[:love_text_en]
          r.money_text_en = result[:money_text_en]
          r.work_text_en = result[:work_text_en]
          r.total_text_en = result[:total_text_en]
          r.save!
        end
      end
      render :json => {} and return
    rescue => e
      log_error e
      render :json => [e.message], :status => :bad_request  and return
    end
    render json: {}
  end

  def delete
    begin
      FortuneTellingResult.transaction do
        FortuneTellingResult.where({target_date: params[:target_date]}).delete_all
      end
      render :json => {} and return
    rescue => e
      log_error e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def upload
    if errors = self.validate_upload(params) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      FortuneTellingResult.transaction do
        FortuneTellingResult.bulk_load(params[:result_data].data.force_encoding("utf-8"))
      end
      render :json => {} and return
    rescue => e
      log_error e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def validate_upload(params)
    errors = []

    params[:result_data] = URI::Data.new(params[:result])
    params[:result_data].data.split(/\r?\n/).each_with_index do |line,i|
      parts = line.split(",")
      unless parts.length == Rails.application.config.fortune_telling_csv_column_num then
        errors.push("line:#{i+1} column num is incorrect")
      end
    end

    errors
  end
end
