class Admin::DreamPhotosController < Admin::BaseController
  def list
    where = {
      billing_status: %w(none complete).map{|s| Rails.application.config.billing_status_map[s.to_sym]},
    }
    custom_where = ''
    custom_params = []

    if params.key?(:completed) then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += (params[:completed].to_i == 0 ? 'dream_photos.completed_at IS NULL' : 'dream_photos.completed_at IS NOT NULL')
    end

    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'dream_photos.id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = DreamPhoto.joins(:user).where(where).where(custom_where, *custom_params).order("created_at desc").map{|r| DreamPhoto.convert(r)} 
    render json: {
      data: data
    }
  end

  def show
    begin
      dream_photo = DreamPhoto.convert(DreamPhoto.find(params[:id].to_i))
      render :json => dream_photo 
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      DreamPhoto.transaction do
        dream_photo = self.register(params, :update)
        if params[:result_photo_data] then
 
          dream_photo.user.send_raw_email!(
            VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
            I18n.t('mail.dream_photo_result_subject', :locale => dream_photo.user.locale.to_sym),
            render_to_string( 
                          :template => 'mail/dream_photo_result',
                          :layout => 'mail'),
            { 
              attachments: {
                "#{dream_photo.result_photo_url}" => dream_photo.download_photo("result").body.read,
              }
            }
          )
        end
      end
    rescue => e
      log_error e
      render :json => [e.message], :status => :bad_request  and return
    end

    data = DreamPhoto.convert(DreamPhoto.find(params[:id].to_i))
    render json: {
      data: data
    }
  end

  def register(params, mode)
    dream_photo = nil
    case mode
    when :update
      dream_photo = params[:dream_photo] 
    end

    dream_photo.upload_result_photo(params[:result_photo_data])
    dream_photo.result_photo_url = dream_photo.generate_filename(params[:result_photo_data], 'result')
    dream_photo.completed_at = Time.now()
    dream_photo.save!
    dream_photo
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:dream_photo] = DreamPhoto.find(params[:id].to_i)
        rescue
          errors.push('dream_photo not found')
        end
      else
        errors.push('id is required')
      end
    end
    
    params[:result_photo_data] = URI::Data.new(params[:result_photo])
    if params[:result_photo_data].nil? or not ( /^image\/.*$/.match(params[:result_photo_data].content_type) ) then
      errors.push('result photo is required')
    end

    errors
  end
end
