class Admin::OperatorsController < Admin::BaseController

  before_action :authenticate_api, except: [:login, :logout, :logined]
  before_action :public_api, only: [:login, :logout, :logined]

  def index
    where = {
      :deleted_at => nil,
    }

    custom_where = ''
    custom_params = []

    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = Operator.where(where).where([custom_where, *custom_params]).order('id desc').limit(Rails.application.config.admin_data_num)
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end
  
  def create
    if errors = self.validate(params, :create) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      operator = nil
      Operator.transaction do
        operator = Operator.register(params[:account_id], params[:name], params[:email], params[:password])
      end
      render :json => operator and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      operator = params[:operator]
      Operator.transaction do
        operator.account_id = params[:account_id]
        operator.name = params[:name]
        operator.email = params[:email]
        operator.password = Operator.encrypt_password(params[:password]) if params.key?(:password)
        operator.save!
      end
      render :json => operator and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end
  end

  def show
    begin
      operator = Operator.find(params[:id].to_i)
      render :json => operator 
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def profile
    render :json => {
      :account_id => @operator.account_id,
      :name       => @operator.name,
      :email      => @operator.email,
    
    }
  end

  def login
    account_id_or_email = params[:account_id_or_email] or ''
    password = params[:password] or ''

    operator = Operator.authenticate(account_id_or_email, password)
    if operator then
      session[:account_id] = operator.account_id
      render :json => {
        :account_id => operator.account_id,
        :name => operator.name,
      } 
    else
      render :nothing => true, :status => :bad_request
    end
  end

  def logout
    session.delete(:account_id)
    render :nothing => true
  end

  def logined
    render :json => {
      :logined => !session[:account_id].nil?,
    }
  end

  def delete
    begin
      operator = Operator.find(params[:id].to_i)
      operator.deleted_at = Time.now
      operator.save!
      render :nothing => true and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:operator] = Operator.find(params[:id].to_i)
        rescue
          errors.push('operator not found')
        end
      else
        errors.push('id is required')
      end
    end

    if params[:account_id].to_s.strip.length == 0 then
      errors.push('account_id is required')
    else
      if Operator.where({:account_id => params[:account_id].to_s.strip}).count > 0 then
        errors.push("#{params[:account_id].to_s.strip} is already exists")
      end
    end

    if params[:name].to_s.strip.length == 0 then
      errors.push('name is required')
    end

    if params[:email].to_s.strip.length == 0 then
      errors.push('email is required')
    else
      if Operator.where({:email => params[:email].to_s.strip}).count > 0 then
        errors.push("#{params[:email].to_s.strip} is already exists")
      end
    end

    if :update == mode then
      if params.key?(:password) and params[:password].to_s.strip.length == 0 then
        errors.push('password is required')
      end
    elsif params[:password].to_s.strip.length == 0 then
      errors.push('password is required')
    end
  end

end
