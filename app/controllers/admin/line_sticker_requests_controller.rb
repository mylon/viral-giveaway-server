class Admin::LineStickerRequestsController < Admin::BaseController
  def list
    where = {}
    if params.key?(:account_id) and params[:account_id].length > 0 then
      where['users.account_id'] = params[:account_id]
    end

    custom_where = ''
    custom_params = []
    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = LineStickerRequest.joins(:user).where(where).where([custom_where, *custom_params]).order('id desc').limit(Rails.application.config.admin_data_num).map do |src|
      dest = src.attributes
      dest[:user] = src.user
      dest
    end
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end

  def show
    begin
      r = LineStickerRequest.find(params[:id].to_i)
      h = r.attributes
      h['user'] = r.user
      render json: h and return
    rescue => e
      log_error e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def update
    if errors = self.validate_update(params) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      LineStickerRequest.transaction do
        request = params[:request]
        request.line_id = params[:line_id]
        request.sticker_name = params[:sticker_name]
        request.coin = params[:coin].to_i
        request.status = params[:status]
        if request.status == Rails.application.config.line_sticker_status_map[:sent] and request.sent_at.nil? then
          request.sent_at = Time.now
        elsif request.status != Rails.application.config.line_sticker_status_map[:sent]
          request.sent_at = nil 
        end
        request.save!
      end
    
      r = LineStickerRequest.find(params[:id].to_i)
      h = r.attributes
      h['user'] = r.user
      render json: h and return
    rescue => e
      log_error e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def validate_update(params)
    errors = []

    request = LineStickerRequest.where({id:params[:id].to_i}).first
    if request then
      params[:request] = request
    else
      errors.push "Not found"
    end

    unless Rails.application.config.line_sticker_status_symbol_map[params[:status].to_i] then
      errors.push "Invalid status"
    end

    if Rails.application.config.line_sticker_status_symbol_map[params[:status].to_i] == :sent then
      if params[:coin].to_i <= 0 then
        errors.push "Please input coin"
      elsif not request.user.can_withdraw?(params[:coin].to_i / 120 * 50) then
        errors.push "Insufficient point"
      end
    end

    errors
  end
end
