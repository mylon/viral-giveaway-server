class Admin::CampaignsController < Admin::BaseController
  def list
    where = {
      :deleted_at => nil,
    }

    custom_where = ''
    custom_params = []
    if params.key?(:keyword) and params[:keyword].length > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'name LIKE ?'
      custom_params.push('%' + params[:keyword] + '%')
    end

    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = Campaign.where(where).where([custom_where, *custom_params]).order('id desc').limit(Rails.application.config.admin_data_num)
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end

  def show
    begin
      campaign = Campaign.find(params[:id].to_i)
      render :json => campaign 
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def create
    if errors = self.validate(params, :create) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      campaign = nil
      Campaign.transaction do
        campaign = self.register(params, :create)
      end
      render :json => campaign and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      campaign = nil
      Campaign.transaction do
        campaign = self.register(params, :update)
      end
      render :json => campaign and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def delete
    begin
      campaign = Campaign.find(params[:id].to_i)
      campaign.deleted_at = Time.now
      campaign.save!
      render :nothing => true and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def register(params, mode)
    now = Time.now

    campaign = nil
    if mode == :create then
      campaign = Campaign.new()
      campaign.created_at = now
    else
      campaign = params[:campaign] 
    end

    campaign.name = params[:name]
    campaign.cover_image_url = params[:cover_image_url]
    campaign.point_cost = params[:point_cost]
    campaign.start_at = params[:start_at_date]
    campaign.end_at = params[:end_at_date]
    campaign.order_num = params[:order_num].to_s.length > 0 ? params[:order_num].to_i : nil
    campaign.regulary = !!params[:regulary]
    campaign.period = params[:period].to_s
    campaign.clone_campaign_id = params[:clone_campaign_id]

    campaign.save!
    campaign
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:campaign] = Campaign.find(params[:id].to_i)
        rescue
          errors.push('campaign not found')
        end
      else
        errors.push('id is required')
      end
    end

    if params[:name].nil? or params[:name].length == 0 then
      errors.push('name is required')
    end

    if params.key?(:start_at) then
      begin
        params[:start_at_date] = Time.strptime(params[:start_at], Rails.application.config.admin_request_date_format)
      rescue
        errors.push("start_at is invalid format. format is #{Rails.application.config.admin_request_date_format_readable}")
      end
    else
      errors.push('start_at is required')
    end

    if params.key?(:end_at) then
      begin
        params[:end_at_date] = Time.strptime(params[:end_at], Rails.application.config.admin_request_date_format)
      rescue
        errors.push("end_at is invalid format. format is #{Rails.application.config.admin_request_date_format_readable}")
      end
    else
      errors.push('end_at is required')
    end

    if params.key?('start_at_date') and params.key?('end_at_date') then
      if params[:start_at_date] >= params[:end_at_date] then
        errors.push('specify start_at < end_at')
      end
    end

  end

end
