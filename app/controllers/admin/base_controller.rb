class Admin::BaseController < ApplicationController

  before_action :authenticate_api

  def public_api
  end

  def authenticate_api
    account_id = session[:account_id]
    if account_id then
      @operator = Operator.where({
        :deleted_at => nil,
        :account_id => account_id,
      }).first
    end

    if @operator.nil? then
      render :nothing => true, :status => :unauthorized
      return false
    end
  end
end
