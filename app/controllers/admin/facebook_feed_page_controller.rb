class Admin::FacebookFeedPageController < Admin::BaseController
	def list
		where = {}
	    where[:facebook_page_region] = params[:facebook_page_region].to_s if params[:facebook_page_region].to_s  != ""
	    where[:facebook_page_type] = params[:facebook_page_type].to_s if params[:facebook_page_type].to_s  != ""
	    where[:city_id] = params[:city_id].to_i if params[:city_id].to_i  > 0

	    custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

	    data = FacebookFeedPage.where(where).where([custom_where, *custom_params]).order('id desc').limit(100)	
		feed_count = FacebookFeedPage.where(where).count

		render :json => {
	      :data => data,
	      :feed_count => feed_count,
	      :limit => 100
	    }
	end

	def show
	    begin
	      data = FacebookFeedPage.find(params[:id].to_i)
	      render :json => data
	    rescue
	      render :nothing => true, :status => :not_found
	    end
	end

	def create
	    begin
	      if errors = self.validate(params) and errors.length > 0 then
	      	render :json => errors, :status => :bad_request and return
	      end

	      region_list  = params[:facebook_page_region]

	      fbfeedpage = nil
	      region_list.each do |region|
		      FacebookFeedPage.transaction do
		        fbfeedpage = FacebookFeedPage.register(params[:facebook_page_id], params[:facebook_page_type], region , params[:city_id])	       
		      end
		  end
	      render :json => fbfeedpage and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request and return
	    end
	end

	def update
	    begin
	      fbfeedpage = nil

	      FacebookFeedPage.transaction do
	        fbfeedpage = FacebookFeedPage.find_by(id: params[:id])   
	        fbfeedpage.facebook_page_id = params[:facebook_page_id]
	        fbfeedpage.facebook_page_type = params[:facebook_page_type]
	        fbfeedpage.city_id = params[:city_id]
	        fbfeedpage.facebook_page_region = params[:facebook_page_region]
	        fbfeedpage.updated_at = Time.now
	        fbfeedpage.save!
	      end
	      render :json => fbfeedpage and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end
	end

	def delete
	    begin
	      fbfeedpage = FacebookFeedPage.where({id: params[:id].to_i}).first   
	      if fbfeedpage then
	      	 fbfeedpage.destroy!
	      	 render :nothing => true and return
	      else
	      	render :json => ['Cannot find source ID'], :status => :bad_request  and return
	      end
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end   
	end

	def validate(params)
		errors = []

		if params.key?(:facebook_page_id) then
	      facebook_page_id = params[:facebook_page_id]
	      if facebook_page_id.length == 0 then
	        errors.push('facebook page id is required')
	      end
	    else
	      errors.push('facebook page id is required')
	    end

	    if params.key?(:facebook_page_type) then
	      facebook_page_type = params[:facebook_page_type]
	      if facebook_page_type.length == 0 then
	        errors.push('facebook page type is required')
	      end
	    else
	      errors.push('facebook page type is required')
	    end

	    errors
	end

end
