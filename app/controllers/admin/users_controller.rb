class Admin::UsersController < Admin::BaseController
  def index
    where = {
      :deleted_at => nil,
    }
    where[:account_id] = params[:account_id].to_s unless params[:account_id].to_s == ""
    where[:canonical_email] = params[:email].to_s.downcase unless params[:email].to_s == ""
    where[:country] = params[:country].to_s unless params[:country].to_s == ""
    where[:provider] = params[:provider].to_s unless params[:provider].to_s == ""

    custom_where = ''
    custom_params = []
    if params.key?(:name) and params[:name].length > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'name LIKE ?'
      custom_params.push('%' + params[:name] + '%')
    end

    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    data = User.where(where).where([custom_where, *custom_params]).order('id desc').limit(Rails.application.config.admin_data_num)
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end

  def summary
    today = Date.today
    firstday = today - (today.day - 1)
    summary = {
      :register => {
        :today => User.where("created_at >= ? and created_at < ?", today, today + 1).count,
        :yesterday => User.where("created_at >= ? and created_at < ?", today - 1, today).count,
        :this_week => User.where("created_at >= ? and created_at < ?", today - today.wday, today + (7 - today.wday)).count,
        :last_week => User.where("created_at >= ? and created_at < ?", today - today.wday - 7, today - today.wday).count,
        :this_month=> User.where("created_at >= ? and created_at < ?", firstday, firstday >> 1).count,
        :last_month=> User.where("created_at >= ? and created_at < ?", firstday << 1, firstday).count,
      },
    }
    render :json => summary
  end
  
  def show
    begin
      user = User.find(params[:id].to_i)
      h = user.attributes
      render :json => h 
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      user = nil
      User.transaction do
        user = self.register(params, :update)
      end
      render :json => user and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def register(params, mode)
    now = Time.now

    user = nil
    if mode == :create then
      user = User.new()
      user.created_at = now
    else
      user = params[:user] 
    end

    user.country = params[:country]

    user.save!
    user
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:user] = User.find(params[:id].to_i)
        rescue
          errors.push('user not found')
        end
      else
        errors.push('id is required')
      end
    end

   if not( params.key?(:country) and params[:country].strip.length > 0 ) then
      errors.push('country is required')
   end 
  end

  def grant_point
    begin
    if params[:user_id].to_i > 0 and params[:point].to_i > 0 then
      user = User.find(params[:user_id].to_i)
      user.deposit_point(params[:point].to_i, :grant_operator, @operator.id)
      render :json => {}
    end
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end
end
