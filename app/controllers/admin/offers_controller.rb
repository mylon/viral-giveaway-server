class Admin::OffersController < Admin::BaseController

  def index
    where = {
      :deleted_at => nil,
    }
    where[:type] = params[:type].to_i if params[:type].to_i > 0
    where[:provider] = params[:provider].to_i if params[:provider].to_i > 0
    where[:provider_offer_id] = params[:provider_offer_id].to_s if params[:provider_offer_id].to_s.length > 0
    where[:promotional] = true if params[:promotional].to_s == '1'
    where[:promotional] = false if params[:promotional].to_s == '0'
    where[:platform] = params[:platform].to_i if params[:platform].to_i > 0
    where[:country] = params[:country].to_s if params[:country].to_s.length > 0
    where[:publish_status] = params[:publish_status].to_i if params[:publish_status].to_i > 0

    custom_where = ''
    custom_params = []
    if params.key?(:keyword) and params[:keyword].length > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'name LIKE ?'
      custom_params.push('%' + params[:keyword] + '%')
    end

    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    if params.key?(:live) and params[:live].to_s == '1' then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'start_at < NOW() and NOW() < end_at'
    end

    data = Offer.where(where).where([custom_where, *custom_params]).order('id desc').limit(Rails.application.config.admin_data_num)
    render :json => {
      :data => data,
      :limit => Rails.application.config.admin_data_num,
    }
  end

  def show
    begin
      offer = Offer.find(params[:id].to_i)
      render :json => offer 
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def create
    if errors = self.validate(params, :create) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      offer = nil
      Offer.transaction do
        offer = self.register(params, :create)
      end
      render :json => offer and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      offer = nil
      Offer.transaction do
        offer = self.register(params, :update)
      end
      render :json => offer and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def delete
    begin
      offer = Offer.find(params[:id].to_i)
      offer.deleted_at = Time.now
      offer.save!
      render :nothing => true and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def register(params, mode)
    now = Time.now

    offer = nil
    if mode == :create then
      offer = Offer.new()
      offer.created_at = now
    else
      offer = params[:offer] 
    end

    Rails.application.config.offer_type_map.each do |k,v|
      offer.type = v if v == params[:type].to_i
    end
    offer.name = params[:name]
    offer.description = params[:description].to_s
    offer.start_at = params[:start_at_date]
    offer.end_at = params[:end_at_date]
    offer.country = params[:country]
    offer.platform = params[:platform]
    offer.order_num = params[:order_num].to_s.length > 0 ? params[:order_num].to_i : nil
    offer.publish_status = params[:publish_status].to_i
    offer.preview_url = params[:preview_url]

    case offer.type
    when Rails.application.config.offer_type_map[:information],
         Rails.application.config.offer_type_map[:video]
      offer.provider         = Rails.application.config.offer_provider_map[:viral_giveaway]
      offer.cover_image_url  = params[:cover_image_url]
      offer.youtube_video_id = params[:youtube_video_id]
      offer.video_url        = params[:video_url]
    when Rails.application.config.offer_type_map[:cpi]
      offer.promotional      = !!params[:promotional]
      offer.promotional_payment = params[:promotional_payment].to_i
      Rails.application.config.offer_provider_map.each do |k,v|
        offer.provider = v if v == params[:provider].to_i
      end
      offer.app_icon_url         = params[:app_icon_url]
      offer.click_url            = params[:click_url]
      offer.provider_offer_id = params[:provider_offer_id]
      offer.provider_payment     = params[:provider_payment].to_i
      offer.provider_point       = params[:provider_point].to_i
    when Rails.application.config.offer_type_map[:nativex],
         Rails.application.config.offer_type_map[:super_rewards],
         Rails.application.config.offer_type_map[:woobi],
         Rails.application.config.offer_type_map[:personaly],
         Rails.application.config.offer_type_map[:aarki],
         Rails.application.config.offer_type_map[:trialpay],
         Rails.application.config.offer_type_map[:adcolony],
         Rails.application.config.offer_type_map[:fyber],
         Rails.application.config.offer_type_map[:supersonicads]
      offer.provider              = Rails.application.config.offer_provider_map[:viral_giveaway]
      offer.cover_image_url       = params[:cover_image_url]
    end

    offer.save!
    offer
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:offer] = Offer.find(params[:id].to_i)
        rescue
          errors.push('offer not found')
        end
      else
        errors.push('id is required')
      end
    end

    if params.key?(:type) then
      if Rails.application.config.offer_type_map.select{|k,v| v == params[:type].to_i} == 0 then
        errors.push('type is invalid')
      end
    else
      errors.push('type is required')
    end

    if params[:name].nil? or params[:name].length == 0 then
      errors.push('name is required')
    end

    if params[:name].nil? or params[:name].length == 0 then
      errors.push('country is required')
    end

    if params.key?(:start_at) then
      begin
        params[:start_at_date] = Time.strptime(params[:start_at], Rails.application.config.admin_request_date_format)
      rescue
        errors.push("start_at is invalid format. format is #{Rails.application.config.admin_request_date_format_readable}")
      end
    else
      errors.push('start_at is required')
    end

    if params.key?(:end_at) then
      begin
        params[:end_at_date] = Time.strptime(params[:end_at], Rails.application.config.admin_request_date_format)
      rescue
        errors.push("end_at is invalid format. format is #{Rails.application.config.admin_request_date_format_readable}")
      end
    else
      errors.push('end_at is required')
    end

    if params.key?('start_at_date') and params.key?('end_at_date') then
      if params[:start_at_date] >= params[:end_at_date] then
        errors.push('specify start_at < end_at')
      end
    end

    if params[:publish_status].nil? then
      errors.push('publish_status is required')
    elsif Rails.application.config.offer_publish_status_map.select{|_,v| v == params[:publish_status].to_i}.length == 0 then
      errors.push('publish_status is invalid')
    end

    case params[:type].to_i
    when Rails.application.config.offer_type_map[:information],
         Rails.application.config.offer_type_map[:video]
      if not params.key?(:cover_image_url) or params[:cover_image_url].length == 0 then
        errors.push('cover_image_url is required')
      end

      if (params[:youtube_video_id].nil? or params[:youtube_video_id].length == 0) and 
         (params[:video_url].nil? or params[:video_url].length == 0) then
        errors.push('specify youtube_video_id or video_url')
      end
    when Rails.application.config.offer_type_map[:cpi]
      if Rails.application.config.offer_provider_map.select{|k,v| v == params[:provider].to_i} == 0 then
        errors.push('provider is invalid')
      end

      if params[:app_icon_url].nil? or params[:app_icon_url].length == 0 then
        errors.push('app_icon_url is required')
      end

      if params[:click_url].nil? or params[:click_url].length == 0 then
        errors.push('click_url is required')
      end

      if params[:provider_offer_id].nil? or params[:provider_offer_id].length == 0 then
        errors.push('provider_offer_id is required')
      end

      if params.key?(:provider_payment) then
        if params[:provider_payment].to_i <= 0 then
          errors.push('specify provider_payment is than 0')
        end
      else
        errors.push('provider_payment is required')
      end

      if params.key?(:provider_point) then
        if params[:provider_point].to_i <= 0 then
          errors.push('specify provider_point is than 0')
        end
      else
        errors.push('provider_point is required')
      end
    end

    errors
  end

  def bulk_publish
    begin
      Offer.transaction do
        update_publish_statuses(Rails.application.config.offer_publish_status_map[:publish], params[:ids])
      end
      render :json => {} and return
    rescue => e
      log_error e
      render :json => {}, :status => :internal_server_error and return
    end
  end

  def bulk_unpublish
    begin
      Offer.transaction do
        update_publish_statuses(Rails.application.config.offer_publish_status_map[:unpublish], params[:ids])
      end
      render :json => {} and return
    rescue => e
      log_error e
      render :json => {}, :status => :internal_server_error and return
    end
  end

  def bulk_draft
    begin
      Offer.transaction do
        update_publish_statuses(Rails.application.config.offer_publish_status_map[:draft], params[:ids])
      end
      render :json => {} and return
    rescue => e
      log_error e
      render :json => {}, :status => :internal_server_error and return
    end
  end

  def update_publish_statuses(publish_status, ids)
    Offer.where({:id => ids}).update_all({:publish_status => publish_status})
  end
end
