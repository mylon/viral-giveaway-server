class Admin::NewsPickController < Admin::BaseController


	def list
		region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil	     
	    }
	    custom_city = ''
		custom_city_params = ''
		if params[:city_id] != "all" and params[:city_id] !=  "" and params[:city_id].to_i > 0 then
			where[:city_id] = params[:city_id].to_i 
		elsif params[:city_id] != "all" and params[:city_id].to_i == 0
			custom_city = 'city_id IS NULL OR city_id = 0'
		end

		#where[:city_id] = params[:city_id].to_i if params[:city_id] == "all" && params[:city_id].to_i > 0
		where[:category_id] = params[:category_id].to_i if params[:category_id].to_i  > 0
		where[:classified_sub_category] = params[:classified_sub_category].to_i if params[:classified_sub_category].to_i  > 0
		where[:publish_status] = params[:publish_status].to_i if params[:publish_status].to_i > 0
		where[:news_event] = params[:news_event].to_i if params[:news_event].to_i > 0
		where[:news_promotion] = params[:news_promotion].to_i if params[:news_promotion].to_i > 0
		where[:hot_news] = params[:hot_news].to_i if params[:hot_news].to_i > 0
		where[:show_in_list] = params[:show_in_list].to_i if params[:show_in_list].to_s  != "" and params[:show_in_list].to_s  != "all"
		where[:created_by] = params[:created_by].to_s if params[:created_by].to_s != ""
		where[:is_magazine] = params[:is_magazine].to_i if params[:is_magazine].to_s  != "" and params[:is_magazine].to_s  != "all"

		web_url = ""
		custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

	    start_date_where = ''
	    end_date_where = ''

	    if params.key?(:start_date) and params[:start_date] != '' then
	    	start_selected_date = params[:start_date].to_date.beginning_of_day
	    	start_date_where = ['created_at >= ?', start_selected_date] 
	    end

	    if params.key?(:end_date) and params[:end_date] != '' then
	    	end_selected_date = params[:end_date].to_date.end_of_day
	    	end_date_where = ['created_at <= ?', end_selected_date]
	    end

	    web_url_where = ''
	    if params.key?(:web_url) and params[:web_url].to_s != "" then
	     	web_url = params[:web_url].to_s
	     	web_url_where = ["web_url LIKE ?", "%#{web_url}%"]
	    end

	    search_text_where = ''
	    if params.key?(:search_text) and params[:search_text].to_s != "" then
	    	search_text = params[:search_text].to_s.downcase
	    	search_text_where = ["lower(title) LIKE (?) OR lower(news_description) LIKE (?) OR lower(news_pick_shop) LIKE (?) ", "%#{search_text}%", "%#{search_text}%", "%#{search_text}%"]
	    end

	    news_url_where = ''
	    if params.key?(:news_url) and params[:news_url].to_s != "" then
	     	news_url = params[:news_url].to_s
	     	news_url_where = ["news_url LIKE ?", "%#{news_url}%"]
	    end

	    data = NewsPick.where(where).where([custom_where, *custom_params]).where(search_text_where).where(web_url_where).where(news_url_where).where(start_date_where).where(end_date_where).where(custom_city).order('id desc').limit(100)	
	    news_count = NewsPick.where(where).where(search_text_where).where(web_url_where).where(news_url_where).where(start_date_where).where(end_date_where).where(custom_city).count
		operators = Operator.where({deleted_at: nil})

		
		render :json => {
	      :data => data,
	      :news_count => news_count,
	      :operators => operators,
	      :limit => 100
	    }
	end

	def show
	    begin
	      newspick = NewsPick.find(params[:id].to_i)
	      render :json => newspick
	    rescue
	      render :nothing => true, :status => :not_found
	    end
	end

	def operators
		begin
			operators = Operator.where({deleted_at: nil})
			render :json => operators
		rescue
	      render :nothing => true, :status => :not_found
	    end
	end

	def create

		require 'uri'

		params[:news_url] = URI.unescape(params[:news_url])
	    if errors = self.validate(params, :create) and errors.length > 0 then
	      render :json => errors, :status => :bad_request and return
	    end

	    news_description = params[:news_description]

	    if !news_description.to_s.blank? then
	    	news_description = params[:news_description].chars.select(&:valid_encoding?).join
	    end

	    title = params[:title]
	    if params.key?(:title) && params[:title] != "" then 
	    	title = params[:title]
	    elsif params.key?(:og_title) && params[:og_title] != "" then
	    	title = params[:og_title]
	    end

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end

	    category_condition = [ 0, '0']
	    if params.key?(:category_id) and params[:category_id].to_i > 0 then
	    	category_condition.push(params[:category_id].to_i)
	    end

	    region_no = ""
	    if params[:region] == 'th' then
	        region_no = 1
	    elsif params[:region] == 'jp' then
	        region_no = 2
	    elsif params[:region] == 'en' then
	        region_no = 3
	    end

	    result_search_tag = ""
	    news_tag_array = []
	    news_tag = NewsTag.where({region: region_no}).where("city_id IS NULL OR city_id IN (?)", city_condition).where("category_id IS NULL OR category_id IN (?)", category_condition)

	    news_tag.each do |each_tag|
          if news_description.downcase.include?(each_tag.tag.downcase) or title.downcase.include?(each_tag.tag.downcase) then
          	  if !news_tag_array.include?(each_tag.tag.downcase) then
             		news_tag_array.push(each_tag.tag.downcase)
              end
              if !each_tag.tags_group.blank? then
              	  group_tags = each_tag.tags_group.split(',')
              	  group_tags.each do |each_group_tag|
              	  	 	if !news_tag_array.include?(each_group_tag.downcase) then
              	  			news_tag_array.push(each_group_tag.downcase)
              	  		end
              	  end
              end
          end
        end

        if !news_tag_array.blank? then
          result_search_tag = news_tag_array.join(",")
        end

        params[:tags] = result_search_tag

        image_url =  params[:image_url]
        if image_url != "" then
        	image_url = URI.decode(params[:image_url])
        end

        account_id = session[:account_id]

	    begin
	      newspick = nil
	      NewsPick.transaction do
	        newspick = NewsPick.register(title, params[:category_id], params[:region], params[:news_url], image_url, params[:city_id], 
	        	params[:publish_status], params[:tags], params[:hot_news], params[:publishers], params[:web_url], params[:news_promotion], params[:news_event], news_description, params[:news_staff_checked], params[:classified_sub_category], params[:news_pick_shop], params[:show_in_list].to_i, account_id.to_s, params[:is_magazine].to_i)	       
	      end
	      latest_id = NewsPick.order('id desc').limit(1).select('id')
	      render :json => latest_id and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request and return
	    end
	end

	def update
		require 'uri'
	    if errors = self.validate(params, :update) and errors.length > 0 then
	      render :json => errors, :status => :bad_request and return
	    end

	    news_description = params[:news_description]

	    if !news_description.to_s.blank? then
	    	news_description = params[:news_description].chars.select(&:valid_encoding?).join
	    end

	    classified_sub_category = params[:classified_sub_category].to_i
	    if classified_sub_category.blank? ||  params[:category_id].to_i != 97 then
	    	classified_sub_category = 0
	    end

	    title = params[:title]
	    if params.key?(:title) && params[:title] != "" then 
	    	title = params[:title]
	    elsif params.key?(:og_title) && params[:og_title] != "" then
	    	title = params[:og_title]
	    end

	    image_url =  params[:image_url]
        if image_url != "" then
        	image_url = URI.decode(params[:image_url])
        end

        publish_status_check = NewsPick.where(:id => params[:id]).first
        previous_status = publish_status_check.publish_status

        account_id = session[:account_id]

	    begin
	      newspick = nil
	      NewsPick.transaction do
	        newspick = NewsPick.find_by(id: params[:id])   
	        newspick.title = title
	        newspick.category_id = params[:category_id]
	        newspick.news_url = params[:news_url]
	        newspick.image_url = image_url
	        newspick.city_id = params[:city_id]
	        newspick.publish_status = params[:publish_status]
	        newspick.tags = params[:tags].delete(' ')
	        newspick.hot_news = params[:hot_news]
	        newspick.news_promotion = params[:news_promotion]
	        newspick.news_event = params[:news_event]
	        newspick.news_description = news_description
	        newspick.publishers = params[:publishers]
	        newspick.web_url = params[:web_url]
	        newspick.updated_at = Time.now
	        if (previous_status.to_i == 1 or  previous_status.to_i == 3) and params[:publish_status].to_i == 2 then
	        	newspick.created_at = Time.now
	        end
	        newspick.news_staff_checked = 1
	        newspick.classified_sub_category = classified_sub_category.to_i
	        newspick.news_pick_shop = params[:news_pick_shop]
	        newspick.show_in_list = params[:show_in_list].to_i
	        newspick.updated_by = account_id.to_s
	        newspick.is_magazine = params[:is_magazine].to_i
	        newspick.save!
	      end
	      render :json => newspick and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end
	end

	def delete
	    begin
	      newspick = NewsPick.find(params[:id].to_i)
	      newspick.deleted_at = Time.now
	      newspick.save!
	      render :nothing => true and return
	    rescue => e
	      render :json => [e.message], :status => :bad_request  and return
	    end
	end

	def check_duplicate
		begin
			where = {
		      :deleted_at => nil	     
		    }
			newspick = NewsPick.where({news_url: params[:news_url]}).where(where).first			
			render :json => newspick
		rescue => e
			render :json => [e.message], :status => :bad_request  and return
		end
	end

	def validate(params, mode)
	    errors = []

	    if :update == mode then
	      if params.key?(:id) then
	        begin
	          params[:id] = NewsPick.find(params[:id].to_i)
	        rescue
	          errors.push('News not found')
	        end
	      else
	        errors.push('id is required')
	      end
	    end
=begin
	    if params.key?(:title) then
	      title = params[:title]
	      if title.length == 0 then
	        errors.push('title is required')
	      end
	    else
	      errors.push('title is required')
	    end
=end
	    if params.key?(:news_url) then
	      content = params[:news_url]
	      if content.length == 0 then
	        errors.push('News URL is required')
	      end
	    else
	      errors.push('News URL is required')
	    end

	    if params.key?(:city_id) then
	      content = params[:city_id]
	      if content == '' then
	        errors.push('City ID is required')
	      end
	    else
	      errors.push('City ID is required')
	    end

	    if :create == mode then
		    if params.key?(:web_url) then
		      content = NewsPick.where({web_url: params[:web_url]}).first	     
		      if content then
		        errors.push('This web url already in used or empty please change')
		      end
		    else
		      errors.push('This web url already in used or empty please change')
		    end	

		    if params.key?(:news_url) then		      
				where = {
			      :region => params[:region],
			      :news_url => params[:news_url],
			      :deleted_at => nil	     
			    }
			    city_condition = [ 0, '0']
			    if params.key?(:city_id) and params[:city_id].to_i > 0 then
			    	city_condition.push(params[:city_id].to_i)
			    end
				#where[:city_id] = params[:city_id].to_i if params[:city_id].to_i > 0
				#where[:category_id] = params[:category_id].to_i if params[:category_id].to_i > 0
		      	content = NewsPick.where(where).first
			    if content then
			        errors.push('News URL is already added')
			    end
		    else
		      errors.push('News URL is required')
		    end
		end
	    
		if params.key?(:category_id) then
	      content = params[:category_id]
	      if (content == nil) then
	        errors.push('Category is required')
	      end
	    else
	      errors.push('Category is required')
	    end


	    if params.key?(:publish_status) then
	      content = params[:publish_status]
	      if (content == nil) then
	        errors.push('Publish status is required')
	      end
	    else
	      errors.push('Publish status is required')
	    end

	    errors
	end
end
