class Admin::InformationController < Admin::BaseController
  def index
    render :json => Information.where({:deleted_at => nil}).order('id desc').all
  end

  def show
    begin
      information = Information.find(params[:id].to_i)
      render :json => information
    rescue
      render :nothing => true, :status => :not_found
    end
  end

  def create
    if errors = self.validate(params, :create) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      information = nil
      Information.transaction do
        information = Information.register(params[:app], params[:title], params[:content], params[:published_at_date], params[:image_url], params[:video_url])
      end
      render :json => information and return
    rescue => e
      render :json => [e.message], :status => :bad_request and return
    end
  end

  def update
    if errors = self.validate(params, :update) and errors.length > 0 then
      render :json => errors, :status => :bad_request and return
    end

    begin
      information = nil
      Information.transaction do
        information = params[:information]
        information.app_id = params[:app].id
        information.title = params[:title]
        information.content = params[:content]
        information.image_url = params[:image_url]
        information.video_url = params[:video_url]
        information.published_at = params[:published_at_date]
        information.updated_at = Time.now
        information.save!
      end
      render :json => information and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def delete
    begin
      information = Information.find(params[:id].to_i)
      information.deleted_at = Time.now
      information.save!
      render :nothing => true and return
    rescue => e
      render :json => [e.message], :status => :bad_request  and return
    end
  end

  def validate(params, mode)
    errors = []

    if :update == mode then
      if params.key?(:id) then
        begin
          params[:information] = Information.find(params[:id].to_i)
        rescue
          errors.push('infomration not found')
        end
      else
        errors.push('id is required')
      end
    end

    app = App.where({id: params[:app_id].to_i}).first
    if app then
      params[:app] = app
    else
      errors.push('app is required')
    end

    if params.key?(:title) then
      title = params[:title]
      if title.length == 0 then
        errors.push('title is required')
      end
    else
      errors.push('title is required')
    end

    if params.key?(:content) then
      content = params[:content]
      if content.length == 0 then
        errors.push('content is required')
      end
    else
      errors.push('content is required')
    end

    if params.key?(:published_at) then
      published_at = params[:published_at]
      begin
        params[:published_at_date] = Time.strptime(published_at, Rails.application.config.admin_request_date_format)
      rescue
        errors.push("published_at is invalid format. format is #{Rails.application.config.admin_request_date_format_readable}")
      end
    else
      errors.push('published_at is required')
    end 

    errors
  end
end
