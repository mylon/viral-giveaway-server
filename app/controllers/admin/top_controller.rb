class Admin::TopController < Admin::BaseController
  before_action :authenticate_api, except: :index
  before_action :public_api, only: :index

  def index
    render :layout => 'admin'
  end
end
