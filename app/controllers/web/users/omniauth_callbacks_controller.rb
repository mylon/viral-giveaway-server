class Web::Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    _callback(:facebook)
  end

  def twitter
    _callback(:twitter)
  end

  def google_oauth2
    _callback(:google_oauth2)
  end

  def _callback(provider)
    logger.info "provider=#{provider}, omniauth.auth=#{request.env["omniauth.auth"].to_json}, omniauth.params=#{request.env["omniauth.params"].to_json}"
    omniauth_params = request.env["omniauth.params"] or {}
    logger.info "omniauth.params=#{omniauth_params.to_json}"
    case omniauth_params["action"]
    when "post" then
      begin
        VGLogger.post({ path: "/users/auth/#{provider}/callback", action: 'post', provider: provider })
        _post_action_callback(provider)
      rescue => e
        log_error e
        redirect_to "/#/home" and return
      end
    when "like" then
      VGLogger.post({ path: "/users/auth/#{provider}/callback", action: 'like', provider: provider })
      _like_action_callback(provider)
    when "follow" then
      VGLogger.post({ path: "/users/auth/#{provider}/callback", action: 'follow', provider: provider })
      _follow_action_callback(provider)
    else
      _register_callback(provider)
    end
  end

  def _register_callback(provider)
    begin
      auth = request.env['omniauth.auth']
      if errors = User.validate_register_by_auth(auth) and errors.length > 0 then
        reset_session
        flash[:errors] = errors 
        redirect_to "/#/login" and return
      end

      user = nil
      User.transaction do
        user = User.register_by_auth(auth, {:source => session[:source], :referral_code => session[:referral_code]})
      end
 
      VGLogger.post({ 
        path: "/users/auth/#{provider}/callback", 
        action: 'register', 
        provider: provider, 
        account_id: user.account_id, 
        source: session[:source], 
      })

      # create session
      session[:user_id] = user.account_id
      session[:token]   = user.token
           
      redirect_to "/#/home" and return
    rescue => e
      log_error e
      logger.error request.env['omniauth.auth'].to_json
      redirect_to "/#/login" and return
    end
  end

  def _post_action_callback(provider)
    message = request.env["omniauth.params"]['message']
    auth    = request.env["omniauth.auth"]
    case provider
    when :twitter then
      _post_twitter(message, auth)
    when :facebook then
      _post_facebook(message, auth)
    end
    redirect_to "/#/home"
  end

  def _like_action_callback(provider)
    auth = request.env["omniauth.auth"]
    case provider
    when :facebook then
      _like_facebook(auth)
    end
    redirect_to "/#/home"
  end

  def _follow_action_callback(provider)
    auth = request.env["omniauth.auth"]
    case provider
    when :twitter then
      _follow_twitter(auth)
    end
    redirect_to "/#/home"
  end

  def _like_facebook(auth)
    page = FbGraph::Page.new(Rails.application.config.facebook_page_id)
    user = FbGraph::User.me(auth[:credentials][:token])
    liked = user.like?(page)
    logger.debug("liked=#{liked}")

    if liked then 
      track_type = :facebook_like
      point_num  = Rails.application.config.facebook_like_point_num
      point_item_type = :facebook_like
      track_point(track_type, point_num, point_item_type)
    end
  end

  def _follow_twitter(auth)
    client = Twitter::REST::Client.new do |config|
      config.consumer_key       = Rails.application.config.twitter_consumer_key
      config.consumer_secret    = Rails.application.config.twitter_consumer_secret
      config.oauth_token        = auth[:credentials][:token]
      config.oauth_token_secret = auth[:credentials][:secret]
    end
    user = client.user(Rails.application.config.twitter_account_id)
    followed_users = client.follow!(user)
    logger.debug("followed_users.length=#{followed_users.length}")

    if followed_users.length == 1 then
      track_type = :twitter_follow
      point_num  = Rails.application.config.twitter_follow_point_num
      point_item_type = :twitter_follow
      track_point(track_type, point_num, point_item_type)
    end
  end

  def _post_twitter(message, auth)
    logger.info "credentials=#{auth[:credentials]}"
    client = Twitter::REST::Client.new do |config|
      config.consumer_key       = Rails.application.config.twitter_consumer_key
      config.consumer_secret    = Rails.application.config.twitter_consumer_secret
      config.oauth_token        = auth[:credentials][:token]
      config.oauth_token_secret = auth[:credentials][:secret]
    end
    result = client.update(message)
    logger.debug "twitter.post result=#{result}"
    track_share_bonus(:twitter)
  end

  def _post_facebook(message, auth)
    logger.info "credentials=#{auth[:credentials]}"
    user = FbGraph::User.me(auth[:credentials][:token])
    result = user.feed!(
      :message => message
    )
    logger.debug "facebook.post result=#{result}"
    track_share_bonus(:facebook)
  end

  def track_share_bonus(provider)
    track_type = nil
    point_num  = nil
    point_item_type = nil
    case provider
    when :twitter then
      track_type = :twitter_share
      point_num  = Rails.application.config.twitter_share_point_num
      point_item_type = :twitter_share
    when :facebook then
      track_type = :facebook_share
      point_num  = Rails.application.config.facebook_share_point_num
      point_item_type = :facebook_share
    end

    track_point(track_type, point_num, point_item_type)
  end

  def track_point(track_type, point_num, point_item_type)
    Tracking.transaction do
      tracking = nil
      user = User.where({:deleted_at => nil, :account_id => session[:user_id].to_s}).where("verified_at IS NOT NULL").first
      if user then
        unless Tracking.tracked?(user, '1', track_type, :conversioned) then
          tracking = Tracking.track(user, '1', track_type, :conversioned)
          user.deposit_point(point_num, point_item_type, tracking.id)
          Message.register_of_point(user, track_type, point_num)
        end
      end
    end
  end

  # GET|POST /resource/auth/twitter
  def passthru
    super
  end

  # GET|POST /users/auth/twitter/callback
  def failure
    super
  end

  # protected

  # The path used when OmniAuth fails
  def after_omniauth_failure_path_for(scope)
    super(scope)
  end
end
