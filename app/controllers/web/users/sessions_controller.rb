class Web::Users::SessionsController < Web::BaseController
  def create
    begin
      user = User.find_by_email_and_password(params[:email], params[:password])
      if user then
        user.platform = Thread.current[:platform].to_i
        user.save

        session[:user_id] = user.account_id
        session[:source] = user.an_source
        session[:referral_code] = user.an_referral_code
        session[:token] = user.token

        data = { 
          :authenticated => true,
          :user => { 
            :id => session[:user_id], 
            :token => session[:token],
            :source => session[:source], 
            :referral_code => session[:referral_code], 
          },
        }

        VGLogger.post({ path: '/users/session/create', user_id: user.account_id })
        render_to_json(data) and return
      else
        render_to_error_json([ I18n.t('error.authorization.login_failed') ], { :status => :bad_request }) and return
      end
    rescue => e
      log_error(e)
      render_to_error_json([ I18n.t('error.authorization.login_failed') ], { :status => :bad_request }) and return
    end
  end

  def delete
    if session[:user_id] then
      VGLogger.post({ path: '/users/session/delete', user_id: session[:user_id] })
    end
    session[:user_id] = nil
    session[:token] = nil
    session[:source] = nil
    session[:referral_code] = nil
    render :json => {} and return
  end
end
