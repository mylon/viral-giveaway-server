class Web::ConfigController < Web::BaseController
  def index
    data = {
      :rp_endpoint => Rails.application.config.rp_endpoint,
      :session_id => session.id,
      :shown_invitation_dialog => false,
      :api => {
        :endpoint => Rails.application.config.api_endpoint,
      },
      :question_map => Rails.application.config.question_map,
      :referral_code => session[:referral_code].to_s,
      :source => session[:source].to_s,
    }
    if @user then
      data[:shown_invitation_dialog] = Tracking.tracked?(@user, 1, :show_invitation_dialog, :conversioned).present?
      data[:shared_facebook] = Tracking.tracked?(@user, 1, :facebook_share, :conversioned).present?
      data[:shared_twitter] = Tracking.tracked?(@user, 1, :twitter_share, :conversioned).present?
      data[:liked_facebook] = Tracking.tracked?(@user, 1, :facebook_like, :conversioned).present?
      data[:followed_twitter] = Tracking.tracked?(@user, 1, :twitter_follow, :conversioned).present?
    end

    logger.info "session user_id=#{session[:user_id]}, token=#{session[:token]}"
    if session[:user_id] and session[:token] then
      data[:user] = {
        :id    => session[:user_id],
        :token => session[:token],
      }
    end
    render :json => data
  end
end
