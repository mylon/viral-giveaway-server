class Web::StaticController < Web::BaseController
  def index
    if params.key?(:rc) then
      session[:referral_code] = params[:rc].to_s
    end

    if params.key?(:source) then
      session[:source] = params[:source].to_s
    end
   
    if params.key?(:rc) or params.key?(:source) then   
      redirect_to('/') and return
    end

    render :layout => 'web' and return
  end
end
