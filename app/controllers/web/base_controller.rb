class Web::BaseController < ApplicationController
  before_action :set_analytics, :set_session_id, :set_user

  def render_pc
    ua = Thread.current[:ua]
    logger.info "render_pc. ua=#{ua}"
    if ua.nil? or ua[:category] == :pc then
      render :layout => false, :template => 'web/static/pc' and return
    end
  end

  def set_analytics
    Thread.current[:source] = session['source'].to_s
  end

  def set_session_id
    Thread.current[:session_id] = session.id 
  end

  def set_user
    if session[:user_id] then
      Thread.current[:account_id] = session[:user_id]
      @user = User.where({
        :account_id => session[:user_id],
        :deleted_at => nil,
      }).where("verified_at IS NOT NULL").first
    end 
  end
end
