class Api::V1::LoggingController < Api::V1::BaseController
  before_action :authenticate_api, except: :logging
  before_action :public_api, only: :logging

  def logging
    begin
      VGLogger.post({
        :path => params[:path],
      })
      render_to_json({}) and return
    rescue => e
      log_error e
      render_to_error_json([], {:status => :internal_server_error})
    end
  end
end
