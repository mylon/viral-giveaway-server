class Api::V1::BonusController < Api::V1::BaseController

  def daily
    begin
      tracking = nil
      point_num = 1
      Tracking.transaction do
        today = Date.today
        unless Tracking.tracked?(@user, today.to_s, :daily_bonus, :conversioned) then
          tracking = Tracking.track(@user, today.to_s, :daily_bonus, :conversioned)
          @user.deposit_point(point_num, :daily_bonus, tracking.id)
          Message.register_of_point(@user, :daily_bonus, point_num)
        end
      end

      VGLogger.post({ path: '/v1/bonus/daily' }) if tracking

      offers = Offer.fetch_recommends(@user, @user.platform).map{ |o| Offer.convert(o) }.select{ |o| o.present? }
      render_to_json({
        :point_num => tracking.nil? ? 0 : point_num,
        :offers => offers,
      })
    rescue => e
      log_error e
      render_to_error_json(['ERROR Please retry'], { :status => :internal_server_error })
    end 
  end

  def share_facebook
    VGLogger.post({ path: "/v1/bonus/share_facebook" })
    track_type = :facebook_share
    point_num  = Rails.application.config.facebook_share_point_num
    point_item_type = :facebook_share
    tracking = track_point(track_type, point_num, point_item_type)
    render_to_json({
      :point_num => tracking.present? ? point_num : 0
    })
  end

  def like_facebook
    VGLogger.post({ path: "/v1/bonus/like_facebook" })
    track_type = :facebook_like
    point_num  = Rails.application.config.facebook_like_point_num
    point_item_type = :facebook_like
    tracking = track_point(track_type, point_num, point_item_type)
    render_to_json({
      :point_num => tracking.present? ? point_num : 0
    })
  end


  def track_share_bonus(provider)
    track_type = nil
    point_num  = nil
    point_item_type = nil
    case provider
    when :twitter then
      track_type = :twitter_share
      point_num  = Rails.application.config.twitter_share_point_num
      point_item_type = :twitter_share
    when :facebook then
      track_type = :facebook_share
      point_num  = Rails.application.config.facebook_share_point_num
      point_item_type = :facebook_share
    end

    track_point(track_type, point_num, point_item_type)
  end

  def track_point(track_type, point_num, point_item_type)
    tracking = nil
    Tracking.transaction do
      unless Tracking.tracked?(@user, '1', track_type, :conversioned) then
        tracking = Tracking.track(@user, '1', track_type, :conversioned)
        @user.deposit_point(point_num, point_item_type, tracking.id)
        Message.register_of_point(@user, track_type, point_num)
      end
    end
    tracking
  end

end
