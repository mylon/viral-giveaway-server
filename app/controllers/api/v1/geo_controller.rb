class Api::V1::GeoController < Api::V1::BaseController
  def detect_country
    begin
      imsi = params[:imsi].to_s
      country_iso = MobileCountryCode.find_country_iso(imsi)
      render_to_json({ 
        :country_iso => country_iso, 
        :ip => request.remote_ip, 
        :imsi => imsi, 
        :imsi_invalid => imsi 
      })
    rescue => e
      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
      render_to_error_json(['Error'], { :status => 404 })
    end
  end
end
