class Api::V1::Apps::Payments::AppleController < Api::V1::Apps::Payments::BaseController
  def verify
    begin
      verified = PurchaseLog.verify_apple_receipt(params[:receipt])

      if verified then
        PurchaseLog.transaction do
          #XXX can't get transaction id?
          purchase_log = PurchaseLog.register(@user, :apple, params[:transaction_id], params[:receipt], params[:item], params[:item_id])
          purchase_log.update_item_status()
        end
        render_to_json({ verified: verified }) and return
      else
        render_to_error_json(['Invalid Receipt'], { :status => :bad_request }) and return
      end
    rescue ActiveRecord::RecordNotUnique => e
      log_error e
      render_to_error_json(['Receipt is alraedy used'], { :status => :bad_request }) and return
    rescue => e
      log_error e
      render_to_error_json(['ERROR Please retry'], { :status => :bad_request }) and return
    end
  end
end
