class Api::V1::Apps::UsersController < Api::V1::Apps::BaseController
  before_action :authenticate_api, except: [:register, :verify, :reset_password, :resend_verify, :user_login, :unverify]
  before_action :public_api, only: [:register, :verify, :reset_password, :resend_verify]

  def register
    begin      
      if errors = validate_register(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      app_layout = 'mail/np'
      app_template = 'mail/np/verify'
      app_subject = 'mail.np.verify_subject'
      app_name = "Local Picks"
      if params[:region] == 'jp' then
        if params[:city_id].to_i == 1 then
          app_layout = 'mail/npbkkjp'
          app_template = 'mail/np/verifybkkjp'
          app_subject = 'mail.np.verify_bkk_jp_subject'
          app_name = "バンコクピックス"
        elsif params[:city_id].to_i == 6 then
          app_layout = 'mail/npjakjp'
          app_template = 'mail/np/verifyjakjp'
          app_subject = 'mail.np.verify_jak_jp_subject'
          app_name = "ジャカルタ"
        elsif params[:city_id].to_i == 19 then
          app_layout = 'mail/npnykjp'
          app_template = 'mail/np/verifynykjp'
          app_subject = 'mail.np.verify_nyk_jp_subject'
          app_name = "ニューヨーク"
        end
      end
 
      user = nil 
      User.transaction do           
        user = User.register_from_app(params) 
        if user and user.is_verified === false and user.provider == 'np' then
          user.send_email!(
            VGMailUtil.build_sender(Rails.application.config.np_noreply_source, app_name),
            I18n.t(app_subject, :locale => user.locale.to_sym),
            render_to_string( :template => app_template, :layout => app_layout, :locals => { verify_code: user.verification_code } )
          )
        end
      end 

      if user then
        render_to_json({
          user_id: user.id,
          name: user.name,
          city_id: params[:city_id],
          region: params[:region],
          account_id: user.account_id,
          token: user.token,  
          tag_line: user.tag_line,       
          verified: user.is_verified
        }) and return       
      else
        render_to_error_json([e.message], { :status => :bad_request }) and return
      end
    rescue => e
      log_error(e)
      render_to_error_json([e.message],{ :status => :bad_request }) and return
    end
  end

  def user_login
       user = User.where({ deleted_at: nil, canonical_email: params[:email].downcase , encrypted_password: User.encrypt_password(params[:password].to_s)}).first
       if user then
        render_to_json({
          user_id: user.id,
          name: user.name,
          account_id: user.account_id,
          token: user.token,  
          tag_line: user.tag_line,       
          verified: user.is_verified
        }) and return       
      else
        render_to_error_json(['Not found user please check your username and password'], { :status => :bad_request }) and return
      end
  end

  def verify
    begin
      user = nil
      User.transaction do
        user = User.verify(params[:user_id].to_s, params[:code].to_s)
      end

      VGLogger.post({ path: '/v1/apps/users/verify', user_id: user.id, app: @app.code }) if user

      render_to_json({
          user_id: user.id,
          name: user.name,
          account_id: user.account_id,
          token: user.token,  
          tag_line: user.tag_line,       
          verified: user.is_verified
        }) and return
    rescue => e
      log_error(e)
      #render_to_error_json(['Verify Failed'], { :status => :bad_request }) and return
       user = User.where({ id: params[:user_id] }).first
       if user then
        render_to_json({
          user_id: user.id,
          name: user.name,
          account_id: user.account_id,
          token: user.token,  
          tag_line: user.tag_line,       
          verified: user.is_verified
        }) and return    
       else
        render_to_error_json(['Verify Failed'], { :status => :bad_request }) and return
      end
    end
  end

  def unverify
    begin
      user = nil
      User.transaction do
        user = User.unverify(params[:user_id].to_s)
      end

      VGLogger.post({ path: '/v1/apps/users/verify', user_id: user.id, app: @app.code }) if user

       render_to_json({
          user_id: user.id,
          name: user.name,
          account_id: user.account_id,
          token: user.token,  
          tag_line: user.tag_line,       
          verified: user.is_verified
        }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Verify Failed'], { :status => :bad_request }) and return
    end
  end

  def resend_verify
    begin
      user = User.where({ :canonical_email => params[:email].to_s.downcase, :deleted_at => nil }).first
      if user.nil? then
        render_to_error_json(['Resend Failed'], { :status => :bad_request }) and return
      elsif user.is_verified then
        render_to_error_json([I18n.t('error.user.already_verified', :localse => user.locale.to_sym)], { :status => :bad_request }) and return
      end

      app_layout = 'mail/np'
      app_template = 'mail/np/resend_verify'
      app_subject = 'mail.np.resend_verify_subject'
      app_name = @app.name
      if params[:region] == 'jp' then
        if params[:city_id].to_i == 1 then
          app_layout = 'mail/npbkkjp'
          app_template = 'mail/np/resend_verifybkkjp'
          app_subject = 'mail.np.resend_verify_bkk_jp_subject'
          app_name = "バンコクピックス"
        elsif params[:city_id].to_i == 6 then
          app_layout = 'mail/npjakjp'
          app_template = 'mail/np/resend_verifyjakjp'
          app_subject = 'mail.np.resend_verify_jak_jp_subject'
          app_name = "ジャカルタ"
        elsif params[:city_id].to_i == 19 then
          app_layout = 'mail/npnykjp'
          app_template = 'mail/np/resend_verifynykjp'
          app_subject = 'mail.np.resend_verify_nyk_jp_subject'
          app_name = "ニューヨーク"
        end
      end

      VGLogger.post({ path: '/v1/users/resend_verify', user_id: user.account_id, app: @app.code })
      
      user.send_email!(
        VGMailUtil.build_sender(Rails.application.config.np_noreply_source, app_name),
        I18n.t(app_subject, :locale => user.locale.to_sym),
        render_to_string( :template => app_template, :layout => app_layout, :locals => { verify_code: user.verification_code } )
      )

      render_to_json({ sent: user.present? }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Resend Failed'], { :status => :bad_request }) and return
    end
  end

  def reset_password
    begin
      user = nil
      password = nil
      User.transaction do
        user = User.find_by_email(params[:email])
        if user then
          password = User.generate_password()
          user.encrypted_password = User.encrypt_password(password)
          user.save!
        end
      end
      if user then
        VGLogger.post({ path: '/v1/users/password_reset', user_id: user.account_id, app: @app.code })

        user.send_email!(
          VGMailUtil.build_sender(Rails.application.config.np_noreply_source, @app.name),
          I18n.t('mail.np.password_reset_subject', :locale => user.locale.to_sym),
          render_to_string( :template => 'mail/np/password_reset', :layout => 'mail/np', :locals => { new_password: password } ) 
        )
      end
      render_to_json({ sent: user.present? }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Not found'], { :status => :bad_request }) and return
    end
  end

  def profile
    render_to_json(_profile(@user))
  end
  
  def profile_update
    begin
      if errors = @user.validate_profile(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      User.transaction do
        @user.name      = params[:name].strip if params.key?(:name)
        @user.birthdate = params[:birthdate_date] if params.key?(:birthdate)
        @user.gender    = params[:gender] if params.key?(:gender)
        @user.tag_line      = params[:tag_line].strip if params.key?(:tag_line)
        @user.encrypted_password  = User.encrypt_password(params[:new_password]) if params[:new_password].to_s.length > 0

        @user.save!
      end

      VGLogger.post({ path: '/v1/users/profile_update', app: @app.code })

      render_to_json(_profile(@user))
    rescue => e
      log_error e
      render_to_error_json(['ERROR Please retry'], { :status => :bad_request })
    end
  end

  def _profile(user)
    birthdate = user.birthdate.blank? ? nil : user.birthdate.strftime(Rails.application.config.birthdate_format)
    data = {
      :user_id      => user.account_id,
      :name         => user.name,
      :birthdate    => birthdate,
      :gender       => user.gender,
      :email        => user.email,
      :tag_line     => user.tag_line,
      :saved_password => user.encrypted_password.to_s.length > 0,
    }
    if user.birthdate then
      data[:birthdate_year]  = user.birthdate.year
      data[:birthdate_month] = user.birthdate.month
      data[:birthdate_day]   = user.birthdate.day
    end

    data
  end

  def validate_register(params)
    errors = []
    
    email = params[:email].to_s.strip
    if params[:provider].to_s != "twitter" and params[:provider].to_s != "facebook" then
      if email.blank? then
        errors.push(I18n.t('error.user.email_blank'))
      elsif not email.match(Rails.application.config.email_regex) then
        errors.push(I18n.t('error.user.email_format_invalid'))
      elsif email.length > Rails.application.config.email_length_max then
        errors.push(I18n.t('error.user.email_length_max', length:Rails.application.config.email_length_max))
      end
    end

    case params[:provider].to_s
    when "facebook"
      begin
        user = FbGraph::User.me(params[:access_token].to_s).fetch
        logger.info "user=#{user.inspect}"
        if user.identifier == params[:provider_user_id] then
          params[:user] = user
        else
          errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end
      rescue => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      end
    when "google"
      begin
        user = GooglePlus::Person.get(params[:provider_user_id], api_key: Rails.application.config.google_api_key, access_token: params[:access_token].to_s)
        logger.info "user=#{user.inspect}"
        if user.id == params[:provider_user_id] then
          params[:user] = user
        else
          errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end
      rescue GooglePlus::RequestError => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      rescue => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      end
    when "twitter"
      begin
        twitter_consumer_key = Rails.application.config.newspick_bkk_jp_twitter_consumer_key
        twitter_consumer_secret  = Rails.application.config.newspick_bkk_jp_twitter_consumer_secret       

        if params[:region] == 'jp' then
          if params[:city_id].to_i == 1 then
            twitter_consumer_key = Rails.application.config.newspick_bkk_jp_twitter_consumer_key
            twitter_consumer_secret  = Rails.application.config.newspick_bkk_jp_twitter_consumer_secret  
          elsif params[:city_id].to_i == 6 then
            twitter_consumer_key = Rails.application.config.newspick_jak_jp_twitter_consumer_key
            twitter_consumer_secret  = Rails.application.config.newspick_jak_jp_twitter_consumer_secret
          elsif params[:city_id].to_i == 19 then
            twitter_consumer_key = Rails.application.config.newspick_nyk_jp_twitter_consumer_key
            twitter_consumer_secret  = Rails.application.config.newspick_nyk_jp_twitter_consumer_secret
          end
        end


        client = Twitter::REST::Client.new do |config|
          #temporary solution need better
          config.consumer_key    = twitter_consumer_key
          config.consumer_secret = twitter_consumer_secret    
        end
        user = client.user(params[:provider_user_id])
        logger.info "user=#{user.inspect}"
        if user then
            params[:user] = user           
        else
            errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end        
      rescue => e
        log_error e
        errors.push(e)       
      end
    when "vg", "np"
=begin
      password_len = params[:password].to_s.length
      if password_len < Rails.application.config.password_length_min or password_len > Rails.application.config.password_length_max then
        errors.push(I18n.t('error.user.password_length', min: Rails.application.config.password_length_min, max: Rails.application.config.password_length_max))
      end
=end
    else
      errors.push(I18n.t('error.user.provider_invalid'))
    end 

    return errors if errors.length > 0 

   if params[:provider].to_s == 'twitter' or params[:provider].to_s == 'facebook' then
     user = User.where({ deleted_at: nil, uid: params[:provider_user_id] }).first
   else
     user = User.where({ deleted_at: nil, canonical_email: params[:email].downcase }).first
   end

    if user then
      case params[:provider].to_s
      when "facebook", "google", "twitter"
        unless user.uid == params[:provider_user_id] then
          errors.push(I18n.t('error.user.email_exists'))
        end
      when "vg", "np"
=begin
        unless user.encrypted_password == User.encrypt_password(params[:password].to_s) then
          errors.push(I18n.t('error.authorization.login_failed'))
        end  
=end      
      end
    end

    errors
  end
end
