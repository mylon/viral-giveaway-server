class Api::V1::Apps::SessionsController < Api::V1::Apps::BaseController
  before_action :authenticate_api, except: [:create]
  before_action :public_api, only: [:create]

  def create
    begin
      if errors = validate_create(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end
    
      user = nil 
      User.transaction do
        user = User.register_from_app(params) 
      end 

      if user then
        render_to_json({
          user_id: user.account_id,
          token: user.token,
          verified: user.verified_at.present?, 
        }) and return
      else
        render_to_error_json([I18n.t('error.authorization.login_failed')], { :status => :bad_request }) and return
      end
    rescue => e
      log_error(e)
      render_to_error_json(['ERROR Please retry'], { :status => :bad_request }) and return
    end
  end

  def validate_create(params)
    errors = []

    case params[:provider].to_s
    when "facebook"
      begin
        user = FbGraph::User.me(params[:access_token].to_s).fetch
        if user.identifier == params[:provider_user_id] then
          params[:user] = user
          params[:email] = user.email
        else
          errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end
      rescue => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      end
    when "google"
      begin
        user = GooglePlus::Person.me(params[:provider_user_id], :access_token => params[:access_token].to_s)
        if user.id == params[:provider_user_id] then
          params[:user] = user
          params[:email] = user.emails.first['value']
        else
          errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end
      rescue => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      end
    when "vg"
    else
      errors.push(I18n.t('error.user.provider_invalid'))
    end 

    errors
  end

end
