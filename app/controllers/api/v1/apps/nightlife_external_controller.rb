class Api::V1::Apps::NightlifeExternalController < Api::V1::Apps::BaseController

	before_action :authenticate_api, except: [:find_nearby, :get_club_data,:get_club_image]
	before_action :authenticate_app_api, except: [:find_nearby, :get_club_data,:get_club_image]

	def find_nearby
		
		#result = Geocoder.search("Paris", :bounds => [[32.1,-95.9], [33.9,-94.3]])
		filter_result = []
		club_name = ""
		lat = params[:lat].to_s
		long = params[:long].to_s
		center = lat+','+long
		
		result = FbGraph::Place.search("club", :distance => 100000, :center => center , :access_token => '739891342809062|fff51d97a9a283264f56c05f658cf8b1')
		result.each do |club|
			city = ""
			category_name = ""
			club_image = ""

			if club.raw_attributes['location']['city'] != "" then
				city = club.raw_attributes['location']['city'].gsub(/\s+/, " ").strip
			end

			ActiveRecord::Base.establish_connection Rails.application.config.db_host_nightlife
			find_club = NightlifeNearbyClub.where({club_title: club.name}).first
			if find_club then
				filter_result.push(find_club)
			else
				club_result = get_club_data(club.name.downcase.gsub(/\s+/, " ").strip, city)
				if club_result != "" then
					if get_club_image(club.identifier) then
						club_image = get_club_image(club.identifier)
					end
					temp = {}
					temp['club_item_id'] = club_result['id']
					temp['club_title'] =  club_result['title']
					temp['club_url'] = "https://www.nightlife8hrs.com/"+club_result['alias']
					if club_image != "" then
						temp['club_image'] = club_image
					else
						temp['club_image'] = club_result['image']
					end
			 		ActiveRecord::Base.establish_connection Rails.application.config.db_host_nightlife
				    newspick = nil
			        NewsPick.transaction do
			        	newspick = NightlifeNearbyClub.register(temp['club_item_id'], temp['club_title'], temp['club_url'], temp['club_image'])	       
			     	end
					filter_result.push(temp)
				end
			end
		end

		render_to_json({
	     	result: filter_result
	    })

	end

	def get_club_data(club_name_from_facebook, city_name_from_facebook)

		require 'digest/md5'

		final_result = ""
		club_name = club_name_from_facebook.gsub(/'/, "''")
		city_name = city_name_from_facebook.gsub(/'/, "''")

		#connect ot nightlife database
		ActiveRecord::Base.establish_connection "production_night"

		#sql = "SELECT title, alias,extra_fields FROM nl8hours_k2_items WHERE published = 1 and trash = 0 and LOWER(title) LIKE '%#{club_name}%' LIMIT 1"
		sql = "SELECT id, title, alias FROM nl8hours_k2_items WHERE published = 1 and trash = 0 and LOWER(extra_fields_search) LIKE '%#{city_name}%' and LOWER(title) = '#{club_name}' LIMIT 1"
		result = ActiveRecord::Base.connection.execute(sql)

		result.each(:as => :hash) do |row|
			row['image'] = 'https://www.nightlife8hrs.com/media/k2/items/cache/'+Digest::MD5.hexdigest("Image"+row['id'].to_s)+'_L.jpg'
			final_result = row
		end

		final_result
	end

	def get_club_image(page_id)
		require "net/https"
		require "uri"

		url = URI.parse('https://graph.facebook.com/v2.5/'+page_id+'/picture?access_token=739891342809062|fff51d97a9a283264f56c05f658cf8b1')		
		r = Net::HTTP.get_response(url)
		return r["location"]
	    
	end


end
