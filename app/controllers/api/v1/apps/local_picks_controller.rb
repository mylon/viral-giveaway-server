class Api::V1::Apps::LocalPicksController < Api::V1::Apps::BaseController
	before_action :authenticate_api, except: [:find_nearby_four, :find_nearby_google, :top_menu_link, :get_event_by_city, :get_event_by_latlong, :region_menu_index, :side_menu_link, :device_token, :get_twitter_data, :find_nearby, :now_menu, :category_menu, :region_menu, :get_sidebar_menu, :get_city_name, :get_news, :find_city_id, :create_news_object, :get_related_news]
	before_action :authenticate_app_api, except: []

	require 'uri'


	def create_news_object(item, lang_code)

		bookmark_count = 0
		bookmark_count = NewsBookmark.where(:news_id => item.id).count
		comment_count = 0
		comment_count =  Comment.where(:news_id => item.id).count

		temp = {}
		temp["id"] = item.id
		temp["title"] =item.title
		temp["category_id"] =item.category_id
		temp["region"] =item.region
		temp["news_url"] = URI.encode(item.news_url)
		temp["image_url"] = URI.encode(item.image_url)
		temp["city_id"] =item.city_id
		temp["publish_status"] =item.publish_status
		temp["deleted_at"] =item.deleted_at
		temp["created_at"] =item.created_at			
		temp["updated_at"] =item.updated_at
		temp["tags"] =item.tags
		temp["hot_news"] =item.hot_news
		temp["publishers"] =item.publishers
		temp["web_url"] = item.web_url
		temp["bookmark"] = false
		#temp["news_description"] =item.news_description
		temp["news_event"] =item.news_event
		temp["news_promotion"] =item.news_promotion
		temp["classified_sub_category"] =item.classified_sub_category
		temp["news_pick_shop"] =item.news_pick_shop
		temp["show_in_list"] =item.show_in_list
		temp["is_magazine"] =item.is_magazine
		temp["city_name"] = 'all'
		temp["bookmark_count"] = bookmark_count
		temp["comment_count"] = comment_count
		temp["twitter_link"] = "https://mobile.twitter.com/search?q="
		temp["google_link"] = 'https://www.google.co.th/search?hl='+lang_code+'&tbm=nws&authuser=0&q='
		if item.news_pick_shop.blank? then
			#search_text_split = item.title.downcase.split(' ')
			#search_text = search_text_split[0]
	    	#if search_text_split.length > 1 then
	    	#	search_text = search_text + " " + search_text_split[1]
	    	#end
			#temp["twitter_link"] = URI.encode('https://mobile.twitter.com/search?q='+search_text)
		else
			temp["twitter_link"] = 'https://mobile.twitter.com/search?q='+CGI.escape(item.news_pick_shop)
			temp["google_link"] = 'https://www.google.co.th/search?hl='+lang_code+'&tbm=nws&authuser=0&q='+CGI.escape(item.news_pick_shop)+"&oq="+CGI.escape(item.news_pick_shop)+"&sourceid=chrome&ie=UTF-8#q="+CGI.escape(item.news_pick_shop)+"&tbs=qdr:y,sbd:1"
		end
		Rails.application.config.newspick_city_map.each do |k, v|
			if item.city_id == k then
				temp["city_name"] = v
			end
		end
		if temp["image_url"] == "" then
			Rails.application.config.localpicks_category_map.each do |k, v|
				if item.category_id.to_i == k.to_i then
					random_number = rand(1..5)
					temp["image_url"] = Rails.application.config.news_app_s3_bucket_url+'news_cat_square/'+v.to_s+'_'+random_number.to_s+'.jpg'
				end
			end
		end

		temp

	end

	def get_event_by_latlong
		require 'net/http'
		require 'json'
		require 'cgi'

		lat = params[:lat].to_s
		long = params[:long].to_s
		result = []

		#if lat.blank? or long.blank?  then
		#	lat = "13.7563309".to_s
		#	long = "100.5017651".to_s
		#end

		uri = URI('https://api.allevents.in/events/geo/?latitude='+lat+'&longitude='+long+'&radius=10')

		request = Net::HTTP::Post.new(uri.request_uri)
		# Request headers
		request['Ocp-Apim-Subscription-Key'] = 'f0dae8696d7746a497e5102ce8a56afc'
		# Request body
		request.body = "{body}"

		response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
		    http.request(request)
		end

		raw_result = JSON.parse(response.body)
		raw_result["data"].each do |each_data|
			temp = {}
			temp["name"] = (CGI.unescapeHTML(each_data["eventname"])).html_safe
			temp["time"] = each_data["start_time_display"]
			temp["timestamp"] = each_data["start_time"]
			temp["image"] = each_data["thumb_url_large"]
			temp["url"] = each_data["share_url"]
			temp["publisher"] = "allevents.in"
			temp["location"] = each_data["location"]
			temp["facebook_location"] = URI.encode("https://m.facebook.com/search/pages/?q="+each_data["location"])
			result.push(temp)
		end

		render_to_json({	
		  event: result
	    })
	end

	def region_menu
		render_to_json({	     
	      menu: Rails.application.config.localpicks_region_menu,
	    })
	end

	def region_menu_index
		render_to_json({	     
	      menu: Rails.application.config.region_menu_ios,
	    })
	end

	def category_menu
		render_to_json({	     
	      category: Rails.application.config.localpicks_category_color,
	    })
	end

	def now_menu
		render_to_json({	     
	      category: Rails.application.config.localpicks_nearby_menu,
	    })
	end

	def get_city_name
		begin
			lat = params[:lat].to_s
			long = params[:long].to_s
			center = lat+','+long

			if lat.blank? or lat.to_i <= 0 or long.blank?  or long.to_i <= 0 then
				city_name = "Local Picks"
			else
				results = FbGraph::Place.search('city', :distance => 10000, :center => center , :access_token => '739891342809062|fff51d97a9a283264f56c05f658cf8b1')
				results.each do |result|
					if result.category.downcase == "city" then
						city_name = result.raw_attributes["location"]["city"]+' Picks'
					end
				end
			end
			render_to_json({	     
		      city_name: city_name,
		      twitter_link: URI.encode('https://mobile.twitter.com/search?q='+city_name)
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def get_sidebar_menu
		begin
			sidebar_menu = {:error => "No Menu"}
			region = "en"

			if params.key?(:region) and params[:region] != "" then
				region = params[:region].to_s
			end

			if region == "en" then
				sidebar_menu = Rails.application.config.localpicks_sidebar
			elsif region == "th" then
				sidebar_menu = Rails.application.config.localpicks_th_sidebar
			elsif region == "jp" then
				sidebar_menu = Rails.application.config.localpicks_jp_sidebar
			end
				
			render_to_json({	     
		      sidebar_menu: sidebar_menu
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def find_city_id(city_name)
		city_id = 0

		if !city_name.to_s.blank? then
			Rails.application.config.localpick_city_map.each do |id, name|
				if city_name.to_s.downcase.gsub(/\s+/, "") == name.downcase.gsub(/\s+/, "") then
					city_id = id
				end
			end
		end

		city_id
	end

	def get_news
		begin
			city_name = "Local"
			city_name_original = "Local"
			city_id = 0
			region = "en"

			if params.key?(:city_name) and params[:city_name] != "" then
				city_name = params[:city_name].to_s
			end

			if params.key?(:city_name_original) and params[:city_name_original] != "" then
				city_name_original = params[:city_name_original].to_s
			end

			if city_name != "Local" and city_name_original == "Local" then
				city_name_original = city_name
			end

			if params.key?(:region) and params[:region] != "" then
				region = params[:region].to_s
			end

			lang = " lang:en"
			lang_code = "en"
			if region == "jp" then
				lang = " lang:ja"
				lang_code = "ja"
			elsif region == "en" then
				lang = " lang:en"
				lang_code = "en"
			elsif region == "th" then
				lang = " lang:th"
				lang_code = "th"
			end

			city_id  = find_city_id(city_name)
			newspicks = ""
			news_link = {}
			news_link["google_link"] = URI.encode('https://www.google.co.th/search?hl='+lang_code+'&tbm=nws&authuser=0&q='+city_name_original+"&oq="+city_name_original+"&sourceid=chrome&ie=UTF-8#q="+city_name_original+"&tbs=qdr:y,sbd:1")
			where = {
		      :region => region,
		      :deleted_at => nil,
		      :publish_status => 2,
		      :news_staff_checked =>1,
		      :show_in_list => 1,   
		    }
		    where[:category_id] = params[:category_id].to_i if params[:category_id].to_i > 0 and params[:category_id].to_i != 999
		    where[:hot_news] = 1 if params[:category_id].to_i == 999	

		    city_condition = [ 0, '0']
		    city_condition.push(city_id.to_i)
		    newspicks = NewsPick.where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc').limit(60)
		    	
		    result = []	
			newspicks.each do |item|
				result.push(create_news_object(item, lang_code))
			end	
			newspicks = result
			
			render_to_json({
			  city_id: city_id,
		      news_link: news_link,
		      newspick: newspicks,
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def get_related_news
		begin
			region = params[:region]
			id = params[:id]
			where = {
		      :region => region,	      
		      :deleted_at => nil,
		      :publish_status => 2,
		      :news_staff_checked =>1	     
		    }

		   	get_news = NewsPick.where({id: id}).first
		   	is_hot_news = get_news.hot_news
		   	#exclude_cat = [14]
		   	#exclude_job_cat = ['category_id NOT IN (?)', exclude_cat]
		   	#if get_news.category_id.to_i == 14 then
		   	#	exclude_job_cat = ""
		   	#end

		   	lang = " lang:en"
			lang_code = "en"
			if region == "jp" then
				lang = " lang:ja"
				lang_code = "ja"
			elsif region == "en" then
				lang = " lang:en"
				lang_code = "en"
			elsif region == "th" then
				lang = " lang:th"
				lang_code = "th"
			end


		    city_condition = [ 0, '0']
		    if params.key?(:city_id) and params[:city_id].to_i > 0 then
		    	city_condition.push(params[:city_id].to_i)
		    end

		    tags = NewsPick.where({id: id}).select('tags').first
		    tags_array_result = []
		    if !tags.nil? && tags.tags != nil then
		    	tags_array_result = tags.tags.split(',').map(&:strip)
		    	if !get_news.news_pick_shop.blank? then
		    		tags_array_result.push(get_news.news_pick_shop)
		    	end
		    end

		    newspicks = NewsPick.where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).where.not(:id => id).order('created_at desc')
		    result = []
		    temp_id = []

		    if !tags.nil? && tags.tags != nil then
			    newspicks.each do |item|
			    	if item.tags != nil && item.tags != "" then
				    	tag_array = item.tags.split(',')			    		    	
				    	if tag_array.length > 0 then			    		
							tag_array.each do |tag|											
								if tags_array_result.include?(tag.strip)  then
									if !temp_id.include?(item.id) then	
										result.push(item)
									end
									temp_id.push(item.id)								
								end					
							end				
						end
					end
				end
			end

			related_result = []	
			result.each do |item|	
				related_result.push(create_news_object(item, lang_code))
			end	

			render_to_json({
		      newspick: related_result  
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def device_token	
		if params.key?(:device_type) and (params[:device_type] == 'ios' or params[:device_type] == 'android') then
			device_type = params[:device_type]
		else
			render_to_error_json(['No Device Type'], {:status => :bad_request}) and return
		end

		if params.key?(:device_token) then
			device_token = params[:device_token]
		else
			render_to_error_json(['No Device Token'], {:status => :bad_request}) and return
		end

		if params.key?(:device_notification) then
			device_notification = params[:device_notification]
		else
			render_to_error_json(['No Device Notification'], {:status => :bad_request}) and return
		end

		unique_device_id = 0;
		if params.key?(:unique_device_id) then
			unique_device_id = params[:unique_device_id]
		end


		if  params[:device_type] == 'android' then
			result = LocalPicksDeviceToken.register_android(device_type, device_token, device_notification, unique_device_id)
		else
			result = LocalPicksDeviceToken.register(device_type, device_token, device_notification, unique_device_id)
		end;
		
		device_profile = result

		if result then
		device_profile = LocalPicksDeviceToken.where({
					    	device_token: device_token, 
					    	device_type: device_type,
					    	unique_device_id: unique_device_id 
					    }).first
		end

		render_to_json({	    
		  profile: device_profile
	     
	    })
	end

	def top_menu_link
		begin
			lang = " lang:en"
			lang_code = "en"
			if params[:region] == "jp" then
				lang = " lang:ja"
				lang_code = "ja"
			elsif params[:region] == "en" then
				lang = " lang:en"
				lang_code = "en"
			elsif params[:region] == "th" then
				lang = " lang:th"
				lang_code = "th"
			end

			city_name = "Local"
			city_name_original = "Local"

			if params.key?(:city_name) and params[:city_name] != "" then
				city_name = params[:city_name].to_s
			end

			if params.key?(:city_name_original) and params[:city_name_original] != "" then
				city_name_original = params[:city_name_original].to_s
			end

			if city_name != "Local" and city_name_original == "Local" then
				city_name_original = city_name
			end

			randorm_word = random_string(params[:region])

			current_month_year = Time.now.year.to_s+"-"+Time.now.month.to_s
			link_temp = {}
			link_temp["twitter_link"] = URI.encode('https://mobile.twitter.com/search?q='+city_name_original+"+"+lang)
			link_temp["facebook_link"] = URI.encode('https://m.facebook.com/search/pages/?q='+city_name+" "+randorm_word)

			render_to_json({
				top_link: link_temp
		    })
		
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def random_string(region)
		result = ""

		if region == "jp" then
		else
			result = ['ก','ภ','ถ','ค','ต','จ','ข','ช','ฎ','พ','ธ','ร','ณ','น','ย','ญ','บ','ฐ','ล','ฟ','ฤ','ห','ก','ด','ษ','ส','ศ','ว','ง','ผ','ป','ฉ','อ','ท','ม','ฝ'].sample
		end

		result
	end

	def side_menu_link
		begin
			search_word_search = ""
			search_word_fb = ""
			if  params[:search_word] != "" then
				search_word_search = params[:search_word]
			end

			if  params[:search_word_fb] != "" then
				search_word_fb = params[:search_word_fb]
			end

			if search_word_fb == "" and search_word_search != "" then
				search_word_fb = search_word_search
			end

			post_word = ""
			if search_word_search == "best" or search_word_search == "ベスト" then
				post_word = "-escort"
			end

			lang = " lang:en"
			lang_code = "en"
			if params[:region] == "jp" then
				lang = " lang:ja"
				lang_code = "ja"
			elsif params[:region] == "en" then
				lang = " lang:en"
				lang_code = "en"
			elsif params[:region] == "th" then
				lang = " lang:th"
				lang_code = "th"
			end

			city_name = ""
			city_name_original = ""

			if params.key?(:city_name) and params[:city_name] != "" then
				city_name = params[:city_name].to_s
			end

			if params.key?(:city_name_original) and params[:city_name_original] != "" then
				city_name_original = params[:city_name_original].to_s
			end

			if city_name != "" and city_name_original == "" then
				city_name_original = city_name
			end

			link_temp = {}
			current_month_year = Time.now.year.to_s+"-"+Time.now.month.to_s
			link_temp["twitter_link"] = URI.encode('https://mobile.twitter.com/search?q='+search_word_search+"+"+city_name_original+"+"+lang+"+"+post_word)
			link_temp["google_link"] = URI.encode('https://www.google.co.th/search?hl='+lang_code+'&tbm=nws&authuser=0&q='+search_word_search+"+"+city_name_original+"&oq="+search_word_search+"+"+city_name_original+"&sourceid=chrome&ie=UTF-8#q="+search_word_search+"+"+city_name_original+"&tbs=qdr:y,sbd:1")
			link_temp["facebook_link"] = URI.encode('https://m.facebook.com/search/pages/?q='+search_word_fb+" "+city_name)

			render_to_json({
				link_data: link_temp
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def find_nearby_four
		begin
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end 

	def find_nearby_google
		begin
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end


	def find_nearby
		begin
			search_word = []
			if params[:search_word] == "all" or params[:search_word] == "" then
				search_word = ['restaurant', 'bar', 'beauty', 'cafe', 'museum', 'hotel', 'shopping', 'club', 'event', 'live', 'nightlife', 'market', 'art']
				#search_word = ['restaurant', 'bar', 'cafe', 'hotel', 'shopping', 'club', 'event', 'live']
			elsif params[:search_word] != "all" and params[:search_word] != "" then
				if params[:search_word] == 'restaurant' then
					search_word = ["restaurant","italian","bistro","steak","ramen","sushi"]
				elsif params[:search_word] == 'western' then
					search_word = ["steak","bistro","italian restaurant","tapas"]
				elsif params[:search_word] == 'asian' then
					search_word = ["thai restaurant","korean restaurant","chinese restaurant", "greek restaurant"]
				elsif params[:search_word] == 'japanese' then
					search_word = ["sushi","ramen","japanese restaurant"]
				elsif params[:search_word] == 'more' then
					search_word = ["restaurant","food court","burger"]
				elsif params[:search_word] == 'bar' or  params[:search_word] == 'wine' then
					search_word = ["bar","wine"]
				elsif params[:search_word] == 'shopping' then
					search_word = ["shopping","vintage"]
				elsif params[:search_word] == 'live' then
					search_word = ["live","concert"]
				elsif params[:search_word] == 'beauty' then
					search_word = ["beauty", "clinic"]
				else
					search_word.push(params[:search_word])
				end
			end

			city_name = ""
			city_name_original = ""

			if params.key?(:city_name) and params[:city_name] != "" then
				city_name = params[:city_name].to_s
			end

			if params.key?(:city_name_original) and params[:city_name_original] != "" then
				city_name_original = params[:city_name_original].to_s
			end

			if city_name != "" and city_name_original == "" then
				city_name_original = city_name
			end

			filter_result = []
			restaurant_name = ""
			lat = params[:lat].to_s
			long = params[:long].to_s

			if lat.blank? or lat.to_i <= 0 or long.blank?  or long.to_i <= 0 then
				if params[:city_id].to_i == 1 then
					lat = "13.7563309".to_s
					long = "100.5017651".to_s
				end
			end

			area_id = 0
			if !params[:area_id].blank? then
				area_id = params[:area_id].to_i
			end

			lang = " lang:en"
			lang_code = "en"
			if params[:region] == "jp" then
				lang = " lang:ja"
				lang_code = "ja"
			elsif params[:region] == "en" then
				lang = " lang:en"
				lang_code = "en"
			elsif params[:region] == "th" then
				lang = " lang:th"
				lang_code = "th"
			end

			center = lat+','+long
			twitter_temp = {}
			current_month_year = Time.now.year.to_s+"-"+Time.now.month.to_s
			twitter_temp["name"] = city_name_original+" "+params[:place_name]+" Now"
			twitter_temp["image"] = "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png"
			twitter_temp["region"] = params[:region]
			twitter_temp["city_id"] = find_city_id(city_name)
			twitter_temp["twitter_link"] = URI.encode('https://mobile.twitter.com/search?q='+city_name_original+"+"+params[:place_name]+"+"+lang)
			twitter_temp["twitter_geo_link"] = URI.encode('https://mobile.twitter.com/search?q=geocode:'+center+",1km"+lang)
			twitter_temp["google_link"] = URI.encode('https://www.google.com/search?q='+params[:place_name]+"+"+city_name_original+"&oq="+params[:place_name]+"+"+city_name_original+"&sourceid=chrome&ie=UTF-8#q="+params[:place_name]+"+"+city_name_original+"&tbs=qdr:y,sbd:1")
			twitter_temp["facebook_link"] = URI.encode('https://m.facebook.com/search/pages/?q='+params[:place_name]+" "+city_name)
		
			search_word.each do |word|
				result = FbGraph::Place.search(word, :distance => 10000, :center => center , :access_token => '1732227407033183|390a7ab39fe967ec1d6ff76b6ef0794d')
				result.each do |restaurant|
					if restaurant.category.downcase != "city" and restaurant.category.downcase != "university"  then
						temp = {}
						#temp["facebook_id"] = restaurant.identifier
						temp["name"] = restaurant.name
						temp["link"] = restaurant.link
						#temp["link"] = 'https://m.facebook.com/'+restaurant.identifier
						temp["category"] = restaurant.category
						temp["image"] = "https://graph.facebook.com/v2.6/"+restaurant.identifier+"/picture?width=200"
						temp["twitter_link"] = URI.encode('https://mobile.twitter.com/search?q='+city_name+" "+restaurant.name.downcase)
						temp["instagram_link"] = 'https://www.instagram.com/explore/locations/'+restaurant.identifier
						temp["google_link"] = 'https://www.google.com/search?q='+CGI.escape(restaurant.name.downcase)+'&oq='+CGI.escape(restaurant.name.downcase)
						#FacebookPageDatum.register(restaurant.identifier, temp["name"], temp['link'], temp['image'], restaurant.category, temp["twitter_link"], temp["instagram_link"], temp["google_link"])	
						filter_result.push(temp)
					end
				end
			end

			render_to_json({
				twitter_data: twitter_temp,
		     	fb_data: filter_result.uniq
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end
end
