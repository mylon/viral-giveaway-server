class Api::V1::Apps::BaseController < Api::V1::BaseController
  before_action :authenticate_app_api

  def authenticate_app_api
    @app = App.where({code: params[:app_code].to_s}).first
    unless @app then
      render_to_error_json( [I18n.t('error.authorization.app_invalid')], { :status => 400 } ) and return
    end
    Thread.current[:app] = @app
  end

end
