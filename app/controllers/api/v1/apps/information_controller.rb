class Api::V1::Apps::InformationController < Api::V1::Apps::BaseController
  def list 
    custom_where = 'now() >= published_at'
    custom_params = []
    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end

    information = Information.where({
      deleted_at: nil,
      app_id: @app.id,
    }).where([custom_where, *custom_where]).
    order('published_at desc').
    limit(Rails.application.config.information_load_num).
    map{|i| Information.convert(i, @user)}.select{|i| i.present?}
    render_to_json({
      limit: Rails.application.config.information_load_num,
      information: information,
    })
  end

  def view
    if errors = Information.validate_view(params) and errors.length > 0 then
      render_to_error_json(errors, {:status => :bad_request}) and return
    end

    viewed = nil
    begin
      Information.transaction do
        viewed = Information.find(params[:information_id].to_i).viewed(@user)
      end
      VGLogger.post({ path: '/v1/information/view' }) if viewed
    rescue
      render_to_error_json(['ERROR Please retry'], {:status => :bad_request}) and return
    end

    render_to_json({
      id: viewed.id,
      information_id: viewed.information_id,
      user_id: viewed.user.account_id,
    })
  end

  def count
    information = Information.where({
      deleted_at: nil,
      app_id: @app.id,
    }).where(['? >= published_at', Time.now]).
    order('published_at desc').
    limit(Rails.application.config.information_load_num).
    map{|i| Information.convert(i, @user)}.select{|i| i.present? and not i[:viewed]}

    render_to_json({
      :unread_count => information.length,
    })
  end
end
