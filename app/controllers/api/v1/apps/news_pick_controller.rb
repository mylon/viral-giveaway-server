class Api::V1::Apps::NewsPickController < Api::V1::Apps::BaseController
	before_action :authenticate_api, except: [:rsstoplist, :rssnews, :toplist, :operators, :find_nearby_test, :get_related_twitter, :nearby_menu, :classified_menu, :nearby_news, :find_nearby, :like_comment, :unlike_comment, :get_comment_profile, :add_comment, :get_comment, :edit_comment, :remove_comment, :add_bookmark, :remove_bookmark, :get_bookmark ,:test_push, :list_android, :news_cateogry, :auto_tags, :xml_test,:list, :tags, :view, :category, :city, :category_data, :hot_news, :get_related_news, :device_token, :people_news, :promotions_news, :events_news, :classified_news, :news_web]
	before_action :authenticate_app_api, except: [:test_push]
	
	require 'uri'

	def operators
		operators = Operator.where({deleted_at: nil})
		render_to_json({
		    data: operators
		})
	end

	def auto_tags
		where = {
	      :deleted_at => nil,
	      :publish_status => 2       
	    }

	    offset = params[:offset]
	    
	    newspicks = NewsPick.where(where).order('id asc').limit(2000).offset(offset)
	    news_id_only = []
	    news_id_array = []
	    if !newspicks.blank? then
		    newspicks.each do |news|
		      tags_array = []
		      news_tag_array = []
		      array_tag_no = ""
		      result_search_tag = ""
		      combine_final_array = []
		     if !news.news_pick_shop.blank? then
	            news_tag_array.push(news.news_pick_shop)
	        end
		      

		      news_id_only.push(news.id)

		      #if news.tags then
		      #  old_tags = news.tags.split(/,/)
		      #end

		     
		      region_no = ""
		      if news.region == 'th' then
		        region_no = 1
		      elsif news.region == 'jp' then
		        region_no = 2
		      elsif news.region == 'en' then
		        region_no = 3
		      end

		      city_condition = [ 0, '0']
		      if news.city_id.to_i > 0 then
		        city_condition.push(news.city_id.to_i)
		      end

		      category_condition = [ 0, '0']
		      if news.category_id.to_i > 0 then
		        category_condition.push(news.category_id.to_i)
		      end
		 
		      begin
		        news_update = nil
		        tags_array = NewsTag.where({region: region_no}).where("city_id IS NULL OR city_id IN (?)", city_condition).where("category_id IS NULL OR category_id IN (?)", category_condition)
		        tags_array.each do |each_tag|
		        if news.title.include?(each_tag.tag) then
		                if !news_tag_array.include?(each_tag.tag) then
		                  news_tag_array.push(each_tag.tag)
		                end
		                if !each_tag.tags_group.blank? then
		                    group_tags = each_tag.tags_group.split(',')
		                    group_tags.each do |each_group_tag|
		                        if !news_tag_array.include?(each_group_tag) then
		                          news_tag_array.push(each_group_tag)
		                        end
		                    end
		                end
		          end
		        end
		        #merge two array test
		        #combine_final_array = old_tags | news_tag
		        if !news_tag_array.blank? then
		          result_search_tag = news_tag_array.join(",")
		        end
		      
		        if !result_search_tag.blank? then
		        	  news_id_array.push([news.id, result_search_tag])
			        NewsPick.transaction do
			            news_update = NewsPick.find_by(id: news.id) 
			            news_update.tags = result_search_tag
			            news_update.updated_at = Time.now
			            news_update.news_staff_checked = 1
			            news_update.save!
			        end
			    end
		        render_to_json({
			      update: "ok",
			      news_id_only: news_id_only,
			      news_id_array: news_id_array
			    })
		      rescue => e
		        log_error(e)
		      end
		    end
		else
			render_to_json({
		      update: "done"
		    })
		end
	end

	def create_news_object(item)

		bookmark_count = 0
		bookmark_count = NewsBookmark.where(:news_id => item.id).count
		comment_count = 0
		comment_count =  Comment.where(:news_id => item.id).count

		temp = {}
		temp["id"] = item.id
		temp["title"] =item.title
		temp["category_id"] =item.category_id
		temp["region"] =item.region
		temp["news_url"] = URI.encode(item.news_url)
		temp["image_url"] = URI.encode(item.image_url)
		temp["city_id"] =item.city_id
		temp["publish_status"] =item.publish_status
		temp["deleted_at"] =item.deleted_at
		temp["created_at"] =item.created_at			
		temp["updated_at"] =item.updated_at
		temp["tags"] =item.tags
		temp["hot_news"] =item.hot_news
		temp["publishers"] =item.publishers
		temp["web_url"] = item.web_url
		temp["bookmark"] = false
		#temp["news_description"] =item.news_description
		temp["news_event"] =item.news_event
		temp["news_promotion"] =item.news_promotion
		temp["classified_sub_category"] =item.classified_sub_category
		temp["news_pick_shop"] =item.news_pick_shop
		temp["show_in_list"] =item.show_in_list
		temp["is_magazine"] =item.is_magazine
		temp["city_name"] = 'all'
		temp["bookmark_count"] = bookmark_count
		temp["comment_count"] = comment_count
		temp["twitter_link"] = "https://mobile.twitter.com/search?q="
		if item.news_pick_shop.blank? then
			#search_text_split = item.title.downcase.split(' ')
			#search_text = search_text_split[0]
	    	#if search_text_split.length > 1 then
	    	#	search_text = search_text + " " + search_text_split[1]
	    	#end
			#temp["twitter_link"] = URI.encode('https://mobile.twitter.com/search?q='+search_text)
		else
			temp["twitter_link"] = 'https://mobile.twitter.com/search?q='+CGI.escape(URI.encode(item.news_pick_shop))
		end
		Rails.application.config.newspick_city_map.each do |k, v|
			if item.city_id == k then
				temp["city_name"] = v
			end
		end
		if temp["image_url"] == "" then
			Rails.application.config.newspick_category_map.each do |k, v|
				if item.category_id.to_i == k.to_i then
					random_number = rand(1..5)
					temp["image_url"] = Rails.application.config.news_app_s3_bucket_url+'news_cat_square/'+v.to_s+'_'+random_number.to_s+'.jpg'
				end
			end
		end

		temp

	end

	def web_news_object(item)
		temp = {}
		bookmark_count = 0;
		bookmark_count = NewsBookmark.where(:news_id => item.id).count
		comment_count = 0
		comment_count =  Comment.where(:news_id => item.id).count
		temp["id"] = item.id			
		temp["title"] =item.title
		temp["category_id"] =item.category_id
		temp["region"] =item.region
		temp["news_url"] =item.news_url
		temp["image_url"] = URI.encode(item.image_url)
		temp["city_id"] =item.city_id
		temp["publish_status"] =item.publish_status
		temp["deleted_at"] =item.deleted_at
		temp["created_at"] =item.created_at			
		temp["updated_at"] =item.updated_at
		temp["tags"] =item.tags
		temp["hot_news"] =item.hot_news
		temp["publishers"] =item.publishers
		temp["web_url"] = item.web_url
		temp["bookmark"] = true
		temp["news_description"] =item.news_description
		temp["news_event"] =item.news_event
		temp["news_promotion"] =item.news_promotion
		temp["classified_sub_category"] =item.classified_sub_category
		temp["news_pick_shop"] =item.news_pick_shop
		temp["show_in_list"] =item.show_in_list
		temp["is_magazine"] =item.is_magazine
		temp["city_name"] = 'all'
		temp["facebook"] = 'https://www.local-picks.com'
		temp["twitter"] = 'https://www.local-picks.com'
		temp['city_logo'] = ''
		temp['category_name'] = ''
		temp["bookmark_count"] = bookmark_count
		temp["comment_count"] = comment_count
		Rails.application.config.newspick_city_map.each do |k, v|
			if item.city_id == k then
				temp["city_name"] = v
			end
		end

		city_facebook_url = Rails.application.config.city_facebook_url
		city_twitter_url = Rails.application.config.city_twitter_url
		if item.region.to_s == "en" then
			city_facebook_url = Rails.application.config.city_facebook_en_url
			city_twitter_url = Rails.application.config.city_twitter_en_url
		elsif item.region.to_s == "th" then
			city_facebook_url = Rails.application.config.city_facebook_th_url
			city_twitter_url = Rails.application.config.city_twitter_th_url
		end

		city_facebook_url.each do |k, v|
			if item.city_id.to_i == k.to_i then
				temp['facebook'] = v
			end
		end

		if temp['facebook'].blank? then
			if item.region.to_s == "en" then
				temp['facebook'] = 'https://www.facebook.com/Local-Picks-1699190647000819/'
			elsif item.region.to_s == "th" then
				temp['facebook'] = 'https://www.facebook.com/Local-Picks-TH-604398826404559/'
			elsif item.region.to_s == "jp" then
				temp['facebook'] = 'https://www.facebook.com/ローカルピックス-1295743873786243/'
			end
		end

		city_twitter_url.each do |k, v|
			if item.city_id.to_i == k.to_i then
				temp['twitter'] = v
			end
		end

		if temp['twitter'].blank? then
			if item.region.to_s == "en" then
				temp['twitter'] = 'https://twitter.com/localpicksen'
			elsif item.region.to_s == "th" then
				temp['twitter'] = 'https://twitter.com/localpicksth'
			elsif item.region.to_s == "jp" then
				temp['twitter'] = 'https://twitter.com/localpicksja'
			end
		end

		temp['city_logo'] = Rails.application.config.news_app_s3_bucket_url+'news_app_logo/local_picks_web_icon.png'
		Rails.application.config.news_city_logo.each do |k, v|
			if item.city_id.to_i == k.to_i then
				temp['city_logo'] = Rails.application.config.news_app_s3_bucket_url+'news_app_logo/'+v
			end
		end

		Rails.application.config.newspick_category_map.each do |k, v|
			if item.category_id.to_i == k.to_i then
				temp['category_name'] = v.to_s
			end
		end
		if temp["image_url"] == "" or temp["image_url"].include?("ameba.jp") then
			Rails.application.config.newspick_category_map.each do |k, v|
				if item.category_id.to_i == k.to_i then
					random_number = rand(1..5)
					category_name = v.to_s
					if category_name == "people" then
						category_name = "tech"
					end
					temp["image_url"] = Rails.application.config.news_app_s3_bucket_url+'news_cat_square/'+category_name+'_'+random_number.to_s+'.jpg'
				end
			end
		end

		temp
	end

	def toplist
		region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1,
	      :show_in_list => 1,   
	      :hot_news => 1,
	    }
	    city_condition = [ 0, '0']

	    newspicks = NewsPick.where(where).where("city_id IS NOT NULL OR city_id NOT IN (?)", city_condition).order('created_at desc').limit(30)	
		result = []	
		newspicks.each do |item|
			result.push(create_news_object(item))
		end	
		newspicks = result

		render_to_json({
	      newspick: newspicks
	    })

	end

	def rsstoplist
		region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1,
	      :show_in_list => 1,   
	      :hot_news => 1,
	    }
	    city_condition = [ 0, '0']
	    limit_news =99999
		if params.key?(:limit) and params[:limit].to_i > 0 then
			limit_news = params[:limit]
	    end	

	    newspicks = NewsPick.where(where).where("city_id IS NOT NULL OR city_id NOT IN (?)", city_condition).order('created_at desc').limit(limit_news)	
		result = []	
		newspicks.each do |item|
			result.push(create_news_object(item))
		end	
		newspicks = result

		render_to_json({
	      newspick: newspicks
	    })

	end

	def rssnews
		region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1,
	      :show_in_list => 1,   
	    }
		where[:category_id] = params[:category_id].to_i if params[:category_id].to_i > 0	
		where[:hot_news] = params[:hot_news].to_i if params[:hot_news].to_i > 0		
		
		limit_news =99999
		if params.key?(:limit) and params[:limit].to_i > 0 then
			limit_news = params[:limit]
	    end	

		custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end
	    
	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	where[:city_id] = params[:city_id].to_i if params[:city_id].to_i > 0
	    	count_newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NOT NULL OR city_id NOT IN (?)", city_condition).order('created_at desc').count
	    	newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NOT NULL OR city_id NOT IN (?)", city_condition).order('created_at desc').limit(limit_news)	
	    else
	    	count_newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc').count
	   		newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc').limit(limit_news)	
	    end
	   
		result = []	
		newspicks.each do |item|
			result.push(create_news_object(item))
		end	
		newspicks = result	

		render_to_json({
	      newspick: newspicks
	    })
	end

	def list
		region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1,
	      :show_in_list => 1,   
	    }
		#where[:city_id] = params[:city_id].to_i if params[:city_id].to_i > 0

		where[:category_id] = params[:category_id].to_i if params[:category_id].to_i > 0	
		where[:hot_news] = params[:hot_news].to_i if params[:hot_news].to_i > 0		

		limit_news =99999
		if params.key?(:limit) and params[:limit].to_i > 0 then
			limit_news = params[:limit]
	    end	

		custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end

	    count_newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc').count

		newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc').limit(limit_news)	
		result = []	
		newspicks.each do |item|
			result.push(create_news_object(item))
		end	
		newspicks = result	
		

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

		render_to_json({
	      limit: limit_news,
	      city: city_condition,
	      newscount: count_newspicks,
	      newspick: newspicks
	    })
	end

	def list_android

		region = params[:region]

		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1,
	      :show_in_list => 1,   	     
	    }
		#where[:city_id] = params[:city_id].to_i if params[:city_id].to_i > 0

		where[:category_id] = params[:category_id].to_i if params[:category_id].to_i > 0	
		where[:hot_news] = params[:hot_news].to_i if params[:hot_news].to_i > 0		

		limit =99999
		if params.key?(:limit) and params[:limit].to_i > 0 then
			limit = params[:limit]
	    end	
	    offset = 0
	    if params.key?(:offset) and params[:offset].to_i > 0 then
			offset = params[:offset]
	    end	

		custom_where = ''
    	custom_params = []

    	if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end

		newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc').limit(limit).offset(offset)	
		result = []	
		newspicks.each do |item|			 
			result.push(create_news_object(item))
		end	
		newspicks = result	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

		render_to_json({
	      limit: limit,
	      offset: offset,
	      city: city_condition,
	      newspick: newspicks,
	    })
	end

	def news_cateogry
		begin
		region = params[:region]
		where = {
	      :region => region,	      
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1,
	      :show_in_list => 1,   	     
	    }
	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end

	    newspicks = NewsPick.where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')		
	    result = []	
		newspicks.each do |item|			 
			result.push(create_news_object(item))
		end	
		newspicks = result	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end	

		hot_news = []
		hot_news_full = []
		bookmark = []
		bookmark_full = []
		local = []
		local_full = []
		gourmet = []
		gourmet_full = []
		fashion = []
		fashion_full = []
		beauty = []
		beauty_full = []
		travel = []
		travel_full = []
		sport = []
		sport_full = []
		entertainment = []
		entertainment_full = []
		lifestyle= []
		lifestyle_full = []
		learning = []
		learning_full = []
		job = []
		job_full = []
		spot = []
		spot_full = []
		calssified = []
		calssified_full = []
		people = []
		people_full = []
		usualday = []
		usualday_full = []
		newspicks.each do |news|
			if news['hot_news'].to_i == 1 and hot_news.length < 20 then
				hot_news.push(news)
			end
			if news['hot_news'].to_i == 1 then
				hot_news_full.push(news)
			end
			if news['category_id'].to_i == 1 and local.length < 20 then
				local.push(news)
			end
			if news['category_id'].to_i == 2 and gourmet.length < 20 then
				gourmet.push(news)
			end
			if news['category_id'].to_i == 3 and fashion.length < 20 then
				fashion.push(news)
			end
			if news['category_id'].to_i == 4 and beauty.length < 20 then
				beauty.push(news)
			end
			if news['category_id'].to_i == 6 and travel.length < 20 then
				travel.push(news)
			end
			if news['category_id'].to_i == 9 and sport.length < 20 then
				sport.push(news)
			end
			if news['category_id'].to_i == 10 and entertainment.length < 20 then
				entertainment.push(news)
			end
			if news['category_id'].to_i == 12 and lifestyle.length < 20 then
				lifestyle.push(news)
			end
			if news['category_id'].to_i == 13 and learning.length < 20 then
				learning.push(news)
			end
			if news['category_id'].to_i == 14 and job.length < 20 then
				job.push(news)
			end
			if news['category_id'].to_i == 15 and spot.length < 20 then
				spot.push(news)
			end
			if news['category_id'].to_i == 16 and usualday.length < 20 then
				usualday.push(news)
			end
			if news['category_id'].to_i == 99 and people.length < 20 then
				people.push(news)
			end

			if news['category_id'].to_i == 1 then
				local_full.push(news)
			end
			if news['category_id'].to_i == 2  then
				gourmet_full.push(news)
			end
			if news['category_id'] == 3 then
				fashion_full.push(news)
			end
			if news['category_id'].to_i == 4  then
				beauty_full.push(news)
			end
			if news['category_id'].to_i == 6 then
				travel_full.push(news)
			end
			if news['category_id'].to_i == 9  then
				sport_full.push(news)
			end
			if news['category_id'].to_i == 10  then
				entertainment_full.push(news)
			end
			if news['category_id'].to_i == 12 then
				lifestyle_full.push(news)
			end
			if news['category_id'].to_i == 13  then
				learning_full.push(news)
			end
			if news['category_id'].to_i == 14 then
				job_full.push(news)
			end
			if news['category_id'].to_i == 15 then
				spot_full.push(news)
			end
			if news['category_id'].to_i == 16  then
				usualday_full.push(news)
			end
			if news['category_id'].to_i == 99  then
				people_full.push(news)
			end
		end

		render_to_json({
	      top: hot_news,
	      top_length: hot_news_full.length,
	      matome: job,
	      matome_length: job_full.length,
	      news: local,
	      news_length: local_full.length,
	      gourmet: gourmet,
	      gourmet_length: gourmet_full.length,
	      spot: spot,
	      spot_length: spot_full.length,
	      fashion: fashion,
	      fashion_length: fashion_full.length,
	      beauty: beauty,
	      beauty_length: beauty_full.length,
	      tech: people,
	      tech_length: people_full.length,
	      travel: travel,
	      travel_length: travel_full.length,
	      sport: sport,
	      sport_length: sport_full.length,
	      entertainment: entertainment,
	      entertainment_length: entertainment_full.length,
	      life_style: lifestyle,
	      life_style_length: lifestyle_full.length,
	      learning: learning,
	      learning_length: learning_full.length,
	      usualday: usualday,
	      usualday_length: usualday_full.length,
	      #newspick: newspicks,
	    })
	    rescue Exception => e
	      log_error e
	      render_to_error_json(newspicks, { :status => :bad_request}) and return	    
		end

	end

	def get_related_news
		region = params[:region]
		id = params[:id]
		where = {
	      :region => region,	      
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1	     
	    }

	    where[:news_event] = params[:news_event].to_i if params[:news_event].to_i > 0
		where[:news_promotion] = params[:news_promotion].to_i if params[:news_promotion].to_i > 0

	   	get_news = NewsPick.where({id: id}).first
	   	is_hot_news = get_news.hot_news
	   	exclude_cat = [14]
	   	exclude_job_cat = ['category_id NOT IN (?)', exclude_cat]
	   	if get_news.category_id.to_i == 14 then
	   		exclude_job_cat = ""
	   	end

	   	#if is_hot_news.to_i == 0 then
	    #	where[:category_id] = params[:category_id].to_i if params[:category_id].to_i > 0
	    #end

	    #if is_hot_news.to_i == 1 then
	    #	exclude_cat = [14,97]
	    #	exclude_hot_cat = ['category_id NOT IN (?)', exclude_cat]
	    #end

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end

	    tags = NewsPick.where({id: id}).select('tags').first
	    tags_array_result = []
	    if !tags.nil? && tags.tags != nil then
	    	tags_array_result = tags.tags.split(',').map(&:strip)
	    	if !get_news.news_pick_shop.blank? then
	    		tags_array_result.push(get_news.news_pick_shop)
	    	end
	    end

	    newspicks = NewsPick.where(where).where(exclude_job_cat).where("city_id IS NULL OR city_id IN (?)", city_condition).where.not(:id => id).order('created_at desc')
	    result = []
	    temp_id = []

	    if !tags.nil? && tags.tags != nil then
		    newspicks.each do |item|
		    	if item.tags != nil && item.tags != "" then
			    	tag_array = item.tags.split(',')			    		    	
			    	if tag_array.length > 0 then			    		
						tag_array.each do |tag|											
							if tags_array_result.include?(tag.strip)  then
								if !temp_id.include?(item.id) then	
									result.push(item)
								end
								temp_id.push(item.id)								
							end					
						end				
					end
				end
			end
		end

		bookmark_result = []	
		result.each do |item|	
			bookmark_result.push(create_news_object(item))
		end	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')				
				if bookmarks_id then
					bookmark_result.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

		render_to_json({
	      limit: 100,
	      newspick: bookmark_result  
	    })

	end

	def people_news
		region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :category_id => 99,
	      :news_staff_checked =>1
	    }
	   #where[:city_id] = params[:city_id].to_i if (params[:city_id].to_i > 0 && region != 'th')   

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end


	    newspicks = NewsPick.where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')	
		result = []	
		newspicks.each do |item|			 
			result.push(create_news_object(item))
		end	
		newspicks = result	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

	    render_to_json({
	      limit: 100,
	      newspick: newspicks,
	    })
	end

	def classified_news
		region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :category_id => 97,
	      :news_staff_checked =>1
	    }
	    #where[:city_id] = params[:city_id].to_i if (params[:city_id].to_i > 0 && region != 'th')  
	    where[:classified_sub_category] = params[:classified_sub_category].to_i if params[:classified_sub_category].to_i > 0

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end 

	    newspicks = NewsPick.where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')	
		result = []	
		newspicks.each do |item|			 
			result.push(create_news_object(item))
		end	
		newspicks = result	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where("city_id IS NULL OR city_id IN (?)", city_condition).where(where).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

	    render_to_json({
	      limit: 100,
	      newspick: newspicks,
	    })
	end

	def hot_news

	    region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :hot_news => 1,
	      :news_staff_checked =>1	      
	    }

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end 

	    #where[:city_id] = params[:city_id].to_i if (params[:city_id].to_i > 0 && region != 'th')
		custom_where = ''
    	custom_params = []

		if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

		newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')
		result = []	
		newspicks.each do |item|			 
			result.push(create_news_object(item))
		end	
		newspicks = result	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where("city_id IS NULL OR city_id IN (?)", city_condition).where(where).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

		render_to_json({
	      limit: 100,
	      newspick: newspicks,
	    })
	end

	def events_news

	    region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_event => 1,
	      :news_staff_checked =>1	      
	    }

	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end

	    #where[:city_id] = params[:city_id].to_i if (params[:city_id].to_i > 0 && region != 'th')
		custom_where = ''
    	custom_params = []

		if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

		newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')
		result = []	
		newspicks.each do |item|			 
			result.push(create_news_object(item))
		end	
		newspicks = result	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

		render_to_json({
	      limit: 100,
	      newspick: newspicks,
	    })
	end

	def promotions_news

	    region = params[:region]
		where = {
	      :region => region,
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_promotion => 1,
	      :news_staff_checked =>1	      
	    }

	    #where[:city_id] = params[:city_id].to_i if (params[:city_id].to_i > 0 && region != 'th')
	    city_condition = [ 0, '0']
	    if params.key?(:city_id) and params[:city_id].to_i > 0 then
	    	city_condition.push(params[:city_id].to_i)
	    end

		custom_where = ''
    	custom_params = []

		if params.key?(:since_id) and params[:since_id].to_i > 0 then
	      custom_where += ' AND ' if custom_where.length > 0
	      custom_where += 'id < ?'
	      custom_params.push(params[:since_id].to_i)
	    end

		newspicks = NewsPick.where(where).where([custom_where, *custom_params]).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')
		result = []	
		newspicks.each do |item|			 
			result.push(create_news_object(item))
		end	
		newspicks = result	

		if (params[:user_id]) then			
			bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
			if bookmarks then
				bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where("city_id IS NULL OR city_id IN (?)", city_condition).where(where).order('created_at desc')				
				if bookmarks_id then
					newspicks.each do |item|
						bookmarks_id.each do |bookmark|
							if item["id"] == bookmark.id then
								item["bookmark"] = true
							end
						end
					end
				end			    		    
		    end
		end

		render_to_json({
	      limit: 100,
	      newspick: newspicks,
	    })
	end

	def add_bookmark		
		begin
			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			if params.key?(:news_id) and params[:news_id].to_i > 0 then
				news_id = params[:news_id]
			else
				render_to_error_json(['No News ID'], {:status => :bad_request}) and return
			end

			newspick = NewsPick.where({id: news_id}).first
			if !newspick then
				render_to_error_json(['Cannot find news'], {:status => :bad_request}) and return
			end

			user = User.where({id: user_id}).first
			if !user then
				render_to_error_json(['Cannot find user'], {:status => :bad_request}) and return
			end

			bookmark = NewsBookmark.where({user_id: user_id, news_id: news_id}).first
		    if bookmark then
		      render_to_error_json(['User already like this news'], {:status => :bad_request}) and return
		    else
		    	NewsBookmark.register(user_id, news_id)
		    	render_to_json({ success: 'success' }) and return
		    end
		rescue => e
	      log_error e
	      render_to_error_json(['ERROR Please retry'], { :status => :bad_request}) and return	    
		end
	end

	def remove_bookmark
		begin
			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			if params.key?(:news_id) and params[:news_id].to_i > 0 then
				news_id = params[:news_id]
			else
				render_to_error_json(['No News ID'], {:status => :bad_request}) and return
			end

			newspick = NewsPick.where({id: news_id}).first
			if !newspick then
				render_to_error_json(['Cannot find news'], {:status => :bad_request}) and return
			end

			user = User.where({id: user_id}).first
			if !user then
				render_to_error_json(['Cannot find user'], {:status => :bad_request}) and return
			end

			bookmark = NewsBookmark.where({user_id: user_id, news_id: news_id}).first
		    if bookmark then
		      	bookmark.destroy
				render_to_json({ success: 'success' }) and return
		    else		    	
		    	render_to_error_json(['Cannot find the user likes record'], {:status => :bad_request}) and return
		    end			
		rescue => e
	      log_error e
	      render_to_error_json([e.message], { :status => :bad_request}) and return	    
		end
	end

	def get_bookmark
		begin
			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			user = User.where({id: user_id}).first
			if !user then
				render_to_error_json(['Cannot find user'], {:status => :bad_request}) and return
			end

			bookmarks = NewsBookmark.where({user_id: user_id}).select('news_id')
			if bookmarks then
				region = params[:region]
				where = {
			      :region => region,
			      :deleted_at => nil,
			      :publish_status => 2,
	      		  :news_staff_checked =>1	     
			    }			    
				#where[:city_id] = params[:city_id].to_i if (params[:city_id].to_i > 0 && region != 'th')	

				city_condition = [ 0, '0']
			    if params.key?(:city_id) and params[:city_id].to_i > 0 then
			    	city_condition.push(params[:city_id].to_i)
			    end		

				newspicks = NewsPick.where("id IN (?)", bookmarks).where("city_id IS NULL OR city_id IN (?)", city_condition).where(where).order('created_at desc')
				result = []	
				newspicks.each do |item|			 
					result.push(create_news_object(item))
				end	
				newspicks = result

				if (params[:user_id]) then			
					bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
					if bookmarks then
						bookmarks_id = NewsPick.where("id IN (?)", bookmarks).where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')				
						if bookmarks_id then
							newspicks.each do |item|
								bookmarks_id.each do |bookmark|
									if item["id"] == bookmark.id then
										item["bookmark"] = true
									end
								end
							end
						end			    		    
				    end
				end

		      	render_to_json({  
			      newspicks: newspicks
			    })
		    else		    	
		    	render_to_error_json(['Cannot find the user likes record'], {:status => :bad_request}) and return
		    end
			
		rescue => e
	      log_error e
	      render_to_error_json([e.message], { :status => :bad_request}) and return	    
		end
	end

	def like_comment		
		begin
			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			if params.key?(:comment_id) and params[:comment_id].to_i > 0 then
				comment_id = params[:comment_id]
			else
				render_to_error_json(['No Comment ID'], {:status => :bad_request}) and return
			end

			comment = Comment.where({id: comment_id}).first
			if !comment then
				render_to_error_json(['Cannot find comment'], {:status => :bad_request}) and return
			end

			user = User.where({id: user_id}).first
			if !user then
				render_to_error_json(['Cannot find user'], {:status => :bad_request}) and return
			end

			likecomment = CommentLike.where({user_id: user_id, comment_id: comment_id}).first
		    if likecomment then
		      render_to_error_json(['User already like this comment'], {:status => :bad_request}) and return
		    else
		    	CommentLike.register(user_id, comment_id)
		    	render_to_json({ success: 'success' }) and return
		    end
		rescue => e
	      log_error e
	      render_to_error_json(['ERROR Please retry'], { :status => :bad_request}) and return	    
		end
	end

	def unlike_comment
		begin
			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			if params.key?(:comment_id) and params[:comment_id].to_i > 0 then
				comment_id = params[:comment_id]
			else
				render_to_error_json(['No Comment ID'], {:status => :bad_request}) and return
			end

			comment = Comment.where({id: comment_id}).first
			if !comment then
				render_to_error_json(['Cannot find comment'], {:status => :bad_request}) and return
			end

			user = User.where({id: user_id}).first
			if !user then
				render_to_error_json(['Cannot find user'], {:status => :bad_request}) and return
			end

			unlikecomment = CommentLike.where({user_id: user_id, comment_id: comment_id}).first
		    if unlikecomment then
		      	unlikecomment.destroy
				render_to_json({ success: 'success' }) and return
		    else		    	
		    	render_to_error_json(['Cannot find the user likes record'], {:status => :bad_request}) and return
		    end			
		rescue => e
	      log_error e
	      render_to_error_json([e.message], { :status => :bad_request}) and return	    
		end
	end


	def get_comment_profile
		begin
			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			result = []
			combine_comment = {}
			users = User.where({id: user_id}).first
			all_comments = Comment.where({user_id: user_id}).order('id desc')

			all_comments.each do |item|	
				combine_comment = {}
				comment_count = 0;
				comment_count = CommentLike.where(:comment_id => item.id).count
				newspicks = NewsPick.where({id: item.news_id}).first
				combine_comment["news_title"] = newspicks.title
				combine_comment["news_image"] = URI.encode(newspicks.image_url)
				combine_comment["news_publishers"] = newspicks.publishers
				combine_comment["comment_id"] =  item.id
				combine_comment["comment_text"] =  item.comment_text
				combine_comment["comment_date"] =  item.created_at
				combine_comment["like_comment_count"] =  comment_count
				combine_comment["is_reply"] =  0
				if item.parent_comment_id != 0 then
					combine_comment["is_reply"] =  1
				end
				result.push(combine_comment)
			end

			render_to_json({
				user_id: users.id,
				user_provider: users.provider,
				user_uid: users.uid,
				user_name: users.name,
				user_tag_line: users.tag_line,
				comments: result
			})
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def get_comment
		begin
			if params.key?(:news_id) and params[:news_id].to_i > 0 then
				news_id = params[:news_id]
			else
				render_to_error_json(['No News ID'], {:status => :bad_request}) and return
			end

			result = []
			main_comment_with_count = []
			
			combine_comment = {}
			newspicks = NewsPick.where({id: news_id}).first
			main_comments = Comment.where({news_id: news_id, parent_comment_id: 0}).order('id asc')

			main_comments.each do |item|
				temp_main = {}
				combine_comment = {}
				reply_comment_with_count = []
				comment_count = 0;
				comment_count = CommentLike.where(:comment_id => item.id).count
				main_users = User.where({id: item.user_id}).first
				temp_main["id"] = item.id
				temp_main["user_id"] = item.user_id
				temp_main["user_provider"] = main_users.provider
				temp_main["user_uid"] = main_users.uid
				temp_main["user_name"] = main_users.name
				temp_main["user_tag_line"] = main_users.tag_line
				temp_main["news_id"] = item.news_id
				temp_main["comment_text"] = item.comment_text
				temp_main["parent_comment_id"] = item.parent_comment_id
				temp_main["like_comment_count"] = comment_count
				temp_main["created_at"] = item.created_at
				temp_main["updated_at"] = item.updated_at
				combine_comment["main_comments"] = temp_main
				reply_comment =  Comment.where({news_id: news_id, parent_comment_id: item.id}).order('id asc')
				reply_comment.each do |reply_item|
					temp_reply = {}
					reply_comment_count = 0;
					reply_comment_count = CommentLike.where(:comment_id => reply_item.id).count
					reply_users = User.where({id: reply_item.user_id}).first
					temp_reply["id"] = reply_item.id
					temp_reply["user_id"] = reply_item.user_id
					temp_reply["user_provider"] = reply_users.provider
					temp_reply["user_uid"] = reply_users.uid
					temp_reply["user_name"] = reply_users.name
					temp_reply["user_tag_line"] = reply_users.tag_line
					temp_reply["news_id"] = reply_item.news_id
					temp_reply["comment_text"] = reply_item.comment_text
					temp_reply["parent_comment_id"] = reply_item.parent_comment_id
					temp_reply["like_comment_count"] = reply_comment_count
					temp_reply["created_at"] = reply_item.created_at
					temp_reply["updated_at"] = reply_item.updated_at
					reply_comment_with_count.push(temp_reply)
				end
				combine_comment["reply_comments"] = reply_comment_with_count
				result.push(combine_comment)
			end

			render_to_json({
				id: news_id,
				title: newspicks.title,
				image: newspicks.image_url,
				publishers: newspicks.publishers,
				comments: result
			})
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def add_comment
		begin
			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			if params.key?(:news_id) and params[:news_id].to_i > 0 then
				news_id = params[:news_id]
			else
				render_to_error_json(['No News ID'], {:status => :bad_request}) and return
			end

			if params.key?(:comment_text) and params[:comment_text].to_s != "" then
				comment_text = params[:comment_text]
			else
				render_to_error_json(['No Comment'], {:status => :bad_request}) and return
			end

			if params.key?(:parent_comment_id) and params[:parent_comment_id].to_i > 0 then
				parent_comment_id = params[:parent_comment_id]
			else
				parent_comment_id = 0
			end

			Comment.register(user_id, news_id, comment_text, parent_comment_id)
		    render_to_json({ success: 'success' }) and return

		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def edit_comment
		begin
			if params.key?(:id) and params[:id].to_i > 0 then
				id = params[:id]
			else
				render_to_error_json(['No Comment ID'], {:status => :bad_request}) and return
			end

			if params.key?(:user_id) and params[:user_id].to_i > 0 then
				user_id = params[:user_id]
			else
				render_to_error_json(['No User ID'], {:status => :bad_request}) and return
			end

			if params.key?(:news_id) and params[:news_id].to_i > 0 then
				news_id = params[:news_id]
			else
				render_to_error_json(['No News ID'], {:status => :bad_request}) and return
			end

			if params.key?(:comment_text) and params[:comment_text].to_s != "" then
				comment_text = params[:comment_text]
			else
				render_to_error_json(['No Comment'], {:status => :bad_request}) and return
			end

			comments = nil
		    Comment.transaction do
		        comments = Comment.where({news_id: news_id, user_id: user_id, id: id}).first
		        comments.comment_text = params[:comment_text]
		        comments.updated_at = Time.now
		        comments.save!
		    end

	      	render_to_json({
				news_id: news_id,
				user_id: user_id,
				id: id,
				comment_text: comment_text
			})

		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def remove_comment
		if params.key?(:id) and params[:id].to_i > 0 then
			id = params[:id]
		else
			render_to_error_json(['No Comment ID'], {:status => :bad_request}) and return
		end

		if params.key?(:user_id) and params[:user_id].to_i > 0 then
			user_id = params[:user_id]
		else
			render_to_error_json(['No User ID'], {:status => :bad_request}) and return
		end
		if params.key?(:news_id) and params[:news_id].to_i > 0 then
			news_id = params[:news_id]
		else
			render_to_error_json(['No News ID'], {:status => :bad_request}) and return
		end

		comments = Comment.where({news_id: news_id, user_id: user_id, id: id}).first
	    if comments then
	      	comments.destroy!
			render_to_json({ success: 'success' }) and return
	    else		    	
	    	render_to_error_json(['Cannot find the comment record'], {:status => :bad_request}) and return
	    end			
	end

	def view		
		where = {
	      :region => params[:region],
	      :id => params[:id],
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1	     
	    }

	    tags_array = Array.new
	    newspicks = NewsPick.where(where).first

	    if !newspicks.nil? && newspicks.tags != nil then
	    	tags_array = newspicks.tags.split(',').map(&:strip)
	    end

	    render_to_json({	     
	      tags: tags_array,
	      newspick: newspicks,
	    })	    
	end	

	def tags
		where = {
	      :id => params[:id],
	      :deleted_at => nil,
	      :publish_status => 2,
	      :news_staff_checked =>1	     
	    }
	    tags_array = Array.new
	    tags = NewsPick.where(where).select('tags').first

	    if !tags.nil? && tags.tags != nil then
	   	 tags_array = tags.tags.split(',')  	
	   	end

	    render_to_json({	
	      id: params[:id],     
	      tags: tags_array,
	    })
	end

	def category
		render_to_json({	     
	      category: Rails.application.config.newspick_category_map,
	    })
	end

	def category_data
		render_to_json({	     
	      category: Rails.application.config.newspick_category_color,
	    })
	end

	def classified_menu
		render_to_json({	     
	      category: Rails.application.config.sub_classified_category_menu,
	    })
	end

	def nearby_menu
		render_to_json({	     
	      category: Rails.application.config.nearby_menu,
	    })
	end

	def city
		region = params[:region]
		city_data = nil

		if region == 'en' then
			city_data = Rails.application.config.newspick_city_map
		end
		if region == 'jp' then
			city_data = Rails.application.config.newspick_jpcity_map
		end

		render_to_json({	    
		  region: region, 
	      city: city_data,
	    })
	end

	def device_token	
		user_id = params[:user_id]

		if params.key?(:region) then
			region = params[:region]
		else
			render_to_error_json(['No Region Code'], {:status => :bad_request}) and return
		end

		city_id = 0
		if params.key?(:city_id) and params[:city_id].to_i > 0 then
			city_id = params[:city_id]
		else
			render_to_error_json(['No City ID'], {:status => :bad_request}) and return
		end
		

		if params.key?(:device_type) and (params[:device_type] == 'ios' or params[:device_type] == 'android') then
			device_type = params[:device_type]
		else
			render_to_error_json(['No Device Type'], {:status => :bad_request}) and return
		end

		if params.key?(:device_token) then
			device_token = params[:device_token]
		else
			render_to_error_json(['No Device Token'], {:status => :bad_request}) and return
		end

		if params.key?(:device_notification) then
			device_notification = params[:device_notification]
		else
			render_to_error_json(['No Device Notification'], {:status => :bad_request}) and return
		end

		unique_device_id = 0;
		if params.key?(:unique_device_id) then
			unique_device_id = params[:unique_device_id]
		end


		if  params[:device_type] == 'android' then
			result = NewsPickDeviceToken.register_android(user_id, city_id, region, device_type, device_token, device_notification, unique_device_id)
		else
			result = NewsPickDeviceToken.register(user_id, city_id, region, device_type, device_token, device_notification, unique_device_id)
		end;
		
		device_profile = result

		if result then
		device_profile = NewsPickDeviceToken.where({
					    	device_token: device_token, 
					    	city_id: city_id, 
					    	region: region, 
					    	device_type: device_type,
					    	unique_device_id: unique_device_id 
					    }).first
		end

		render_to_json({	    
		  profile: device_profile
	     
	    })
	end

	def news_web
		begin
			if params.key?(:web_url) and params[:web_url].to_s != "" then
				web_url = params[:web_url]
			else
				render_to_error_json(['No News URL'], {:status => :bad_request}) and return
			end

			newspicks = NewsPick.where(["web_url LIKE ?", "%#{web_url}%"]).where({deleted_at: nil}).order('created_at desc').first
			tags_array_result = []
		    if !newspicks.tags.nil? && newspicks.tags != nil then
		    	tags_array_result = newspicks.tags.split(',').map(&:strip)
		    	if !newspicks.news_pick_shop.blank? then
		    		tags_array_result.push(newspicks.news_pick_shop)
		    	end
		    end

			where = {
		      :region => newspicks.region,	  
		      :deleted_at => nil,
		      :publish_status => 2,
		      :news_staff_checked =>1	     
		    }


		    #exclude_cat = [14]
		   	#exclude_job_cat = ['category_id NOT IN (?)', exclude_cat]
		   	#if newspicks.category_id.to_i == 14 then
		   	#	exclude_job_cat = ""
		   	#end

		    #if newspicks.hot_news.to_i == 0 then
		    #	where[:category_id] = newspicks.category_id.to_i
		    #end
		    
		   	#exclude_hot_cat = ""
		    #if newspicks.hot_news.to_i == 1 then
		    #	exclude_cat = [14,97]
		    #	exclude_hot_cat = ['category_id NOT IN (?)', exclude_cat]
		    #end

		    city_condition = [ 0, '0']
		    if newspicks.city_id.to_i > 0 then
		    	city_condition.push(newspicks.city_id.to_i)
		    end	

			related_news = NewsPick.where(where).where("city_id IS NULL OR city_id IN (?)", city_condition).where.not(:id => newspicks.id).order('created_at desc')
		    result = []
		    temp_id = []

		    if !newspicks.tags.blank?  then
			    related_news.each do |item|
			    	if item.tags != nil && item.tags != "" then
				    	tag_array = item.tags.split(',')			    		    	
				    	if tag_array.length > 0 then			    		
							tag_array.each do |tag|											
								if tags_array_result.include?(tag.strip)  then
									if !temp_id.include?(item.id) then	
										result.push(item)
									end
									temp_id.push(item.id)								
								end					
							end				
						end
					end
				end
			end

			main_news = web_news_object(newspicks)
			related = []	
			if !result.empty? then
				result.each do |item|	
					if related.length < 17 then
						related.push(web_news_object(item))
					end
				end	
			end
			

			render_to_json({
		      main_news: main_news,
		      related_news: related
		    })

		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def get_related_twitter
		begin
			if params.key?(:region) and params[:region].to_s != "" then
				region = params[:region]
			else
				render_to_error_json(['No Region'], {:status => :bad_request}) and return
			end
			if params.key?(:city_id) and params[:city_id].to_i > 0 then
				city_id = params[:city_id].to_i
			else
				render_to_error_json(['No City ID'], {:status => :bad_request}) and return
			end

			related_list = []

			if city_id == 1 then
				if region == 'jp' then
					related_list = [
						{:area_id => 1, :lat => '13.7589374', :long => '100.4971865', :name => 'カオサン', :link => URI.encode('https://mobile.twitter.com/search?q=カオサン'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 2, :lat => '13.7339984', :long => '100.5825355', :name => 'トンロー', :link => URI.encode('https://mobile.twitter.com/search?q=トンロー'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 3, :lat => '13.7194494', :long => '100.5852771', :name => 'エカマイ', :link => URI.encode('https://mobile.twitter.com/search?q=エカマイ'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 4, :lat => '13.6910654', :long => '100.614025', :name => 'プラカノン', :link => URI.encode('https://mobile.twitter.com/search?q=プラカノン'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 5, :lat => '13.7304479', :long => '100.5697677', :name => 'プロンポン', :link => URI.encode('https://mobile.twitter.com/search?q=プロンポン'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 6, :lat => '13.7370069', :long => '100.5603929', :name => 'アソーク', :link => URI.encode('https://mobile.twitter.com/search?q=アソーク'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 7, :lat => '13.7468299', :long => '100.5349284', :name => 'サイアム', :link => URI.encode('https://mobile.twitter.com/search?q=サイアム'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 8, :lat => '13.7503809', :long => '100.542796', :name => 'プラトゥナーム', :link => URI.encode('https://mobile.twitter.com/search?q=プラトゥナーム'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 9, :lat => '13.7502255', :long => '100.4913875', :name => '王宮周辺', :link => URI.encode('https://mobile.twitter.com/search?q=王宮周辺%20バンコク'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 10, :lat => '13.7187494', :long => '100.5141448', :name => 'チャオプラヤ', :link => URI.encode('https://mobile.twitter.com/search?q=チャオプラヤ'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 11, :lat => '13.7257861', :long => '100.5267976', :name => 'シーロム', :link => URI.encode('https://mobile.twitter.com/search?q=シーロム'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 12, :lat => '13.70832', :long => '100.5201019', :name => 'サトーン', :link => URI.encode('https://mobile.twitter.com/search?q=サトーン'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 13, :lat => '13.7411623', :long => '100.5083173', :name => 'ヤワラート', :link => URI.encode('https://mobile.twitter.com/search?q=ヤワラート'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 14, :lat => '13.7627232', :long => '100.5370978', :name => '戦勝記念塔', :link => URI.encode('https://mobile.twitter.com/search?q=トンロー'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 15, :lat => '13.7999767', :long => '100.5508926', :name => 'ウィークエンドマーケット', :link => URI.encode('https://mobile.twitter.com/search?q=ウィークエンドマーケット%20バンコク'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 16, :lat => '13.7482443', :long => '100.5786423', :name => 'バンコク', :link => URI.encode('https://mobile.twitter.com/search?q=バンコク'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 17, :lat => '13.7796464', :long => '100.544598', :name => 'アーリー', :link => URI.encode('https://mobile.twitter.com/search?q=アーリー%20バンコク'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 18, :lat => '13.7784289', :long => '100.5735275', :name => 'ラチャダー', :link => URI.encode('https://mobile.twitter.com/search?q=ラチャダー'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
					]
				elsif region == 'en' then
					related_list = [
						{:area_id => 1, :lat => '13.7589374', :long => '100.4971865', :name => 'Khaosan Now', :link => URI.encode('https://mobile.twitter.com/search?q=Khaosan'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 2, :lat => '13.7339984', :long => '100.5825355', :name => 'Thong Lor Now', :link => URI.encode('https://mobile.twitter.com/search?q=Thong%20Lor'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 3, :lat => '13.7194494', :long => '100.5852771', :name => 'Ekamai Now', :link => URI.encode('https://mobile.twitter.com/search?q=Ekamai'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 4, :lat => '13.6910654', :long => '100.614025', :name => 'Phra Khanong Now', :link => URI.encode('https://mobile.twitter.com/search?q=Phra%20Khanong'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 5, :lat => '13.7304479', :long => '100.5697677', :name => 'Phrom Phong Now', :link => URI.encode('https://mobile.twitter.com/search?q=Phrom%20Phong'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 6, :lat => '13.7370069', :long => '100.5603929', :name => 'Asoke Now', :link => URI.encode('https://mobile.twitter.com/search?q=Asoke'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 7, :lat => '13.7468299', :long => '100.5349284', :name => 'Siam Now', :link => URI.encode('https://mobile.twitter.com/search?q=Siam'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 8, :lat => '13.7503809', :long => '100.542796', :name => 'Pratunam Now', :link => URI.encode('https://mobile.twitter.com/search?q=Pratunam'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 9, :lat => '13.7502255', :long => '100.4913875', :name => "King's Palace", :link => URI.encode('https://mobile.twitter.com/search?q=King%20Palace%20Bangkok'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 10, :lat => '13.7187494', :long => '100.5141448', :name => 'Chao Phraya Now', :link => URI.encode('https://mobile.twitter.com/search?q=Chao%20Phraya%20Bangkok'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 11, :lat => '13.7257861', :long => '100.5267976', :name => 'Si Lom Now', :link => URI.encode('https://mobile.twitter.com/search?q=Si%20Lom'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 12, :lat => '13.70832', :long => '100.5201019', :name => 'Sathon Now', :link => URI.encode('https://mobile.twitter.com/search?q=Sathon'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 13, :lat => '13.7411623', :long => '100.5083173', :name => 'Yaowarat Now', :link => URI.encode('https://mobile.twitter.com/search?q=Yaowarat'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 14, :lat => '13.7627232', :long => '100.5370978', :name => 'Victory Monument Now', :link => URI.encode('https://mobile.twitter.com/search?q=Victory%20Monument'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 15, :lat => '13.7999767', :long => '100.5508926', :name => 'Weekend Market Now', :link => URI.encode('https://mobile.twitter.com/search?q=Weekend%20Market'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 16, :lat => '13.7482443', :long => '100.5786423', :name => 'RCA Now', :link => URI.encode('https://mobile.twitter.com/search?q=RCA%20Bangkok'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 17, :lat => '13.7796464', :long => '100.544598', :name => 'Ari Now', :link => URI.encode('https://mobile.twitter.com/search?q=Ari%20Bangkok'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 18, :lat => '13.7784289', :long => '100.5735275', :name => 'Ratchadaphisek Road Now', :link => URI.encode('https://mobile.twitter.com/search?q=Ratchadaphisek%20Road'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
					]
				elsif region == 'th' then	
					related_list = [
						{:area_id => 1, :lat => '13.7589374', :long => '100.4971865', :name => 'ถนนข้าวสาร', :link => URI.encode('https://mobile.twitter.com/search?q=ถนนข้าวสาร'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 2, :lat => '13.7339984', :long => '100.5825355', :name => 'ถนนทองหล่อ', :link => URI.encode('https://mobile.twitter.com/search?q=ทองหล่อ'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 3, :lat => '13.7194494', :long => '100.5852771', :name => 'เอกมัย', :link => URI.encode('https://mobile.twitter.com/search?q=เอกมัย'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 4, :lat => '13.6910654', :long => '100.614025', :name => 'พระขโนง', :link => URI.encode('https://mobile.twitter.com/search?q=พระขโนง'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 5, :lat => '13.7304479', :long => '100.5697677', :name => 'พร้อมพงษ์', :link => URI.encode('https://mobile.twitter.com/search?q=พร้อมพงษ์'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 6, :lat => '13.7370069', :long => '100.5603929', :name => 'อโศก', :link => URI.encode('https://mobile.twitter.com/search?q=อโศก'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 7, :lat => '13.7468299', :long => '100.5349284', :name => 'สยาม', :link => URI.encode('https://mobile.twitter.com/search?q=สยาม'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 8, :lat => '13.7503809', :long => '100.542796', :name => 'ประตูน้ำ', :link => URI.encode('https://mobile.twitter.com/search?q=ประตูน้ำ'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 9, :lat => '13.7502255', :long => '100.4913875', :name => "พระบรมมหาราชวัง", :link => URI.encode('https://mobile.twitter.com/search?q=พระบรมมหาราชวัง'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 10, :lat => '13.7187494', :long => '100.5141448', :name => 'เจ้าพระยา', :link => URI.encode('https://mobile.twitter.com/search?q=เจ้าพระยา%20กรุงเทพ'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 11, :lat => '13.7257861', :long => '100.5267976', :name => 'สีลม', :link => URI.encode('https://mobile.twitter.com/search?q=สีลม'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 12, :lat => '13.70832', :long => '100.5201019', :name => 'สาทร', :link => URI.encode('https://mobile.twitter.com/search?q=สาทร'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 13, :lat => '13.7411623', :long => '100.5083173', :name => 'เยาวราช', :link => URI.encode('https://mobile.twitter.com/search?q=เยาวราช'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 14, :lat => '13.7627232', :long => '100.5370978', :name => 'อนุสาวรีย์ชัยสมรภูมิ', :link => URI.encode('https://mobile.twitter.com/search?q=อนุสาวรีย์ชัยสมรภูมิ'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 15, :lat => '13.7999767', :long => '100.5508926', :name => 'ตลาดนัดจตุจักร', :link => URI.encode('https://mobile.twitter.com/search?q=ตลาดนัดจตุจักร'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 16, :lat => '13.7482443', :long => '100.5786423', :name => 'อาร์ซีเอ', :link => URI.encode('https://mobile.twitter.com/search?q=อาร์ซีเอ'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 17, :lat => '13.7796464', :long => '100.544598', :name => 'อารีย์', :link => URI.encode('https://mobile.twitter.com/search?q=อารีย์'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
						{:area_id => 18, :lat => '13.7784289', :long => '100.5735275', :name => 'รัชดา', :link => URI.encode('https://mobile.twitter.com/search?q=รัชดา'), :image => 'https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png'},
					]	
				end
			end
			render_to_json({
		     	related_data: related_list
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end

	end

	def get_twitter_data(region, city_id, area_id=0)
		twitter_temp = {}
		twitter_temp["name"] = ""
		twitter_temp["link"] = ""
		twitter_temp["image"] = "https://s3-ap-southeast-1.amazonaws.com/prod.local-picks.com/sns_icon/twitter_timeline.png"
		twitter_temp["region"] = ""
		twitter_temp["city_id"] = ""

		if city_id.to_i == 1 then 
			if region == 'jp' then
				twitter_temp["name"] = "バンコク"
				twitter_temp["link"] = URI.encode("https://mobile.twitter.com/search?q=バンコク")
				twitter_temp["region"] = "jp"
				twitter_temp["city_id"] = 1
				if area_id.to_i == 1 then
					twitter_temp["name"] = "カオサン"
				elsif area_id.to_i == 2 then 
					twitter_temp["name"] = "トンロー"
				elsif area_id.to_i == 3 then 
					twitter_temp["name"] = "エカマイ"
				elsif area_id.to_i == 4 then
					twitter_temp["name"] = "プラカノン"
				elsif area_id.to_i == 5 then 
					twitter_temp["name"] = "プロンポン"
				elsif area_id.to_i == 6 then 
					twitter_temp["name"] = "アソーク"
				elsif area_id.to_i == 7 then 
					twitter_temp["name"] = "サイアム"
				elsif area_id.to_i == 8 then 
					twitter_temp["name"] = "プラトゥナーム"
				elsif area_id.to_i == 9 then 
					twitter_temp["name"] = "王宮周辺"
				elsif area_id.to_i == 10 then 
					twitter_temp["name"] = "チャオプラヤ"
				elsif area_id.to_i == 11 then 
					twitter_temp["name"] = "シーロム"
				elsif area_id.to_i == 12 then 
					twitter_temp["name"] = "サトーン"
				elsif area_id.to_i == 13 then 
					twitter_temp["name"] = "ヤワラート"
				elsif area_id.to_i == 14 then 
					twitter_temp["name"] = "戦勝記念塔"
				elsif area_id.to_i == 15 then 
					twitter_temp["name"] = "ウィークエンドマーケット"
				elsif area_id.to_i == 16 then 
					twitter_temp["name"] = "バンコク"
				elsif area_id.to_i == 17 then 
					twitter_temp["name"] = "アーリー"
				elsif area_id.to_i == 18 then 
					twitter_temp["name"] = "ラチャダー"
				end 	
				if area_id.to_i > 0 then
					twitter_temp["link"] = URI.encode('https://mobile.twitter.com/search?q='+twitter_temp["name"])
				end
				twitter_temp["name"] = twitter_temp["name"] +  "のつぶやき"
			elsif region == 'en' then
				twitter_temp["name"] = "Bangkok Now"
				twitter_temp["link"] = URI.encode("https://mobile.twitter.com/search?q=bangkok")
				twitter_temp["region"] = "en"
				twitter_temp["city_id"] = 1
				if area_id.to_i == 1 then
					twitter_temp["name"] = "Khaosan Now"
				elsif area_id.to_i == 2 then 
					twitter_temp["name"] = "Thong Lor Now"
				elsif area_id.to_i == 3 then 
					twitter_temp["name"] = "Ekamai Now"
				elsif area_id.to_i == 4 then
					twitter_temp["name"] = "Phra Khanong Now"
				elsif area_id.to_i == 5 then 
					twitter_temp["name"] = "Phrom Phong Now"
				elsif area_id.to_i == 6 then 
					twitter_temp["name"] = "Asoke Now"
				elsif area_id.to_i == 7 then 
					twitter_temp["name"] = "Siam Now"
				elsif area_id.to_i == 8 then 
					twitter_temp["name"] = "Pratunam Now"
				elsif area_id.to_i == 9 then 
					twitter_temp["name"] = "King's Palace"
				elsif area_id.to_i == 10 then 
					twitter_temp["name"] = "Chao Phraya Now"
				elsif area_id.to_i == 11 then 
					twitter_temp["name"] = "Si Lom Now"
				elsif area_id.to_i == 12 then 
					twitter_temp["name"] = "Sathon Now"
				elsif area_id.to_i == 13 then 
					twitter_temp["name"] = "Yaowarat Now"
				elsif area_id.to_i == 14 then 
					twitter_temp["name"] = "Victory Monument Now"
				elsif area_id.to_i == 15 then 
					twitter_temp["name"] = "Weekend Market Now"
				elsif area_id.to_i == 16 then 
					twitter_temp["name"] = "RCA Now"
				elsif area_id.to_i == 17 then 
					twitter_temp["name"] = "Ari Now"
				elsif area_id.to_i == 18 then 
					twitter_temp["name"] = "Ratchadaphisek Road Now"
				end
				if area_id.to_i > 0 then
					twitter_temp["link"] = URI.encode('https://mobile.twitter.com/search?q='+twitter_temp["name"])
				end
			elsif region == 'th' then
				twitter_temp["name"] = "กรุงเทพ"
				twitter_temp["link"] = URI.encode("https://mobile.twitter.com/search?q=กรุงเทพ")
				twitter_temp["region"] = "th"
				twitter_temp["city_id"] = 1
				if area_id.to_i == 1 then
					twitter_temp["name"] = "ถนนข้าวสาร"
				elsif area_id.to_i == 2 then 
					twitter_temp["name"] = "ถนนทองหล่อ"
				elsif area_id.to_i == 3 then 
					twitter_temp["name"] = "เอกมัย"
				elsif area_id.to_i == 4 then
					twitter_temp["name"] = "พระขโนง"
				elsif area_id.to_i == 5 then 
					twitter_temp["name"] = "พร้อมพงษ์"
				elsif area_id.to_i == 6 then 
					twitter_temp["name"] = "อโศก"
				elsif area_id.to_i == 7 then 
					twitter_temp["name"] = "สยาม"
				elsif area_id.to_i == 8 then 
					twitter_temp["name"] = "ประตูน้ำ"
				elsif area_id.to_i == 9 then 
					twitter_temp["name"] = "พระบรมมหาราชวัง"
				elsif area_id.to_i == 10 then 
					twitter_temp["name"] = "เจ้าพระยา"
				elsif area_id.to_i == 11 then 
					twitter_temp["name"] = "สีลม"
				elsif area_id.to_i == 12 then 
					twitter_temp["name"] = "สาทร"
				elsif area_id.to_i == 13 then 
					twitter_temp["name"] = "เยาวราช"
				elsif area_id.to_i == 14 then 
					twitter_temp["name"] = "อนุสาวรีย์ชัยสมรภูมิ"
				elsif area_id.to_i == 15 then 
					twitter_temp["name"] = "ตลาดนัดจตุจักร"
				elsif area_id.to_i == 16 then 
					twitter_temp["name"] = "อาร์ซีเอ"
				elsif area_id.to_i == 17 then 
					twitter_temp["name"] = "อารีย์"
				elsif area_id.to_i == 18 then 
					twitter_temp["name"] = "รัชดา"
				end
				if area_id.to_i > 0 then
					twitter_temp["link"] = URI.encode('https://mobile.twitter.com/search?q='+twitter_temp["name"])
				end
			end	
		end

		twitter_temp
	end

	def find_nearby_test
		begin
			require "net/https"
			require "uri"


			url = URI.parse('https://graph.facebook.com/search?q=restaurant&type=page&access_token=739891342809062|fff51d97a9a283264f56c05f658cf8b1')		
			r = Net::HTTP.get(url)
			
			render_to_json({
				data: r
		    })

		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def find_nearby
		begin
			search_word = []
			if params[:search_word] == "all" or params[:search_word] == "" then
				search_word = ['restaurant', 'bar', 'beauty', 'cafe', 'museum', 'hotel', 'shopping', 'club', 'event', 'live', 'nightlife', 'market', 'art']
			elsif params[:search_word] != "all" and params[:search_word] != "" then
				if params[:search_word] == 'restaurant' then
					search_word = ["restaurant","italian","bistro","steak","ramen","sushi"]
				elsif params[:search_word] == 'bar' then
					search_word = ["bar","wine"]
				elsif params[:search_word] == 'shopping' then
					search_word = ["shop","shopping"]
				elsif params[:search_word] == 'live' then
					search_word = ["live","concert"]
				elsif params[:search_word] == 'beauty' then
					search_word = ["beauty", "clinic"]
				else
					search_word.push(params[:search_word])
				end
			end

			filter_result = []
			restaurant_name = ""
			lat = params[:lat].to_s
			long = params[:long].to_s
			city_name = params[:city_name].to_s

			if lat.blank? or lat.to_i <= 0 or long.blank?  or long.to_i <= 0 then
				if params[:city_id].to_i == 1 then
					lat = "13.7563309".to_s
					long = "100.5017651".to_s
				end
			end

			area_id = 0
			if !params[:area_id].blank? then
				area_id = params[:area_id].to_i
			end

			center = lat+','+long
			if params[:city_id].to_i > 0 then
				twitter_temp = get_twitter_data(params[:region], params[:city_id].to_i, area_id.to_i)
			else
				twitter_temp["name"] = city_name + " Now"
				twitter_temp["link"] = URI.encode("https://mobile.twitter.com/search?q="+city_name)
				twitter_temp["region"] = params[:region]
				twitter_temp["city_id"] = 0
			end

			search_word.each do |word|
				result = FbGraph::Place.search(word, :distance => 10000, :center => center , :access_token => '739891342809062|fff51d97a9a283264f56c05f658cf8b1')
				result.each do |restaurant|
					if restaurant.category.downcase != "city" and restaurant.category.downcase != "university"  then
						temp = {}
						temp["facebook_id"] = restaurant.identifier
						temp["name"] = restaurant.name
						temp["link"] = restaurant.link
						temp["category"] = restaurant.category
						temp["image"] = "https://graph.facebook.com/v2.6/"+restaurant.identifier+"/picture?width=200"
						temp["twitter_link"] = URI.encode('https://mobile.twitter.com/search?q='+restaurant.name.downcase)
						temp["instagram_link"] = 'https://www.instagram.com/explore/locations/'+restaurant.identifier
						temp["google_link"] = ''
						FacebookPageDatum.register(restaurant.identifier, temp["name"], temp['link'], temp['image'], temp['category'], temp["twitter_link"], temp["instagram_link"], temp["google_link"])	
						filter_result.push(temp)
					end
				end
			end

			render_to_json({
				twitter_data: twitter_temp,
		     	fb_data: filter_result.uniq
		    })
		rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end

	def nearby_news
		begin
			filter_result = []
			search_text =  params[:name].to_s.downcase
			search_text_split = search_text.split(' ')
			first_text = search_text_split[0]
	    	search_text_where = ["lower(title) LIKE (?) OR lower(news_description) LIKE (?) ", "%#{first_text}%", "%#{first_text}%"]

	    	if search_text_split.length > 1 then
	    		second_text = search_text_split[1]
	    		search_text_where = ["lower(title) LIKE (?) OR lower(news_description) LIKE (?) OR lower(title) LIKE (?) OR lower(news_description) LIKE (?) ", "%#{first_text}%", "%#{first_text}%", "%#{second_text}%", "%#{second_text}%"]
	    	end

	    	region = "jp"
	    	if params[:region].to_s != "" then
	    		region = params[:region].to_s
	    	end	

	    	city_condition = [ 0, '0']
		    if params[:city_id].to_i > 0 then
		    	city_condition.push(params[:city_id].to_i)
		    end	
			
			facebook_temp = {}
			facebook_temp["name"] = ""
			facebook_temp["link"] = ""
			facebook_temp["image"] = ""
			facebook_temp["twitter_link"] = ""

			if params.key?(:facebook_id) and params[:facebook_id].to_i != "" then
				facebook_page = FacebookPageDatum.where({:facebook_page_id => params[:facebook_id]}).first
				facebook_temp["name"] = facebook_page.facebook_page_name
				facebook_temp["link"] = facebook_page.facebook_page_url
				facebook_temp["image"] = facebook_page.facebook_page_image+'?width=200'
				facebook_temp["twitter_link"] = facebook_page.twitter_link
				facebook_temp["instagram_link"] = facebook_page.instagram_link
			end

			find_news = NewsPick.where(search_text_where).where({region: params[:region]}).where("city_id IS NULL OR city_id IN (?)", city_condition).order('created_at desc')
			if find_news then
				result = []	
				find_news.each do |item|
					result.push(create_news_object(item))
				end	
				
				find_news = result

				if (params[:user_id]) then			
					bookmarks = NewsBookmark.where({user_id: params[:user_id]}).select('news_id')
					if bookmarks then
						bookmarks_id = NewsPick.where("id IN (?)", bookmarks).order('created_at desc')				
						if bookmarks_id then
							find_news.each do |item|
								bookmarks_id.each do |bookmark|
									if item["id"] == bookmark.id then
										item["bookmark"] = true
									end
								end
							end
						end			    		    
				    end
				end
			end
			render_to_json({
				search_text_where: search_text_where,
				facebook_data: facebook_temp,
		     	data: find_news.uniq
		    })
	    rescue Exception => e
			log_error e
	      	render_to_error_json([e.message], { :status => :bad_request}) and return	 
		end
	end
end
