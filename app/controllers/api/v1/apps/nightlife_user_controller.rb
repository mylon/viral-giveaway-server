class Api::V1::Apps::NightlifeUserController < Api::V1::Apps::BaseController
  before_action :authenticate_api, except: [:register, :verify, :reset_password, :resend_verify]
  before_action :public_api, only: [:register, :verify, :reset_password, :resend_verify]

  def register
    begin      
      if errors = validate_register(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end
 
      user = nil 
      NightlifeUser.transaction do           
        user = NightlifeUser.register_from_app(params) 
      end 

      if user then

        profile_image = NightlifeUserImage.where({user_id: user.id, is_profile: 1}).first
        user_profile_image = ""
        if profile_image then
          user_profile_image = 'https://s3-ap-southeast-1.amazonaws.com/'+Rails.application.config.nightlife_s3_bucket+'/'+NightlifeUserImage.generate_dest_path(profile_image.image_url, user.id, 'profile_picture')
        end
        render_to_json({
          user_id: user.id,
          account_id: user.account_id,
          token: user.token,        
          gender: user.gender,       
          tag_line: user.tag_line,
          show_men: user.show_men,
          show_women: user.show_women,     
          device_udid: user.device_udid,
          is_dummy: user.is_dummy,
          match_notification: user.match_notification,
          message_notification: user.message_notification,
          like_notification: user.like_notification,
          is_online: user.is_online,
          platform: user.platform,
          profile_image: user_profile_image,
          verified: user.verified_at.present? 
        }) and return       
      else
        render_to_error_json(['Error Please retry'], { :status => :bad_request }) and return
      end
    rescue => e
      log_error(e)
      render_to_error_json([e],{ :status => :bad_request }) and return
    end
  end

  def verify
    begin
      user = nil
      NightlifeUser.transaction do
        user = NightlifeUser.verify(params[:uid].to_s, params[:code].to_s)
      end

      VGLogger.post({ path: '/v1/apps/nightlife_user/verify', user_id: user.account_id, app: @app.code }) if user

      render_to_json({ :verified => user.present? }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Verify Failed'], { :status => :bad_request }) and return
    end
  end

  def resend_verify
    begin
      user = NightlifeUser.where({ :canonical_email => params[:email].to_s.downcase, :deleted_at => nil }).first
      if user.nil? then
        render_to_error_json(['Resend Failed'], { :status => :bad_request }) and return
      elsif user.verified_at then
        render_to_error_json([I18n.t('error.user.already_verified', :localse => user.locale.to_sym)], { :status => :bad_request }) and return
      end

      VGLogger.post({ path: '/v1/nightlife_user/resend_verify', user_id: user.account_id, app: @app.code })
      
      user.send_email!(
        VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, @app.name),
        I18n.t('mail.dp.resend_verify_subject', :locale => user.locale.to_sym),
        render_to_string( :template => 'mail/dp/resend_verify', :layout => 'mail/dp', :locals => { verify_code: user.verification_code } )
      )

      render_to_json({ sent: user.present? }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Resend Failed'], { :status => :bad_request }) and return
    end
  end

  def reset_password
    begin
      user = nil
      password = nil
      NightlifeUser.transaction do
        user = NightlifeUser.find_by_email(params[:email])
        if user then
          password = NightlifeUser.generate_password()
          user.encrypted_password = NightlifeUser.encrypt_password(password)
          user.save!
        end
      end
      if user then
        VGLogger.post({ path: '/v1/nightlife_user/password_reset', user_id: user.account_id, app: @app.code })

        user.send_email!(
          VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, @app.name),
          I18n.t('mail.dp.password_reset_subject', :locale => user.locale.to_sym),
          render_to_string( :template => 'mail/dp/password_reset', :layout => 'mail/dp', :locals => { new_password: password } ) 
        )
      end
      render_to_json({ sent: user.present? }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Not found'], { :status => :bad_request }) and return
    end
  end

  def profile
    render_to_json(_profile(@user))
  end
  
  def profile_update
    begin
      if errors = @user.validate_profile(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      NightlifeUser.transaction do
        @user.name      = params[:name].strip if params.key?(:name)
        @user.birthdate = params[:birthdate_date] if params.key?(:birthdate)
        @user.gender    = params[:gender] if params.key?(:gender)
        @user.encrypted_password  = NightlifeUser.encrypt_password(params[:new_password]) if params[:new_password].to_s.length > 0

        @user.save!
      end

      VGLogger.post({ path: '/v1/nightlife_user/profile_update', app: @app.code })

      render_to_json(_profile(@user))
    rescue => e
      log_error e
      render_to_error_json(['ERROR Please retry'], { :status => :bad_request })
    end
  end

  def _profile(user)
    birthdate = user.birthdate.blank? ? nil : user.birthdate.strftime(Rails.application.config.birthdate_format)
    data = {
      :user_id      => user.account_id,
      :name         => user.name,
      :birthdate    => birthdate,
      :gender       => user.gender,
      :email        => user.email,
      :saved_password => user.encrypted_password.to_s.length > 0,
    }
    if user.birthdate then
      data[:birthdate_year]  = user.birthdate.year
      data[:birthdate_month] = user.birthdate.month
      data[:birthdate_day]   = user.birthdate.day
    end

    data
  end

  def validate_register(params)
    errors = []
    
    email = params[:email].to_s.strip
    if params[:provider].to_s != "twitter" and params[:provider].to_s != "facebook" then
      if email.blank? then
        errors.push(I18n.t('error.user.email_blank'))
      elsif not email.match(Rails.application.config.email_regex) then
        errors.push(I18n.t('error.user.email_format_invalid'))
      elsif email.length > Rails.application.config.email_length_max then
        errors.push(I18n.t('error.user.email_length_max', length:Rails.application.config.email_length_max))
      end
    end

    case params[:provider].to_s
    when "facebook"
      begin
        user = FbGraph::User.me(params[:access_token].to_s).fetch
        logger.info "user=#{user.inspect}"
        if user.identifier == params[:provider_user_id] then
          params[:user] = user
        else
          errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end
      rescue => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      end
    when "google"
      begin
        user = GooglePlus::Person.get(params[:provider_user_id], api_key: Rails.application.config.google_api_key, access_token: params[:access_token].to_s)
        logger.info "user=#{user.inspect}"
        if user.id == params[:provider_user_id] then
          params[:user] = user
        else
          errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end
      rescue GooglePlus::RequestError => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      rescue => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))
      end
    when "twitter"
      begin
        twitter_consumer_key = Rails.application.config.newspick_bkk_jp_twitter_consumer_key
        twitter_consumer_secret  = Rails.application.config.newspick_bkk_jp_twitter_consumer_secret  

        client = Twitter::REST::Client.new do |config|
          #temporary solution need better
          config.consumer_key    = twitter_consumer_key
          config.consumer_secret = twitter_consumer_secret    
        end
        user = client.user(params[:provider_user_id])
        logger.info "user=#{user.inspect}"
        if user then
            params[:user] = user           
        else
            errors.push(I18n.t("error.user.provider_user_id_invalid"))
        end        
      rescue => e
        log_error e
        errors.push(I18n.t("error.user.credentials_invalid"))       
      end
    when "vg", "nl"
      password_len = params[:password].to_s.length
      if password_len < Rails.application.config.password_length_min or password_len > Rails.application.config.password_length_max then
        errors.push(I18n.t('error.user.password_length', min: Rails.application.config.password_length_min, max: Rails.application.config.password_length_max))
      end
    else
      errors.push(I18n.t('error.user.provider_invalid'))
    end 

    return errors if errors.length > 0 

   if params[:provider].to_s == 'twitter' or params[:provider].to_s == 'facebook' then
     user = NightlifeUser.where({ deleted_at: nil, uid: params[:provider_user_id] }).first
   else
     user = NightlifeUser.where({ deleted_at: nil, canonical_email: params[:email].downcase }).first
   end

    if user then
      case params[:provider].to_s
      when "facebook", "google", "twitter"
        unless user.uid == params[:provider_user_id] then
          errors.push(I18n.t('error.user.email_exists'))
        end
      when "vg", "nl"
        unless user.encrypted_password == NightlifeUser.encrypt_password(params[:password].to_s) then
          errors.push(I18n.t('error.authorization.login_failed'))
        end        
      end
    end

    errors
  end
end
