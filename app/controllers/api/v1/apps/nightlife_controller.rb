class Api::V1::Apps::NightlifeController < Api::V1::Apps::BaseController
	before_action :authenticate_api, except: [:upload_photo_from_app, :get_upload_photo, :get_profile_photo, :user_request, :create_group]

	def upload_photo_from_app
		begin
		  params[:image_url] = URI::Data.new(params[:image_url])
		  if params[:image_url].nil? or not ( /^image\/.*$/.match(params[:image_url].content_type) ) then
		     render_to_error_json(I18n.t('error.campaign.invalid_photo'), {:status => :bad_request}) and return
		  end

		  now = Time.now
	      user_id = params[:user_id].to_i
	      is_profile = params[:is_profile].to_i
	      filename = rand(10 ** 10)
	      image_url = NightlifeUserImage.generate_filename(params[:image_url], filename.to_s)

	      if is_profile == 1 then
	      	NightlifeUserImage.where({user_id: user_id}).update_all(is_profile: 0)
	      end

	      NightlifeUserImage.upload_photo(params[:image_url], filename, user_id, opt={})
	     
	      np = NightlifeUserImage.new(
	          :user_id => user_id,
	          :is_profile => is_profile,
	          :image_url => image_url,
	          :created_at => now,
	      	  :updated_at => now,
	       ).save!

	      render_to_json({ 
	      	nightlife_user_photo: np
	      })
	    rescue ActiveRecord::RecordInvalid => e
	      log_error e
	      render_to_error_json(e, { :status => :internal_server_error }) and return
	    end
	end

	def get_upload_photo
		begin
			user_id = params['user_id']
			result = []
			user_upload_image = NightlifeUserImage.where({user_id: user_id})
			if user_upload_image then
				user_upload_image.each do |user_image|
					temp = {}
					temp = user_image
					temp['image_url'] = 'https://s3-ap-southeast-1.amazonaws.com/'+Rails.application.config.nightlife_s3_bucket+'/'+NightlifeUserImage.generate_dest_path(user_image.image_url, user_id, 'profile_picture')
				end
			end
			render_to_json({ 
		      	user_upload_image: user_upload_image
		    })
		rescue ActiveRecord::RecordInvalid => e
			log_error e
	      	render_to_error_json(e, { :status => :internal_server_error }) and return
		end
	end

	def get_profile_photo
		begin
			user_id = params['user_id']
			user_upload_image = NightlifeUserImage.where({user_id: user_id, is_profile: 1}).first
			if user_upload_image then
				profile_image = 'https://s3-ap-southeast-1.amazonaws.com/'+Rails.application.config.nightlife_s3_bucket+'/'+NightlifeUserImage.generate_dest_path(user_upload_image.image_url, user_id, 'profile_picture')
			end
			render_to_json({ 
		      	image_url: profile_image
		    })
		rescue ActiveRecord::RecordInvalid => e
			log_error e
	      	render_to_error_json(e, { :status => :internal_server_error }) and return
		end
	end

	def create_group
		begin
			require 'rqrcode'
			user_id = params['user_id']

			#check user is already group or not
			check_user = NightlifeGroupUser.where({user_id: user_id}).first
			if check_user then
				render_to_error_json(["User already create or join a group"], {:status => :bad_request}) and return
			end

			group_barcode = rand(36**10).to_s(36)



		rescue ActiveRecord::RecordInvalid => e
			log_error e
	      	render_to_error_json(e, { :status => :internal_server_error }) and return
		end
	end

	def user_request
		begin
			from_user_id = params[:from_user_id]
			to_user_id = params[:to_user_id]

			#Check user
			from_nightlife_user = NightlifeUser.where({id: from_user_id}).first
			if !from_nightlife_user then
				render_to_error_json(["Not found from user profile"], { :status => :internal_server_error }) and return
			end
			to_nightlife_user = NightlifeUser.where({id: to_user_id}).first
			if !to_nightlife_user then
				render_to_error_json(["Not found to user profile"], { :status => :internal_server_error }) and return
			end

			#check if already request return value
			check_already_request = NightlifeUserRequest.where({from_user_id: from_user_id, to_user_id: to_user_id}).first
			if check_already_request then
				
				render_to_json({ 
			      	from_user_id: check_already_request.from_user_id,
			      	to_user_id: check_already_request.to_user_id,
			      	is_match: check_already_request.is_match,
			      	chat_room: chat_room_id
			    }) and return
			else
				#check is match or not and never request
				check_match_user = NightlifeUserRequest.where({to_user_id: from_user_id, from_user_id: to_user_id, is_match: 0}).first
				if check_match_user then
					#insert from user request
					from_user_quest = NightlifeUserRequest.register(from_user_id, to_user_id, 1)
					check_match_user.is_match = 1
					check_match_user.save!

					#create_group_for_match
					createdby_match = 1
					from_user_group = 0
					to_user_group = 0
					single_single = 0
					single_group = 0
					group_group = 0
					group_join = 0
					from_user_group_id = 0
					to_user_group_id = 0
					from_user_list = []
					to_user_list = []
					combine_user_list = []

					#check have group
					check_from_user_group = NightlifeGroupUser.where({user_id: from_user_id}).first
					if check_from_user_group then
						from_user_group_id = check_from_user_group[0].group_id
						#if have group check is not chat
						check_group_join = NightlifeGroup.where({id: from_user_group_id, group_join: 1})
						if check_group_join then
							from_user_group = 1
						end
					end
					#check have group
					check_to_user_group = NightlifeGroupUser.where({user_id: to_user_id}).first
					if check_to_user_group then
						to_user_group_id = check_to_user_group[0].group_id
						#if have group check is not chat
						check_group_join = NightlifeGroup.where({id: to_user_group_id, group_join: 1})
						if check_group_join then
							to_user_group = 1
						end
					end

					if from_user_group == 0 and to_user_group == 0 then
						single_single = 1
					end

					if (from_user_group == 1 and to_user_group == 0 ) or (from_user_group == 0 and to_user_group == 1 ) then
						single_group = 1
					end

					if from_user_group == 1 and to_user_group == 1 then
						group_group = 1
					end
					
					#create_group
					create_group = NightlifeGroup.register(nil,createdby_match, nil, single_single, single_group, group_group, group_join)
					latest_group_id = NightlifeGroup.order('id desc').limit(1).select('id')

					#insert from and to user first
					insert_from_user = NightlifeGroupUser.register(latest_group_id, from_user_id, 0)
					insert_to_user = NightlifeGroupUser.register(latest_group_id, to_user_id, 0)

					#create_group_user
					if from_user_group == 1 then
						from_user_list = NightlifeGroupUser.where({group_id: from_user_group_id}).where.not(:user_id => from_user_id).select('user_id')
					end

					if to_user_group == 1 then
						to_user_list = NightlifeGroupUser.where({group_id: to_user_group_id}).where.not(:user_id => to_user_id).select('user_id')
					end

					combine_user_list = from_user_list | to_user_list
					if !combine_user_list.blank? then
						combine_user_list.each do |each_user_id|
							NightlifeGroupUser.register(latest_group_id, each_user_id, 0)
						end
					end

					chat_room = NightlifeChatroom.register(latest_group_id)
					latest_chatroom_id = NightlifeChatroom.order('id desc').limit(1).select('id')

					render_to_json({ 
				      	from_user_id: from_user_id,
				      	to_user_id: to_user_id,
				      	is_match: 1,
				      	chat_room: latest_chatroom_id[0].id
				    }) and return
				else
					from_user_quest = NightlifeUserRequest.register(from_user_id, to_user_id, 0)
					render_to_json({ 
				      	from_user_id: from_user_id,
				      	to_user_id: to_user_id,
				      	is_match: 0,
				      	chat_room: nil
				    }) and return
				end
			end
		rescue ActiveRecord::RecordInvalid => e
			log_error e
	      	render_to_error_json(e, { :status => :internal_server_error }) and return
		end
	end

end
