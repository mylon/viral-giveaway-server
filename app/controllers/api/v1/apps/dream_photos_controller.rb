class Api::V1::Apps::DreamPhotosController < Api::V1::Apps::BaseController
  def apply
    begin
      params[:id] = Campaign.where({tag:'DreamPhoto'}).first.id
      if errors = Campaign.validate_apply(@user, params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      dp = nil
      DreamPhoto.transaction do
        dp = DreamPhoto.create(
          :user => @user,
          :photo_type => params[:photo_type].to_i,
          :comment => params[:comment].to_s,
          :billing_status => Rails.application.config.billing_status_map[:pending],
        )
        dp.upload_user_photo(params[:photo1_data])
        dp.upload_base_photo(params[:photo2_data])
        dp.user_photo_url = dp.generate_filename(params[:photo1_data], "user")
        dp.base_photo_url = dp.generate_filename(params[:photo2_data], "base")
        dp.save!
      end
      render_to_json({ dream_photo: {id: dp.id} })
    rescue => e
      log_error e
      render_to_error_json(['ERROR Please retry'], { :status => :internal_server_error }) and return
    end
  end

  def validate_apply(params)
    errors = []

    campaign = Campaign.search(@user, { :tag => "dream_photo" }).first
    if campaign then
      params[:campaign] = campaign
    else
      errors.push(I18n.t('error.campaign.not_found'))
      return errors
    end
   
    unless Rails.application.cofnig.dream_photo_photo_type_map.values.include?(params[:photo_type].to_i) then
      errors.push(I18n.t('error.campaign.invalid_photo_type'))
    end 

    params[:photo1_data] = URI::Data.new(params[:photo1])
    if params[:photo1_data].nil? or not ( /^image\/.*$/.match(params[:photo1_data].content_type) ) then
      errors.push(I18n.t('error.campaign.invalid_photo'))
    end
    params[:photo2_data] = URI::Data.new(params[:photo2])
    if params[:photo2_data].nil? or not ( /^image\/.*$/.match(params[:photo2_data].content_type) ) then
      errors.push(I18n.t('error.campaign.invalid_photo'))
    end

    errors
  end
end
