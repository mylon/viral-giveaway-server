class Api::V1::TicketsController < Api::V1::BaseController
  def list
    custom_where = ''
    custom_params = []
    if params.key?(:since_id) and params[:since_id].to_i > 0 then
      custom_where += ' AND ' if custom_where.length > 0
      custom_where += 'id < ?'
      custom_params.push(params[:since_id].to_i)
    end
    
    now = Time.now
    tickets = Ticket.joins(:campaign).where({
      :user => @user,
    }).where([custom_where, *custom_params])
    .order('id desc')
    .limit(Rails.application.config.information_load_num).map do |t|
      {
        :id         => t.id,
        :number     => t.number, 
        :expired    => (t.campaign.end_at <= now),
        :created_at => t.created_at,
      }
    end

    render_to_json({
      :limit => Rails.application.config.information_load_num,
      :tickets => tickets,
    })
  end

  def count
    render_to_json({ :count => Ticket.where({:user => @user}).count })
  end
end
