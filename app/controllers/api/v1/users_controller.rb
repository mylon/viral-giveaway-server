class Api::V1::UsersController < Api::V1::BaseController
  before_action :authenticate_api, except: [:register, :verify, :password_reset, :resend_verify]
  before_action :public_api, only: [:register, :verify, :password_reset, :resend_verify]

  def register
    begin
      if errors = User.validate_register(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      user = nil
      User.transaction do
        user = User.register_by_email( params[:email], params[:password], {:referral_code => params[:referral_code]} )
      end

      VGLogger.post({ :path => '/v1/users/register', :account_id => user.account_id })

      user.send_email!(
        VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
        I18n.t('mail.register_complete_subject', :locale => user.locale.to_sym),
        render_to_string( :template => 'mail/register_complete', :layout => 'mail', :locals => { :verify_id => user.account_id, :verify_code => user.verification_code } )
      )

      user.send_email!(
        VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
        I18n.t('mail.register_complete_after_support_subject', :locale => user.locale.to_sym),
        render_to_string( 
                         :template => 'mail/register_complete_after_support', 
                         :layout => 'mail', 
                         :locals => {
                            :promotion_offers => Offer.search(user, { :category => 'promotion', :country => user.country, :platform => user.platform, :exclude_installed => true }),
                         } 
                        ),
        { :wait => 1.hour }
      )
      render_to_json(_profile(user)) and return
    rescue => e
      log_error(e)
      render_to_error_json(['ERROR Please retry'], { :status => :bad_request }) and return
    end
  end

  def unregister
    begin
      if errors = @user.validate_unregister(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      # logging question
      Fluent::Logger.post('vg.delete_account', {
        :user_id         => @user.id,
        :question_answer => params[:question_answer].to_i,
        :question_reqson => params[:question_reason].to_s,
        :request_ip      => request.remote_ip,
      })
    
      User.transaction do
        @user.unregister();
      end

      VGLogger.post({ path: '/v1/users/unregister' })

      @user.send_email!(
          VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
          I18n.t('mail.unregister_complete_subject', :locale => @user.locale.to_sym),
          render_to_string( :template => 'mail/unregister_complete', :layout => 'mail' ) 
      )

      render_to_json({}) and return
    rescue => e
      log_error(e)
      render_to_error_json(['ERROR Please retry'], { :status => :bad_request }) and return
    end
  end

  def invited
    begin
      invitation = nil
      User.transaction do
        invitation = @user.register_invite_friend(params[:referral_code])
        Tracking.track(@user, 1, :show_invitation_dialog, :conversioned)
      end

      if invitation then
        invitation.from_user.send_email(
          VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
          I18n.t('mail.got_point_subject', :locale => invitation.from_user.locale.to_sym),
          render_to_string( 
                           :template => 'mail/got_point', 
                           :layout => 'mail', 
                           :locals => { 
                              point: Rails.application.config.invite_point_num, 
                              action: :invitation,
                              promotion_offers: Offer.search(@user, { :category => 'promotion', :country => @user.country, :platform => @user.platform, :exclude_installed => true, :limit => Rails.application.config.mail_promotion_offer_num }),
                           } 
                          )
        )
      end

      render_to_json({ :invited => not(invitation.nil?) }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Invite Failed'], { :status => :bad_request }) and return
    end
  end

  def track
    begin
      tracking = nil
      User.transaction do
        type_key = nil
        status_key = nil
        case params[:type]
        when 'invitation' then
          type_key   = :show_invitation_dialog
          status_key = params[:value].to_s == 'ok' ? :conversioned : :skip
        end
        tracking = Tracking.track(@user, 1, type_key, status_key)
      end
      render_to_json({ :tracked => not(tracking.nil?) }) and return
    rescue => e
      log_error(e)
      render_to_error_json(["Can't tracked"], { :status => :bad_request }) and return
    end
  end

  def verify
    begin
      user = nil
      User.transaction do
        user = User.verify(params[:id].to_s, params[:code].to_s)
      end

      VGLogger.post({ path: '/v1/users/verify', user_id: user.account_id }) if user

      render_to_json({ :verified => user.present? }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Verify Failed'], { :status => :bad_request }) and return
    end
  end

  def resend_verify
    begin
      user = User.where({ :canonical_email => params[:email].to_s.downcase, :deleted_at => nil }).first
      if user.nil? then
        render_to_error_json(['Resend Failed'], { :status => :bad_request }) and return
      elsif user.verified_at then
        render_to_error_json([I18n.t('error.user.already_verified', :localse => user.locale.to_sym)], { :status => :bad_request }) and return
      end

      VGLogger.post({ path: '/v1/users/resend_verify', user_id: user.account_id })
      
      user.send_email!(
        VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
        I18n.t('mail.resend_verify_subject', :locale => user.locale.to_sym),
        render_to_string( :template => 'mail/resend_verify', :layout => 'mail', :locals => { verify_id: user.account_id, verify_code: user.verification_code } )
      )

      render_to_json({}) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Resend Failed'], { :status => :bad_request }) and return
    end
  end

  def password_reset
    begin
      user = nil
      password = nil
      User.transaction do
        user = User.find_by_email(params[:email])
        password = User.generate_password()
        user.encrypted_password = User.encrypt_password(password)
        user.save!
      end
      if user then
        VGLogger.post({ path: '/v1/users/password_reset', user_id: user.account_id })

        user.send_email!(
          VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
          I18n.t('mail.password_reset_subject', :locale => user.locale.to_sym),
          render_to_string( :template => 'mail/password_reset', :layout => 'mail', :locals => { new_password: password } ) 
        )
      end
      render_to_json({ :sent => not(user.nil?) }) and return
    rescue => e
      log_error(e)
      render_to_error_json(['Not found'], { :status => :bad_request }) and return
    end
  end

  def profile
    render_to_json(_profile(@user))
  end

  def profile_update

    begin
      if errors = @user.validate_profile(params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      User.transaction do
        @user.name                = params[:name].strip if params.key?(:name)
        @user.email               = params[:email].strip if params.key?(:email)
        @user.canonical_email     = params[:email].strip.downcase if params.key?(:email)
        @user.birthdate           = params[:birthdate_date] if params.key?(:birthdate)
        @user.gender              = params[:gender] if params.key?(:gender)
        @user.notification        = params[:notification] if params.key?(:notification)
        @user.encrypted_password  = User.encrypt_password(params[:new_password]) if params[:new_password].to_s.length > 0

        @user.save!
      end

      VGLogger.post({ path: '/v1/users/profile_update' })

      render_to_json(_profile(@user))
    rescue => e
      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
      render_to_error_json(['ERROR Please retry'], { :status => :bad_request })
    end
  end

  def _profile(user)
    birthdate = user.birthdate.blank? ? nil : user.birthdate.strftime(Rails.application.config.birthdate_format)
    data = {
      :account_id   => user.account_id,
      :name         => user.name,
      :birthdate    => birthdate,
      :gender       => user.gender,
      :email        => user.email,
      :country      => user.country,
      :notification => user.notification,
      :saved_password => user.encrypted_password.to_s.length > 0,
    }
    if user.birthdate then
      data[:birthdate_year]  = user.birthdate.year
      data[:birthdate_month] = user.birthdate.month
      data[:birthdate_day]   = user.birthdate.day
    end

    data
  end
end
