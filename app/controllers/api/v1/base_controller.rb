class Api::V1::BaseController < Api::BaseController

  before_action :set_analytics, :set_locale, :set_account_id, :set_session_id, :authenticate_app_api, :authenticate_api

  def set_analytics
    Thread.current[:source] = request.headers['X-SOURCE'].to_s
  end

  def set_session_id
    @session_id = request.headers['X-SESSION-ID'].to_s
    Thread.current[:session_id] = @session_id
  end

  def set_account_id
    @account_id = request.headers['X-UID'].to_s
    Thread.current[:account_id] = @account_id
  end

  def set_locale
    self.support_locale(params[:locale].to_s)
  end

  def check_version
    logger.debug("check_version")
    if params.key?(:v) then
      vers = params[:v].split('.').map{|v| v.to_i}
      if need_upgrade(Rails.application.config.versions, vers) then
        render_to_error_json( [I18n.t('error.authorization.need_upgrade')], { :status => 501 } ) and return
      end
    else
      # NOT IMPLEMENTED
      #render_to_error_json( [I18n.t('error.authorization.need_upgrade')], { :status => 501 } ) and return
    end
  end

  def check_maintenance
    logger.debug("check_maintenance")
    @system_information = SystemInformation.first
    if @system_information.maintenance then
      render_to_error_json( [I18n.t('error.authorization.maintenance')], { :status => 503 } ) and return
    end
  end

  def public_api
    logger.debug("public_api")
    self.support_locale(params[:locale].to_s)
  end

  def authenticate_app_api
    @app = App.where({code: 'vg'}).first
    Thread.current[:app] = @app
  end

  def authenticate_api
    logger.debug("authenticate_api")

    @user = User.where({:account_id => @account_id, :deleted_at => nil}).where("verified_at IS NOT NULL").first
    if @user.blank? then
      render_to_error_json([I18n.t('error.authorization.required'), I18n.t('error.authorization.uid_invalid')], { :status => 401 }) and return
    end
    
    if Rails.env.production? or request.headers.key?('X-API-TOKEN') then
      if @user.token != request.headers['X-API-TOKEN'].to_s then
        render_to_error_json([I18n.t('error.authorization.required'), I18n.t('error.authorization.token_invalid')], { :status => 401 }) and return
      end
    end

    @user.locale = I18n.locale
    @user.last_accessed_at = Time.now
    @user.save
  end

  def support_locale(locale)
    I18n.locale = I18n.available_locales.include?(locale.to_sym) ? locale.to_sym : Rails.application.config.i18n.default_locale
  end

  def signature(params)
    puts params
    require 'digest/sha2'

    sig = ''
    params.keys.sort.each do |k|
      next if k == 'action'
      next if k == 'controller'
      next if k == 'signature'
      next if k =~ /^photo[1-9]*$/

      sig += "&" if sig.length > 0
      sig += "#{k.to_s}=#{params[k]}"
    end

    Digest::SHA512.hexdigest(sig)
  end

  private
    def need_upgrade(vers, req_vers)
      return true if vers[0] > req_vers[0] # major
      return true if vers[1] > req_vers[1] # minor
      return false
    end
end
