class Api::V1::GlueController < Api::V1::BaseController
  before_action :authenticate_api, except: :index
  before_action :public_api, only: :index

  def index
    responses = []
    uris = params[:uri]
    uris = [uris] unless uris.instance_of?(Array)
    uris.each do |h|
      begin
        h.each do |k,uri|
          status, _, body = forward(uri)
          logger.debug "status=#{status}, body=#{body.body}"
          responses.push({
            :status => status,
            :response => JSON.parse(body.body),
          })
          body.close
        end
      rescue => e
        log_error e
        responses.push({
          :status => 500,
          :response => {
            :status => 'failure',
            :data => nil
          }
        })
        next
      end
    end

    render_to_json( responses )
  end

  # see: https://coderwall.com/p/gghtkq/rails-internal-requests
  def forward(uri)
    opts = {
      :method        => 'GET',
      'rack.session' => session,
    }
    opts['HTTP_X_UID'] = request.headers['X-UID'].to_s if request.headers.key?('X-UID') 
    opts['HTTP_X_API_TOKEN'] = request.headers['X-API-TOKEN'].to_s if request.headers.key?('X-API-TOKEN') 
    opts['HTTP_X_SESSION_ID'] = request.headers['X-SESSION-ID'].to_s if request.headers.key?('X-SESSION-ID') 
    opts['HTTP_USER_AGENT'] = request.env['HTTP_USER_AGENT'].to_s
    logger.debug "uri=#{uri}, opts=#{opts}"
    request_env = Rack::MockRequest.env_for(uri).merge(opts)
    Rails.application.routes.call(request_env)
  end
end
