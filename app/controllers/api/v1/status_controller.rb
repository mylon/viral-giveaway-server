class Api::V1::StatusController < Api::V1::BaseController
  before_action :authenticate_api, except: :index
  before_action :check_version, except: :index
  before_action :check_maintenance, except: :index
  before_action :public_api, only: :index

  def index
    render_to_json({
      :maintenance => SystemInformation.first.maintenance,
    })
  end
end
