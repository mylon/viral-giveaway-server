class Api::V1::Provider::NativeXController < Api::V1::Provider::BaseController
  def callback
    offer_id = params[:offer_id]
    user_id  = params[:publisher_user_id]
    dollar   = params[:device_payout]
    
    begin
      cent = dollar.to_f * 100
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :nothing => true, :status => :bad_request and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :nothing => true, :status => :bad_request and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :nativex)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, dollar=#{dollar}, offer_id=#{offer_id}")
        render :nothing => true and return
      end

      result = self.register(user, :nativex, offer_id, cent)
      self.notify(user, result)
    rescue => e
      log_error e
      render :nothing => true, :status => :internal_server_error and return
    end

    render :nothing => true
  end
end
