class Api::V1::Provider::AppDriverController < Api::V1::Provider::BaseController

  def callback
    user_id             = params[:identifier]
    archive_id          = params[:archive_id]
    accepted_time       = params[:accepted_time]
    offer_id         = params[:campaign_id]
    offer_name       = params[:campaign_name]
    advertisement_id    = params[:advertisement_id]
    advertisement_name  = params[:advertisement_name]
    cent                = params[:payment].to_f

    begin
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :text => '0', :layout => false and return
      end

      offer = Offer.where({
        type: Rails.application.config.offer_type_map[:cpi],
        provider: Rails.application.config.offer_provider_map[:app_driver],
        provider_offer_id: offer_id,
      }).first

      if offer.blank? then
        logger.warn("=#{offer_id} is not found")
        render :text => '0', :layout => false and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :text => '0', :layout => false and return
      end

      tracking = Tracking.conversioned?(user, offer.provider_offer_id, :app_driver)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, offer_id=#{offer_id}")
        render :text => '1', :layout => false and return
      end

      result = self.register(user, :app_driver, offer.provider_offer_id, self.payment(offer))

      result[:offer] = offer
      self.notify(user, result)
    rescue => e
      log_error e
      render :text => '999', :layout => false and return
    end

    render :text => '1', :layout => false
  end
end
