class Api::V1::Provider::BaseController < Api::V1::BaseController
  before_action :authenticate_api, except: :callback
  before_action :check_version, except: :callback
  before_action :check_maintenance, except: :callback
  before_action :public_api, only: :callback
  after_action :logging, only: :callback

  def callback
  end

  def logging_cpi_conversion(result, opt)
    dest = {
      :provider      => opt[:provider],
      :offer_id      => opt[:offer_id],
      :user_id       => opt[:user_id],
      :cent          => opt[:cent],
      :point         => opt[:point],
      :exchange_rate => opt[:exchange_rate].to_f,
      :invited       => {},
      :params        => params,
    }

    if iu = result[:invited_user] then
      dest[:invited] = {
        :user_id => iu.id,
        :point => result[:invite_user_granted_point].to_i,
      }
    end
   
    Fluent::Logger.post("vg.cpi_conversion", dest)
  end

  def register(user, provider, offer_id, cent)
    result = {
      :point => 0,
      :invited_user => nil,
      :invited_user_granted_point => 0,
    }
    point = Point.to_point(cent)
    result[:point] = point

    Tracking.transaction do
      tracking = Tracking.track(user, offer_id, provider, :conversioned)
      user.deposit_point(point, :cpi, tracking.id)

      # grant invited user
      invitation = Invitation.where({:to_user => user}).first
      if invitation then
        iu = invitation.from_user
        result[:invited_user] = iu 
        unless Tracking.tracked?(user, iu.id.to_s, :friend_cpi, :conversioned) then
          grant_traking = Tracking.track(user, iu.id.to_s, :friend_cpi, :conversioned)
          grant_point = Rails.application.config.friend_cpi_point_num
          iu.deposit_point(grant_point, :friend_cpi, grant_traking.id)
          result[:invited_user_granted_point] = grant_point 
        end
      end

      # logging 
      self.logging_cpi_conversion(
        result, 
        {
          :offer_id => offer_id,
          :provider => Rails.application.config.offer_provider_map[provider],
          :cent     => cent,
          :point    => point,
          :user_id  => user.id,
        }
      )
    end

    result
  end

  def notify(user, result)
    user.send_email(
      VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
      I18n.t('mail.got_point_subject', :locale => user.locale.to_sym),
      render_to_string( 
                       :template => 'mail/got_point', 
                       :layout => 'mail', 
                       :locals => { 
                          point: result[:point], 
                          action: :cpi, 
                          offer: result[:offer],
                          promotion_offers: Offer.search(user, { :category => 'promotion', :country => user.country, :platform => user.platform, :exclude_installed => true, :limit => Rails.application.config.mail_promotion_offer_num }),
                       }
                      ) 
    )
    Message.register_of_point(user, :cpi, result[:point])

    if iu = result[:invited_user] and result[:invited_user_granted_point]  > 0 then
      iu.send_email(
        VGMailUtil.build_sender(Rails.application.config.ses_noreply_source, Rails.application.config.ses_noreply_name),
        I18n.t('mail.got_point_subject', :locale => iu.locale.to_sym), 
        render_to_string( 
                         :template => 'mail/got_point', 
                         :layout => 'mail', 
                         :locals => { 
                            point: result[:invited_user_granted_point],
                            action: :friend_cpi,
                            promotion_offers: Offer.search(iu, { :category => 'promotion', :country => iu.country, :platform => iu.platform, :exclude_installed => true, :limit => Rails.application.config.mail_promotion_offer_num }),
                         } 
                        )
      ) 
      Message.register_of_point(result[:invited_user], :friend_cpi, result[:invited_user_granted_point]) 
    end
  end

  def payment(offer)
    offer.payment()
  end

  private
    def logging
      Fluent::Logger.post("vg.provider.callback", {
        :params => params,
        :response => {
          :status_code => response.status,
        }
      })
    end
end
