class Api::V1::Provider::SuperRewardsController < Api::V1::Provider::BaseController
  def callback
    id = params[:id]
    uid = params[:uid]
    oid = params[:oid]
    new = params[:new]
    cent = new.to_i
    total = params[:total]
    sig = params[:sig]

    is_valid = Rails.application.config.super_rewards_secret_keys.map{|secret_key| Digest::MD5.hexdigest("#{id}:#{new}:#{uid}:#{secret_key}")}.select{|v| v == sig}.length > 0
    unless is_valid then
      # This script will output a status code of 1 (Success) or 0 (Try sending again later).
      render :text => '0', :layout => false and return
    end

    begin
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :text => '0', :layout => false and return
      end

      user = User.where({:account_id => uid, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{uid} is not found")
        render :text => '0', :layout => false and return
      end

      tracking = Tracking.conversioned?(user, oid, :super_rewards)
      if tracking then
        logger.warn("Already conversioned. id=#{id}, uid=#{uid}, oid=#{oid}, new=#{new}, total=#{total}, sig=#{sig}")
        render :text => '1', :layout => false and return
      end

      result = self.register(user, :super_rewards, oid, cent)
      self.notify(user, result)
    rescue => e
      logger.error("error=#{e.message}")
      render :text => '0', :layout => false and return
    end

    render :text => '1', :layout => false and return
  end
end
