class Api::V1::Provider::TrialPayController < Api::V1::Provider::BaseController
  def callback
    offer_id = params[:oid]
    user_id  = params[:sid]
    dollar   = params[:dollars]
    sig      = request.headers['TrialPay-HMAC-MD5']
  
    # HMAC-MD5 query_string with key
    is_valid = Rails.application.config.trialpay_secret_keys.map{|secret_key| Digest::HMAC.hexdigest(request.env['QUERY_STRING'], secret_key, Digest::MD5)}.select{|v| v == sig}.length > 0
    unless is_valid then
      render :text => 'Invalid Signature', :layout => false, :status => :bad_request and return
    end

    begin
      cent = dollar.to_f * 100
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :text => 'Point is zero', :layout => false, :status => :bad_request and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :text => "User not found. user_id = #{user_id}", :layout => false, :status => :bad_request and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :trialpay)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, cent=#{cent}, offer_id=#{offer_id}")
        render :text => "1", :layout => false and return
      end

      result = self.register(user, :trialpay, offer_id, cent)
      self.notify(user, result)
    rescue => e
      logger.error("error=#{e.message}")
      render :text => "Database Error", :layout => false, :status => :internal_server_error and return
    end

    render :text => "1", :layout => false and return
  end
end
