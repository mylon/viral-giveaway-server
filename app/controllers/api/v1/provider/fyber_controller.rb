class Api::V1::Provider::FyberController < Api::V1::Provider::BaseController

  def callback
    user_id  = params[:uid]
    amount   = params[:amount]
    sid      = params[:sid]
    trans_id = params[:_trans_id_]
    offer_id = params[:pub0]
    pub_str  = ""
    10.times do |i|
      key = "pub#{i}".to_sym
      pub_str += params[key] if params.key?(key)
    end

    is_valid = Rails.application.config.fyber_security_tokens.map{|security_token| Digest::SHA1.hexdigest("#{security_token}#{user_id}#{amount}#{trans_id}#{pub_str}")}.select{|v| v == sid}.length > 0
    unless is_valid then
      logger.warn("invalid security_token")
      render :nothing => true, :status => :bad_request and return
    end

    begin
      # fyber virtual currency -> USD(cent) -> THB
      cent = amount.to_i / Rails.application.config.fyber_point_rate
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :nothing => true, :status => :bad_request and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :nothing => true, :status => :bad_request and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :fyber)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, amount=#{amount}, sid=#{sid}, trans_id=#{trans_id}, offer_id=#{offer_id}")
        render :nothing => true and return
      end

      result = self.register(user, :fyber, offer_id, cent)
      self.notify(user, result)
    rescue => e
      logger.error("error=#{e.message}")
      render :nothing => true, :status => :internal_server_error and return
    end

    render :nothing => true
  end
end
