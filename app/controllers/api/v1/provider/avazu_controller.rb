class Api::V1::Provider::AvazuController < Api::V1::Provider::BaseController
  def callback
    offer_id = params[:offer_id]
    user_id  = params[:user_id]
    provider = :avazu

    begin
      offer = Offer.where({
        type: Rails.application.config.offer_type_map[:cpi],
        provider: Rails.application.config.offer_provider_map[provider],
        provider_offer_id: offer_id,
      }).first

      if offer.blank? then
        logger.warn("offer=#{offer_id} is not found")
        render :nothing => true and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :nothing => true and return
      end

      tracking = Tracking.conversioned?(user, offer.provider_offer_id, provider)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, offer_id=#{offer_id}")
        render :nothing => true and return
      end

      result = self.register(user, provider, offer.provider_offer_id, self.payment(offer))

      result[:offer] = offer
      self.notify(user, result)
    rescue => e
      log_error e
      render :nothing => true, :status => :internal_server_error and return
    end

    render :nothing => true and return
  end
end
