class Api::V1::Provider::PersonalyController < Api::V1::Provider::BaseController
  def callback
    offer_id = params[:offer_id]
    user_id  = params[:user_id]
    amount   = params[:amount]
    cent     = amount.to_i
    sig      = params[:signature]
  
    is_valid = Rails.application.config.personaly_apps.map{|app| Digest::MD5.hexdigest("#{user_id}:#{app[:app_hash]}:#{app[:app_secret]}")}.select{|v| v == sig}.length > 0
    unless is_valid then
      render :text => '0', :layout => false, :status => :bad_request and return
    end

    begin
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :text => '0', :layout => false, :status => :bad_request and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :text => '0', :layout => false, :status => :bad_request and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :personaly)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, cent=#{cent}, offer_id=#{offer_id}")
        render :text => '1', :layout => false, :status => :bad_request and return
      end

      result = self.register(user, :personaly, offer_id, cent)
      self.notify(user, result)
    rescue => e
      logger.error("error=#{e.message}")
      render :text => '0', :layout => false, :status => :internal_server_error and return
    end

    render :text => '1', :layout => false
  end
end
