class Api::V1::Provider::AarkiController < Api::V1::Provider::BaseController
  def callback
    offer_id = params[:id]
    user_id  = params[:user]
    dollar   = params[:dollars]
    reward = params[:units]
    sig      = params[:sha1_signature]
  
    # SHA1_HEX_DIGEST ( CONCATENATE (transaction_id, user_id, reward, secret_key) )
    is_valid = Rails.application.config.aarki_secret_keys.map{|secret_key| Digest::SHA1.hexdigest("#{offer_id}#{user_id}#{reward}#{secret_key}")}.select{|v| v == sig}.length > 0
    unless is_valid then
      render :nothing => true, :status => :bad_request and return
    end

    begin
      cent = dollar.to_f * 100
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :nothing => true, :status => :bad_request and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :nothing => true, :status => :bad_request and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :aarki)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, cent=#{cent}, offer_id=#{offer_id}")
        render :nothing => true and return
      end

      result = self.register(user, :aarki, offer_id, cent)
      self.notify(user, result)
    rescue => e
      logger.error("error=#{e.message}")
      render :nothing => true, :status => :internal_server_error and return
    end

    render :nothing => true and return
  end
end
