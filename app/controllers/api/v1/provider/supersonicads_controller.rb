class Api::V1::Provider::SupersonicadsController < Api::V1::Provider::BaseController
  def callback
    offer_id = params[:event_id]
    user_id  = params[:user_id]
    payment = params[:payment]
    cent     = payment.to_f
    timestamp = params[:timestamp]
    sig = params[:signature]
   
    # md5([TIMESTAMP][EVENT_ID][USER_ID][REWARDS][PRIVATE_KEY]) 
    is_valid = Rails.application.config.supersonicads_private_keys.map{|private_key| Digest::MD5.hexdigest("#{timestamp}#{offer_id}#{user_id}#{payment}#{private_key}")}.select{|v| v == sig}.length > 0
    unless is_valid then
      render :text => "#{offer_id}:NG", :layout => false, :status => :bad_request and return
    end

    begin
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :text => "#{offer_id}:OK", :layout => false and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :text => "#{offer_id}:OK", :layout => false and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :supersonicads)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, cent=#{cent}, offer_id=#{offer_id}")
        render :text => "#{offer_id}:OK", :layout => false and return
      end

      result = self.register(user, :supersonicads, offer_id, cent)
      self.notify(user, result)
    rescue => e
      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
      render :text => "#{offer_id}:NG", :layout => false, :status => :internal_server_error and return
    end

    render :text => "#{offer_id}:OK", :layout => false and return
  end
end
