class Api::V1::Provider::TyrooController < Api::V1::Provider::BaseController
  def callback
    offer_id = params[:campaign_id]
    user_id  = params[:subid1]
    dollar   = params[:payment]
    
    begin
      cent = dollar.to_f * 100
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :nothing => true, :status => :bad_request and return
      end

      offer = Offer.where({
        type: Rails.application.config.offer_type_map[:cpi],
        provider: Rails.application.config.offer_provider_map[:tyroo],
        provider_offer_id: offer_id,
      }).first

      if offer.blank? then
        logger.warn("offer=#{offer_id} is not found")
        render :nothing => true, :status => :bad_request and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :nothing => true, :status => :bad_request and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :tyroo)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, dollar=#{dollar}, offer_id=#{offer_id}")
        render :nothing => true and return
      end

      result = self.register(user, :tyroo, offer_id, cent)
      self.notify(user, result)
    rescue => e
      logger.error("error=#{e.message}")
      render :nothing => true, :status => :internal_server_error and return
    end

    render :nothing => true
  end
end
