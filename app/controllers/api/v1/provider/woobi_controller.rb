class Api::V1::Provider::WoobiController < Api::V1::Provider::BaseController
  def callback
    offer_id       = params[:ad_id]
    user_id        = params[:user_id]
    cent           = params[:payment].to_f
    gross          = params[:gross].to_f
    net            = params[:net].to_f
    transaction_id = params[:transaction_id]
    click_id       = params[:click_id]
    sig            = params[:sig]
    h              = params[:h]
   
    # HASH_SIGNATURE = MD5(HASH_VALUE + SECRET_KEY)
    is_valid = Rails.application.config.woobi_secret_keys.map{|secret_key| Digest::MD5.hexdigest("#{h}#{secret_key}")}.select{|v| v == sig}.length > 0
    unless is_valid then
      render :nothing => true, :status => :bad_request and return
    end

    begin
      if cent <= 0 then
        logger.warn("cent is zero.")
        render :nothing => true, :status => :bad_request and return
      end

      user = User.where({:account_id => user_id, :deleted_at => nil}).first
      if user.blank? then
        logger.warn("user=#{user_id} is not found")
        render :nothing => true, :status => :bad_request and return
      end

      tracking = Tracking.conversioned?(user, offer_id, :woobi)
      if tracking then
        logger.warn("Already conversioned. user_id=#{user_id}, cent=#{cent}, offer_id=#{offer_id}")
        render :nothing => true and return
      end

      result = nil
      Tracking.transaction do
        Tracking.track(user, offer_id, :woobi, :conversioned)
        result = Ticket.registers(user, cent)
        self.logging_cpi_conversion(
          result, 
          {
            :offer_id => offer_id,
            :provider => Rails.application.config.offer_provider_map[:woobi],
            :cent => cent,
            :user_id => user.id,
          }
        )
      end

      result = self.register(user, :woobi, offer_id, cent)
      self.notify(user, result)
    rescue => e
      logger.error("error=#{e.message}")
      render :nothing => true, :status => :internal_server_error and return
    end

    render :nothing => true
  end
end
