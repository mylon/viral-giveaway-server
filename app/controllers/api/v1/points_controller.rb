class Api::V1::PointsController < Api::V1::BaseController
  def count
    render_to_json({ :count => @user.point() })
  end
end
