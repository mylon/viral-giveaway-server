class Api::V1::OffersController < Api::V1::BaseController
  def list 
    option = {
      :category => params[:cat],
      :country => @user.country,
      :platform => @user.platform,
    }

    begin
      offers = Offer.search(@user, option).map{ |o| Offer.convert(o) }.select{ |o| o.present? }
      render_to_json( { 
        :offers => offers, 
      } )
    rescue => e
      log_error e
      render_to_error_json(["Error Please try again"], {:status => :bad_request})
    end
  end

  def recommends
    offers = Offer.fetch_recommends(@user, @user.platform).map{ |o| Offer.convert(o) }.select{ |o| o.present? }
    render_to_json({
      :offers => offers,
    })
  end

  def show_recommend
    begin
      offers = Offer.search(@user, {
        :category          => 'promotion',
        :country           => @user.country,
        :platform          => @user.platform,
        :id                => params[:id],
        :exclude_installed => true,
        :limit             => 1,
      }).map{|o| Offer.convert(o)}
      render_to_json({ :offers => offers })
    rescue => e
      log_error e
      render_to_json({ :offers => [] })
    end
  end
end
