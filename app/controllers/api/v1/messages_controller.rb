class Api::V1::MessagesController < Api::V1::BaseController
  def list
    messages = Message.where({ :user => @user, :viewed_at => nil }).map{|m| Message.convert(m) }
    if messages.length > 0 then
      Message.where({:id => messages.map{|m| m[:id] }}).update_all({:viewed_at => Time.now})
    end
    render_to_json({
      :messages => messages,
    })
  end
end
