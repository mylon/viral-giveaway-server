class Api::V1::CampaignsController < Api::V1::BaseController
  before_action :authenticate_api, except: [:search_by_tag]
  before_action :public_api, only: [:search_by_tag]

  def list
    campaigns = Campaign.search(@user).map{|c| Campaign.convert(c)}
    ticket_counts = Ticket.where({
      :user => @user, 
      :campaign_id => campaigns.map{|c| c[:id]},
    }).group(:campaign_id).count()
    campaigns.each do |c|
      c[:ticket_num] = ticket_counts[c[:id]] || 0
    end
    render_to_json({
      :campaigns => campaigns,
    })
  end

  def result
    results = []
    render_to_json({
      :results => results,
    })
  end

  def apply
    begin
      if errors = Campaign.validate_apply(@user, params) and errors.length > 0 then
        render_to_error_json(errors, {:status => :bad_request}) and return
      end

      campaign = params[:campaign]
      result = nil
      Campaign.transaction do
        result = campaign.apply(@user, params)
      end

      case campaign.type
      when "prize"
        render_to_json({ 
          :ticket => result,
          :point => @user.point(),
        }) and return
      when "dream_photo", "fortune_telling", "line_sticker"
        render_to_json({ 
          :point => @user.point(),
        }) and return
      end
    rescue => e
      log_error e
      render_to_error_json(['ERROR Please retry'], { :status => :internal_server_error }) and return
    end
  end

  def search_by_tag 
    begin
      campaigns = Campaign.search(@user, { :tag => params[:tag].to_s }).map{|c| Campaign.convert(c)}
      render_to_json({ :campaigns => campaigns })
    rescue => e
      log_error e
      render_to_error_json(['ERROR Please retry'], { :status => :internal_server_error }) and return
    end
  end
end
