class Api::BaseController < ApplicationController
  protect_from_forgery with: :null_session

  def render_to_json(data, *args)
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,X-UID,X-API-TOKEN,X-CSRF-TOKEN"

    options = {
      :json => {
        :status => 'success',
        :data => data,
      }
    }

    if args.length > 0 then
      args.each do |a|
        a.each do |k,v|
          options[k] = v
        end
      end
    end

    if options.key?(:status) and options[:status] != :ok
      options[:json][:status] = 'failure'
    end

    render( options )
  end

  def render_to_error_json(messages, *args)
    render_to_json( { :messages => messages }, *args )
  end
end
