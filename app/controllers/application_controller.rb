class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_request_filter, :set_platform

  Fluent::Logger::FluentLogger.open(nil, :host=>Rails.application.config.fluent_host, :port=>Rails.application.config.fluent_port)

  def set_request_filter
    Thread.current[:request] = request
  end  

  def set_platform
    ua = Woothee.parse( request.env["HTTP_USER_AGENT"] )
    Thread.current[:ua] = ua
    case ua[:os]
    when 'iPhone', 'iPad', 'iPod' then
      @platform = Rails.application.config.offer_platform_map[:ios]
    when 'Android' then
      @platform = Rails.application.config.offer_platform_map[:android]
    else
      @platform = 0
    end
    Thread.current[:platform] = @platform
    logger.debug "set_platform ua=#{request.env["HTTP_USER_AGENT"]}, parse=#{ua}, platform=#{@platform}"
  end

  def open_fluent_logger
    Fluent::Logger::FluentLogger.open(nil, :host=>Rails.application.config.fluent_host, :port=>Rails.application.config.fluent_port)
  end

  def static_url(path)
    "#{Rails.application.config.static_host}#{path}"
  end

  def log_error(e)
    logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
  end

  def render_to_json(data, *args)
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,X-UID,X-API-TOKEN,X-CSRF-TOKEN"

    options = {
      :json => {
        :status => 'success',
        :data => data,
      }
    }

    if args.length > 0 then
      args.each do |a|
        a.each do |k,v|
          options[k] = v
        end
      end
    end

    if options.key?(:status) and options[:status] != :ok
      options[:json][:status] = 'failure'
    end

    render( options )
  end

  def render_to_error_json(messages, *args)
    render_to_json( { :messages => messages }, *args )
  end
end
