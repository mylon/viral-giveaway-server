class NightlifeUserRequest < ActiveRecord::Base
	belongs_to :nightlife_user

	def self.register(from_user_id, to_user_id, is_match=0)
		now = Time.now
		register = self.new(     
	      :from_user_id => from_user_id,	     
	      :to_user_id => to_user_id,	
	      :is_match => is_match,     
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    register
	end
end
