class NewsPicksSource < ActiveRecord::Base
	def self.register(source_url, region, city_id)
	    now = Time.now
	    news = self.new(     
	      :source_url => source_url,	     
	      :region => region,	
	      :city_id => city_id,     
	      :deleted_at => nil,
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    news
  	end
end
