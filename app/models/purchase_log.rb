class PurchaseLog < ActiveRecord::Base
  belongs_to :user

  def register(user, provider_key, transaction_id, receipt, item, item_id)
    PurchaseLog.create(
      user: user, 
      platform: Rails.application.config.billing_provider_map[provider_key], 
      transaction_id: transaction_id, 
      receipt: receipt,
      item: item,
      item_id: item_id)
  end

  def update_item_status
    case self.item
    when "dream_photo"
      dp = DreamPhoto.find(self.item_id)
      raise ArgumentError, "Invalid User" unless dp.user_id == self.user_id
      dp.billing_status = Rails.application.config.billing_status_map[:complete]
      dp.save!
    else
      raise ArgumentError, "Invalid Item"
    end
  end

  # see http://stackoverflow.com/questions/18230317/validate-in-app-purchase-android-google-on-server-side
  # see https://developer.android.com/google/play/developer-api.html#subscriptions_api_overview
  def self.verify_google_receipt(receipt, signature)
    order = ["orderId", "packageName", "productId", "purchaseTime", "purchaseState", "purchaseToken"]
    #data = Hash[order.map{|o|} [o, receipt[o]]] 
    data = {}
    order.each do |o|
      data[o] = receipt[o]
    end
    key = OpenSSL::PKey::RSA.new(Base64.decode64(Rails.application.config.google_billing_public_key))
    verified = key.verify(OpenSSL::Digest::SHA1.new, Base64.decode64(signature), data.to_json)

    if verified then
      #TODO use Purchase Status API.
      true
    else
      false
    end
  end

  # see https://developer.apple.com/library/ios/releasenotes/General/ValidateAppStoreReceipt/Chapters/ValidateRemotely.html#//apple_ref/doc/uid/TP40010573-CH104-SW1
  def self.verify_apple_receipt(base64_receipt)
    host = Rails.env.production? ? 'https://buy.itunes.apple.com' : 'https://sandbox.itunes.apple.com'
    conn = Faraday.new(:url => host) do |c|
      c.use FaradayMiddleware::ParseJson, content_type: 'application/json'
      c.use Faraday::Request::UrlEncoded
      c.use Faraday::Adapter::NetHttp
    end
    response = conn.post '/verifyReceipt', {'receipt-data' => base64_receipt}
    if response['status'] == 0 then
      true
    else
      false
    end 
  end
end
