class NightlifeUser < ActiveRecord::Base
	  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :omniauthable
  devise :omniauthable

  ##########################################################
  # validate
  ##########################################################
  def self.validate_register_by_auth(auth)
    errors = []

    user = self.find_by_auth(auth)
    if user then
      return []
    end
  
    email = auth[:extra][:raw_info][:email].to_s
    user = NightlifeUser.where({ :canonical_email => email.downcase, :deleted_at => nil }).first
    if user then
      errors.push(I18n.t('error.user.email_exists'))
    end

    errors
  end

  def validate_profile(params)
    errors = []

    if params.key?(:name) then
      name = params[:name].strip
      if name.blank? then
        errors.push(I18n.t('error.user.name_blank'))
      elsif name.length > Rails.application.config.name_length_max then
        errors.push(I18n.t('error.user.name_length_max', length: Rails.application.config.name_length_max))
      end
    end

    if params.key?(:email) then
      email = params[:email].to_s.strip
      if email.blank? then
        errors.push(I18n.t('error.user.email_blank'))
      elsif not email.match(Rails.application.config.email_regex) then
        errors.push(I18n.t('error.user.email_format_invalid'))
      elsif email.length > Rails.application.config.email_length_max then
        errors.push(I18n.t('error.user.email_length_max', length:Rails.application.config.email_length_max))
      elsif NightlifeUser.where({ :deleted_at => nil, :canonical_email => params[:email].to_s.downcase }).where("id != ?", self.id).first then
        errors.push(I18n.t('error.user.email_exists'))
      end
    end

    if params.key?(:birthdate) then
      birthdate = params[:birthdate].strip
      if birthdate.blank? then
        errors.push(I18n.t('error.user.birthdate_blank'))
      else
        begin
          date = Date.strptime(birthdate, Rails.application.config.birthdate_format)
          params[:birthdate_date] = date
        rescue ArgumentError
          errors.push(I18n.t('error.user.birthdate_format_invalid'))
        end
      end
    end

    if params.key?(:gender) then
      unless Rails.application.config.gender_symbol_map[params[:gender].to_i] then
        errors.push(I18n.t('error.user.gender_invalid'))
      end
    end

    if params.key?(:notification) then
      notification = params[:notification]
      unless notification.is_a?(TrueClass) or notification.is_a?(FalseClass) then
        errors.push(I18n.t('error.user.notification_invalid'))
      end
    end

    if params[:current_password].to_s.length > 0 or params[:new_password].to_s.length > 0 or params[:password_confirm].to_s.length > 0 then
      current_password_hash = NightlifeUser.encrypt_password(params[:current_password]) if params[:current_password]
      if self.encrypted_password and self.encrypted_password != current_password_hash.to_s then
        errors.push(I18n.t('error.user.password_invalid'))
      end

      password_len = params[:new_password].to_s.length
      if password_len < Rails.application.config.password_length_min or password_len > Rails.application.config.password_length_max then
        errors.push(I18n.t('error.user.password_length', min: Rails.application.config.password_length_min, max: Rails.application.config.password_length_max))
      elsif params[:new_password].to_s != params[:password_confirm].to_s then
        errors.push(I18n.t('error.user.password_confirm'))
      end
    end

    errors
  end

  def self.validate_register(params)
    errors = []

    if params.key?(:email) then
      email = params[:email].strip
      if email.blank? then
        errors.push(I18n.t('error.user.email_blank'))
      elsif not email.match(Rails.application.config.email_regex) then
        errors.push(I18n.t('error.user.email_format_invalid'))
      elsif email.length > Rails.application.config.email_length_max then
        errors.push(I18n.t('error.user.email_length_max', length:Rails.application.config.email_length_max))
      elsif NightlifeUser.where({:canonical_email => params[:email].to_s.downcase, :deleted_at => nil}).first then
        errors.push(I18n.t('error.user.email_exists'))
      end
    else
      errors.push(I18n.t('error.user.email_blank'))
    end

    password_len = params[:password].to_s.length
    if password_len < Rails.application.config.password_length_min or password_len > Rails.application.config.password_length_max then
      errors.push(I18n.t('error.user.password_length', min: Rails.application.config.password_length_min, max: Rails.application.config.password_length_max))
    end

    errors
  end
  
  def validate_unregister(params)
    errors = []

    unless params[:question_answer].to_i > 0 then
      errors.push(I18n.t('error.delete_account.question_answer_invalid'))
    end

    unless self.encrypted_password == NightlifeUser.encrypt_password(params[:password].to_s) then
      errors.push(I18n.t('error.delete_account.password_incorrect'))
    end

    errors
  end

  ##########################################################
  # /validate
  ##########################################################

  def self.find_by_account_id(account_id)
    NightlifeUser.where({
      :deleted_at => nil,
      :account_id => account_id
    }).first
  end

  def self.register(params)
    nightlife_user = NightlifeUser.new(
      :account_id        => '', 
      :token             => self.generate_token(), 
      :locale            => I18n.locale,
      :name              => params[:name].to_s, 
      :email             => params[:email].to_s,
      :canonical_email   => params[:email].to_s.downcase,
      :country           => params[:country].to_s,
      :provider          => params[:provider],
      :uid               => params[:uid],
      :birthdate         => params[:birthdate],
      :gender            => params[:gender],
      :notification      => true,
      :verification_code => params[:verification_code],
      :verified_at       => params[:verified_at],
      :last_accessed_at  => Time.now,
      :platform          => Thread.current[:platform].to_i,
      :an_source => (params[:an_source] or Thread.current[:source]).to_s,
      :an_referral_code => params[:an_referral_code].to_s,   
    )
    nightlife_user.country = self.country_iso_from_request() unless params[:country]
    nightlife_user.encrypted_password = NightlifeUser.encrypt_password(params[:password]) if params.key?(:password)

    nightlife_user.tag_line = params[:tag_line] if params.key?(:tag_line)
    nightlife_user.show_men = params[:show_men] if params.key?(:show_men)
    nightlife_user.show_women = params[:show_women] if params.key?(:show_women)
    nightlife_user.device_udid = params[:device_udid] if params.key?(:device_udid)
    nightlife_user.is_dummy = params[:is_dummy] if params.key?(:is_dummy)
    nightlife_user.match_notification = params[:match_notification] if params.key?(:match_notification)
    nightlife_user.message_notification = params[:message_notification] if params.key?(:message_notification)
    nightlife_user.like_notification = params[:like_notification] if params.key?(:like_notification)
    nightlife_user.is_online = params[:is_online] if params.key?(:is_online)
    nightlife_user.platform = params[:platform] if params.key?(:platform)


    nightlife_user.save!
    nightlife_user.account_id = (Rails.application.config.initial_user_id + nightlife_user.id).to_s
    nightlife_user.save!

    #UserApp.register(user, Thread.current[:app])

    nightlife_user
  end

  def unregister
    self.deleted_at = Time.now
    self.save!
  end

  def register_invite_friend(invite_friend_id)
    self.an_referral_code = invite_friend_id.to_s
    self.save!

    invitation = nil
    u = NightlifeUser.where({:account_id => invite_friend_id.to_s, :deleted_at => nil}).first
    if u then
      if Invitation.available?(u, self) then
        invitation = Invitation.register(u, self)
        point_num = Rails.application.config.invite_point_num
        u.deposit_point(point_num, :invitation, self.id)
        Message.register_of_point(u, :invitation, point_num)
      end
    end

    invitation
  end

  def self.find_by_auth(auth)
    NightlifeUser.where({
      :canonical_email => auth[:extra][:raw_info][:email].downcase,
      :provider   => auth[:provider], 
      :uid        => auth[:uid], 
      :deleted_at => nil, 
    }).first
  end

  def self.find_by_email(email)
    NightlifeUser.where({
      :canonical_email => email.to_s.downcase,
      :deleted_at => nil,
    }).where("verified_at IS NOT NULL").first
  end

  def self.find_by_email_and_password(email, password)
    NightlifeUser.where({
      :canonical_email => email.to_s.downcase,
      :encrypted_password => NightlifeUser.encrypt_password(password),
      :deleted_at => nil,
    }).where("verified_at IS NOT NULL").first
  end

  def self.register_by_email(email, password, opt={})
    params = {
      :email             => email,
      :canonical_email   => email.downcase,
      :password          => password,
      :verification_code => NightlifeUser.generate_verification_code(),
      :provider          => 'nl',
      :uid               => '',
      :an_referral_code => opt[:referral_code],
    }

    self.register(params)
  end

  def self.register_by_auth(auth, opt={})
    user = self.find_by_auth(auth)
    if user then
      user.platform = Thread.current[:platform].to_i
      user.save!
      return user
    end

    birthdate = nil
    gender = Rails.application.config.gender_map[auth[:extra][:raw_info][:gender].to_s.to_sym] 
    case auth[:provider]
    when 'facebook'
      begin
        if auth[:extra][:raw_info][:birthday].to_s.length > 0 then
          birthdate = Date.strptime(auth[:extra][:raw_info][:birthday].to_s, Rails.application.config.birthdate_format)
        end
      rescue => e
        logger.warn "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}" 
      end
    end

    params = {
      :name        => auth[:info][:name],
      :email       => auth[:extra][:raw_info][:email],
      :canonical_email => auth[:extra][:raw_info][:email].downcase,
      :provider    => auth[:provider],
      :uid         => auth[:uid],
      :birthdate   => birthdate,
      :gender      => gender,
      :verified_at => Time.now,
      :an_source   => opt[:source],
      :an_referral_code => opt[:referral_code],
    }

    self.register(params)
  end

  def self.register_from_app(params)     
    where = { 
      :deleted_at => nil, 
      :provider => params[:provider],
      :platform => params[:platform]
    }
    where[:canonical_email] = params[:email].downcase if params[:canonical_email].to_s != ''

    case params[:provider].to_s
    when "facebook", "google", "twitter"
      where[:uid] = params[:provider_user_id]
    when "vg"
      where[:encrypted_password] = encrypt_password(params[:password])
    when "nl"
      where[:encrypted_password] = encrypt_password(params[:password])
    end

    nightlife_user = NightlifeUser.where(where).first
    if nightlife_user then
      nightlife_user.platform = Thread.current[:platform].to_i
      nightlife_user.tag_line = params[:tag_line] if params.key?(:tag_line)
      nightlife_user.show_men = params[:show_men] if params.key?(:show_men)
      nightlife_user.show_women = params[:show_women] if params.key?(:show_women)
      nightlife_user.device_udid = params[:device_udid] if params.key?(:device_udid)
      nightlife_user.is_dummy = params[:is_dummy] if params.key?(:is_dummy)
      nightlife_user.match_notification = params[:match_notification] if params.key?(:match_notification)
      nightlife_user.message_notification = params[:message_notification] if params.key?(:message_notification)
      nightlife_user.like_notification = params[:like_notification] if params.key?(:like_notification)
      nightlife_user.is_online = params[:is_online] if params.key?(:is_online)
      nightlife_user.platform = params[:platform] if params.key?(:platform)
      nightlife_user.gender = params[:gender] if params.key?(:gender)
      nightlife_user.save!
      return nightlife_user
    end

    register_params = {
      email: params[:email],
      provider: params[:provider],
      uid: params[:provider_user_id].to_s,
    }
    case params[:provider].to_s
    when "facebook"
      u = params[:user]
      register_params[:name] = params[:user].name
      register_params[:gender] = Rails.application.config.gender_map[u.gender.to_sym] if u.gender
      register_params[:birthdate] = u.birthday if u.birthday 
      register_params[:verified_at] = Time.now
    when "google"
      u = params[:user]
      register_params[:name] = u.display_name
      register_params[:gender] = Rails.application.config.gender_map[u.gender.to_sym] if u.attributes["gender"] 
      register_params[:verified_at] = Time.now
    when "twitter"
      u = params[:user]
      register_params[:name] = u.name    
      register_params[:verified_at] = Time.now
    when "vg"
      register_params[:verification_code] = generate_app_verification_code()
      register_params[:password] = params[:password]
    when "nl"
      register_params[:verification_code] = generate_app_verification_code()
      register_params[:password] = params[:password]
      register_params[:verified_at] = Time.now
    end

    register_params[:tag_line] = params[:tag_line] if params.key?(:tag_line)
    register_params[:show_men] = params[:show_men] if params.key?(:show_men)
    register_params[:show_women] = params[:show_women] if params.key?(:show_women)
    register_params[:device_udid] = params[:device_udid] if params.key?(:device_udid)
    register_params[:is_dummy] = params[:is_dummy] if params.key?(:is_dummy)
    register_params[:match_notification] = params[:match_notification] if params.key?(:match_notification)
    register_params[:message_notification] = params[:message_notification] if params.key?(:message_notification)
    register_params[:like_notification] = params[:like_notification] if params.key?(:like_notification)
    register_params[:is_online] = params[:is_online] if params.key?(:is_online)
    register_params[:platform] = params[:platform] if params.key?(:platform)

    self.register(register_params) 
  end

  def self.verify(account_id, code)
    logger.info "verify account_id=#{account_id}, code=#{code}"
    user = NightlifeUser.where({
      :account_id => account_id,
      :verification_code => code,
      :deleted_at => nil,
    }).first
    return unless user

    if user.verified_at.nil? then
      user.verified_at = Time.now
      user.save!
    end

    user
  end

  def self.generate_token()
    arr = ((0..9).to_a + ("a".."z").to_a + ("A".."Z").to_a)
    n = Rails.application.config.token_length
    (1..n).to_a.map{arr[rand(arr.length)]}.shuffle.join
  end

  def self.generate_verification_code()
    arr = ((0..9).to_a + ("a".."z").to_a + ("A".."Z").to_a)
    n = Rails.application.config.verification_code_length
    (1..n).to_a.map{arr[rand(arr.length)]}.shuffle.join
  end

  def self.generate_app_verification_code()
    arr = ((0..9).to_a)
    n = Rails.application.config.app_verification_code_length
    (1..n).to_a.map{arr[rand(arr.length)]}.shuffle.join
  end

  def self.age(birthdate, now=Date.today)
    now.year - birthdate.year - (birthdate.to_date.change(:year => now.year) > now ? 1 : 0)
  end

  def self.generate_password()
    arr = ((0..9).to_a + ("a".."z").to_a + ("A".."Z").to_a)
    n = Rails.application.config.password_reset_length
    (1..n).to_a.map{arr[rand(arr.length)]}.shuffle.join
  end

  def self.encrypt_password(password)
    Digest::SHA512.hexdigest(password)
  end

  def send_email!(from, subject, body, opt={})
    opt[:force] = true
    self.send_email(from, subject, body, opt)
  end

  def send_email(from, subject, body, opt={})
    return unless opt[:force] or self.notification
    return if !!opt[:force] == false and self.deleted_at.present?

    if opt[:wait] then
      wait = opt[:wait]
      opt.delete(:wait)
      SendmailJob.set(wait: wait).perform_later(from, [self.email], subject, body, opt)
    else
      SendmailJob.perform_later(from, [self.email], subject, body, opt)
    end
  end

  def send_raw_email(from, subject, body, opt={})
    return unless opt[:force] or self.notification
    return if !!opt[:force] == false and self.deleted_at.present?

    opt[:raw] = true
    
    mail = Mail.new
    mail.to = self.email
    mail.from = from
    mail.subject = subject
    mail.html_part = Mail::Part.new do
      content_type 'text/html; charaset=UTF-8'
      body body
    end
    opt.delete(:attachments).to_h.each {|k,v| mail.attachments[k] = v }
    
    ses = Aws::SES::Client.new(
      region: Rails.application.config.ses_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )
    resp = ses.send_raw_email({
      raw_message: {
        data: mail.to_s
      }
    })
    logger.info "message_id=#{resp.message_id}"
  end
  
  def send_raw_email!(from, subject, body, opt={})
    opt[:force] = true
    self.send_raw_email(from, subject, body, opt)
  end

  def self.country_iso_from_request
    retry_num = 0
    begin
      res = Thread.current[:request].location
      return res.country_code.downcase if res
      retry_num += 1
    end while retry_num < Rails.application.config.geocoder_retry_num
  end

  # helpers for point
  
  def point
    p = Point.where({:user => self}).order('id desc').first
    p.nil? ? 0 : p.balance
  end

  def deposit_point(point, item_type_key, item_id)
    Point.deposit(self, point, nil, item_type_key, item_id)
  end

  def withdraw_point(point, item_type_key, item_id)
    Point.withdraw(self, point, nil, item_type_key, item_id)
  end

  def transfer_point(to_user, point, item_type_key, item_id)
    Point.transfer(self, to_user, point, item_type_key, item_id)
  end

  def can_withdraw?(point)
    Point.can_withdraw?(self, point)
  end

  def self.find_by_mail_segment(segment)
    users = []
    case segment
    when :all
      users = NightlifeUser.where({
        :deleted_at => nil
      })
    when :only_register
      where =<<-"SQL"
      deleted_at IS NULL AND users.id NOT IN (
        SELECT t.user_id FROM trackings AS t WHERE t.type in (?)
      )
      SQL
      exclude_types = [
        Rails.application.config.tracking_type_map[:app_driver],
        Rails.application.config.tracking_type_map[:adatha],
        Rails.application.config.tracking_type_map[:facebook_share],
        Rails.application.config.tracking_type_map[:twitter_share],
        Rails.application.config.tracking_type_map[:avazu],
        Rails.application.config.tracking_type_map[:mobvista],
        Rails.application.config.tracking_type_map[:facebook_like],
        Rails.application.config.tracking_type_map[:twitter_follow],
      ]
      users = NightlifeUser.where(where, exclude_types)
    when :only_action
      where =<<-"SQL"
      deleted_at IS NULL AND users.id NOT IN (
        SELECT t.user_id FROM trackings AS t WHERE t.type in (?)
      ) AND users.id IN (
        SELECT t.user_id FROM trackings AS t WHERE t.type in (?)
      )
      SQL
      exclude_types = [
        Rails.application.config.tracking_type_map[:app_driver],
        Rails.application.config.tracking_type_map[:adatha],
        Rails.application.config.tracking_type_map[:avazu],
        Rails.application.config.tracking_type_map[:mobvista],
      ]
      include_types = [
        Rails.application.config.tracking_type_map[:facebook_share],
        Rails.application.config.tracking_type_map[:twitter_share],
        Rails.application.config.tracking_type_map[:facebook_like],
        Rails.application.config.tracking_type_map[:twitter_follow],
      ]
      users = NightlifeUser.where(where, exclude_types, include_types)
    when :done_cpi_have_promotional_offer
      where =<<-"SQL"
      deleted_at IS NULL AND users.id IN (
        SELECT t.user_id FROM trackings AS t WHERE t.type in (?)
      )
      SQL
      include_types = [
        Rails.application.config.tracking_type_map[:app_driver],
        Rails.application.config.tracking_type_map[:adatha],
        Rails.application.config.tracking_type_map[:avazu],
        Rails.application.config.tracking_type_map[:mobvista],
      ]
      users = NightlifeUser.where(where, include_types).each do |u|
        promotion_offers = Offer.search(user, {
          :category => 'promotion',
          :country => u.country,
          :platform => u.platform,
          :exclude_installed => true,
        })
        next if promotion_offers.length == 0

        u
      end
    when :done_cpi_not_have_promotional_offer
      where =<<-"SQL"
      deleted_at IS NULL AND users.id IN (
        SELECT t.user_id FROM trackings AS t WHERE t.type in (?)
      )
      SQL
      include_types = [
        Rails.application.config.tracking_type_map[:app_driver],
        Rails.application.config.tracking_type_map[:adatha],
        Rails.application.config.tracking_type_map[:avazu],
        Rails.application.config.tracking_type_map[:mobvista],
      ]
      users = NightlifeUser.where(where, include_types).each do |u|
        promotion_offers = Offer.search(user, {
          :category => 'promotion',
          :country => u.country,
          :platform => u.platform,
          :exclude_installed => true,
        })
        next if promotion_offers.length > 0

        u
      end
    end
    users
  end

  def include_segment?(segment)
    case segment
    when :only_register
      exclude_types = [
        Rails.application.config.tracking_type_map[:app_driver],
        Rails.application.config.tracking_type_map[:adatha],
        Rails.application.config.tracking_type_map[:facebook_share],
        Rails.application.config.tracking_type_map[:twitter_share],
        Rails.application.config.tracking_type_map[:avazu],
        Rails.application.config.tracking_type_map[:mobvista],
        Rails.application.config.tracking_type_map[:facebook_like],
        Rails.application.config.tracking_type_map[:twitter_follow],
      ]
      Tracking.where({:user_id => self.id, type: exclude_types}).count() == 0
    when :only_action
      exclude_types = [
        Rails.application.config.tracking_type_map[:app_driver],
        Rails.application.config.tracking_type_map[:adatha],
        Rails.application.config.tracking_type_map[:avazu],
        Rails.application.config.tracking_type_map[:mobvista],
      ]
      include_types = [
        Rails.application.config.tracking_type_map[:facebook_share],
        Rails.application.config.tracking_type_map[:twitter_share],
        Rails.application.config.tracking_type_map[:facebook_like],
        Rails.application.config.tracking_type_map[:twitter_follow],
      ]
      Tracking.where({:user_id => self.id, type: exclude_types}).count() == 0 and Tracking.where({:user_id => self.id, type: include_types}).count() > 0
    else
      false
    end
  end

  def last_cpi_tracking
    cpi_types = [
      Rails.application.config.tracking_type_map[:app_driver],
      Rails.application.config.tracking_type_map[:adatha],
      Rails.application.config.tracking_type_map[:avazu],
      Rails.application.config.tracking_type_map[:mobvista],
    ]
    Tracking.where({:user_id => self.id, :type => cpi_types}).order("created_at desc").first
  end
end
