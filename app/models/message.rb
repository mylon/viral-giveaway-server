class Message < ActiveRecord::Base
  self.inheritance_column = :_type_disabled
  belongs_to :user

  def self.register(user, type_key, title, content, point_num=nil)
    Message.create(
      :user => user,
      :type => Rails.application.config.message_type_map[type_key],
      :title => title,
      :content => content,
      :point_num => point_num,
    )
  end

  def self.register_of_point(user, type_key, point_num)
    self.register(user, type_key, '', '', point_num)
  end

  def self.convert(src)
    dest = {
      :id => src.id,
      :type => Rails.application.config.message_type_symbol_map[src.type],
      :point => src.point_num,
    }

    dest
  end
end
