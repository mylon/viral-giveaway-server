class LocalPushNotificationLog < ActiveRecord::Base
	def self.register(device_id, message="see our updates", title=nil, news_link=nil, image_link=nil, category_id=nil)
		now = Time.now
	    push = self.new(     
	      :device_id => device_id,	     
	      :message => message,	
	      :title => title,     
	      :news_link => news_link,  
	      :image_link => image_link,  
	      :category_id => category_id,  
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    push
	end
end
