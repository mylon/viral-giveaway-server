class FortuneTellingResult < ActiveRecord::Base
  # Don't use _jp column. see VG-338
  def self.bulk_load(csv_data)
    csv_data.split(/\r?\n/).each_with_index do |line,i|
      next if i == 0

      data = parse(line)
      r = FortuneTellingResult.where({target_date: data['date'], constellation: data['constellation']}).first_or_initialize(target_date: data['date'])
      %w(love money work).each do |k|
        r.send("#{k}_text_en=".to_sym, data["#{k}_Text"]) 
        r.send("#{k}_text_jp=".to_sym, "") unless r.send("#{k}_text_jp".to_sym)
        r.send("#{k}_point_100=".to_sym, data["#{k}_Point_100"]) 
        r.send("#{k}_point_5a=".to_sym, data["#{k}_Point_5A"]) 
        r.send("#{k}_point_5b=".to_sym, data["#{k}_Point_5B"]) 
        r.send("#{k}_point_10=".to_sym, data["#{k}_Point_10"]) 
      end
      r.total_text_en = data['total_Text']
      r.total_text_jp = "" unless r.total_text_jp
      r.total_point_100 = data['total_Point_100'] 
      %w(direction color food fashion item person place).each do |k|
        r.send("lucky_#{k}_en=".to_sym, data["lucky_#{k}"]) 
        r.send("lucky_#{k}_jp=".to_sym, "") unless r.send("lucky_#{k}_jp".to_sym)
      end
      r.lucky_number = data['lucky_number'] 
      r.save!
    end
  end

  def self.parse(line)
    logger.debug "line=#{line}"
    data = {}
    line.split(",").each_with_index do |part,i|
      data[Rails.application.config.fortune_telling_csv_columns[i]] = part
    end
    logger.debug "data=#{data.inspect}"
    data['date'] = Date.strptime(data['date'], Rails.application.config.fortune_telling_csv_date_format)
    data
  end

  def self.ready?(date)
    FortuneTellingResult.where({target_date:date}).map{|r|
      keys = %w(love money work total)
      keys.map{|k| r.send("#{k}_text_en".to_sym).length > 0}.select{|k| k}.length == keys.length
    }.select{|k| k}.length == 12
  end
end
