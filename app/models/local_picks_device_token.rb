class LocalPicksDeviceToken < ActiveRecord::Base

	def self.register(device_type, device_token, device_notification="Y", unique_device_id="0")
	    LocalPicksDeviceToken.where({
	    	device_token: device_token, 
	    	device_type: device_type
	    }).first_or_create.update(
		    device_type: device_type, 
		    device_token: device_token,
		    device_notification: device_notification,
		    unique_device_id: unique_device_id 
	    )
	end

	def self.register_android(device_type, device_token, device_notification="Y", unique_device_id="0")
	    LocalPicksDeviceToken.where({
	    	device_type: device_type,
	    	unique_device_id: unique_device_id 
	    }).first_or_create.update(
		    device_type: device_type, 
		    device_token: device_token,
		    device_notification: device_notification,
		    unique_device_id: unique_device_id 
	    )
	end
end
