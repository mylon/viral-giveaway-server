class NightlifeGroupUser < ActiveRecord::Base
	belongs_to :nightlife_user
	belongs_to :nightlife_group

	def self.register(group_id, user_id, is_temp=0)
		now = Time.now
		nightlifegroupuser = self.new(     
	      :group_id => group_id,	     
	      :user_id => user_id,	
	      :is_temp => is_temp,
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    nightlifegroupuser
	end
end
