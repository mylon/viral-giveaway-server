class NightlifeNearbyClub < ActiveRecord::Base
	def self.register(club_item_id, club_title, club_url, club_image)
		now = Time.now
	    club = self.new(     
	      :club_item_id => club_item_id,	     
	      :club_title => club_title,	
	      :club_url => club_url,     
	      :club_image => club_image,  
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    club
	end
end
