class NewsPick < ActiveRecord::Base
	attr_accessor :bookmark

	def self.register(title, category_id, region, news_url, image_url=nil, city_id=0, publish_status=1, tags=nil, hot_news=0, publishers=nil, web_url=nil, news_promotion='N', news_event="N", news_description=nil, news_staff_checked=1, classified_sub_category=0, news_pick_shop=nil, show_in_list=1, created_by=nil, updated_by=nil, is_magazine)
	    now = Time.now
	    news = self.new(     
	      :title => title,
	      :category_id => category_id,
	      :region => region,
	      :news_url => news_url,
	      :image_url => image_url,
	      :city_id => city_id,
	      :publish_status => publish_status,
	      :deleted_at => nil,
	      :created_at => now,
	      :updated_at => now,
	      :tags => tags.gsub(/\s+/, ""),
	      :hot_news => hot_news,
	      :publishers => publishers,
	      :web_url => web_url,
	      :news_promotion => news_promotion,
	      :news_event => news_event,
	      :news_description => news_description,
	      :news_staff_checked => news_staff_checked,
	      :classified_sub_category => classified_sub_category,
	      :news_pick_shop => news_pick_shop,
	      :show_in_list => show_in_list,
	      :created_by => created_by,
	      :updated_by => updated_by,
	      :is_magazine => is_magazine,
	    ).save!	 
	    
	    news
  	end
end


