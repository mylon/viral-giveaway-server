class FacebookPageDatum < ActiveRecord::Base
	def self.register(facebook_page_id, facebook_page_name, facebook_page_url, facebook_page_image, facebook_page_category, twitter_link, instagram_link, google_link)
	    FacebookPageDatum.where({
	    	facebook_page_id: facebook_page_id
	    }).first_or_create.update(
		    facebook_page_id: facebook_page_id, 
		    facebook_page_name: facebook_page_name, 
		    facebook_page_url: facebook_page_url, 
		    facebook_page_image: facebook_page_image, 
		    facebook_page_category: facebook_page_category,
		    twitter_link: twitter_link,
		    instagram_link: instagram_link,
		    google_link: google_link,
	    )
	end
end
