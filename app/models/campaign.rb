class Campaign < ActiveRecord::Base
  self.inheritance_column = :_type_disabled

  ##########################################################
  # validate
  ##########################################################
  
  def self.validate_apply(user, params)
    errors = []

    campaign = Campaign.where({ :id => params[:id].to_i, :deleted_at => nil }).first
    if campaign.nil? then
      errors.push(I18n.t('error.campaign.not_found'))
      return errors
    end
    params[:campaign] = campaign

    now = Time.now
    unless campaign.start_at <= now and now < campaign.end_at then
      errors.push(I18n.t('error.campaign.already_finish'))
    end 

    if Thread.current[:app].code == 'vg' then
      point_cost = campaign.point_cost
      point_cost = line_coin_to_point(params[:coin].to_i) if campaign.type == 'line_sticker'
      unless user.can_withdraw?(point_cost) then
        errors.push(I18n.t('error.point.insufficient'))
      end
    end

    case campaign.type
    when "dream_photo"
      params[:photo1_data] = URI::Data.new(params[:photo1])
      if params[:photo1_data].nil? or not ( /^image\/.*$/.match(params[:photo1_data].content_type) ) then
        errors.push(I18n.t('error.campaign.invalid_photo'))
      end
      params[:photo2_data] = URI::Data.new(params[:photo2])
      if params[:photo2_data].nil? or not ( /^image\/.*$/.match(params[:photo2_data].content_type) ) then
        errors.push(I18n.t('error.campaign.invalid_photo'))
      end
    when "fortune_telling"
      if FortuneTellingUser.where({user:user}).first then
        errors.push(I18n.t('error.campaign.already_applied'))
      end

      if params.key?(:birthdate) then
        birthdate = params[:birthdate].strip
        if birthdate.blank? then
          errors.push(I18n.t('error.user.birthdate_blank'))
        else
          begin
            date = Date.strptime(birthdate, Rails.application.config.birthdate_format)
            params[:birthdate_date] = date
          rescue ArgumentError
            errors.push(I18n.t('error.user.birthdate_format_invalid'))
          end
        end
      end
    when "line_sticker"
      errors.push(I18n.t('error.campaign.invalid_line_id')) if params[:line_id].to_s.length == 0 
      errors.push(I18n.t('error.campaign.invalid_sticker_name')) if params[:sticker_name].to_s.length == 0
      errors.push(I18n.t('error.campaign.invalid_coin')) unless %w(50 100).include?(params[:coin].to_s)
    end

    errors
  end

  ##########################################################
  # /validate
  ##########################################################

  def apply(user, params)
    case self.type
    when "prize"
      ticket = Ticket.register(self.id, user)
      user.withdraw_point(self.point_cost, :ticket, ticket.id)
      VGLogger.post({ path: '/v1/campaigns/apply', ticket_id: ticket.id })
      ticket
    when "dream_photo"
      dp = DreamPhoto.create(
        :user => user,
        :photo_type => params[:photo_type].to_i,
        :comment => params[:comment].to_s,
      )
      dp.upload_user_photo(params[:photo1_data])
      dp.upload_base_photo(params[:photo2_data])
      dp.user_photo_url = dp.generate_filename(params[:photo1_data], "user")
      dp.base_photo_url = dp.generate_filename(params[:photo2_data], "base")
      dp.save!

      user.withdraw_point(self.point_cost, :dream_photo, dp.id)
      VGLogger.post({ path: '/v1/campaigns/apply', dream_photo_id: dream_photo.id })
      dp
    when "fortune_telling"
      ft = FortuneTellingUser.create(user: user, birthdate: params[:birthdate_date], constellation: FortuneTellingUser.constellation(params[:birthdate_date]))
      user.withdraw_point(self.point_cost, :fortune_telling, ft.id)
      ft
    when "line_sticker"
      sticker = LineStickerRequest.create(
        user: user, 
        line_id: params[:line_id], 
        sticker_name: params[:sticker_name],
        coin: params[:coin].to_i,
        status: Rails.application.config.line_sticker_status_map[:pending],
      )
      user.withdraw_point(Campaign.line_coin_to_point(params[:coin].to_i), :line_sticker, sticker.id)
      VGLogger.post({ path: '/v1/campaigns/apply', line_sticker_request_id: sticker.id })
      sticker
    end
  end

  def self.search(user, opt={})
    now = Time.now
    custom_where = "start_at <= ? AND ? < end_at"
    custom_params = [ now, now ]

    if opt.key?(:tag) then
      custom_where += " AND" if custom_where.length > 0
      custom_where += " tag = ?"
      custom_params.push( opt[:tag] )
    end
    
    Campaign.where({ :deleted_at => nil }).where(custom_where, *custom_params).order('-order_num DESC, id DESC')
  end

  def self.convert(src)
    dest = {
      :id              => src.id,
      :name            => src.name,
      :cover_image_url => src.cover_image_url,
      :point_cost      => src.point_cost,
      :start_at        => src.start_at,
      :start_at_time   => src.start_at.to_i,
      :end_at          => src.end_at,
      :end_at_time     => src.end_at.to_i,
      :type            => src.type,
    }

    case src.type
    when 'dream_photo', 'line_sticker'
      dest[:processing_time] = src.processing_time
    end

    dest
  end

  def self.line_coin_to_point(coin)
    coin / 50 * 120
  end
end
