class Ticket < ActiveRecord::Base
  self.inheritance_column = :_type_disabled
  belongs_to :user
  belongs_to :campaign

  def self.register(campaign_id, user)
    begin
     number = self.generate_number()
     ticket = Ticket.where({:campaign_id => campaign_id, :number => number}).first

     if ticket then
       logger.warn "duplicate number=#{number}"
     else
       ticket = Ticket.new(
         :campaign_id => campaign_id,
         :user => user,
         :number => number,
       )
       ticket.save!
       break
     end
    end while true

    ticket
  end

  def self.generate_number()
    Rails.application.config.ticket_digit_num.times.map{|j| rand(10) }.join('')
  end
end
