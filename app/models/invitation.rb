class Invitation < ActiveRecord::Base
  belongs_to :from_user, :class_name => 'User'
  belongs_to :to_user, :class_name => 'User'

  def self.register(from_user, to_user)
    Invitation.where({:from_user => from_user, :to_user => to_user}).first_or_create(
      :from_user => from_user,
      :to_user => to_user,
    )
  end

  #TODO optimize query
  def self.available?(from_user, to_user)
    return false if Invitation.where({:from_user => from_user}).count >= Rails.application.config.invite_limit

    uids = User.where({:email => to_user.email}).all.map {|r| r.id}
    Invitation.where({:to_user_id => uids}).count == 0
  end
end
