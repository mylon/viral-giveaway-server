class CurrencyExchangeRate < ActiveRecord::Base
  def self.register(from, to, published_at, rate)
    exchange_rate = CurrencyExchangeRate.where({
      :from => from.upcase,
      :to => to.upcase,
      :published_at => published_at,
    }).first_or_initialize()

    exchange_rate.from = from
    exchange_rate.to = to
    exchange_rate.rate = rate
    exchange_rate.published_at = published_at
    exchange_rate.save!
    exchange_rate
  end

  def self.latest(from, to='USD')
    CurrencyExchangeRate.where({
      :from => from.upcase,
      :to => to.upcase,
    }).order('published_at desc').first
  end
end
