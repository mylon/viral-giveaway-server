class NightlifeGroup < ActiveRecord::Base
	belongs_to :nightlife_user

	def self.register(createdby_user_id=nil,createdby_match=nil, group_barcode=nil, single_single=0, single_group=0, group_group=0, group_join=0)
		now = Time.now
		nightlifegroup = self.new(     
	      :createdby_user_id => createdby_user_id,	     
	      :createdby_match => createdby_match,	 
	      :group_barcode => group_barcode,	
	      :single_single => single_single,    
	      :single_group => single_group,    
	      :group_group => group_group,    
	      :group_join => group_join,
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    nightlifegroup
	end
end
