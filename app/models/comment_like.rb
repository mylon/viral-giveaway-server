class CommentLike < ActiveRecord::Base
	belongs_to :user
	belongs_to :comment

	def self.register(user_id, comment_id)
	    CommentLike.where({user_id: user_id, comment_id: comment_id}).first_or_create(user_id: user_id, comment_id: comment_id)
	end
end
