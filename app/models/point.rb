class Point < ActiveRecord::Base
  belongs_to :user
  belongs_to :transfer_user, :class_name => 'User'

  def self.to_amount(point)
    point
  end

  def self.to_point(cent)
    cent
  end


  def self.deposit(user, point, transfer_user, item_type_key, item_id)
    balance = user.point()

    p = self.new(
      :user      => user,
      :amount    => point,
      :balance   => balance + point,
      :item_type => Rails.application.config.point_item_type_map[item_type_key],
      :item_id   => item_id,
    )
    p.transfer_user = transfer_user if transfer_user
    p.save!
    p
  end

  def self.withdraw(user, point, transfer_user = nil, item_type_key, item_id)
    balance = user.point()
    if balance - point < 0 then
      raise ArgumentError, 'point_insufficient'
    end

    p = self.new(
      :user => user,
      :amount => -1 * point,
      :balance => balance - point,
      :item_type => Rails.application.config.point_item_type_map[item_type_key],
      :item_id   => item_id,
    )
    p.transfer_user = transfer_user if transfer_user
    p.save!
    p
  end

  def self.can_withdraw?(user, point)
    user.point() - point >= 0
  end

  def self.transfer(from_user, to_user, point, item_type_key, item_id)
    self.withdraw(from_user, point, to_user, item_type_key, item_id)
    self.deposit(to_user, point, from_user, item_type_key, item_id)
  end

end
