class NewsTag < ActiveRecord::Base
	def self.register(tag, region, city_id=0, category_id =0, tags_group=nil)
	    now = Time.now
	    news = self.new(     
	      :tag => tag,	     
	      :region => region,	
	      :city_id => city_id,   
	      :category_id => category_id,  
	      :tags_group => tags_group, 
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    news
  	end

  	def self.bulk_load(csv_data)
  		now = Time.now
	    csv_data.split(/\r?\n/).each_with_index do |line,i|
	      next if i == 0
	      data = parse(line)
	      city_condition = [ 0, '0']
		  if data['city_id'].to_i > 0 then
		   	city_condition.push(data['city_id'].to_i)
		  end
		  category_condition = [ 0, '0']
		  if data['category_id'].to_i > 0 then
		   	category_condition.push(data['category_id'].to_i)
		  end
	      r = NewsTag.where({tag: data['tag'], region: data['region']}).where("category_id IS NULL OR category_id IN (?)", category_condition).where("city_id IS NULL OR city_id IN (?)", city_condition).first_or_initialize(tag: data['tag'], region: data['region'], city_id: data['city_id'], category_id: data['category_id'])
	      r.created_at = now
	      r.updated_at = now
	      r.save!
	    end
  	end

	def self.parse(line)
	    logger.debug "line=#{line}"
	    data = {}
	    line.split(",").each_with_index do |part,i|
	      data[Rails.application.config.tag_csv_columns[i]] = part
	    end
	    logger.debug "data=#{data.inspect}"
	    data
	end
end
