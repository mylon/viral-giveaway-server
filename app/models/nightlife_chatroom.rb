class NightlifeChatroom < ActiveRecord::Base
	belongs_to :nightlife_user
	belongs_to :nightlife_group

	def self.register(group_id)
		now = Time.now
		chatroom = self.new(     
	      :group_id => group_id,	
	      :created_at => now,
	      :updated_at => now	   
	    ).save!	 
	    
	    chatroom
	end

end
