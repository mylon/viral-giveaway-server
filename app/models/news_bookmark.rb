class NewsBookmark < ActiveRecord::Base
	belongs_to :user
	belongs_to :news_pick

	def self.register(user_id, news_id)
	    NewsBookmark.where({user_id: user_id, news_id: news_id}).first_or_create(user_id: user_id, news_id: news_id)
	end
end
