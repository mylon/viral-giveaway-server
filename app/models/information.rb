class Information < ActiveRecord::Base
  belongs_to :user
  belongs_to :app

  def self.validate_view(params)
    errors = []

    if params.key?(:information_id) then
      information_id = params[:information_id].to_i
      information = self.where({:deleted_at => nil, :id => information_id}).where(['? > published_at', Time.now]).first
      if information.nil? then
        errors.push(I18n.t('error_information_not_found'))
      end
    else
      errors.push(I18n.t('error_information_id_required'))
    end

    errors
  end

  def self.convert(src, user)
    dest = {
      :id => src.id,
      :title => src.title,
      :content => src.content,
      :video_url => src.video_url,
      :image_url => src.image_url,
      :published_at => src.published_at,
    }

    v = InformationView.where({
      :deleted_at => nil,
      :information => src,
      :user => user,
    }).first
    dest[:viewed] = v.present?

    dest
  end

  def self.register(app, title, content, published_at, image_url=nil, video_url=nil)
    now = Time.now
    self.new(
      :app => app,
      :title => title,
      :content => content,
      :image_url => image_url,
      :video_url => video_url,
      :published_at => published_at,
      :deleted_at => nil,
      :created_at => now,
      :updated_at => now
    ).save!
  end

  def viewed(user)
    v = nil

    Information.transaction do
      v = InformationView.where({:information => self, :user => user, :deleted_at => nil}).first
      unless v then
        now = Time.now
        v = InformationView.new(
          :information => self,
          :user => user,
          :deleted_at => nil,
          :created_at => now,
          :updated_at => now
        )
        v.save!
      end
    end

    v
  end
end
