module ABTestLogger
  def self.post(log)
    Rails.logger.debug "log=#{log}"
    post_fluentd(log)
    post_sixpack(log)
  end

  def self.post_fluentd(log)
  end

  def self.post_sixpack(log)
  end

  def self.particiate(log)
    self.post(log)
  end

  def self.convert(log)
    self.post(log)
  end
end
