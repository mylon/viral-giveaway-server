module VGLogger
  def self.post(log)
    log[:ua]         = Thread.current[:ua]
    log[:remote_ip]  = Thread.current[:request].remote_ip
    log[:session_id] = Thread.current[:session_id].to_s
    log[:account_id] = (log[:account_id] || Thread.current[:account_id]).to_s
    log[:source] = (log[:source] || Thread.current[:source]).to_s

    Rails.logger.debug "log=#{log}"
    Fluent::Logger.post("vg.app_log", log)
  end
end
