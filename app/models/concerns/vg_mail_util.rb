module VGMailUtil 
  def self.build_sender(email, name)
    "#{NKF.nkf('-jWM', name)} <#{email}>"
  end
end
