class UserApp < ActiveRecord::Base
  belongs_to :user
  belongs_to :app

  def self.register(user, app)
    UserApp.where({user: user, app: app}).first_or_create(user: user, app: app)
  end
end
