class MobileCountryCode < ActiveRecord::Base
  def self.find_by_imsi(imsi)
    if imsi.to_s.length > 0 then
      mcc = MobileCountryCode.where({:code => imsi[0,3]}).first
      mcc if mcc
    end
  end

  def self.find_country_iso(imsi)
    mcc = self.find_by_imsi(imsi)
    country_iso = mcc.country_iso if mcc

    imsi_invalid = false
    unless country_iso then
      imsi_invalid = imsi.to_s.length > 0 

      if Thread.current[:request] then
        res = Thread.current[:request].location
        if res then
          country_iso = res.country_code.downcase 
        end
      end
    end

    country_iso
  end
end
