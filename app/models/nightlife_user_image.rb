class NightlifeUserImage < ActiveRecord::Base
	belongs_to :nightlife_user

	def self.upload_photo(src, filetype, user_id, opt={})
	    opt[:bucket] ||= Rails.application.config.nightlife_s3_bucket
	    dest = self.generate_dest_path(opt[:filename] || self.generate_filename(src, filetype), user_id)
	    logger.info "src=#{src.inspect}"
	    body = case src
	           when URI::Data
	             src.data
	           when String
	             IO.read(src)
	           else
	             raise ArgumentError, "invalid src type. src.class=#{src.class}"
	           end 

	    @s3 = Aws::S3::Client.new(
	      region: Rails.application.config.s3_region,
	      access_key_id: Rails.application.config.aws_access_key_id,
	      secret_access_key: Rails.application.config.aws_secret_access_key
	    )
	   
	    params = {
	      bucket: opt[:bucket],
	      key: dest,
	      body: body,
	    }
	    params[:acl] = opt[:acl] if opt[:acl]
	    params[:content_type] = opt[:content_type] if opt[:content_type]

	    logger.info "params=#{params}" 
	    @s3.put_object(params)
	end

	def self.generate_filename(src, filetype)
	    case src
	    when URI::Data
	      extension = MIME::Types[src.content_type].first.preferred_extension
	      "#{filetype}.#{extension}"
	    else
	      raise ArgumentError, "invalid src type."
	    end
	end

	def self.generate_dest_path(filename, user_id, profile_picture='profile_picture')
	    "#{Rails.application.config.nightlife_s3_prefix}#{profile_picture}/#{user_id}/#{filename}"
	end

	def self.presigned_url(filetype)
	    begin
	      s3 = NightlifeUserImage.generate_s3_client()
	      object = Aws::S3::Object.new({
	        bucket_name: Rails.application.config.nightlife_s3_bucket,
	        key: self.generate_dest_path( self.send("#{filetype}_photo_url".to_sym) ),
	        client: s3,
	      })
	      object.presigned_url(:get, expires_in: 600)
	    rescue => e
	      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
	      ""
	    end
	end
  
  def self.download_photo(filetype)
    begin
      s3 = NightlifeUserImage.generate_s3_client()
      s3.get_object({
        bucket: Rails.application.config.nightlife_s3_bucket, 
        key: self.generate_dest_path( self.send("#{filetype}_photo_url".to_sym) ),
      })
    rescue => e
      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
      nil 
    end
  end

  def self.generate_s3_client
    Aws::S3::Client.new(
      region: Rails.application.config.s3_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )
  end
end
