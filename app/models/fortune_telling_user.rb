class FortuneTellingUser < ActiveRecord::Base
  belongs_to :user
  
  def self.constellation(date)
    Rails.application.config.fortune_telling_constellation_date_map.each do |constellation,dates|
      if dates.first.is_a?(Hash) then
        return Rails.application.config.fortune_telling_constellation_map[constellation] if between_date?(date, dates) 
      elsif dates.first.is_a?(Array) then
        return Rails.application.config.fortune_telling_constellation_map[constellation] if between_date?(date, dates[0]) or between_date?(date, dates[1]) 
      else
        raise ArgumentError, "Invalid config"
      end
    end    
  end

  def self.between_date?(date, dates)
    d1 = Date.new(date.year, dates[0][:m], dates[0][:d])
    d2 = Date.new(date.year, dates[1][:m], dates[1][:d])
    d1 <= date && date <= d2
  end
end
