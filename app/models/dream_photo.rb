class DreamPhoto < ActiveRecord::Base
  belongs_to :user

  def upload_user_photo(src, opt={})
    self.upload_photo(src, "user", opt)
  end

  def upload_base_photo(src, opt={})
    self.upload_photo(src, "base", opt)
  end

  def upload_result_photo(src, opt={})
    self.upload_photo(src, "result", opt)
  end

  def upload_photo(src, filetype, opt={})
    opt[:bucket] ||= Rails.application.config.dream_photo_s3_bucket
    dest = self.generate_dest_path(opt[:filename] || self.generate_filename(src, filetype))
    logger.info "src=#{src.inspect}"
    body = case src
           when URI::Data
             src.data
           when String
             IO.read(src)
           else
             raise ArgumentError, "invalid src type. src.class=#{src.class}"
           end 

    @s3 = Aws::S3::Client.new(
      region: Rails.application.config.s3_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )
   
    params = {
      bucket: opt[:bucket],
      key: dest,
      body: body,
    }
    params[:acl] = opt[:acl] if opt[:acl]
    params[:content_type] = opt[:content_type] if opt[:content_type]

    logger.info "params=#{params}" 
    @s3.put_object(params)
  end

  def generate_filename(src, filetype)
    case src
    when URI::Data
      extension = MIME::Types[src.content_type].first.preferred_extension
      "#{filetype}.#{extension}"
    else
      raise ArgumentError, "invalid src type."
    end
  end

  def generate_dest_path(filename)
    "#{Rails.application.config.dream_photo_s3_prefix}#{self.user_id}/#{self.id}/#{filename}"
  end

  def presigned_url(filetype)
    begin
      s3 = DreamPhoto.generate_s3_client()
      object = Aws::S3::Object.new({
        bucket_name: Rails.application.config.dream_photo_s3_bucket,
        key: self.generate_dest_path( self.send("#{filetype}_photo_url".to_sym) ),
        client: s3,
      })
      object.presigned_url(:get, expires_in: 600)
    rescue => e
      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
      ""
    end
  end
  
  def download_photo(filetype)
    begin
      s3 = DreamPhoto.generate_s3_client()
      s3.get_object({
        bucket: Rails.application.config.dream_photo_s3_bucket, 
        key: self.generate_dest_path( self.send("#{filetype}_photo_url".to_sym) ),
      })
    rescue => e
      logger.error "message=#{e.message}, backtrace=#{e.backtrace.join("\n\t")}"
      nil 
    end
  end

  def self.generate_s3_client
    Aws::S3::Client.new(
      region: Rails.application.config.s3_region,
      access_key_id: Rails.application.config.aws_access_key_id,
      secret_access_key: Rails.application.config.aws_secret_access_key
    )
  end

  def self.convert(src)
    dest = {
      id: src.id,
      user: src.user,
      photo_type: src.photo_type,
      comment: src.comment,
      billing_status: src.billing_status,
      completed_at: src.completed_at,
      created_at: src.created_at,
    }
    dest[:user_photo_url] = src.presigned_url("user") if src.user_photo_url.to_s.length > 0
    dest[:base_photo_url] = src.presigned_url("base") if src.base_photo_url.to_s.length > 0
    dest[:result_photo_url] = src.presigned_url("result") if src.result_photo_url.to_s.length > 0

    dest
  end
end
