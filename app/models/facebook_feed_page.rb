class FacebookFeedPage < ActiveRecord::Base
	def self.register(facebook_page_id, facebook_page_type, facebook_page_region, city_id)
	    now = Time.now
	    news = self.new(     
	      :facebook_page_id => facebook_page_id,
	      :facebook_page_type => facebook_page_type,
	      :facebook_page_region => facebook_page_region,
	      :city_id => city_id,
	      :created_at => now,
	      :updated_at => now,
	    ).save!	 
	    
	    news
  	end
end
