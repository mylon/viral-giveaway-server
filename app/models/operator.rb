class Operator < ActiveRecord::Base
  def self.authenticate(account_id_or_email, password)
    encrypt_password = self.encrypt_password(password) 

    Operator.where("deleted_at IS NULL and password = ? and (account_id = ? or email = ?)", encrypt_password, account_id_or_email, account_id_or_email).first
  end

  def self.encrypt_password(password)
    require 'digest/sha2'
    Digest::SHA512.hexdigest(password)
  end

  def self.register(account_id, name, email, password)
    now = Time.now
    o = Operator.new(
      :account_id => account_id,
      :name => name,
      :email => email,
      :password => self.encrypt_password(password),
      :created_at => now,
      :updated_at => now,
      :deleted_at => nil,
    )
    o.save!
    o
  end
end
