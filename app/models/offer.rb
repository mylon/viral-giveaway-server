class Offer < ActiveRecord::Base
  self.inheritance_column = :_type_disabled

  def self.search(user, option)
    where = {
      :deleted_at => nil,
      :publish_status => Rails.application.config.offer_publish_status_map[:publish],
    }

    now = Time.now
    custom_where = "platform IN (?, ?) AND country IN (?, ?) AND start_at <= ? AND ? < end_at"
    custom_params = [option[:platform], Rails.application.config.offer_platform_map[:all], option[:country], 'all', now, now]

    if option.key?(:id) then
      custom_where += " AND id = ?"
      custom_params.push(option[:id].to_i)
    end

    if option.key?(:since_time) then
      custom_where += " AND created_at > ?"
      custom_params.push(option[:since_time])
    end

    unless option[:category].nil? then
      case option[:category] 
      when 'wall'
        where[:type] = Rails.application.config.offer_types_per_category[:wall].map{|t| Rails.application.config.offer_type_map[t]}
      when 'promotion'
        where[:type] = Rails.application.config.offer_types_per_category[:application].map{|t| Rails.application.config.offer_type_map[t]}
        where[:promotional] = true
        option[:exclude_installed] = true
      when 'application'
        where[:type] = Rails.application.config.offer_types_per_category[:application].map{|t| Rails.application.config.offer_type_map[t]}
        where[:promotional] = false
        option[:exclude_installed] = true
      when 'video'
        where[:type] = Rails.application.config.offer_types_per_category[:video].map{|t| Rails.application.config.offer_type_map[t]}
      when 'information'
        where[:type] = Rails.application.config.offer_types_per_category[:information].map{|t| Rails.application.config.offer_type_map[t]}
      else
        return []
      end
    end

    limit = option[:limit] or Rails.application.config.offer_search_limit
    offers = Offer.where(where).where(custom_where, *custom_params).order('-order_num DESC, id DESC').limit(limit)
    
    offers = self.exclude_installed_offers(user, offers) if option[:exclude_installed]

    offers
  end

  def self.find_available(id)
    now = Time.now
    Offer.where({
      :deleted_at => nil,
      :publish_status => Rails.application.config.offer_publish_status_map[:publish],
      :id => id,
    }).
    where("start_at <= ? AND ? < end_at", now, now).
    limit(1).
    first
  end

  def self.convert(src)
    dest = {
      :id          => src.id,
      :type        => src.type,
      :name        => src.name,
      :description => src.description,
      :start_at    => src.start_at,
      :end_at      => src.end_at,
      :provider    => src.provider,
    }

    case src.type
    when Rails.application.config.offer_type_map[:information],
         Rails.application.config.offer_type_map[:video]
      dest[:cover_image_url] = src.cover_image_url
      dest[:video_url] = src.video_url
      dest[:youtube_video_id] = src.youtube_video_id
    when Rails.application.config.offer_type_map[:cpi]
      dest[:app_icon_url]      = src.app_icon_url
      dest[:click_url]         = src.click_url
      dest[:normal_point]      = src.humanized_provider_payment()
      dest[:point]             = src.humanized_payment()
      dest[:promotional]       = src.promotional
      dest[:special]           = Offer.special_payout?()
    when Rails.application.config.offer_type_map[:nativex],
         Rails.application.config.offer_type_map[:super_rewards],
         Rails.application.config.offer_type_map[:woobi],
         Rails.application.config.offer_type_map[:personaly],
         Rails.application.config.offer_type_map[:aarki],
         Rails.application.config.offer_type_map[:trialpay],
         Rails.application.config.offer_type_map[:adcolony],
         Rails.application.config.offer_type_map[:fyber],
         Rails.application.config.offer_type_map[:supersonicads]
      dest[:cover_image_url] = src.cover_image_url
    else
      return nil
    end

    dest
  end

  def self.fetch_recommends(user, platform)
    offers = self.search(user, { 
      :category => 'promotion', 
      :country => user.country,
      :platform => platform, 
      :exclude_installed => true, 
    })
    if offers.length == 0 then
      offers = self.search(user, { :category => 'wall', :country => user.country, :limit => 1 })
    else
      offers = [ offers.first ]
    end
    offers
  end

  def self.exclude_installed_offers(user, offers)
    trackings = Tracking.where({
      :user     => user, 
      :track_id => offers.map{|o| o.provider_offer_id },
      :status   => Rails.application.config.tracking_status_map[:conversioned],
    })

    filtered_offers = []
    offers.each do |o|
      provider_symbol = Rails.application.config.offer_provider_symbol_map[o.provider]
      tracked = false
      trackings.each do |t|
        type_symbol = Rails.application.config.tracking_type_symbol_map[t.type]
        if type_symbol == provider_symbol and o.provider_offer_id == t.track_id then 
          tracked = true
          break
        else
          tracked = false
        end
      end
      filtered_offers.push(o) unless tracked 
    end
  
    filtered_offers
  end

  def self.humanize_payment(point)
    return "#{point / 1000}k" if point >= 1000 and point % 1000 == 0
    return "#{point / 1000.to_f}k" if point >= 1000
    return point.to_s
  end

  def humanized_provider_payment
    Offer.humanize_payment(self.provider_payment)
  end

  def humanized_promotional_payment
    Offer.humanize_payment(self.promotional_payment)
  end

  def humanized_payment
    Offer.humanize_payment(self.payment())
  end

  def payment
    if Offer.special_payout?() then
      return self.provider_payment * Rails.application.config.special_payout_rate
    elsif self.promotional and self.promotional_payment > 0 then
      return self.promotional_payment
    else
      return self.provider_payment
    end
  end

  def self.special_payout?
    Rails.application.config.special_payout_week_days[Date.today.wday.to_s] == true
  end
end
