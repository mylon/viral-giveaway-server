class Comment < ActiveRecord::Base
	belongs_to :user
	belongs_to :news_pick

	def self.register(user_id, news_id, comment_text="I will share", parent_comment_id = 0)
	    now = Time.now
	    comments = self.new(     
	      :user_id => user_id,
	      :news_id => news_id,
	      :comment_text => comment_text,	
	      :parent_comment_id => parent_comment_id,	      
	      :created_at => now,
	      :updated_at => now	    
	    ).save!	    
	    comments
	end
end
