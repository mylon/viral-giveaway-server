class Tracking < ActiveRecord::Base
  self.inheritance_column = :_type_disabled
  belongs_to :user

  def self.track(user, track_id, type_key, status_key)
    if t = self.tracked?(user, track_id, type_key, status_key) then
      return t
    end

    t = Tracking.new(
      :user       => user,
      :track_id   => track_id.to_s,
      :type       => Rails.application.config.tracking_type_map[type_key],
      :status     => Rails.application.config.tracking_status_map[status_key],
      :deleted_at => nil,
    )
    t.save!
    t
  end

  def self.tracked?(user, track_id, type_key, status_key)
    Tracking.where({
      :deleted_at => nil,
      :user       => user,
      :track_id   => track_id.to_s,
      :type       => Rails.application.config.tracking_type_map[type_key],
      :status     => Rails.application.config.tracking_status_map[status_key],
    }).first
  end

  def self.conversioned?(user, track_id, type_key)
    Tracking.tracked?(
      user, 
      track_id,
      type_key, 
      :conversioned
    )
  end
end
