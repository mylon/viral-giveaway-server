class NewsPickDeviceToken < ActiveRecord::Base
	belongs_to :user

	def self.register(user_id, city_id, region, device_type, device_token, device_notification="Y", unique_device_id="0")
	    NewsPickDeviceToken.where({
	    	device_token: device_token, 
	    	city_id: city_id, 
	    	region: region, 
	    	device_type: device_type
	    }).first_or_create.update(
		    user_id: user_id, 
		    city_id: city_id, 
		    region: region, 
		    device_type: device_type, 
		    device_token: device_token,
		    device_notification: device_notification,
		    unique_device_id: unique_device_id 
	    )
	end

	def self.register_android(user_id, city_id, region, device_type, device_token, device_notification="Y", unique_device_id="0")
	    NewsPickDeviceToken.where({
	    	city_id: city_id, 
	    	region: region, 
	    	device_type: device_type,
	    	unique_device_id: unique_device_id 
	    }).first_or_create.update(
		    user_id: user_id, 
		    city_id: city_id, 
		    region: region, 
		    device_type: device_type, 
		    device_token: device_token,
		    device_notification: device_notification,
		    unique_device_id: unique_device_id 
	    )
	end
end
